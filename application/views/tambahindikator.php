<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
  <script>
            function get_sub(){
                var id_tema = $("#sub").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('penjadwalan/get_sub'); ?>", 
                    data:"id_tema="+id_tema, 
                    success: function(msg) {
                            $("#div_sub").html(msg);
                    }
                });
            }
        </script>
<body>
 <?php if($this->uri->segment(3)){
  $kode=$data->kode_indikator;
  $nama=$data->nama_indikator;
  $aksi='Indikator/aksi_ubahindikator/'.$data->id_indikator;
  $title='Ubah';
 }else{
  $kode='';
  $nama='';
  $aksi='Indikator/insertindikator/';
  $title='Tambah';
 }?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title"><?php echo $title?> Indikator</h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Home/dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home/indikator');?>">Indikator</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo $title?> Indikator</li>
                      </ol>
                    </nav>
                      <form class="forms-sample"  method="post" enctype="multipart/form-data"action="<?php echo site_url($aksi);?>">
                      <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Aspek</label>
                          <div class="col-sm-9">
                             <select required name="f1" class="form-control" >
                              <?php
                             foreach($prg as $r){
                            ?>
                         <option value="<?php echo $r->id_program; ?>" <?php if($this->uri->segment(3)){ if($data->id_program==$r->id_program) echo "selected"; }?>>  <?php echo $r->nama_program; ?></option>
                         <?php
                              } ?>
                            </select>
                           </div>
                      </div>
                         

                       <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Kompetensi Dasar</label>
                          <div class="col-sm-9">
                         <select  class="form-control"   style="width:100%" class="form-control form-control-sm" name="f5[]" id="f5">
                            <?php
                             foreach($kd as $a){
                            ?>
                         <option value="<?php echo $a->kode_kd; ?>" <?php if($this->uri->segment(3)){if($data->kode_kd==$a->kode_kd)echo "selected"; }?>>  <?php echo $a->nama_kd; ?></option>
                         <?php
                              } ?>
                        </select>
                           </div>
                       
                      </div>
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Kode Indikator</label>
                        <div class="col-sm-9">
                       <input required type="text" class="form-control" name="kode" value="<?php echo $kode?>">
                        </div>
                      </div>
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Indikator</label>
                        <div class="col-sm-9">
                       <input required type="text" class="form-control" name="ind" value="<?php echo $nama?>">
                        </div>
                      </div>
                      <input type="hidden" name="sekolah" class="form-control form-control-lg" value="<?=$this->session->userdata('id_sekolah');?>">
                     
                     

                        <button type="submit"  onclick="alert('Data Anda Disimpan')" class="btn btn-success mr-2"><?php echo $title?></button>
                        <a href="<?php echo site_url('home/indikator');?>" class="btn btn-danger" value="Batal"><i class="icon-remove"></i> Batal</a>
                      </form>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>