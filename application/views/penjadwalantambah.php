<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
  <script>
            function get_sub(){
                var id_tema = $("#tema").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('penjadwalan/get_sub'); ?>", 
                    data:"id_tema="+id_tema, 
                    success: function(msg) {
                            $("#div_sub").html(msg);
                    }
                });
            }
        </script>
        <script>
            function get_subsub(){
                var id_subtema = $("#sub").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('Penjadwalan/get_subsub'); ?>", 
                    data:"id_subtema="+id_subtema, 
                    success: function(msg) {
                            $("#div_subsub").html(msg);
                    }
                });
            }
        </script>
<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah Perencanaan</h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Home/dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Penjadwalan/jadwal');?>">Perencanaan</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah perencanaan</li>
                      </ol>
                    </nav>
                      <form class="col-sm-12" action="<?php echo site_url('Penjadwalan/aksiinsert/');?>" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tahun Ajaran</label>
                          <div class="col-sm-9">
                          <select required name="thn" class="form-control form-control-sm">
                            <option value=""> Pilih Tahun</option>
                             <?php
                            foreach($thn as $r){
                            echo "<option value='".$r->id_tahunajaran."'>".$r->nama_tahun."</option>";}
                        ?> 
                          </select>
                           </div>
                      </div>
                      <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Semester</label>
                          <div class="col-sm-9">
                          <select required name="smt" class="form-control form-control-sm">
                          <option value=""> Pilih Semester</option>
                          <?php foreach($smt as $r){
                            echo "<option value='".$r->id_semester."'>".$r->nama_semester."</option>";}
                            ?> 
                          </select>
                           </div>
                       
                      </div>
                       <div class="form-group row">
                    <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tanggal Awal</label>
                          <div class="col-sm-3">
                          <input  type="date" class="form-control form-control-sm" name="f1">
                           </div>
                           <label for="exampleInputPassword2" class="col-sm-2 col-form-label">Tanggal Akhir</label>
                          <div class="col-sm-3">
                          <input  type="date" class="form-control form-control-sm" name="tglakhir">
                           </div>
                      </div>
                        <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Kelompok</label>
                          <div class="col-sm-9">
                           <select class="js-example-basic-multiple" multiple="multiple" style="width:100%" class="form-control form-control-sm" name="f2[]" id="f72">
                          
                          <?php
                                   foreach($kls as $k){

                              echo "<option value='".$k->id_kelas."'>".$k->nama_kelas."</option>";
                                  }
                                         ?> 
                        </select>
                           </div>
                       
                      </div>
                      <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Aspek</label>
                          <div class="col-sm-9">
                             <select required name="f3" class="form-control"> 
                             <option value="">---Pilih Program---</option>
                                   <?php
                                  foreach($prg as $r){
                                    echo "<option value='".$r->id_program."'>".$r->nama_program."</option>";}
                                         ?> 
                                       </select>
                           </div>
                      </div>
                          <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> Tema</label>
                          <div class="col-sm-9">
                         <select name="f4" class=" form-control form-control-sm" id="tema" onchange="get_sub()">
                           <option value=""> Pilih tema</option>
                             <?php
                            foreach($tema as $r){
                            echo "<option value='".$r->id_tema."'>".$r->nama_tema."</option>";}
                        ?> 
                         </select>
                           </div>
                           </div>
                        <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label">Subtema</label>
                        <div class="col-sm-9" id="div_sub">
                          <select name="f5" class=" form-control form-control-sm" id="sub" onchange="get_subsub();">
                           <option> Pilih subtema</option>
                         </select>
                        </div>
                        </div>
                          <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label">Subsubtema</label>
                        <div class="col-sm-9" id="div_subsub">
                          <select name="fsubsub" class=" form-control form-control-sm">
                           <option> Pilih subsubtema</option>
                         </select>
                        </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Kompetensi Dasar</label>
                        <div class="col-sm-9">
                        <select class="js-example-basic-multiple" multiple="multiple" style="width:100%" class="form-control form-control-sm" name="f6[]" id="f6">
                          <?php
                                   foreach($kd as $a){
                              echo "<option value='".$a->id_kd."'>"."(".$a->kode_kd.")".$a->nama_kd."</option>";
                                  }
                                         ?> 
                        </select>
                        </div>
                      </div>
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Indikator</label>
                        <div class="col-sm-9">
                        <select class="js-example-basic-multiple" multiple="multiple" style="width:100%" class="form-control form-control-sm" name="f7[]" id="f7">
                          
                          <?php
                                   foreach($ind as $a){

                              echo "<option value='".$a->id_indikator."'>"."(".$a->kode_kd.")"."(".$a->kode_indikator.")".$a->nama_indikator."</option>";
                                  }
                                         ?> 
                        </select>
                        </div>
                      </div>
                       <!-- <div class="form-group row"> 
                    <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Materi</label>
                          <div class="col-sm-9">
                          <input  type="Text" class="form-control form-control-sm" name="mtr">
                           </div>
                      </div>-->
                      
                        <button type="submit"   class="btn btn-success mr-2">Submit</button>
                        <a href="<?php echo site_url('Penjadwalan/jadwal');?>" class="btn btn-danger" value="Batal"><i class="icon-remove"></i> Batal</a>
                      </form>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>