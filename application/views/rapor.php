<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
   <script>
            function get_semester(){
                var id_tahunajaran = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('report/get_semester'); ?>", 
                    data:"id_tahunajaran="+id_tahunajaran, 
                    success: function(msg) {
                            $("#div_ktd").html(msg);
                    }
                });
            }
        </script>
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-lg-10">
                      <h4 class="card-title">Rapor Siswa </h4>
                    </div>
                    <div class="col-lg-2">
                     <!--  -->
                    </div>
                  </div>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Rapor Siswa</li>
                      </ol>
                    </nav>
              <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                     <form method="post" action="<?php echo site_url('Report/nilaisemester');?>">
                     <div class="form-group row">
                          <label  class="col-sm-2 col-form-label" >Tahun Ajaran </label>
                         <div class="col-sm-2">
                             <select required name="thn" class=" form-control form-control-sm" id="prov" onchange="get_semester()" style="
                             margin-left: -60px;">
                           <option value=""> Pilih tahun ajaran </option>
                             <?php
                            foreach($thn as $r){
                            echo "<option value='".$r->id_tahunajaran."'>".$r->nama_tahun."</option>";}
                        ?> 
                         </select>
                           </div> 
                            <label for="exampleInputPassword2" class="col-form-label" style="
                                margin-left: -30px;">Siswa </label>
                        <div class="form-group row col-lg-6" id="div_ktd">
                          <div class="col-sm-5">
                            <select required name="siswa" class="form-control form-control-sm"></select>
                          </div>
                         <label for="exampleInputPassword2" class="col-form-label" style=" margin-left: 30px;">Semester</label>
                        <div class="col-sm-4">
                          <select required name="smt" class="form-control" >
                          <option value="">Pilih semester</option>
                          </select>
                        </div>
                     </div>
                         <span class="input-group-btn"  >
                                <button class="btn btn-primary btn-sm" type="submit">Cari
                                </button>
                            </span>
                      </div>
                    </form>
                 

                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th  rowspan="2">No</th>
                         <th  rowspan="2"><center>Nama</center></th>
                        <th  rowspan="2"><center>Tempat lahir</center></th>
                        <th  rowspan="2"><center>Agama</center></th>
                        <th colspan="2"><center>Aksi</center></th>
                      </tr>
                       <tr>
                                  
                              <th>Semester 1</th><th>Semester 2</th>
                              </tr>
                    </thead>
                 <tbody>
                        <?php
                        $no = 1;
                        foreach($siswa->result() as $b){
                        ?>
                        <tr>
                        <td><center> <?php echo $b->no_induk?></center></td>
                        <td><center> <?php echo $b->nama_siswa ?></center></td>
                        <td><center> <?php echo $b->tempat_lahir ?></center></td>
                        <td><center> <?php echo $b->agama ?></center></td>
                        <td><center>
                        <!--   <a class='btn btn-primary btn-sm' href="<?php echo site_url('report/cetak/').$b->id_siswa?>">
                           <i class="fa fa-print"></i>Ijazah </a> -->
                            <a class='btn btn-primary btn-sm' target="_blank" href="<?php echo site_url('report/cetakrapor/').$b->id_siswa.'/'.'1'?>">
                           <i class="fa fa-print"></i> Rapor</a>
                          </center></td>
                          <td><center>
                            <a class='btn btn-primary btn-sm' target="_blank" href="<?php echo site_url('report/cetakrapor/').$b->id_siswa.'/'.'2'?>">
                           <i class="fa fa-print"></i> Rapor</a>
                          </center></td>
                            
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                </div>
                </div>
                    </div>
                  </div>
                </div>
          </div>
        </div>
         <div class="modal fade" id="exampleModal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel-2">Modal title</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                        <div class="form-group row" >
                        <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Semester</label>
                          <div class="col-sm-9">
                          <select required class=" form-control form-control-sm" name="kls">
                          </select>
                           </div>
                      </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-success">Submit</button>
                          <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                        </div>
                      </div>
                    </div>
                  </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>