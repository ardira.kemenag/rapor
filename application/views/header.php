<!DOCTYPE html>
<html>


<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Aplikasi Rapor Digital</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/perfect-scrollbar/css/perfect-scrollbar.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/icheck/skins/all.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/dropify/dist/css/dropify.min.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo_kemenag.png">


</head>
 <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar navbar-light col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper"> <a class="navbar-brand brand-logo" href="index.html"><img src="<?php echo base_url();?>assets/rania_logo-2.png" alt="logo"></a> <a class="navbar-brand brand-logo-mini" href="index.html"><img src="<?php echo base_url();?>assets/images/rania_logo-2.png" alt="logo"></a> </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler align-self-center mr-2" type="button" data-toggle="minimize"> <span class="mdi mdi-equal display-3"></span> </button>
       <!-- <form class="form-inline mt-2 mt-md-0 d-none d-lg-block">
          <div class="input-group search"> <span class="input-group-addon bg-transparent" id="basic-addon1"><i class="mdi mdi-magnify"></i></span> <input type="text" class="form-control bg-transparent" placeholder="Search..." aria-label="Username" aria-describedby="basic-addon1"> </div>
        </form>-->
        <ul class="navbar-nav nav-header-item-wrapper">
          <li class="nav-item d-none d-sm-block dropdown">
           <a class="btn bg-transparent dropdown-toggle text-default-color" id="userDropdown" href="#" data-toggle="dropdown"> 
           <?php
                echo $this->session->userdata('username') ?>
          </a>
            <div class="dropdown-menu navbar-dropdown preview-list p-3" aria-labelledby="userDropdown"><!-- <a href="#" class="btn btn-block btn-link text-dark">View Profile</a> <a href="#" class="btn btn-block btn-link text-dark">Account Settings</a>-->
              <div class="dropdown-divider mb-4"></div><a href="<?php echo site_url('login/logout');?>" class="btn btn-block btn-primary">Logout</a> </div>
          </li>         
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas"> <span class="mdi mdi-equal display-3"></span> </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('home/dashboard');?>"> <i class="mdi mdi-compass-outline menu-icon"></i> <span class="menu-title">Dashboard</span> </a> </li>
          <li class="nav-item"> <a class="nav-link" data-toggle="collapse" href="#uiBasicSubmenu" aria-expanded="false" aria-controls="collapseExample"> <i class="mdi mdi-account-multiple-outline menu-icon"></i> <span class="menu-title">Identitas Peserta Didik</span> <i class="mdi mdi-menu-down menu-arrow"></i> </a>
            <div class="collapse" id="uiBasicSubmenu">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('Siswa/dt');?>">Data Peserta Didik</a></li>
                 <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('Siswa/uploadsw');?>">Upload Peserta Didik</a></li>
              </ul>
            </div>
          </li>
            <li class="nav-item"> <a class="nav-link" data-toggle="collapse" href="#uiAdvancedSubmenu" aria-expanded="false" aria-controls="collapseExample"> <i class="mdi mdi-cube-outline menu-icon"></i> <span class="menu-title">Kompetensi</span> <i class="mdi mdi-menu-down menu-arrow"></i> </a>
            <div class="collapse" id="uiAdvancedSubmenu">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('home/ki');?>">Kompetensi Inti</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('home/kd');?>">Kompetensi Dasar</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('home/indikator');?>">Indikator</a></li>
              </ul>
            </div>
          </li>
         
          <li class="nav-item"> <a class="nav-link" data-toggle="collapse" href="#EditorSubmenu" aria-expanded="false" aria-controls="collapseExample"> <i class="mdi mdi-code-brackets menu-icon"></i> <span class="menu-title">Tema</span> <i class="mdi mdi-menu-down menu-arrow"></i> </a>
            <div class="collapse" id="EditorSubmenu">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"><a class="nav-link" href="<?php echo site_url('Tema/tema');?>">Tema</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo site_url('Subtheme/subtheme');?>">Sub Tema</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo site_url('Supersub/listdata');?>">Sub Sub Tema</a></li>
              </ul>
            </div>
          </li>
       <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('penjadwalan/jadwal');?>"> <i class="mdi mdi-calendar-text menu-icon"></i> <span class="menu-title">Penjadwalan</span> </a> </li>
       
           <li class="nav-item"> <a class="nav-link" data-toggle="collapse" href="#errorSubmenu" aria-expanded="false" aria-controls="collapseExample"> <i class="mdi mdi-clipboard-outline menu-icon"></i> <span class="menu-title">Input Nilai</span> <i class="mdi mdi-menu-down menu-arrow"></i> </a>
            <div class="collapse" id="errorSubmenu">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('penilaian/nilai');?>"> <span class="menu-title">Harian</span> </a> </li>
               <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('report/perkembangan_akhir');?>"> <span class="menu-title">Perkembangan Semester</span> </a> </li>
              </ul>
            </div>
          </li>
           <li class="nav-item"> <a class="nav-link" data-toggle="collapse" href="#mapsSubmenu" aria-expanded="false" aria-controls="collapseExample"> <i class="mdi mdi-file-document-box menu-icon"></i> <span class="menu-title">Laporan</span> <i class="mdi mdi-menu-down menu-arrow"></i> </a>
            <div class="collapse" id="mapsSubmenu">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('report/harian');?>">Laporan Harian</a></li>
                 <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('report/mingguan');?>">Laporan Mingguan</a></li>
                 <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('report/bulanan');?>">Laporan Bulanan</a></li>
                 <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('report/rapot');?>">Laporan Semester</a></li>
                 <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('report/rekap_bulan');?>">Kirim Laporan Semester</a></li>
                <!-- <li class="nav-item"> <a class="nav-link" href="pages\maps\google-maps.html"> Rapot </a></li> -->
              </ul>
            </div>
          </li>
           

        </ul>
      </nav></html>