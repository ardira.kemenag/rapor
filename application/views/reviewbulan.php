<!DOCTYPE html>
<html lang="en">
<<?php $this->load->view('header.php'); ?>
  <script>
            function get_bulan(){
                var id_tahunajaran = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('report/get_send'); ?>", 
                    data:"id_tahunajaran="+id_tahunajaran, 
                    success: function(msg) {
                            $("#div_ktd").html(msg);
                    }
                });
            }
        </script>
<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
         <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Report Bulanan</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Report Bulanan Siswa</h4>
                  </div> 
                  <div class="col-lg-6 text-right">
                    <a class="btn btn-danger btn-sm" href=""><i class="mdi mdi-download ml-1"></i>File</a>

                  </div>
                  
                  <?php 
                          foreach ( $tgl->result() as $t )
                          { ?>
                            <th width="15%">
                          <?php  $id[]=$t->id_perencanaan;?> </th> 
                          <?php }?>  
                  
                <?php 
                          foreach ( $jdw->result() as $j )
                          { ?>
                            <div class="col-lg-12 text-center">
                                     <div class="form-group row">
                       <label class="col-sm-2 col-form-label">Nama siswa</label>
                          <div class="col-sm-4 col-form-label">: <?php echo $j->nama_siswa?></div>
                          <label class="col-sm-2 col-form-label">Kelompok</label>
                          <div class="col-sm-4 col-form-label">: <?php echo $j->nama_kelas?></div>
                      </div>
                       <div class="form-group row">
                          
                       <label class="col-sm-2 col-form-label">Bulan</label>
                      <div class="col-sm-4 col-form-label">: <?php echo $format=date('F', strtotime($j->tanggal_awal))?></div>
                      <label class="col-sm-2 col-form-label">Tahun</label>
                      <div class="col-sm-4 col-form-label">: <?php echo $format=date('Y', strtotime($j->tanggal_awal))?></div>
                      </div>
                    </div>

                         

                <table id="" class="table table-striped table-advance table-hover nowrap">
                    <thead>
                      <tr> 
                         <th>No</th>
                          <th><center>Program</center></th>
                         <th><center>KD dan Indikator Penilaian</center></th>
                          <?php 
                         $no = 1;
                          foreach ( $mgg->result() as $m )
                          { ?>
                            <th width="15%">
                          <?php $m->tanggal_awal;
                          echo 'MINGGU KE-'.$no++;
                          ;?> </th> 
                          <?php }?>

                      </tr>
                    </thead>
                 <tbody>
                        <?php
                        $no = 1;
                       $nm= $this->model_m->rkpbulan($j->bulan,$j->id_siswa);
                         foreach($nm->result() as $b){?>
                        <tr>
                           <td><center> <?php echo $no++?></center></td>
                           <td><?php echo $b->nama_program?></td>

                           <td><?php   $data=['program'=> $b->nama_program];
                              $ind=$this->model_m->kdbln($id,$b->bulan,$b->id_siswa,$b->program);
                               foreach ($ind->result() as $d ) {
                                $j=json_decode($d->id_indikator);
                               foreach ($j as $key ) {
                                   echo '-'.$b->kode_kd.$this->model_m->cek_ind($key)//.$this->model_m->cek_ind2($key)
                                   ;
                                   echo '<br>';
                               } }?>
                            </td>
                            <?php  foreach($mgg->result() as $c){?>
                          <td> <?php
                         
                          ?>
                               <input type="hidden" value="<?php echo $c->mingguke; ?>"><?php   
                              $ind=$this->model_m->inbln($id,$b->bulan,$b->id_siswa,$b->program,$c->mingguke);
                               foreach ($ind->result() as $d ) {
                                $j=json_decode($d->id_indikator);
                               foreach ($j as $key ) {
                                  echo '-'.$this->model_m->cek_ind2($key);
                                   
                                   if($d->nilai=='1') {
                                    echo "BB<br><br><br>";
                                   }
                                   elseif ($d->nilai=='2') {
                                     echo "MB<br><br><br>";
                                   }
                                   elseif ($d->nilai=='3') {
                                     echo "BSH<br><br><br>";
                                   }
                                   elseif ($d->nilai=='4') {
                                     echo "BSB<br><br><br>";
                                   }
                                   else{
                                    echo "-<br><br><br>";
                                   }
                                   echo '<br>';
                                    $data['minggu'.$c->mingguke]=$d->nilai;
                          ;
                           
                             }
                               }  
                              ?>
                            </td>
                            <?php } ?>
                      </tr>
                        <?php  } ?>
                               
                  </tbody>
                  </table>





                        <?php
                        var_dump($data) ;}?>  
                </div>
                </div>
              
                  
                </div>
              </div>
            </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>