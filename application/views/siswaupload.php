<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
   <script>
            function get_kota(){
                var id_provinsi = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('sekolah/get_kota'); ?>", 
                    data:"id_provinsi="+id_provinsi, 
                    success: function(msg) {
                            $("#div_kota").html(msg);
                    }
                });
            }
        </script>

<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah Peserta Didik </h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Home');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Siswa/siswa');?>">Data Peserta Didik</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Peserta Didik</li>
                      </ol>
                    </nav>
                      <form class="forms-sample" action="<?php echo site_url('Siswa/importsiswa/');?>" method="post" enctype="multipart/form-data">
                        
                           <input type="hidden" name="sekolah" class="form-control form-control-lg" value="<?=$this->session->userdata('id_sekolah');?>">
                           <div class="col-lg-6">
                   <a  class="btn btn-success btn-sm" target="_blank" href="<?php echo site_url('Siswa/download_format')?>" style="margin-bottom: 10px; "><i class="mdi mdi-cloud-download"></i>Unduh format Excel Peserta Didik</a>
				  <!--   <a  class="btn btn-primary btn-sm"  href=""<?php echo site_url('Sinkron/resettahun/');?>" style="margin-bottom: 10px; "><i class="mdi mdi-cloud-download"></i>Sinkron Tahun</a> -->

                  </div> 
                      <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tahun Ajaran</label>
                       <div class="col-sm-9">
                            <select required class=" form-control form-control-sm" name="thn">
                             <option value=""> Pilih tahun</option>
                             <?php
                            foreach($thn->result() as $r){
                            echo "<option value='".$r->id_tahunajaran."'>".$r->nama_tahun."</option>";}
                        ?> 
                            </select>
                           </div>
                      </div>
                      <div class="form-group row" >
                        <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Di terima di kelompok</label>
                          <div class="col-sm-9">
                          <select required class=" form-control form-control-sm" name="kls">
                             <option value=""> Pilih Kelompok</option>
                             <?php
                            foreach($kelas->result() as $r){
                            echo "<option value='".$r->id_kelas."'>".$r->nama_kelas."</option>";}
                        ?> 
                          </select>
                           </div>
                      </div>
                           <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Upload Excel</label>
                          <div class="col-sm-9">
                          <input type="file" name="file"/>
                           </div>
                      </div>

                     
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <a href="<?php echo site_url('Siswa/siswa');?>" class="btn btn-danger" value="Batal"><i class="icon-remove"></i> Batal</a>
                      </form>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>