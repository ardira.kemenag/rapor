<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
<body>

      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-6 grid-margin">
              <div class="card chart-bg">
                  <div class="wrapper d-flex justify-content-center mt-4 mb-4">
                    <div class="icon"><i class="mdi mdi-chart-pie icon-md text-success mr-2"></i></div>
                    <div class="details">
                      <h2 class="mb-1 mt-2">Total Siswa</h2>
                      <h4> <?=$jmlsiswa?> Siswa </h4>
                    </div>
                  </div>
                </div>
             
            </div>
            <div class="col-lg-6 grid-margin">
              <div class="card user-bg">
            
                  <div class="wrapper d-flex justify-content-center mt-4 mb-4">
                    <div class="icon"><i class="mdi mdi-account-multiple icon-md text-info mr-2"></i></div>
                    <div class="details">
                      <h2 class="mb-1 mt-2">Total Guru</h2>
                      <h4><?=$jmlguru?> Guru</h4>
                    </div>
                  </div>
                </div>
            </div>
          </div>
          <!--  <a href="<?php echo site_url('Update/update_aplication')?>"  class="btn btn-success"><i class="mdi mdi-loop"></i>Update Aplikasi</a> -->
          <h4 class="card-title">Sikronisasi</h4>
               <div class="timeline">
                      <div class="timeline-wrapper timeline-wrapper-warning">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                          <div class="timeline-heading">

                            <h6 class="mb-0"><div class="badge badge-pill badge-outline-warning">1</div>
                          <a class="collapsed timeline-title" data-toggle="collapse" href="#collapseTwo-1" aria-expanded="false" aria-controls="collapseTwo-1">
                            Data Siswa
                          </a></h6>
                          </div>
                          <div id="collapseTwo-1" class="collapse" role="tabpanel" aria-labelledby="headingTwo-2" data-parent="#accordion-2">
                          <br><p>
                            Tambah Data Peserta Didik,dengan cara:
                          </p>
                             <ol class="pl-3">
                            <li>Tambahkan satu persatu melalui halaman "Tambah Siswa"</li>
                            <li>atau Upload Data Siswa dengan format .xlsx</li>
                            
                          </ol>
                          </div>
                        </div>
                      </div>
                      <div class="timeline-wrapper timeline-inverted timeline-wrapper-danger">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                          <div class="timeline-heading">
                            <h6 class="mb-0"><div class="badge badge-pill badge-outline-danger">2</div>
                          <a class="collapsed timeline-title" data-toggle="collapse" href="#collapseTwo-2" aria-expanded="false" aria-controls="collapseTwo-2">
                            Data Tema
                          </a></h6>
                          </div>
                          <div id="collapseTwo-2" class="collapse" role="tabpanel" aria-labelledby="headingTwo-2" data-parent="#accordion-2">
                          <br><p>
                            Tambah Data Tema Sesuai Program semester yang telah dibuat sebelum membuat Penjadwalan
                          </p>
                            
                          </div>
                          
                        </div>
                      </div>
                      <div class="timeline-wrapper timeline-wrapper-success">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                          <div class="timeline-heading">
                            
                            <h6 class="mb-0"><div class="badge badge-pill badge-outline-success">3</div>
                          <a class="collapsed timeline-title" data-toggle="collapse" href="#collapseTwo-3" aria-expanded="false" aria-controls="collapseTwo-3">
                            Tambah Penjadwalan
                          </a></h6>
                          </div>
                          <div id="collapseTwo-3" class="collapse" role="tabpanel" aria-labelledby="headingTwo-3" data-parent="#accordion-2">
                          <br><p>
                           Tambahkan data Penjadwalan sesuai Program semester yang telah dibuat
                          </p>
                          
                        </div>
                      </div>
                    </div>
                      <div class="timeline-wrapper timeline-inverted timeline-wrapper-info">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                          <div class="timeline-heading">
                            <h6 class="mb-0"><div class="badge badge-pill badge-outline-info">4</div>
                          <a class="collapsed timeline-title" data-toggle="collapse" href="#collapseTwo-4" aria-expanded="false" aria-controls="collapseTwo-4">
                            Penilaian Siswa
                          </a></h6>
                          </div>
                          <div id="collapseTwo-4" class="collapse" role="tabpanel" aria-labelledby="headingTwo-4" data-parent="#accordion-2">
                          <br><p>
                            Penginputan data nilai siswa perhari
                          </p>
                      </div>
                    </div>
                      </div>
                     <div class="timeline-wrapper timeline-wrapper-primary">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                          <div class="timeline-heading">
                            <h6 class="mb-0"><div class="badge badge-pill badge-outline-primary">5</div>
                          <a class="collapsed timeline-title" data-toggle="collapse" href="#collapseTwo-5" aria-expanded="false" aria-controls="collapseTwo-5">
                           Laporan
                          </a></h6>
                          </div>
                          <div id="collapseTwo-5" class="collapse" role="tabpanel" aria-labelledby="headingTwo-3" data-parent="#accordion-2">
                          <br><p>
                            Laporan nilai siswa
                          </p>
                        </div>
                      </div>
                    </div>
                     <div class="timeline-wrapper timeline-inverted timeline-wrapper-warning">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                          <div class="timeline-heading">
                            <h6 class="mb-0"><div class="badge badge-pill badge-outline-warning">6</div>
                          <a class="collapsed timeline-title" data-toggle="collapse" href="#collapseTwo-6" aria-expanded="false" aria-controls="collapseTwo-6">
                           Download Rekap Semester
                          </a></h6>
                          </div>
                          <div id="collapseTwo-6" class="collapse" role="tabpanel" aria-labelledby="headingTwo-6" data-parent="#accordion-2">
                          <br><p>
                            download rekap semester seluruh siswa lalu kirim data ke Pusat pada halaman online rapor digital
                          </p>
                      </div>
                    </div>
                      </div>
                      

                      
                    </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved. <?php echo VERSI_ARDIRA ?></span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>
 <script src="<?php echo base_url();?>assets/node_modules/sweetalert/dist/sweetalert.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/alerts.js"></script>
   <script type="text/javascript">
    <?php 
    if($this->session->flashdata("msg")=='oke'){?>
      swal({
              title: 'Berhasil Update',
              text: 'Ardira verhasil terupdate',
              timer: 3000,
              button: false
            })
   <?php  }
   elseif($this->session->flashdata("msg")=='gagal'){ ?>
       swal({
              title: 'Gagal',
              text: 'Belum tersedia versi terbaru',
              timer: 3000,
              button: false
            })

  <?php } elseif($this->session->flashdata("msg")=='koneksi'){ ?>
       swal({
              title: 'Gagal',
              text: 'CEK KONEKSI ANDA',
              timer: 3000,
              button: false
            })
     <?php }?>
   </script>
</html>