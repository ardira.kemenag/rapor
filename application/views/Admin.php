<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
  
          
		}
	public function index()
	{
		$this->load->view('admin/login');
	}
	  public function verifikasi() {
      $username = $this->input->post('username');
    $password = $this->input->post('password');

    $data = $this->model_m->cekLogin($username,$password);   
 
    $row = $data->row();  
 

    if(!isset($row)) {
        echo"<script>
          alert('Gagal Login: Cek Username dan Password');
          history.go(-1);
          </script>"; 
      }else{
      $session = array('id_user' => $row->id_user,
              'username'=>$row->username,
              'password' => $row->nama);
              
      $this->session->set_userdata($session);
      redirect('Admin/dashboard');
      } 
    }
	public function dashboard()
	{
		$data['jmlalumni']=$this->model_m->alumni()->num_rows();
		$data['jmlrequest']=$this->model_m->alumnilist()->num_rows();
		$this->load->view('admin/home',$data);
	}
	public function berita()
	{

		$data['berita']=$this->model_m->selectsemua('tbl_berita')->result();
		$this->load->view('admin/berita',$data);
	}
	public function waittinglist()
	{
		//$where = array('status'=>'0');
		$data['alumni']=$this->model_m->alumnilist();
		$this->load->view('admin/listalumni',$data);
	}
	public function dataalumni()
	{
		//$where = array('status'=>'0');
		$data['alumni']=$this->model_m->alumni();
		$this->load->view('admin/dataalumni',$data);
	}
	public function ubahalumni($id_alumni)
	{
		$data['fk']=$this->model_m->selectsemua('fakultas');
		$data['dep']=$this->model_m->selectsemua('departemen');
		$data['gl'] = $this->model_m->selectX('tbl_galeri','id_galeri='.$id_galeri);
		$data['alumni'] = $this->model_m->selectX('tbl_alumni','id_alumni='.$id_alumni)->row();
		$this->load->view('admin/ubahalumni',$data);
	}
	public function aksi_ubahalumni($id_alumni)
	{
	  $data['alumni'] = $this->model_m->selectX('tbl_alumni','id_alumni='.$id_alumni)->row();
      $foto=$this->updateFoto($id_alumni);
      if($foto==NULL){
      $foto=$data['alumni']->foto;
      }
      $datagaleri=array(
             'nama_alumni'=>$this->input->post('f1'),
             'tahun_masuk'=>$this->input->post('f2'),
							'nim'=>$this->input->post('f3'),
							'id_fakultas'=>$this->input->post('f4'),
							'id_departemen'=>$this->input->post('f5'),
							'jenis_kelamin'=>$this->input->post('f6'),
							
							'email'=>$this->input->post('f7'),
							'no_hp'=>$this->input->post('f8'),
							'alamat_rumah'=>$this->input->post('f9'),
							'jenis_pekerjaan'=>$this->input->post('f10'),
							'pekerjaan'=>$this->input->post('f11'),
							'perusahaan'=>$this->input->post('f12'),
							'alamat_kantor'=>$this->input->post('f13'),
							
			  'foto'=>$foto,
      );
      $resultInsert = $this->model_m->update_data("tbl_alumni", $datagaleri, 'id_alumni='.$id_alumni);
      if($resultInsert){
        redirect('Admin/dataalumni/');
      }
	}

	private function updateFoto($id_alumni){
				$tipeFile=explode('.',$_FILES["foto"]["name"]);
				$tipeFile=$tipeFile[count($tipeFile)-1];
				$url=$id_alumni.'.'.$tipeFile;
				if(in_array($tipeFile,array("png","jpg","PNG","jpeg"))){
					if(is_uploaded_file($_FILES["foto"]["tmp_name"])){
							if(move_uploaded_file($_FILES["foto"]["tmp_name"], "./uploads/".$url)){
								return $url;
							}
					}
				}else{
					
				}
		}
	  public function approve($id_alumni){
		$where = array('id_alumni'=>$id_alumni);
		$status = array('status'=>'1');
		$update = $this->model_m->update_data('tbl_alumni',$status,$where);
		
		redirect('admin/waittinglist',$data);
	}
	 public function reject($id_alumni){
            $where = array("id_alumni"=>$id_alumni);
            $this->model_m->delete_data('tbl_alumni',$where);
			//$this->menu_m->delete_data('menu',$where);
            redirect('admin/waittinglist');
        }
	public function insertberita()
	{
		$data['ktg']=$this->model_m->selectsemua('tbl_kategoriberita')->result();
		$this->load->view('admin/tambahberita',$data);
	}
	public function bursa()

	{
		$data['bursa']=$this->model_m->selectsemua('tbl_bursakerja')->result();
		$this->load->view('admin/bursa',$data);
	}
	public function jabatan()

	{
		$data['jabatan']=$this->model_m->selectsemua('jabatan_pengurus')->result();
		$this->load->view('admin/jabatan',$data);
	}
	public function insertjabatan()
	{
		$this->load->view('admin/tambahjabatan');
	}
	public function aksi_insertjabatan()
	{
		$data1 = array('nama_jabatan'=>$this->input->post('f1'),
					   'nomor'=>$this->input->post('f2')
	);
		
		$this->model_m->input_data('jabatan_pengurus',$data1);
					  redirect('Admin/jabatan');
	}
	public function ubahjabatan($id_jabatan)

	{
		$data['jb']=$this->model_m->selectsemua('jabatan_pengurus')->result();
		$data['jabatan'] = $this->model_m->selectX('jabatan_pengurus','id_jabatan='.$id_jabatan)->row();
		$this->load->view('admin/ubahjabatan',$data);
	}
	public function aksi_ubahjabatan()

	{
		$data1 = array('nama_jabatan'=>$this->input->post('f1'),
					   'nomor'=>$this->input->post('f2')
					);
		$where = array('id_jabatan'=>$this->input->post('fid'));
		$this->model_m->update_data('jabatan_pengurus',$data1,$where);
		redirect('Admin/jabatan');
	}
	public function hapusjabatan($id_jabatan){
            $where = array("id_jabatan"=>$id_jabatan);
            $this->model_m->delete_data('jabatan_pengurus',$where);
			//$this->menu_m->delete_data('menu',$where);
            redirect('admin/jabatan');
        }
	public function ubahbursa($id_bursa)

	{
		$data['jenis']=$this->model_m->selectsemua('tbl_jenisbursa')->result();
		$data['bursa'] = $this->model_m->selectX('tbl_bursakerja','id_bursa='.$id_bursa)->row();
		$this->load->view('admin/ubahbursa',$data);
	}
	public function insertbursa()

	{
		$data['jenis']=$this->model_m->selectsemua('tbl_jenisbursa')->result();
		$this->load->view('admin/tambahbursa',$data);
	}
	public function aksi_insertbursa()

	{
		$config['upload_path'] = './uploads';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto')){
			$data=$this->upload->data();			
			//$config['file_name'];
			$foto	= $data["file_name"];				
		}else{ $foto	=''; }
		$data1 = array(
							'nama_bursakerja'=>$this->input->post('f1'),
							'id_jenisbursa'=>$this->input->post('f2'),
							'posisi'=>$this->input->post('f3'),
							'tanggal_berlaku'=>$this->input->post('f4'),
							'persyaratan'=>$this->input->post('f5'),
							
							'foto'=>$foto,
						);

					$this->model_m->input_data('tbl_bursakerja',$data1);
					  redirect('Admin/bursa');
	}

	public function aksi_ubahbursa($id_bursa)

	{
	  $data['bursa'] = $this->model_m->selectX('tbl_bursakerja','id_bursa='.$id_bursa)->row();
      $foto=$this->updategbr($id_bursa);
      if($foto==NULL){
      $foto=$data['bursa']->foto;
      }
      $databursa=array(
              'nama_bursakerja'=>$this->input->post('judul'),
              'posisi'=>$this->input->post('isi'),
              'persyaratan'=>$this->input->post('syarat'),
              'foto'=> $foto
      );
      $resultInsert = $this->model_m->update_data("tbl_bursakerja", $databursa, 'id_bursa='.$id_bursa);
      if($resultInsert){
        redirect('Admin/bursa/'.$id_bursa);
      }
	}

	private function updateGbr($id_berita){
				$tipeFile=explode('.',$_FILES["foto"]["name"]);
				$tipeFile=$tipeFile[count($tipeFile)-1];
				$url=$id_berita.'.'.$tipeFile;
				if(in_array($tipeFile,array("png","jpg","PNG","jpeg"))){
					if(is_uploaded_file($_FILES["foto"]["tmp_name"])){
							if(move_uploaded_file($_FILES["foto"]["tmp_name"], "./uploads/".$url)){
								return $url;
							}
					}
				}else{
					
				}
		}
	

	public function jenisbrs()
	{
		$data['galerisub']=$this->model_m->selectsemua('divisi')->result();
		$data['bursa']=$this->model_m->selectsemua('tbl_jenisbursa')->result();
		$this->load->view('admin/jenisbursa',$data);
	}
	public function insertjenisbrs()

	{
		$this->load->view('admin/tambahjenisbrs');
	}
	public function aksi_insertjenisbrs()
	{
		$data1 = array('nama_jenisbursa'=>$this->input->post('f1'));
					$this->model_m->input_data('tbl_jenisbursa',$data1);
					  redirect('Admin/jenisbrs');
	}
	public function ubahjenisb($id_jenisbursa){
		$data['jbursa'] = $this->model_m->selectX('tbl_jenisbursa','id_jenisbursa='.$id_jenisbursa)->row();
		$this->load->view('admin/ubahjenisbursa',$data);
	}
		public function aksi_ubahjenisb()

	{
		
		$data1 = array('nama_jenisbursa'=>$this->input->post('f1'));
		$where = array('id_jenisbursa'=>$this->input->post('fid'));
		$this->model_m->update_data('tbl_jenisbursa',$data1,$where);
		redirect('Admin/jenisbrs');
	}
	public function kategoribrt()
	{
		$data['media']=$this->model_m->selectsemua('tbl_kategoriberita')->result();
		$this->load->view('admin/kategorib',$data);
	}
	public function insertkategori()
	{
		$this->load->view('admin/tambahkategorib');
	}
	public function aksi_insertkategori()
	{
		$config['upload_path'] = './assets/images/';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
		$this->upload->initialize($config);
		if($this->upload->do_upload('logo')){
			$data=$this->upload->data();			
			//$config['file_name'];
			$logo	= $data["file_name"];				
		}else{ $logo	=''; }
		$data1 = array('nama_kategori'=>$this->input->post('f1'),
			'logo'=>$logo,
						);
		$data['ktg']=$this->model_m->selectX('tbl_kategoriberita')->result();
		$this->model_m->input_data('tbl_kategoriberita',$data1);
					  redirect('Admin/kategoribrt');
	}
	public function ubahktg($id_kategori)
	{
		$data['kategori'] = $this->model_m->selectX('tbl_kategoriberita','id_kategori='.$id_kategori)->row();
	    $this->load->view('admin/ubahkategori',$data);
	}
	public function aksi_ubahkategori($id_kategori)

	{
		$data['kategori'] = $this->model_m->selectX('tbl_kategoriberita','id_kategori='.$id_kategori)->row();
      $logo=$this->updategbrktg($id_kategori);
      if($logo==NULL){
      $logo=$data['kategori']->logo;
      }
		$data1 = array('nama_kategori'=>$this->input->post('f1'),
						'logo'=>$logo
	);
		$resultInsert = $this->model_m->update_data("tbl_kategoriberita", $data1, 'id_kategori='.$id_kategori);
      if($resultInsert){
        redirect('Admin/kategoribrt');
      }
		
	}
	private function updateGbrktg($id_kategori){
				$tipeFile=explode('.',$_FILES["logo"]["name"]);
				$tipeFile=$tipeFile[count($tipeFile)-1];
				$url=$id_kategori.'.'.$tipeFile;
				if(in_array($tipeFile,array("png","jpg","PNG","jpeg"))){
					if(is_uploaded_file($_FILES["logo"]["tmp_name"])){
							if(move_uploaded_file($_FILES["logo"]["tmp_name"], "./assets/images/".$url)){
								return $url;
							}
					}
				}else{
					
				}
		}
	public function divisi()
	{
		$data['div']=$this->model_m->selectsemua('divisi')->result();
		$this->load->view('admin/divisi',$data);
	}
	public function insertdivisi()
	{
		$this->load->view('admin/tambahdivisi');
	}
	public function aksi_insertdivisi()
	{
		$data1 = array('nama_divisi'=>$this->input->post('f1'));
					$this->model_m->input_data('divisi',$data1);
					  redirect('Admin/divisi');
	}
	public function ubahdivisi($id_divisi){
		$data['divisi'] = $this->model_m->selectX('divisi','id_divisi='.$id_divisi)->row();
		$this->load->view('admin/ubahdivisi',$data);
	}
		public function aksi_ubahdivisi()

	{
		
		$data1 = array('nama_divisi'=>$this->input->post('f1'));
		$where = array('id_divisi'=>$this->input->post('fid'));
		$this->model_m->update_data('divisi',$data1,$where);
		redirect('Admin/divisi');
	}

	public function aksi_insertberita()

	{
		$config['upload_path'] = './uploads';
		$config['allowed_types'] = 'jpg|png|jpeg';		
	   $this->load->library('upload', $config);
       $this->upload->initialize($config);
       if ( ! $this->upload->do_upload('gambar'))
       {
       //file gagal  upload
       $this->session->set_flashdata('msg2', 'File Tidak Memungkinkan');
                 redirect('Admin/insertberita');
        }else{
            $gbr= $this->upload->data();
            $data = array(
              "judul"=>$this->input->post('f1'),
              "isi"=>$this->input->post('f2'),
               "keterangan"=>$this->input->post('ket'),
               "tanggalPost"=>$this->input->post('tgl'),
              'gambar'=>$gbr['file_name']
          );
      $this->model_m->input_data("tbl_berita",$data);
      echo "<script>
          alert ('Data Berita berhasil Disimpan');
          window.location.href='".site_url("Admin/berita")."';
          </script>";
    }
	}

	public function ubahberita($id_berita)
	{
		$data['ktg']=$this->model_m->selectsemua('tbl_kategoriberita')->result();
		$data['berita'] = $this->model_m->selectX('tbl_berita','id_berita='.$id_berita)->row();
        $this->load->view('admin/ubahberita',$data);
	}

	public function aksi_ubahberita($id_berita)

	{
	  $data['berita'] = $this->model_m->selectX('tbl_berita','id_berita='.$id_berita)->row();
      $gambar=$this->updateGambar($id_berita);
      if($gambar==NULL){
      $gambar=$data['berita']->gambar;
      }
      $databerita=array(
              'judul'=>$this->input->post('judul'),
              'isi'=>$this->input->post('isi'),
			  'keterangan'=>$this->input->post('ket'),
              'gambar'=> $gambar
      );
      $resultInsert = $this->model_m->update_data("tbl_berita", $databerita, 'id_berita='.$id_berita);
      if($resultInsert){
        redirect('Admin/berita/'.$id_berita);
      }
	}

	private function updateGambar($id_berita){
				$tipeFile=explode('.',$_FILES["gambar"]["name"]);
				$tipeFile=$tipeFile[count($tipeFile)-1];
				$url=$id_berita.'.'.$tipeFile;
				if(in_array($tipeFile,array("png","jpg","PNG","jpeg"))){
					if(is_uploaded_file($_FILES["gambar"]["tmp_name"])){
							if(move_uploaded_file($_FILES["gambar"]["tmp_name"], "./uploads/".$url)){
								return $url;
							}
					}
				}else{
					
				}
		}
	
		public function galeri()

	{
		$data['info']=$this->model_m->glr();
		$this->load->view('admin/galeri',$data);
	}
	public function insertgaleri()

	{
		$data['dv']=$this->model_m->selectsemua('divisi')->result();
		$this->load->view('admin/tambahgaleri',$data);
	}
	public function aksi_insertgaleri()

	{
		$config['upload_path'] = './uploads';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto')){
			$data=$this->upload->data();			
			//$config['file_name'];
			$foto	= $data["file_name"];				
		}else{ $foto	=''; }
		$data1 = array(
							'caption'=>$this->input->post('f1'),
							'id_divisi'=>$this->input->post('dv'),
							'foto'=>$foto,
						);

					$this->model_m->input_data('tbl_galeri',$data1);
					  redirect('Admin/galeri');
	}
	public function ubahgaleri($id_galeri)
	{
		$data['dv']=$this->model_m->selectsemua('divisi');
		$data['gl'] = $this->model_m->selectX('tbl_galeri','id_galeri='.$id_galeri);
		$data['galeri'] = $this->model_m->selectX('tbl_galeri','id_galeri='.$id_galeri)->row();
		$this->load->view('admin/ubahgaleri',$data);
	}
	public function aksi_ubahgaleri($id_galeri)
	{
	  $data['galeri'] = $this->model_m->selectX('tbl_galeri','id_galeri='.$id_galeri)->row();
      $foto=$this->updateGambarG($id_galeri);
      if($foto==NULL){
      $foto=$data['galeri']->foto;
      }
      $datagaleri=array(
              'caption'=>$this->input->post('f1'),
              'id_divisi'=>$this->input->post('div'),
			  'foto'=>$foto,
      );
      $resultInsert = $this->model_m->update_data("tbl_galeri", $datagaleri, 'id_galeri='.$id_galeri);
      if($resultInsert){
        redirect('Admin/galeri/'.$id_galeri);
      }
	}

	private function updateGambarG($id_galeri){
				$tipeFile=explode('.',$_FILES["foto"]["name"]);
				$tipeFile=$tipeFile[count($tipeFile)-1];
				$url=$id_berita.'.'.$tipeFile;
				if(in_array($tipeFile,array("png","jpg","PNG","jpeg"))){
					if(is_uploaded_file($_FILES["foto"]["tmp_name"])){
							if(move_uploaded_file($_FILES["foto"]["tmp_name"], "./uploads/".$url)){
								return $url;
							}
					}
				}else{
					
				}
		}
		public function ubahstruktur($id_pengurus)
	{
		$data['struktur'] = $this->model_m->selectX('tbl_pengurus','id_pengurus='.$id_pengurus)->row();
		$data['dv']=$this->model_m->selectsemua('divisi')->result();
		$data['al']=$this->model_m->alumni()->result();
			$data['jb']=$this->model_m->selectsemua('jabatan_pengurus')->result();
       /* $this->load->view('admin/ubahberita',$data);
		$where= array('id_galeri'=>$id_g);
		$data['vm']=$this->model_m->selectX('tbl_galeri',$where);*/
		$this->load->view('admin/ubahstruktur',$data);
	}
	public function aksi_ubahstruktur($id_pengurus)
	{
	  $data['struktur'] = $this->model_m->selectX('tbl_pengurus','id_pengurus='.$id_pengurus)->row();
     
      $datainfo=array(
              'id_pengurus'=>$this->input->post('nama'),
              'id_jabatan'=>$this->input->post('jab'),
             
              'id_divisi'=>$this->input->post('div'),
			  
      );
      $resultInsert = $this->model_m->update_data("tbl_pengurus", $datainfo, 'id_pengurus='.$id_pengurus);
      if($resultInsert){
        redirect('Informasi/struktur/'.$id_pengurus);
      }
	}

	

	public function insertstruktur()

	{
		$data['jb']=$this->model_m->selectsemua('jabatan_pengurus')->result();
	$where = array('status' =>'1' );
		$data['al']=$this->model_m->selectX('tbl_alumni',$status);
		$data['dv']=$this->model_m->selectsemua('divisi')->result();
		$this->load->view('admin/tambahpengurus',$data);
	}
		public function aksi_insertstruktur()

	{
		
		$data1 = array(
							'id_alumni'=>$this->input->post('f1'),
							'id_jabatan'=>$this->input->post('f2'),
							'id_divisi'=>$this->input->post('div'),
							
						);

					$this->model_m->input_data('tbl_pengurus',$data1);
					  redirect('Informasi/struktur');
	}

	function get_alumni(){
        $this->load->model('model_m');
        $id_alumni = $this->input->post('id_alumni');
        $dataAlumni = $this->model_m->getDataAlumni($id_alumni);
        
         echo '<div class="row form-group"><div class="col-md-4 text-center"><label>Email</label>
                      </div><div class="col-md-6 text-left">';
         foreach($dataAlumni as $a){
        echo '<input type="text" name="f3" class="form-control" value="'.$a->email.'"></div></div>';
        echo '<div class="row form-group"><div class="col-md-4 text-center"><label>Email</label>
                      </div><div class="col-md-6 text-left">';
        
        echo '<input type="text" name="f4" class="form-control" value="'.$a->no_hp.'"></div></div>';
       
        
       
    }


       

    }
	

	

	

}
