<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 
  ?>

<body>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row user-profile"><?php
                                  $datasiswa = $dtRA->result();
                                  ?>
          

            <div class="col-lg-12 side-right stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="wrapper d-block d-sm-flex align-items-center justify-content-between">
                    <h4 class="card-title mb-0">Details</h4>
                    <ul class="nav nav-tabs tab-solid tab-solid-primary mb-0" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-expanded="true">Info</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="avatar-tab" data-toggle="tab" href="#avatar" role="tab" aria-controls="avatar">Siswa</a>
                      </li>
                     
                    </ul>
                  </div>
                  <div class="wrapper">
                    <hr>
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info">
                        <form action="" method="post">
                            <div class="row">
                          <div class="col-xs-12 col-md-8 ">
                          <div class="form-group row" >
                       <label  class="col-sm-5 col-form-label">Nama Madrasah</label>
                          <div class="col-sm-4">: <?php echo $datasiswa[0]->nama_sekolah?></div>
                           </div>
                         <div class="form-group row">
                       <label class="col-sm-5 col-form-label">No Induk</label>
                          <div class="col-sm-4">: <?php echo $datasiswa[0]->NSN?></div>
                      </div>
                       <div class="form-group2 row" >
                       <label  class="col-sm-3 col-form-label">Alamat Madrasah</label>
                      </div>
                       <div class="form-group2 row" >
                       <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Jalan</label>
                          <div class="col-sm-7">: <?php echo $datasiswa[0]->jalan?></div>
                           </div>
                          <div class="form-group2 row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Kelurahan/Desa/Dusun</label>
                          <div class="col-sm-7">: <?php echo $datasiswa[0]->kelurahan?></div>
                           </div>
                            <div class="form-group2 row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Kecamatan</label>
                          <div class="col-sm-5">: <?php echo $datasiswa[0]->kecamatan?></div>
                           </div>
                            <div class="form-group2 row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Provinsi</label>
                          <div class="col-sm-5">: <?php echo $datasiswa[0]->nama_provinsi?></div>
                           </div>
                            <div class="form-group2 row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Kota/Kabupaten</label>
                          <div class="col-sm-5" > : <?php echo $datasiswa[0]->nama_kota?></div>
                           </div>
                           <div class="form-group row">
                       <label class="col-sm-5 col-form-label">Kepala RA</label>
                          <div class="col-sm-4">: <?php echo $datasiswa[0]->kepala_RA?></div>
                      </div>
                        <div class="form-group row">
                       <label class="col-sm-5 col-form-label">Tahun Ajaran</label>
                          <div class="col-sm-4">: <?php echo $datasiswa[0]->tahun_ajaran?></div>
                      </div></div>
                    <div class="col-xs-12 col-md-4"><img class="alignleft" src="<?php echo base_url('uploads/'.$datasiswa[0]->foto);?>"></div>
                          </div>
                      
                      </div><!-- tab content ends -->
                      <div class="tab-pane fade" id="avatar" role="tabpanel" aria-labelledby="avatar-tab">
                        <div class="wrapper mb-5 mt-4">
                          
                        </div>
                        <form action="<?php echo site_url('kepalaRA/aksi_ubahfoto/')?>" method="post" enctype="multipart/form-data">
                        </form>
                        <table id="mytable" class="table table-striped table-advance table-hover">
                           <thead>
                      <tr> 
                         <th>No</th>
                         <th><center>Nama</center></th>
                        <th><center>Tahun ajaran</center></th>
                        <th><center>Kelompok</center></th>
                        
                      </tr>
                    </thead>
                         
                 <tbody>
                        <?php
                        $no = 1;
                        foreach($siswa->result() as $b){
                        ?>
                        <tr>
                    <td><center> <?php echo $no++ ?></center></td>
                    <td><center> <?php echo $b->nama_siswa ?></center></td>
                    <td><center> <?php echo $b->nama_tahun?></center></td>
                   <td><center> <?php echo $b->kelompok?></center></td>
                    
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                        </table>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018  All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
       <script>
            function get_kota(){
                var id_provinsi = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('Sekolah/get_kota'); ?>", 
                    data:"id_provinsi="+id_provinsi, 
                    success: function(msg) {
                            $("#div_kota").html(msg);
                    }
                });
            }
        </script>

 <?php $this->load->view('footer.php'); ?>
</body>

</html>