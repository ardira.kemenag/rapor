
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Basic Admin</title>
  <!-- plugins:css -->

  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/perfect-scrollbar/css/perfect-scrollbar.css">
   <!-- plugin css for this page -->
<!--   <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/jqvmap/dist/jqvmap.min.css"> -->
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/morris.js/morris.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
  <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url();?>assets/admin/css/loadimg.min.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.png">
</head>
 <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar navbar-light col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper"> <a class="navbar-brand brand-logo" href="index.html"><img src="<?php echo base_url();?>assets/images/logo_ardira.png" alt="logo"></a> <a class="navbar-brand brand-logo-mini" href="index.html"><img src="<?php echo base_url();?>assets/images/logo_ardira.png" alt="logo"></a> </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler align-self-center mr-2" type="button" data-toggle="minimize"> <span class="mdi mdi-equal display-3"></span> </button>
       <!-- <form class="form-inline mt-2 mt-md-0 d-none d-lg-block">
          <div class="input-group search"> <span class="input-group-addon bg-transparent" id="basic-addon1"><i class="mdi mdi-magnify"></i></span> <input type="text" class="form-control bg-transparent" placeholder="Search..." aria-label="Username" aria-describedby="basic-addon1"> </div>
        </form>-->
        <ul class="navbar-nav nav-header-item-wrapper">
          <li class="nav-item d-none d-sm-block dropdown">
           <a class="btn bg-transparent dropdown-toggle text-default-color" id="userDropdown" href="#" data-toggle="dropdown"> 
          <?php 
                                    if($this->session->userdata('username')){
                                    $nama = $this->session->userdata('username');
                                    echo ucwords($nama);
                                }?></a>
            <div class="dropdown-menu navbar-dropdown preview-list p-3" aria-labelledby="userDropdown"> <a href="#" class="btn btn-block btn-link text-dark">View Profile</a> <a href="#" class="btn btn-block btn-link text-dark">Account Settings</a>
              <div class="dropdown-divider mb-4"></div><a href="<?php echo site_url('beranda');?>" class="btn btn-block btn-primary">Logout</a> </div>
          </li>         
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas"> <span class="mdi mdi-equal display-3"></span> </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('pusat');?>"> <i class="mdi mdi-compass-outline menu-icon"></i> <span class="menu-title">Dashboard</span> </a> </li>
         
          <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('Sekolah/kanwilschool');?>"> <i class="mdi mdi-calendar-text menu-icon"></i> <span class="menu-title">Sekolah</span> </a> </li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('Registrasi/validasikanwil');?>"> <i class="mdi mdi-calendar-text menu-icon"></i> <span class="menu-title">Validasi Ra</span> <right><span class="count bg-success" style="padding: 3px 7px 1px 6px; border-radius: 50px; color: white; margin-left: 80px;"><?=$jmlsekolah?></span> </right></a> </li>
         
            
         
         
            
          
          

        </ul>
      </nav>