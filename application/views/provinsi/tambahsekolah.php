<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 
  ?>
   <script>
            function get_kota(){
                var id_provinsi = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('Sekolah/get_kota'); ?>", 
                    data:"id_provinsi="+id_provinsi, 
                    success: function(msg) {
                            $("#div_kota").html(msg);
                    }
                });
            }
        </script>

<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah Siswa </h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Home');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Siswa/siswa');?>">Data RA</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah RA</li>
                      </ol>
                    </nav>
                      <form class="forms-sample" action="<?php echo site_url('sekolah/aksi_insert/');?>" method="post" enctype="multipart/form-data">
                        
                        <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama RA </label>
                         <div class="col-sm-9">
                           <input type="text" name="namaS" class="form-control form-control-lg" placeholder=" Nama RA">
                           </div >
                           
                      </div>

                         <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">NSN</label>
                          <div class="col-sm-9">
                           <input type="text" name="nsn" class="form-control form-control-lg">
                           </div>
                      </div>
                     
                       <div class="form-group row" >
                       <label  class="col-sm-3 col-form-label">Alamat RA</label>
                      </div>
                       <div class="form-group row" >
                       <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Jalan</label>
                          <div class="col-sm-9">
                          <input type="text" name="jln" class="form-control form-control-lg" placeholder="Jalan ">
                           </div>
                           </div>
                          <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kelurahan/Desa/Dusun</label>
                          <div class="col-sm-9">
                          <input type="text" name="kel" class="form-control form-control-lg" placeholder="kelurahan ">
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kecamatan</label>
                          <div class="col-sm-9">
                          <input type="text" name="kec" class="form-control form-control-lg" placeholder="Jalan ">
                           </div>
                           </div>
                           <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kode POS</label>
                          <div class="col-sm-9">
                          <input type="text" name="pos" class="form-control form-control-lg" placeholder="Jalan ">
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Provinsi</label>
                          <div class="col-sm-9">
                         <select name="prov" class=" form-control form-control-sm" id="prov" onchange="get_kota()">
                           <option value="0"> Pilih Provinsi</option>
                             <?php
                            foreach($prov as $r){
                            echo "<option value='".$r->id_provinsi."'>".$r->nama_provinsi."</option>";}
                        ?> 
                         </select>
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kota/Kabupaten</label>
                          <div class="col-sm-9" id="div_kota">
                          <select name="kab" class=" form-control form-control-sm">
                           <option> Pilih kabupaten</option>
                         </select>
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Web site</label>
                          <div class="col-sm-9">
                          <input type="text" name="web" class="form-control form-control-lg" placeholder="Jalan ">
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Telpon</label>
                          <div class="col-sm-9">
                          <input type="text" name="tlp" class="form-control form-control-lg" placeholder="Jalan ">
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Email</label>
                          <div class="col-sm-9">
                          <input type="text" name="email" class="form-control form-control-lg" placeholder="Jalan ">
                           </div>
                           </div>
                            <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama Kepala RA</label>
                          <div class="col-sm-9">
                           <input type="text" name="kepRA" class="form-control form-control-lg">
                           </div>
                      </div>
                        <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tahun Ajaran</label>
                          <div class="col-sm-9">
                           <input type="text" name="thn" class="form-control form-control-lg">
                           </div>
                      </div>
                     

                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <a href="<?php echo site_url('Siswa/siswa');?>" class="btn btn-danger" value="Batal"><i class="icon-remove"></i> Batal</a>
                      </form>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>