<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
   

<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah Peserta Didik </h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home/dashboard')?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Siswa/dt');?>">Data Peserta Didik</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Peserta Didik</li>
                      </ol>
                    </nav>
                      <form class="forms-sample" action="<?php echo site_url('Siswa/aksi_insert/');?>" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                     
                          <div class="col-sm-9">
                           <input type="hidden" name="sekolah" class="form-control form-control-lg" value="<?=$this->session->userdata('id_sekolah');?>">
                           </div>
                      </div>
                        
                        <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama Lengkap </label>
                         <div class="col-sm-5">
                           <input type="text" name="namaL" class="form-control form-control-lg" placeholder=" Nama lengkap" required>
                           </div>  <label for="exampleInputPassword2" class="col-form-label">Nama Panggilan </label>
                           <div class="col-sm-2">
                           <input type="text" name="namaP" class="form-control form-control-lg" placeholder=" nama panggilan">
                        </div>
                      </div>

                         <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">No Induk</label>
                          <div class="col-sm-9">
                           <input type="text" name="noinduk" class="form-control form-control-lg" required>
                           </div>
                      </div>
                      <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tempat/ Tanggal Lahir</label>
                          <div class="col-sm-5">
                           <input type="text" name="tlahir" class="form-control form-control-lg"></div> 
                           <div class="col-sm-3">
                            <input type="date" name="tgllahir"  class="form-control form-control-lg" >
                           </div>
                      </div>
                      <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Agama</label>
                          <div class="col-sm-9">
                          <select name="agama" class="form-control form-control-sm">
                            <option value="Islam"> Islam</option>
                            <option value="Protestan"> Kristen Protestan</option>
                            <option value="Katolik"> Kristen Katolik</option>
                            <option value="Hindu"> Hindu</option>
                            <option value="Budha"> Budha</option>
                             <option value="Konghucu"> Konghucu</option>
                           
                          </select>
                           </div>
                       
                      </div>

                       <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Jenis kelamin</label>
                          <div class="col-sm-9">
                          <select name="jk" class="form-control form-control-sm">
                            <option value="Laki-Laki"> Laki-Laki</option>
                            <option value="Perempuan"> Perempuan</option>
                          </select>
                           </div>
                       
                      </div>
                      <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama Orang Tua</label>

                          <div class="col-sm-4">
                           <input type="text" name="ayah" class="form-control form-control-lg" placeholder="Ayah">
                           </div>
                           <div class="col-sm-4">
                           <input type="text" name="ibu" class="form-control form-control-lg" placeholder="Ibu">
                           </div>
                      </div>
                       <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Pekerjaan Orang Tua</label>
                     
                          <div class="col-sm-4">
                           <input type="text" name="payah" class="form-control form-control-lg" placeholder="Ayah">
                           </div>
                           <div class="col-sm-4">
                           <input type="text" name="pibu" class="form-control form-control-lg" placeholder="Ibu">
                           </div>
                      </div>
                        <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Status</label>
                     
                          <div class="col-sm-9">
                            <select class=" form-control form-control-sm" name="status">
                            <option> Status</option>
                            <option> Anak Kandung</option>
                             <option> Anak Angkat</option>
                          </select>
                           </div>
                          
                      </div>
                         <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Anak Ke</label>
                          <div class="col-sm-9">
                           <input type="text" name="anakke" class="form-control form-control-lg">
                           </div>
                      </div>

                       <div class="form-group row" >
                       <label  class="col-sm-3 col-form-label">Alamat Orangtua</label>
                      </div>
                       <div class="form-group row" >
                       <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Jalan</label>
                          <div class="col-sm-9">
                          <input type="text" name="jln" class="form-control form-control-lg" placeholder="Jalan ">
                           </div>
                           </div>
                          <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kelurahan/Desa/Dusun</label>
                          <div class="col-sm-9">
                          <input type="text" name="kel" class="form-control form-control-lg" placeholder="kelurahan ">
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kecamatan</label>
                          <div class="col-sm-9">
                          <input type="text" name="kec" class="form-control form-control-lg" placeholder="Jalan ">
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Provinsi</label>
                          <div class="col-sm-9">
                         <select required name="prov" class=" form-control form-control-sm" id="prov" onchange="get_kota()">
                           <option value=""> Pilih Provinsi</option>
                             <?php
                            foreach($prov as $r){
                            echo "<option value='".$r->nama_provinsi."'>".$r->nama_provinsi."</option>";}
                        ?> 
                         </select>
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kota/Kabupaten</label>
                          <div class="col-sm-9" id="div_kota">
                          <select required name="kab" class=" form-control form-control-sm">
                           <option> Pilih kabupaten</option>
                         </select>
                           </div>
                           </div>
                            <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Diterima Tanggal/ Tahun Ajaran</label>
                          <div class="col-sm-5">
                           <input type="date" name="terima" class="form-control form-control-lg"></div> 
                           <div class="col-sm-3">
                            <select required class=" form-control form-control-sm" name="thn">
                           <option value=""> Pilih tahun</option>
                             <?php
                            foreach($thn->result() as $r){
                            echo "<option value='".$r->id_tahunajaran."'>".$r->nama_tahun."</option>";}
                        ?> 
                            
                          </select>
                           </div>
                      </div>
                             <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Di terima di kelompok</label>
                     
                          <div class="col-sm-9">
                          <select required class=" form-control form-control-sm" name="kls">
                          
                             <option value=""> Pilih Kelompok</option>
                             <?php
                            foreach($kls->result() as $r){
                            echo "<option value='".$r->id_kelas."'>".$r->nama_kelas."</option>";}
                        ?> 
                          </select>
                           </div>
                          
                      </div>
                      <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Foto</label>
                     
                          <div class="col-sm-9">
                           <input type="file" name="foto"  class="form-control form-control-lg" id="exampleInputPassword2" >
                           </div>
                          
                      </div>
                      <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Wali Siswa</label>
                      </div>
                       <div class="form-group row" >
                       <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Nama</label>
                          <div class="col-sm-9">
                          <input type="text" name="wnama" class="form-control form-control-lg" >
                           </div>
                           </div>
                          <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Alamat</label>
                          <div class="col-sm-9">
                          <input type="text" name="walamat" class="form-control form-control-lg" >
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Pekerjaan</label>
                          <div class="col-sm-9">
                          <input type="text" name="wkerja" class="form-control form-control-lg" >
                           </div>
                           </div>

                        <button type="submit"  onclick="alert('Data Anda Disimpan')" class="btn btn-success mr-2">Submit</button>
                        <a href="<?php echo site_url('Siswa/siswa');?>" class="btn btn-danger" value="Batal"><i class="icon-remove"></i> Batal</a>
                      </form>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
 <script>
            function get_kota(){
                var nama_provinsi = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('siswa/get_kota'); ?>", 
                    data:"nama_provinsi="+nama_provinsi, 
                    success: function(msg) {
                            $("#div_kota").html(msg);
                    }
                });
            }
        </script>
</body>

</html>