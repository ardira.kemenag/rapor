<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
  <script>
            function get_rekap(){
                var id_siswa = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('Report/get_rekap'); ?>", 
                    data:"id_siswa="+id_siswa, 
                    success: function(msg) {
                            $("#div_ktd").html(msg);
                    }
                });
            }
        </script>
       
<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
         <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Report Bulanan</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Report Bulanan Siswa</h4>
                  </div> 
                  <div class="col-lg-6 text-right">
                 
                  </div>
                <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                     <!-- <select name="f1" class="form-control" id="prov" onchange="get_rekapmgg()"> 
                              <option value="0">--Pilih KTI---</option>
                          <?php
                            foreach($siswa as $r){
                            echo "<option value='".$r->id_siswa."'>".$r->no_induk.$r->nama_siswa."</option>";}
                        ?> 
                            </select>-->
                            <form method="post" action="<?php echo site_url('Report/search_bulan');?>">
                            <select name="keyword" class="form-control" id="keyword">
                              <option value=""></option>
                                <?php
                                    foreach($siswa as $k){
                                      echo "<option value='".$k->id_siswa."'>"."(".$k->no_induk.")".$k->nama_siswa."</option>";}
                                ?>
                          </select>
                          <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Cari
                                </button>
                            </span>
                        </form>
                     <div id="div_ktd">
                      
                       
                      </div>

                 <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>Nama</th>
                         
                        <th><center>Tahun/Bulan</center></th>
                         <th><center>Indikator</center></th>
                        <th><center>Nilai</center></th>
                        <th><center>Deskripsi</center></th>
                      </tr>
                    </thead>
                    
                 <tbody>
                    <?php  $no = 1;
                        foreach($bulan as $b){
                        ?>
                        <tr>
                           <td><center> <?php echo $b->nama_siswa?></center></td> 
                                <td> <?php
                                                $format=date('  Y F ', strtotime($b->tanggal_awal));
                                                echo $format;?></td>
                                                  <td> <?php
                               $j=json_decode($b->id_indikator);
                               foreach ($j as $key ) {
                                  echo $this->model_m->cek_ind($key).'<br>';
                               }
                               ?></td> 
                                
                                <td><center> <?php if ($b->rekapbulan=='1'){
                          echo 'BB';
                        }
                        elseif ($b->rekapbulan=='2') {
                          echo 'MB';
                        }
                        elseif ($b->rekapbulan=='3') {
                          echo 'BSH';
                        }
                        else{
                          echo 'BSB';
                        
                        }?></center></td>
                                 <td><center> <?php echo $b->deskripsi?></center></td>
                                              
                  
                        
                            
                      </tr>
                                <?php
                                    }
                                ?>
                       
                  </tbody>
                   
                  </table>
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>