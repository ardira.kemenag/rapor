<html>
 <head>
  <title></title>
 </head>
  <?php
                                  $siswaT = $datasiswa->result();
                                  ?>
<?php
function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('/', $tanggal);
  
  // variabel pecahkan 0 = tanggal
  // variabel pecahkan 1 = bulan
  // variabel pecahkan 2 = tahun
 
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}?>
 <body bgcolor="white">

  <table width="100%">
  
        
        <tr>
          
          <td width="100%">
            <div align="center" style="font-size: 18pt;"><b><br/>IDENTITAS ANAK</b></div>
          </td>
          <td rowspan="5" width="25%">&nbsp;</td>
        </tr>
        
       
        <tr>
          <td><div align="center" style="font-size: 10pt;"></div></td>
        </tr>
        </table>
       <hr size="2px" color="black">

      

        <table>
          <tr>
            <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama </td>
            
          </tr>
        <tr>

          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Lengkap </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->nama_siswa))?></td>
        </tr>
        <tr>

          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Panggilan </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->nama_panggilan)?></td>
        </tr>
        <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Induk </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->no_induk)?></td>
        </tr>
        <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tempat, Tgl Lahir </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->tempat_lahir)?>,<?php echo ucwords($siswaT[0]->tanggal_lahir)?></td>
        </tr>
        <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jenis Kelamin </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->jenis_kelamin)?></td>
        </tr>
        <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agama </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->agama)?></td>
        </tr>
         <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Anak Ke </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->anak_ke)?></td>
        </tr>
         <tr>
            <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alamat Orangrtua </td>
            
          </tr>
        <tr>

          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jalan </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->jalan)?></td>
        </tr>
        <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kelurahan/Desa </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->kelurahan)?></td>
        </tr>
         <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kecamatan </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->kecamatan)?></td>
        </tr>
         <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Provinsi </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->provinsi)?></td>
        </tr>
         <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kota/Kabupaten </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->kotakab)?></td>
        </tr>
        <tr>
            <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wali Peserta Didik </td>
          </tr>
        <tr>

          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->nama_wali))?></td>
        </tr>
        <tr>

          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pekerjaan </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->pekerjaan_wali)?></td>
        </tr>
        <tr>

          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alamat </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->alamat_wali)?></td>
        </tr>
        </table>

        <table>
            <table width="100%">
        <tr>
          <td rowspan="5" width="20%">
             <img  src="
                               <?php echo base_url('uploads/'.$siswaT[0]->foto);?>
                            "  width="150px" style="margin: 50PX 30px 30px 20px">
            
          </td>
          <td width="60%">
            <br>
             <div align="right" style="font-size: 12pt;"><b><?php echo ucfirst($siswaT[0]->nama_provinsi)?>,<?php echo tgl_indo(date('Y/m/d'));?></b></div>
            <div align="right" style="font-size: 12pt;"><b>Kepala Raudhatul Athfal</b></div>
          </td>
          <td rowspan="5" width="25%">&nbsp;</td>
        </tr>
        <tr>
          <td></td>
        </tr>
        <tr>
          <td>
            <div align="center" style="font-size: 12pt;"><br> </div>

          </td>
        </tr>
        <tr>
          <td>
            <div align="center" style="font-size: 10pt;">
            
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <div align="center" style="font-size: 10pt;">
            
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <div align="center" style="font-size: 10pt;">
             <td align="right">&nbsp;(<?php echo ucwords($siswaT[0]->kepala_RA)?>)</td>

            </div>
          </td>
        </tr>
        <tr>
          <td> <div align="center" style="font-size: 10pt;">
             <td align="right">&nbsp;NIP:<?php echo ucwords($siswaT[0]->nip)?></td>

            </div></td>
        </tr>
        </table>
       
        <tr>
          <td colspan="3">&nbsp;</td>
        </tr>
        
         
        </table>

  
 </body>
 <script>

    window.print();
</script>
</html>