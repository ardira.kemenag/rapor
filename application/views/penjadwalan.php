<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>

  
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">

                <div class="card-body">
                  <?php echo $this->session->flashdata('msg2');?>
                  <div class="row">
                    <div class="col-lg-6 text-left">
                  <h4 class="card-title">Jadwal</h4></div>
                   <div class="col-lg-6 text-right">
                    <a  class="btn btn-light btn-sm"  href="<?php echo site_url('penjadwalan')?>">Tambah</a>
                  </div>
                </div>
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Home/dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data jadwal</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    
                  </div> 
                 
                <div class="col-lg-12">
                    <form method="post" action="<?php echo site_url('penjadwalan/jadwalfilter');?>">
                          <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-2 col-form-label">Tahun Ajaran</label>
                          <div class="col-sm-8">
                         <select name="thn" class=" form-control form-control-sm">
                           <option value=""> Pilih tahun ajaran </option>
                             <?php
                            foreach($thn as $r){
                            echo "<option value='".$r->id_tahunajaran."'>".$r->nama_tahun."</option>";}
                        ?> 
                         </select>
                           </div>
                     
                      
                          <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Cari
                                </button>
                            </span> </div>
                        </form>
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No</th>
                         <th>Hari/tanggal</th>
                        <th>Minggu Ke</th>
                        <th>Kelompok</th>
                        <th>Guru</th>
                        <th>Program</th>
                        <th>Tema</th>
                        <th>Subtema</th>
                        <th>KD</th>
                        <th>Indikator</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    
                 <tbody>
                        <?php
                        $no = 1;
                        foreach($jdw->result() as $b){
                        ?>
                        <tr>
                           <td> <?php echo $no++?></td>
                           <td>
                       <?php
                       $dayList = array('Sunday' => 'Minggu','Monday' => 'Senin','Tuesday' => 'Selasa',
                              'Wednesday' => 'Rabu','Thursday' => 'Kamis','Friday' => 'Jumat','Saturday' => 'Sabtu');
                       $format=date('d F Y', strtotime($b->tanggal_awal));
                       $day = date('l', strtotime($b->tanggal_awal));
					             $format2=date('d F Y', strtotime($b->tanggal_akhir));
                       $day2 = date('l', strtotime($b->tanggal_akhir));
                       echo $dayList[$day].',';
                       echo $format.'s/d ';
					             echo $dayList[$day2].',';
                       echo $format2;?>
					   
                    </td>
                     <td><?php echo $b->mingguke ?></td>
                     <td><?php
                               $k=json_decode($b->id_kelas);
                               foreach ($k as $key ) {
                                  echo '('.$this->model_m->cek_kls($key).')';
                               }
                               ?> </td>

                   

                     <td><?php
                               $k=json_decode($b->id_kelas);
                               foreach ($k as $key ) {
                                  echo '-'.$this->model_m->cek_kls2($key).'<br>';
                               }
                               ?></td>
                     <td> <?php echo $b->nama_program ?></td>
                     <td><?php echo $b->nama_tema?></td>
                      <td> <?php echo $b->nama_subtema ?></td>
                      <td> <?php
                               $j=json_decode($b->kd);
                               foreach ($j as $key ) {
                                  echo '('.$this->model_m->cek_kd3($key).')';
                               }
                               ?></td>
                      <td> <?php
                               $j=json_decode($b->id_indikator);
                               foreach ($j as $key ) {
                                  echo $this->model_m->cek_ind($key).'<br>';
                               }
                               ?></td> 

                      <td> 
                         
                           <?php 
                             $where=  array('id_jadwal' => $b->id_perencanaan );
                             $data = $this->model_m->selectX('nilai',$where);   
                              $row = $data->row();  
                           if(!isset($row)) { ?>
                             <a class='btn btn-primary btn-sm' href="<?php echo site_url('penjadwalan/ubahjadwal/').$b->id_perencanaan.'/'?>"> <i class='icon-pencil'></i> Ubah</a>
                              <a class='btn btn-primary btn-sm' onClick='return konfirmasi();' href="<?php echo site_url('penjadwalan/hapus_jadwal/').$b->id_perencanaan?>">
                            <i class='icon-trash'></i> hapus</a>
                          <?php }else{
                            echo " Nilai sudah diisi tidak bisa diubah";
                                 } ?>
                           </td>
                  
                        <!-- <td><center>
                          <a class='btn btn-primary btn-sm' href="<?php echo site_url('admin/riwayat/').$b->id_perencanaan?>">
                            <i class='icon-eye'></i> Lihat</a>
                         </td> -->
                             
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
   <script type="text/javascript" language="JavaScript">
 function konfirmasi()
 {
 tanya = confirm("Anda Yakin Menghapus Data Ini ?");
 if (tanya == true) return true;
 else return false;
 }</script>
</body>

</html>