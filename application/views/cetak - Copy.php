<html>
<style type="text/css">
  @page {
  size: legal;
}
@media print {
  .footer {page-break-before: always;}
}
</style>
 <head>

  <title></title>
 </head>
  <?php
                                  $siswaT = $datasiswa->result();
                                  ?>
<?php
function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('/', $tanggal);
  
  // variabel pecahkan 0 = tanggal
  // variabel pecahkan 1 = bulan
  // variabel pecahkan 2 = tahun
 
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}?>
 <body bgcolor="white">

  <table width="100%">
  
        <tr>
          <td><div align="center" ><b><img src="<?php echo base_url();?>assets/images/logo_kemenag.png" width="150px" /></b></div></td>
        </tr>
        <tr>
          <td width="100%">
            <div align="center" style="font-size: 14pt;"><b><br/>LAPORAN <BR></b></div>
          </td>
          <td rowspan="5" width="25%">&nbsp;</td>
        </tr>
        <tr>
          <td><div align="center" style="font-size: 14pt;"><b>PERKEMBANGAN ANAK DIDIK</b><BR></div></td>
        </tr>
        <tr>
          <td><div align="center" style="font-size: 14pt;"><b>RAUDHATUL ATHFAL</b></div></td>
           
        </tr>
         <tr>
          <td><div align="center" style="font-size: 14pt;"><BR><b>(RA)</b><br><br></div></td>
        </tr>
         <tr>
          <td><div align="center" ><b>
             <img  src="
                               <?php echo base_url('uploads/'.$siswaT[0]->foto);?>
                            "  width="150px" ></b></div></td>
        </tr>
        <tr>
          <td><div align="center" style="font-size: 14pt;"><b><br><br>Nama Peserta Didik:</b><BR></div></td>
        </tr>
         <tr>
          <td><div align="center" style="font-size: 14pt;"><b><?php echo ucwords(($siswaT[0]->nama_siswa))?></b><BR></div></td>
        </tr>
        <tr>
          <td><div align="center" style="font-size: 14pt;"><b><br>NIS:</b><BR></div></td>
        </tr>
         <tr>
          <td><div align="center" style="font-size: 14pt;"><b><?php echo ucwords(($siswaT[0]->no_induk))?></b><BR></div></td>
        </tr>
          <tr>
          <td><div align="center" style="font-size: 14pt;"><BR><BR>RAUDHATUL ATHFAL <?php echo ucwords(($siswaT[0]->nama_sekolah))?> </b><BR></div></td>
        </tr>
            <tr>
          <td><div align="center" style="font-size: 14pt;">KEMENTRIAN AGAMA REPUBLIK INDONESIA </b><BR></div></td>
        </tr>
         <tr>
          <td><div align="center" style="font-size: 14pt;">PROVINSI <?php echo strtoupper($siswaT[0]->nama_provinsi)?> </b><BR></div></td>
        </tr>
        </table><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
          <table width="100%" class="footer">
        <tr>
        <td><div align="center" style="font-size: 18pt;"><b> LAPORAN PERKEMBANGAN ANAK DIDIK
        </b></div></td>
        </tr>
        <tr><td><div align="center" style="font-size: 18pt;"><b> RAUDHATUL ATHFAL</b></div></td>
        </tr>
        <tr><td><div align="center" style="font-size: 18pt;"><b> RA</b></div></td>
        </tr>
        </table>

        <table>
         <tr>
          <td width="35%" style="font-size: 15pt;"><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama RA </td>
          <td width="2%"><br><br>:&nbsp;</td>
          <td width="75%" style="font-size: 15pt;"><br><br><?php echo ucwords(($siswaT[0]->nama_sekolah))?></td>
        </tr>

         <tr>
          <td width="35%" style="font-size: 15pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NSM </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 15pt;"><?php echo ucwords(($siswaT[0]->NSN))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 15pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alamat </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 15pt;"><?php echo ucwords(($siswaT[0]->jln))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 15pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desa/Kelurahan </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 15pt;"><?php echo ucwords(($siswaT[0]->kel))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 15pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kecamatan </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 15pt;"><?php echo ucwords(($siswaT[0]->kec))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 15pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kota/Kabupaten </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 15pt;"><?php echo ucwords(($siswaT[0]->kotakab))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 15pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Provinsi </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 15pt;"><?php echo ucwords(($siswaT[0]->nama_provinsi))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 15pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Website </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 15pt;"><?php echo ucwords(($siswaT[0]->website))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 15pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 15pt;"><?php echo ucwords(($siswaT[0]->email))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 15pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Telepon</td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 15pt;"><?php echo ucwords(($siswaT[0]->telepon))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 15pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kepala Ra</td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 15pt;"><?php echo ucwords(($siswaT[0]->kepala_RA))?></td>
        </tr>
        </table><!-- <br><br>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br> -->
       
          <table width="100%" class="footer">
        <tr>
        <td><div align="center" style="font-size: 18pt;" ><b>IDENTITAS PESERTA DIDIK
        </b></div></td>
        </tr>
        </table>

        <table>
         <tr>
          <td width="45%" style="font-size: 14pt;"><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Peserta Didik </td>
         
        </tr>
         <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Lengkap </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->nama_siswa))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Panggilan </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->nama_panggilan))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No induk </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->no_induk))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tempat, Tanggal Lahir</td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->tempat_lahir)?>,<?php echo ucwords($siswaT[0]->tanggal_lahir)?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jenis Kelamin </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php if($siswaT[0]->jenis_kelamin=="P"){echo ucwords('perempuan ');}
          else{
            echo "Laki-laki";
          }?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agama </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->agama))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Anak Ke </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->anak_ke))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Orangtua </td>
         
        </tr>
        <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama ayah</td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->nama_ayah))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pekerjaan</td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->pekerjaan_ayah))?></td>
        </tr>
         
         <!-- <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Telepon</td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->pekerjaan_ayah))?><br></td>
        </tr> -->
        <tr>

          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Ibu</td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->nama_ibu))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pekerjaan</td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->pekerjaan_ibu))?></td>
        </tr>
         <tr>
            <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alamat Orangrtua </td>
            
          </tr>
        <tr>

          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jalan </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->jln)?></td>
        </tr>
        <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kelurahan/Desa </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->kel)?></td>
        </tr>
         <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kecamatan </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->kec)?></td>
        </tr>
         <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Provinsi </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->provinsi)?></td>
        </tr>
         <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kota/Kabupaten </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->kotakab)?></td>
        </tr>
         <!-- <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Telepon</td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->pekerjaan_ayah))?></td>
        </tr> -->
         <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wali Peserta Didik </td>
         
        </tr>
        <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Wali</td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->nama_wali))?></td>
        </tr>
        <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pekerjaan</td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->pekerjaan_wali))?></td>
        </tr>
         <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alamat</td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->alamat_wali))?></td>
        </tr>
         <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Telepon</td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->telepon_wali))?><br></td>
        </tr>
        </table>
        <table>
            <table width="100%">
               <tr>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
          <td rowspan="5" width="20%">
             <img  src="
                               <?php echo base_url('uploads/siswa/'.$siswaT[0]->pic);?>
                            "  width="150px" style="margin: 50PX 30px 30px 20px">
            
          </td>
          <td width="60%">
            <br>
             <div align="right" style="font-size: 12pt;"><b><?php echo $this->model_m->cek_kota(($siswaT[0]->id_kotaKab))?>, <?php echo tgl_indo(date('Y/m/d'));?></b></div>
            <div align="right" style="font-size: 12pt;"><b>Kepala Raudhatul Athfal</b></div>
          </td>
          <td rowspan="5" width="25%">&nbsp;</td>
        </tr>
        <tr>
          <td></td>
        </tr>
        <tr>
          <td><div align="center" style="font-size: 12pt;"><br> </div></td>
        </tr>
        <tr>
          <td><div align="center" style="font-size: 10pt;"></div></td>
        </tr>
        <tr>
          <td><div align="center" style="font-size: 10pt;"></div></td>
        </tr>
        <tr><td><div align="center" style="font-size: 10pt;"><td align="right">&nbsp;(<?php echo $siswaT[0]->kepala_RA?>)</td>
            </div></td>
        </tr>
        <tr>
         <!--  <td> <div align="center" style="font-size: 10pt;">
             <td align="right">&nbsp;NIP:093743276476</td>
            </div></td> -->
        </tr>
        </table>
        <tr>
          <td colspan="3">&nbsp;</td>
        </tr>
        </table>
        <br><br><br><br><br><br><br><br><br>
        <div class="footer">
        </div>
        <table width="100%" class="">
           <hr size="2px" color="black">
           <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->nama_siswa))?></td>
        </tr>
         <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIS </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->no_induk))?></td>
        </tr>
        </table>
        <table width="100%" >
           <hr size="2px" color="black">
        </table>
         <table width="100%">
        <tr>
        <td><div align="center" style="font-size: 18pt;"><b><br> LAPORAN 
        </b></div></td>
        </tr>
        <tr><td><div align="center" style="font-size: 18pt;"><b>PERKEMBANGAN ANAK DIDIK</b></div></td>
        </tr>
        </table>
        <?php
                   $no = 'A';
                  foreach($program->result() as $b){
                   $na= $this->model_m->perkembangan($siswaT[0]->id_siswa,$b->id_program,$this->uri->segment(4));
                             $row = $na->row();
                    if(isset($row)) {
                                foreach ($na->result() as $n ) {
                           
                        ?>    
        <table width="100%">
          <tr>
        <td><div align="left" style="font-size: 16pt;"><b><br> <?php echo $no++ ?> <?php echo $b->nama_program?>
        </b></div></td>
        </tr>
        </table><br>
        <table border="1" style=" margin-left: 56px;">
        <tr><td style="width: 890px;"><center><?php echo $b->nama_program?></center></td></tr>
        <tr><td style="width: 890px; height: 250px;"><center><?php    
                             
                                echo $n->deskripsi;
                          ?>
                         </center></td></tr>
        </table>
          <?php  
        }
           } else {?>
            <table width="100%">
          <tr>
        <td><div align="left" style="font-size: 16pt;"><b><br> <?php echo $no++ ?> <?php echo $b->nama_program?>
        </b></div></td>
        </tr>
        </table><br>
        <table border="1" style=" margin-left: 56px;">
        <tr><td style="width: 890px;"><center><?php echo $b->nama_program?></center></td></tr>
        <tr><td style="width: 890px; height: 250px;"><center>
                         </center></td></tr>
        </table>
         <?php  }
         } ?>
      <!--   <table width="100%">
           <tr>
        <td><div align="left" style="font-size: 16pt;"><b><br> G. Ketidakhadiran 
        </b></div></td>
        </tr>
        </table>
        <table border="1" style="margin-left: 56px;">
        <tr>
        <td style="width: 50px;">Sakit</td>
        <td style="width: 390px;"><center>: ... hari</center></td>
        </tr>
        <tr>
        <td style="width: 50px;">Izin</td>
        <td style="width: 390px;"><center>: ... hari</center></td>
         </tr>
        <tr>
        <td style="width: 50px;">Tanpa Keterangan</td>
        <td style="width: 390px;"><center>: ... hari</center></td>
        </tr>
        </table> -->
        <div class="footer">
        </div>
        <table width="100%">
           <tr>
        <td><div align="left" style="font-size: 16pt;"><b><br> G. Catatan Guru Kelas 
        </b></div></td>
        </tr>
        </table>
         <table border="1" style=" margin-left: 56px;">
       
        <tr><td style="width: 890px; height: 250px;"><center></center></td></tr>
        </table>
         <table width="100%">
           <tr>
        <td><div align="left" style="font-size: 16pt;"><b><br> H. Tanggapan Orangtua/ Wali
        </b></div></td>
        </tr>
        </table>
         <table border="1" style=" margin-left: 56px;">
       
        <tr><td style="width: 890px; height: 250px;"><center></center></td></tr>
        </table>

<br>
       
        <table width="100%">
          <tr>
              <td>
                
              </td>
              <td>
                 <div align="right" style="font-size: 12pt;"><b><?php echo $this->model_m->cek_kota(($siswaT[0]->id_kotaKab))?>, <?php echo tgl_indo(date('Y/m/d'));?></b></div>
              </td>
          </tr>
          <tr>
              <td>
                <div align="left" style="font-size: 12pt;"><b>Orangtua/Wali</b></div>
              </td>
              <td>
                <div align="right" style="font-size: 12pt;"><b>Guru Kelas</b></div>
              </td>
          </tr>
          <tr>
              <td>
                <br>
              </td>
              <td>
                <br>
              </td>
          </tr>
          <tr>
              <td>
                <br>
              </td>
              <td>
                <br>
              </td>
          </tr>
          <tr>
              <td>
                 <div align="left" style="font-size: 12pt;"><b><?php echo ucwords(($siswaT[0]->nama_ayah))?></div>
              </td>
              <td>
                <div align="right" style="font-size: 12pt;">(<?php $g=$this->model_m->selectX('guru','id_guru='.$siswaT[0]->id_guru)->row();
                echo $g->nama_guru?>)</div>
              </td>
          </tr>
          <tr>
              <td>
                <hr width="80%" align="left" size="1px" color="black">
              </td>
              <td>
                <hr width="50%" align="right" size="1px" color="black">
              </td>
          </tr>
          <tr>
              <td>
                <br>
              </td>
              <!-- <td>
                <div align="right" style="font-size: 12pt;">NIP:093743276476</div>
              </td> -->
          </tr>
          <tr>
            <td colspan="2">
              <br>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <br>
            </td>
          </tr>
          <tr>
            <td colspan="2">
               <div align="center" style="font-size: 12pt;">MENGETAHUI</div>
            </td>
          </tr>
           <tr>
            <td colspan="2">
              <div align="center" style="font-size: 12pt;">Kepala Madrasah</div>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <br>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <br>
            </td>
          </tr>
           <tr>
              <td colspan="2">
                <hr width="40%" align="center" size="1px" color="black">
              </td>
          </tr>
          <tr>
            <td colspan="2">
               <div align="center" style="font-size: 12pt;">(<?php echo $siswaT[0]->kepala_RA?>)</div>
            </td>
          </tr>
           <tr>
            <td colspan="2">
               <div align="center" style="font-size: 12pt;">NIP:<?php echo $siswaT[0]->nip?></div>
            </td>
          </tr>
        </table>


        


  
 </body>
 <script>

    window.print();
</script>
</html>