<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
  <script>
            function get_sub(){
                var id_tema = $("#sub").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('penjadwalan/get_sub'); ?>", 
                    data:"id_tema="+id_tema, 
                    success: function(msg) {
                            $("#div_sub").html(msg);
                    }
                });
            }
        </script>
<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Ubah kd</h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Home/dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home/kd');?>">KD</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Ubah KD</li>
                      </ol>
                    </nav>
                      <form class="forms-sample" action="<?php echo site_url('home/aksi_ubahkd/'.$this->uri->segment(3));?>" method="post" enctype="multipart/form-data">
                      <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">KI</label>
                          <div class="col-sm-9">
                             <select required name="f1" class="form-control" >
                             <?php
                             foreach ($ki as $k) {
                            ?>
                         <option value="<?php echo $k->id_KI; ?>" <?php if($data->id_KI==$k->id_KI)echo "selected"; ?>>  <?php echo $k->nama_KI; ?></option>
                         <?php
                              } ?> 
                                       </select>
                           </div>
                      </div>
                         

                       <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Kode KD</label>
                          <div class="col-sm-9">
                          <input required type="text" class="form-control" name="kode" value="<?php echo $data->kode_kd?>">
                           </div>
                       
                      </div>
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nama KD</label>
                        <div class="col-sm-9">
                       <input required type="text" class="form-control" name="nama" value="<?php echo $data->nama_kd?>">
                        </div>
                      </div>
                       
                      <input type="hidden" name="sekolah" class="form-control form-control-lg" value="<?=$this->session->userdata('id_sekolah');?>">
                     
                     

                        <button type="submit"  onclick="alert('Data Anda Disimpan')" class="btn btn-success mr-2">Submit</button>
                        <a href="<?php echo site_url('home/indikator');?>" class="btn btn-danger" value="Batal"><i class="icon-remove"></i> Batal</a>
                      </form>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>