<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
   <script>
            function get_scph(){
                var id_tahunajaran = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('report/get_scph'); ?>", 
                    data:"id_tahunajaran="+id_tahunajaran, 
                    success: function(msg) {
                            $("#div_ktd").html(msg);
                    }
                });
            }
        </script>

<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Laporan Mingguan </h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home/dashboard')?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Laporan Mingguan</li>
                      </ol>
                    </nav>
                    <div class="col-lg-12">
                        <form method="post" action="<?php echo site_url('report/search_keyword');?>">
                     <div class="form-group row">
                          <label  class="col-sm-2 col-form-label" >Tahun Ajaran </label>
                         <div class="col-sm-2">
                           <select required name="thn" class=" form-control form-control-sm" id="prov"  onchange="get_scph()" style="
                             margin-left: -60px;">
                           <option value=""> Pilih tahun ajaran </option>
                             <?php
                            foreach($thn as $r){
                            echo "<option value='".$r->id_tahunajaran."'>".$r->nama_tahun."</option>";}
                        ?> 
                         </select>
                           </div> 
                            <label for="exampleInputPassword2" class="col-form-label" style="
                                margin-left: -30px;">Siswa </label>
                            <div class="form-group row col-lg-6" id="div_ktd">
                           <div class="col-sm-5">
                             <select required name="siswa" class="form-control" id="keyword">
                         <?php
                             foreach ($siswa as $k) {
                            ?>
                         <option value="<?php echo $k->id_siswa; ?>" <?php if($sw->id_siswa==$k->id_siswa)echo "selected"; ?>>  <?php echo $k->nama_siswa; ?></option>
                         <?php
                              } ?> 
                          </select>
                        </div>
                         <label for="exampleInputPassword2" class="col-form-label" style=" margin-left: 30px;">Minggu</label>
                           <div class="col-sm-4">
                            <select required name="keyword" class="form-control" >
                              <option value="">Pilih Minggu</option>
                               <!--  <?php
                                    foreach($minggu as $k){
                                       $format=date('d F Y', strtotime($k->tanggal_awal));
                                      echo "<option value=".$k->tanggal_awal."'>".$format=date('F', strtotime($k->tanggal_awal)).',Minggu Ke-'.$k->mingguke."</option>";}
                                ?> -->
                          </select>
                       
                          </select>
                        </div></div>
                         <span class="input-group-btn"  >
                                <button class="btn btn-primary btn-sm" type="submit">Cari
                                </button>
                            </span>
                      </div>

                         
                        </form>

                         <?php
                                  $data = $dt->result();
                                  ?>
                                   <div class="form-group row">
                       <label class="col-sm-2 col-form-label">Nama siswa</label>
                          <div class="col-sm-4 col-form-label">: <?php echo $data[0]->nama_siswa?></div>
                          <label class="col-sm-2 col-form-label">Kelompok</label>
                          <div class="col-sm-4 col-form-label">: <?php echo $data[0]->nama_kelas?></div>
                      </div>
                      <div class="form-group row">
                         <?php
                                  $data1 = $dtjdw->result();
                                  ?>
                       <label class="col-sm-2 col-form-label">Minggu Ke</label>
                      <div class="col-sm-4 col-form-label">: <?php echo $data1[0]->mingguke?></div>
                      <label class="col-sm-2 col-form-label">Tahun</label>
                      <div class="col-sm-4 col-form-label">: <?php echo $format=date('Y', strtotime($data1[0]->tanggal_awal))?></div>
                      </div>
                       <!-- <form method="post" action="<?php echo site_url('report/search_keyword');?>">
                        <div class="row">
                          <div class="col-lg-3 text-left">
                            <select name="minggu" class="form-control" id="minggu">
                              <option value="">Pilih Minggu</option>
                                <?php
                                    foreach($minggu as $k){
                                       $format=date('d F Y', strtotime($k->tanggal_awal));
                                      echo "<option value=".$k->tanggal_awal."'>".$format=date('F', strtotime($k->tanggal_awal)).',Minggu Ke-'.$k->mingguke."</option>";}
                                ?>
                          </select></div>
                           <div class="col-lg-3 text-right">
                         <select name="keyword" class="form-control" id="keyword">
                         <?php
                             foreach ($siswa as $k) {
                            ?>
                         <option value="<?php echo $k->id_siswa; ?>" <?php if($sw->id_siswa==$k->id_siswa)echo "selected"; ?>>  <?php echo $k->nama_siswa; ?></option>
                         <?php
                              } ?> 
                          </select></div>
                          <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Cari
                                </button>
                            </span>
                        </div>
                        </form> -->
                  <div class="table-responsive">
                    <br>

                         <?php 
                         
                          foreach ( $mgg->result() as $m )
                          { ?>
                            <th width="15%">
                          <?php $id[]=$m->id_perencanaan;
                          ?> </th> 
                          <?php }?>    
                       <a href="<?php echo site_url('report/mingguan_print/'.$data[0]->id_siswa.'/'.$data1[0]->mingguke);?>" class="btn btn-danger btn-sm" ><i class="icon-printer"></i> Cetak</a>
                  <table id="" class="table table-striped table-advance table-hover nowrap">
                    <thead>
                      <tr> 
                         <th>No</th> 
                          <th><center>Program</center></th>
                         <th><center>Indikator Penilaian</center></th>
                         <th><center>Cheklist</center></th>
                          <th><center>Anekdot</center></th>
                         <th><center>Hasil Karya</center></th>
                         <th><center>Capaian Akhir</center></th>
                      </tr>
                    </thead>
                 <tbody>
                        <?php
                        $no = 1;
                         // foreach($mgg->result() as $b)
                         foreach($prg as $b)
                          {?>
                        <tr>
                           <td><center> <?php echo $no++?></center></td>
                           <td> <?php echo $b->nama_program?></td>
                           <td><?php $ind = $this->model_m->ind_week($data[0]->id_siswa,$data1[0]->tanggal_awal,$b->id_program);
                                      foreach($ind->result() as $i){
                                        echo $i->nama_indikator."<br><br>";
                                      }
                           ?></td>
                           <td><?php 
                                      foreach($ind->result() as $i){
                                        if($i->nilai=='1') {
                                    echo "BB<br><br><br>";
                                   }
                                   elseif ($i->nilai=='2') {
                                     echo "MB<br><br><br>";
                                   }
                                   elseif ($i->nilai=='3') {
                                     echo "BSH<br><br><br>";
                                   }
                                   elseif ($i->nilai=='4') {
                                     echo "BSB<br><br><br>";
                                   }
                                   else{
                                    echo "-<br><br><br>";
                                   }
                                      }
                           ?></td>
                             <td><?php $tek = $this->model_m->week_tek($data[0]->id_siswa,$data1[0]->tanggal_awal,$b->id_program,4);
                                      foreach($tek->result() as $t){
                                        if($t->nilai=='1') {
                                    echo "BB<br><br><br>";
                                   }
                                   elseif ($t->nilai=='2') {
                                     echo "MB<br><br><br>";
                                   }
                                   elseif ($t->nilai=='3') {
                                     echo "BSH<br><br><br>";
                                   }
                                   elseif ($t->nilai=='4') {
                                     echo "BSB<br><br><br>";
                                   }
                                   else{
                                    echo "-<br><br><br>";
                                   }
                                       
                                      }
                           ?></td>
                           <td><?php $t2 = $this->model_m->week_tek($data[0]->id_siswa,$data1[0]->tanggal_awal,$b->id_program,5);
                                      foreach($t2->result() as $t2){
                                        if($t2->nilai=='1') {
                                    echo "BB<br><br><br>";
                                   }
                                   elseif ($t2->nilai=='2') {
                                     echo "MB<br><br><br>";
                                   }
                                   elseif ($t2->nilai=='3') {
                                     echo "BSH<br><br><br>";
                                   }
                                   elseif ($t2->nilai=='4') {
                                     echo "BSB<br><br><br>";
                                   }
                                   else{
                                    echo "-<br><br><br>";
                                   }
                                      }
                           ?></td>
                       

                              <td>  <?php 
                               $hsl=$this->model_m->hsl_week($data[0]->id_siswa,$data1[0]->tanggal_awal,$b->id_program);
                       foreach ($hsl->result() as $key ) {
                        // echo $b->kode_indikator.$b->id_jadwal;
                           if($key->nilai=='1') {
                          echo "BB";
                         }
                         elseif ($key->nilai=='2') {
                           echo "MB";
                         }
                         elseif ($key->nilai=='3') {
                           echo "<b>BSH</b>";
                         }
                         else{
                          echo "BSB";
                         }
                       }
                       ?>
                            </td>
                    
                      </tr>
                        <?php  } ?>
                               
                  </tbody>
                  </table>
                </div>
                </div>
                      
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

<?php $this->load->view('footer.php'); ?>
</body>

</html>