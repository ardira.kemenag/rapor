<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('sekolah/header.php'); ?>

<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah Guru </h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('KepalaRA');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Guru/guru');?>">Data Guru</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Guru</li>
                      </ol>
                    </nav>
                      <form class="forms-sample" action="<?php echo site_url('Guru/aksi_insert/');?>" method="post" enctype="multipart/form-data">
                        
                        <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama Guru </label>
                         <div class="col-sm-9">
                          <input type="hidden" name="sekolah" class="form-control form-control-lg" value="<?=$this->session->userdata('id_sekolah');?>">
                           <input type="text" name="f1" class="form-control form-control-lg" placeholder=" Nama Guru">
                           <input type="hidden" name="username" class="form-control form-control-lg" value="<?php echo $kode;?>">
                           <input type="hidden" name="password" class="form-control form-control-lg" value="12345">
                           <input type="hidden" name="role" class="form-control form-control-lg" value="1">
                           <input type="hidden" name="status" class="form-control form-control-lg" value="1">
                           </div >
                           
                      </div>

                       

                        <button type="submit"   class="btn btn-success mr-2">Submit</button>
                        <a href="<?php echo site_url('Guru/guru');?>" class="btn btn-danger" value="Batal"><i class="icon-remove"></i> Batal</a>
                      </form>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>