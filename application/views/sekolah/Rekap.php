<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			if(! $this->session->userdata('username')){
				 redirect('login');
			}
           
          }
	public function index()

	{   
		 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		$data['data']= $this->model_m->terkirim();
		$this->load->view('pusat/rekap',$data);
	}
  public function popupSK(){
	   $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
        	$where = array('id_pengawas'=>$this->input->post('fid'));
            $data['dpX'] = $this->model_m->selectX('datapengawas',$where);
        	$data['id']=$_POST['id'];
			$this->load->view('pusat/showSK',$data);
        }
 // 	public function rkp($id_sekolah,$id_tahunajaran)

	// {
	//  $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
 //    $where = array('id_sekolah' => $id_sekolah,'id_tahunajaran' => $id_tahunajaran);
 //    $data['rkp']= $this->model_m->selectX('coba_rekap',$where);
 //    $data['nama']= $this->model_m->nama($id_tahunajaran);
	//  $this->load->view('pusat/rekapbulanan',$data);
	// }
	public function rkp($semester,$id_sekolah)

	{
	 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
    $where = array('semester' => $semester,'id_sekolah' => $id_sekolah);
    $data['nama']= $this->model_m->filterrekap($id_sekolah,$semester);
    // $data['nama']= $this->model_m->nama($id_tahunajaran);
	 $this->load->view('pusat/rekapbulanan',$data);
	}
	public function popup(){

        	$id=$_POST['id'];
        	$a=$id[2];
			$b=$id[6];

       		$data['hasil']=$this->model_m->hasil2($a,$b)->result();
			$this->load->view('pusat/showrekap',$data);
        }
    public function smt(){

        	$id=$_POST['id'];
        
        	 $a=$id[2];
			 $b=$id[6];
       	 $data['hasil']=$this->model_m->hasil3($a,$b)->result();
			$this->load->view('pusat/showrekap',$data);
        }
 //  public function filterrekap($id_sekolah)

	// {
	// 	 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
	// 	$where = array('id_sekolah' => $id_sekolah );
	
 //            $data['dtRA'] = $this->model_m->detailRA($id_sekolah);
 //            $data['siswa'] = $this->model_m->listsiswa($id_sekolah);
 //             $data['tahun'] = $this->model_m->selectsemua('tahun_ajaran');
 //             $data['sekolah'] = $this->model_m->selectX('coba_rekap','id_sekolah='.$id_sekolah)->row();
	// 	$this->load->view('pusat/filterrekap',$data);
	// }
	public function filterrekap($id_sekolah)

	{
		 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();

		$where = array('id_sekolah' => $id_sekolah );
	$data['rkp']= $this->model_m->rekapdetail();
            $data['dtRA'] = $this->model_m->detailRA($id_sekolah);
            $data['siswa'] = $this->model_m->listsiswarekap($id_sekolah);
            $data['data']= $this->model_m->terkirim2($id_sekolah);
		$this->load->view('pusat/rekapdetail',$data);
	}
	
	 public function rekap($id_sekolah)

	{
		 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		$where = array('id_sekolah' => $id_sekolah );
		$id_tahunajaran= $this->input->post('thn');
		$semester= $this->input->post('smt');

            $data['dtRA'] = $this->model_m->detailRA($id_sekolah);
            $data['sekolah']= $this->model_m->selectX('coba_rekap',$where);
            // $data['siswa'] = $this->model_m->listsiswa($id_sekolah);
             $data['siswa'] = $this->model_m->filterrekap($id_tahunajaran,$semester);
             $data['tahun'] = $this->model_m->selectsemua('tahun_ajaran');
		$this->load->view('pusat/rekapsiswa',$data);
	}
	public function nilaisiswa(){
			 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
       		$id=$_POST['id'];
       		$data['hasil']=$this->model_m->hasilnilai($id)->result();
			$this->load->view('pusat/showrekapsiswa',$data);
           
            
        }
      public function listkelas(){
			 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
       		  $id=$_POST['kls'];
       		$h= json_decode($id);
       		 
		          $a= $h[0]; 
		          $b= $h[1];
       	   
       		 $data['kelas']=$this->model_m->listkls($a,$b)->result();
			$this->load->view('pusat/showkelas',$data);
           
            
        }
 public function rekapkelas($id_sekolah,$kelompok,$semester){
			 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
       		 $where = array('id_sekolah'=>$id_sekolah,'kelompok'=>$kelompok,'semester'=>$semester);
  

           $data['nama']=$this->model_m->rkpkls($id_sekolah,$kelompok,$semester);
          $this->load->view('pusat/rekapkelas',$data);
           
            
        }

	
	
}

?>
