 <!-- plugins:js -->
 
  <script src="<?php echo base_url();?>assets/js/jquery.js"></script>

  <script src="<?php echo base_url();?>assets/node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?php echo base_url();?>assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="<?php echo base_url();?>assets/node_modules/chart.js/dist/Chart.min.js"></script>
  <script src="<?php echo base_url();?>assets/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url();?>assets/node_modules/dropify/dist/js/dropify.min.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?php echo base_url();?>assets/js/off-canvas.js"></script>
  <script src="<?php echo base_url();?>assets/js/hoverable-collapse.js"></script>
  <script src="<?php echo base_url();?>assets/js/misc.js"></script>
  <script src="<?php echo base_url();?>assets/js/settings.js"></script>
  <script src="<?php echo base_url();?>assets/js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?php echo base_url();?>assets/js/dashboard.js"></script>

  <!-- Custom js for this page-->
  <script src="<?php echo base_url();?>assets/js/file-upload.js"></script>
  <script src="<?php echo base_url();?>assets/js/iCheck.js"></script>
  <script src="<?php echo base_url();?>assets/js/select2.js"></script>
  <script src="<?php echo base_url();?>assets/js/dropify.js"></script>
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
   <script type="text/javascript">
  var mytable;
  
  $(document).ready(function() {
    //datatables
    table = $('#mytable').DataTable({ 
      "iDisplayLength": 10,
      "language": {
        "url": "<?php echo base_url('assets/Indonesian.json');?>",
        "sEmptyTable": "Tidak ada data di database",
        "emptyTable": "No data available in table"
      },
      

      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ 0 ], //first column / numbering column
        "orderable": false, //set not orderable
      },
      ],

    });

  });
        </script>
  <!-- End custom js for this page-->