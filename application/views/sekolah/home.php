<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('sekolah/header.php'); ?>

<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
          <div class="row">
            <div class="col-lg-6 grid-margin">
              <div class="card chart-bg">
                  <div class="wrapper d-flex justify-content-center mt-4 mb-4">
                    <div class="icon"><i class="mdi mdi-chart-pie icon-md text-success mr-2"></i></div>
                    <div class="details">
                     <h2 class="mb-1 mt-2">Total Siswa</h2>
                      <h4> <?=$jmlsiswa?> Siswa </h4>
                    </div>
                  </div>
              </div>
            </div>
            <div class="col-lg-6 grid-margin">
              <div class="card user-bg">
                  <div class="wrapper d-flex justify-content-center mt-4 mb-4">
                    <div class="icon"><i class="mdi mdi-account-multiple icon-md text-info mr-2"></i>
                    </div>
                    <div class="details">
                     <h2 class="mb-1 mt-2">Total Guru</h2>
                      <h4> <?=$jmlguru?> Siswa </h4>
                    </div>
                </div>
              </div>
            </div>
          </div>
        <!--  <a href="<?php echo site_url('Update/update_aplication')?>"  class="btn btn-success"><i class="mdi mdi-loop"></i>Update Aplikasi</a> -->
         
        </div>
         <h4 class="card-title">Sikronisasi</h4>
               <div class="timeline">
                      <div class="timeline-wrapper timeline-wrapper-warning">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                          <div class="timeline-heading">
                            <h6 class="timeline-title">Data Guru</h6>
                          </div>
                         <!--  <div class="timeline-body">
                            <p>'Klik "'</p>
                          </div>
                          <div class="timeline-footer d-flex align-items-center">
                              <i class="mdi mdi-heart-outline text-muted mr-1"></i>
                              <span>19</span>
                              <span class="ml-auto font-weight-bold">19 Oct 2017</span>
                          </div> -->
                        </div>
                      </div>
                      <div class="timeline-wrapper timeline-inverted timeline-wrapper-danger">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                          <div class="timeline-heading">
                            <h6 class="timeline-title"> Data Akun Guru</h6>
                          </div>
                          
                        </div>
                      </div>
                      <div class="timeline-wrapper timeline-wrapper-success">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                          <div class="timeline-heading">
                            <h6 class="timeline-title"> Data Kelas/Kelompok</h6>
                          
                        </div>
                      </div>
                    </div>

                      <div class="timeline-wrapper timeline-inverted timeline-wrapper-info">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                          <div class="timeline-heading">
                            <h6 class="timeline-title">Data Tahun Ajaran</h6>
                          </div>
                        </div>
                      </div>
                      
                    </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.<?php echo VERSI_ARDIRA ?></span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>

</body>
 <script src="<?php echo base_url();?>assets/node_modules/sweetalert/dist/sweetalert.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/alerts.js"></script>
   <script type="text/javascript">
    <?php 
    if($this->session->flashdata("msg")=='oke'){?>
      swal({
              title: 'Berhasil',
              text: 'Aplikasi terupdate',
              timer: 3000,
              button: false
            })
   <?php  }?>
   </script>
</html>