<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('sekolah/header.php'); ?>

  
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">
                <div class="card-body">
                  <?php echo $this->session->flashdata('msg2');?>
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data Akun Guru</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Data Akun Guru</h4>
                  </div> 
                  <div class="col-lg-6 text-right">
                    <a  class="btn btn-light btn-sm"  href="<?php echo site_url('guru')?>">Tambah</a>
                  </div>
                <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No</th>
                         <th>Nama Guru</th>
                         <th>Username</th>
                         <th>Password</th>
                         <th>Aksi</th>
                      </tr>
                    </thead>
                    
                   <tbody>
                        <?php
                        $no = 1;
                        foreach($guru->result() as $b){
                        ?>
                        <tr>
                          <td><?php echo $no++?></td>
                          <td><?php echo $b->nama_guru?></td>
                         <td><?php echo $b->username?></td>
                         <td><?php echo $b->password?></td>
                         <td> 
                          <a  class='edit-record btn btn-primary btn-sm' data-id='<?php echo $b->id_guru?>' style="color:white;">
                            <i class='icon-pencil'></i> Ubah</a></td>
                       
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
 <div class="modal fade" id="exampleModal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
   <form action="<?php echo site_url('kepalaRA/aksi_edit/')?>" method="post" enctype="multipart/form-data">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel-2">Detail Akun</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <p></p>
                        </div>
                        <div class="modal-footer">
                          <button type="Submit" class="btn btn-success">Submit</button>
                          <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                        </div>
                      </div>
                    </div>
                  </form>
                  </div>
 <?php $this->load->view('footer.php'); ?>
  <script>
            $(function () {
                $(document).on('click', '.edit-record', function (e) {
                    e.preventDefault();
                    $("#exampleModal-2").modal('show');
                    $.post('<?php echo site_url('registrasi/edit_akun');?>',
                            {id: $(this).attr('data-id')},
                    function (html) {
                        $(".modal-body").html(html);
                    }
                    );
                });
            });
        </script>
</body>

</html>