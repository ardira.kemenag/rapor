<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('sekolah/header.php'); ?>


  
<body>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row user-profile">
            <div class="col-lg-4 side-left d-flex align-items-stretch">
              <div class="row">
                <div class="col-12 grid-margin stretch-card" style="width: 350px;">
                  <div class="card">
                    <div class="card-body avatar">
                      <h4 class="card-title">Info</h4>
                       <?php
                                  $datasiswa = $dtRA->result();
                                  ?>
                      <img  src="
                               <?php echo base_url('uploads/'.$datasiswa[0]->foto);?>
                            ">
                      <p class="name"><?php echo $datasiswa[0]->nama_sekolah?></p>
                      <p class="designation"><?php echo $datasiswa[0]->NSN?></p>
                      <a class="d-block text-center text-dark" href="#"><?php echo $datasiswa[0]->nama_kota?></a>
                      <a class="d-block text-center text-dark" href="#"><?php echo $datasiswa[0]->nama_provinsi?></a>
                    </div>
                  </div>
                </div>
              
              </div>
            </div>

            <div class="col-lg-8 side-right stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="wrapper d-block d-sm-flex align-items-center justify-content-between">
                    <h4 class="card-title mb-0">Details</h4>
                    <ul class="nav nav-tabs tab-solid tab-solid-primary mb-0" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-expanded="true">Info</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="avatar-tab" data-toggle="tab" href="#avatar" role="tab" aria-controls="avatar">Foto Lembaga</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="security-tab" data-toggle="tab" href="#security" role="tab" aria-controls="security">Edit Data</a>
                      </li>
                    </ul>
                  </div>
                  <div class="wrapper">
                    <hr>
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info">
                        <form action="" method="post">
                           
                     
                        
                       <div class="form-group row" >
                       <label  class="col-sm-5 col-form-label">Nama Madrasah</label>
                          <div class="col-sm-4">: <?php echo $datasiswa[0]->nama_sekolah?></div>
                           </div>
                         

                         <div class="form-group row">
                       <label class="col-sm-5 col-form-label">No Induk</label>
                          <div class="col-sm-4">: <?php echo $datasiswa[0]->NSN?></div>
                      </div>
                      
                     <div class="form-group2 row" >
                       <label  class="col-sm-3 col-form-label">Alamat Madrasah</label>
                      </div>
                       <div class="form-group2 row" >
                       <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Jalan</label>
                          <div class="col-sm-7">: <?php echo $datasiswa[0]->jalan?></div>
                           </div>
                          <div class="form-group2 row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Kelurahan/Desa/Dusun</label>
                          <div class="col-sm-7">: <?php echo $datasiswa[0]->kelurahan?></div>
                           </div>
                            <div class="form-group2 row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Kecamatan</label>
                          <div class="col-sm-5">: <?php echo $datasiswa[0]->kecamatan?></div>
                           </div>
                            <div class="form-group2 row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Provinsi</label>
                          <div class="col-sm-5">: <?php echo $datasiswa[0]->nama_provinsi?></div>
                           </div>
                            <div class="form-group2 row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Kota/Kabupaten</label>
                          <div class="col-sm-5" > : <?php echo $datasiswa[0]->nama_kota?></div>
                           </div>
                           <div class="form-group row">
                       <label class="col-sm-5 col-form-label">Kepala RA</label>
                          <div class="col-sm-4">: <?php echo $datasiswa[0]->kepala_RA?></div>
                      </div>
                      <div class="form-group row">
                       <label class="col-sm-5 col-form-label">NIP</label>
                          <div class="col-sm-4">: <?php echo $datasiswa[0]->nip?></div>
                      </div>
                        <div class="form-group row">
                       <label class="col-sm-5 col-form-label">Tahun Ajaran</label>
                          <div class="col-sm-4">: <?php echo $datasiswa[0]->tahun_ajaran?></div>
                      </div>
                        
               
                        
                        </form>
                      </div><!-- tab content ends -->
                      <div class="tab-pane fade" id="avatar" role="tabpanel" aria-labelledby="avatar-tab">
                        <div class="wrapper mb-5 mt-4">
                          <span class="badge badge-warning text-white">Note : </span>
                          <p class="d-inline ml-3 text-muted">Image size is limited to not greater than 1MB .</p>
                        </div>
                        <form action="<?php echo site_url('kepalaRA/aksi_ubahfoto/'.$RA->id_sekolah)?>" method="post" enctype="multipart/form-data">
                          <input type="file" name="foto" class="dropify" data-max-file-size="1mb" data-default-file=" <?php echo base_url('uploads/'.$datasiswa[0]->foto);?>">

                          <div class="form-group mt-5">
                            <button type="submit" class="btn btn-success mr-2">Update</button>
                            <button class="btn btn-outline-danger">Cancel</button>
                          </div>
                        </form>
                      </div>
                      <div class="tab-pane fade" id="security" role="tabpanel" aria-labelledby="security-tab">
                        <form class="forms-sample" action="<?php echo site_url('kepalaRA/aksi_ubah/'.$RA->id_sekolah);?>" method="post" enctype="multipart/form-data">
                        
                       <div class="form-group row" >
                       <label  class="col-sm-5 col-form-label"> Nama Madrasah</label>
                          <div class="col-sm-7">
                            <input type="hidden" name="sekolah" class="form-control form-control-lg" value="<?=$this->session->userdata('id_sekolah');?>">
                          <input type="text" name="f1" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->nama_sekolah?>">
                           </div>
                           </div>
                                              
                         <div class="form-group row">
                       <label class="col-sm-5 col-form-label">No Induk</label>
                          <div class="col-sm-7">
                         <input type="text" name="f2" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->NSN?>"></div>
                      </div>
                      
                     
                        <div class="form-group2 row" >
                       <label  class="col-sm-3 col-form-label">Alamat Madrasah</label>
                      </div>
                       <div class="form-group row" >
                       <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Jalan</label>
                          <div class="col-sm-7">
                          <input type="text" name="jln" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->jalan?>">
                           </div>
                           </div>
                          <div class="form-group row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Kelurahan/Desa/Dusun</label>
                          <div class="col-sm-7">
                          <input type="text" name="kel" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->kelurahan?>">
                          
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Kecamatan</label>
                          <div class="col-sm-7">
                          <input type="text" name="kec" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->kecamatan?>">
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Provinsi</label>
                          <div class="col-sm-7">
                              
                              <select name="prov" class="form-control form-control-sm" id="prov" onchange="get_kota()">
                             
                             <?php
                            foreach($prov as $r){
                        ?> 
                        <option value="<?php echo $r->id_provinsi; ?>" <?php if($RA->id_provinsi==$r->id_provinsi)echo "selected"; ?>>  <?php echo $r->nama_provinsi; ?></option>
                         <?php
                              } ?> 
                          </select>
                         
                           </div>
                           </div>
                             <div class="form-group row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Kota/Kabupaten</label>
                          <div class="col-sm-7" id="div_kota">
                          <select name="kab" class=" form-control form-control-sm">
                         
                             <?php
                            foreach($kota as $r){
                        ?> 
                        <option value="<?php echo $r->id_kotaKab; ?>" <?php if($RA->id_kotaKab==$r->id_kotaKab)echo "selected"; ?>>  <?php echo $r->nama_kota; ?></option>
                         <?php
                              } ?> 
                         </select>
                           </div>
                           </div>
                           <div class="form-group row">
                       <label class="col-sm-5 col-form-label"> Website</label>
                         <div class="col-sm-7">
                         <input type="text" name="web" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->website?>">

                          </div>
                      </div>
                      <div class="form-group row">
                       <label class="col-sm-5 col-form-label">Email</label>
                         <div class="col-sm-7">
                         <input type="text" name="email" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->email?>">

                          </div>
                      </div>
                      <div class="form-group row">
                       <label class="col-sm-5 col-form-label"> Telepon</label>
                         <div class="col-sm-7">
                         <input type="text" name="tlp" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->telepon?>">

                          </div>
                      </div>
                          
                       <div class="form-group row">
                       <label class="col-sm-5 col-form-label">Kepala RA</label>
                         <div class="col-sm-7">
                         <input type="text" name="kepRA" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->kepala_RA?>">

                          </div>
                         </div>
                          <div class="form-group row">
                       <label class="col-sm-5 col-form-label">NIP</label>
                         <div class="col-sm-7">
                         <input type="text" name="nip" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->nip?>">

                          </div>
                      </div>

                       <div class="form-group row">
                       <label class="col-sm-5 col-form-label">Tahun ajaran</label>
                         <div class="col-sm-7">
                          <select name="thn" class="form-control form-control-sm">
                             <option value=""> Pilih tahun</option>
                             <?php
                            foreach($thn->result() as $r){
                        ?> 
                        <option value="<?php echo $r->nama_tahun; ?>" <?php if($RA->tahun_ajaran==$r->nama_tahun)echo "selected"; ?>>  <?php echo $r->nama_tahun; ?></option>
                         <?php
                              } ?> 
                          </select></div>
                      </div>
                     
                          
                          <div class="form-group mt-5">
                            <button type="submit" class="btn btn-success mr-2">Update</button>
                            <button class="btn btn-outline-danger">Cancel</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018  All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
       <script>
            function get_kota(){
                var id_provinsi = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('Sekolah/get_kota'); ?>", 
                    data:"id_provinsi="+id_provinsi, 
                    success: function(msg) {
                            $("#div_kota").html(msg);
                    }
                });
            }
        </script>

 <?php $this->load->view('footer.php'); ?>
</body>

</html>