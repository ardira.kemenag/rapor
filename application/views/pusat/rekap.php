<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 
  ?>
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">
                <div class="card-body">
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home/dashboard')?>">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data Siswa</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Data Siswa</h4>
                  </div> 
                  <div class="col-lg-6 text-right">
                    <a  class="btn btn-light btn-sm"  href="<?php echo site_url('Siswa/siswatmbh')?>">Tambah</a>

                  </div>
                  <div class="col-lg-6 text-left">
                   
                  </div>
                <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No</th>
                          <th>Sekolah</th>
                          <th><center>Tahun Ajaran</center></th>
                          <th><center>Bulan</center></th>
                        <th><center>Aksi</center></th>
                      </tr>
                    </thead>
                    
                 
                   <tbody>
                        <?php

        
                        $no = 1;
                        foreach($data->result() as $b){
                          $id= json_encode([$b->id_sekolah,$b->id_tahunajaran]);
                        ?>
                        <tr>
                        <td><center> <?php echo $no++ ?></center></td>
                        <td><center> <?php echo $b->nama_sekolah?></center></td>
                        <td><center> <?php echo $b->nama_tahun ?></center></td>
                        <td><center> <?php echo $b->bulan ?></center></td>
                        <td><center>
                          <!-- <a class='btn btn-primary btn-sm' data-toggle="modal" data-target="#exampleModal" href="<?php echo site_url('Rekap/').$b->id_tahunajaran.'/'.$b->id_sekolah?>">
                            <i class='icon-eye'></i> lihat</a> -->
                             <a class='btn btn-primary btn-sm' target="_blank" href="<?php echo site_url('Rekap/rkp/').$b->id_tahunajaran.'/'.$b->id_sekolah?>"> <i class='icon-eye'></i> lihat</a>
                             <!--  <td><center><a class='btn btn-primary btn-xs' href='#' onclick='showModal($mslh->id_masalah, $mslh->id_topik)'><i class='fa fa-check'></i> Lihat</a></center> -->

                              <a href='#' class='edit-record btn btn-primary btn-sm' data-id='<?php echo $id?>'> <i class='icon-eye'></i> lihat</a></A></td>

                          </center></td>
                            
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                       
                  </table>
                   <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <table id="lookup" class="table table-bordered table-hover table-striped" style="background-color:white;">
                            <thead>
                                <tr>
                                  <th> Program</th>
                                  <th>Minggu</th>
                                </tr>
                            </thead>
                            <tbody>
                   
                            </tbody>

                        </table>
                        </div>
                        <div class="modal-footer">
                          <a class='btn btn-primary btn-sm' target="_blank" href="<?php echo site_url('Rekap/rkp/').$b->id_tahunajaran.'/'.$b->id_sekolah?>">
                            <i class='icon-eye'></i> lihat</a>
                          
                          <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
 <script>
  var id_sekolah;
  function showModal(id_sekolah, id_tahunajaran){
    $("#exampleModal").modal('show');
    // $('#keluhan').text(keluhan);
    //id_topik =tbody;
    var id_tahunajaran = id_tahunajaran;
    $.ajax({
      url : "<?php echo site_url('masalah/filter/')?>" ,
      data:"id_tahunajaran="+id_tahunajaran, 
      type: "POST",
      success: function(data)
        {
          $("#t2").html(data);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data from ajax');
        }
    })
    id_sekolah = id_sekolah;
    
  }
</script>
 <script>
            $(function () {
                $(document).on('click', '.edit-record', function (e) {
                    e.preventDefault();
                    $("#exampleModal").modal('show');
                    $.post('<?php echo site_url('Rekap/popup');?>',
                            {id: $(this).attr('data-id')},
                    function (html) {
                        $(".modal-body").html(html);
                    }
                    );
                });
            });
        </script>

</body>

</html>