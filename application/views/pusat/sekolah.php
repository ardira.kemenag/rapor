<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 
  ?>

  
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">
                <div class="card-body">
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data jadwal</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Jadwal</h4>
                  </div> 
                  <div class="col-lg-6 text-right">
                    <a  class="btn btn-light btn-sm"  href="<?php echo site_url('sekolah')?>">Tambah</a>
                  </div>
                <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No</th>
                         <th><center>Nama Sekolah</center></th>
                        <th><center>NSN</center></th>
                        <th><center>Provinsi</center></th>
                        <th><center>Kota/Kab</center></th>
                        <th><center>Kepala Sekolah</center></th>
                      </tr>
                    </thead>
                    
                   <tbody>
                        <?php
                        $no = 1;
                        foreach($skl->result() as $b){
                        ?>
                        <tr>
                          <td><center> <?php echo $no++?></center></td>
                          <td><center> <?php echo $b->nama_sekolah?></center></td>
                          <td><center> <?php echo $b->NSN ?></center></td>
                          <td><center> <?php echo $b->nama_provinsi ?></center></td>
                          <td><center> <?php echo $b->nama_kota ?></center></td>
                          <td><center> <?php echo $b->kepala_RA ?></center></td>
                       
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>