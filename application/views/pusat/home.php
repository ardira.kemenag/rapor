<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 
  ?>

<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
          
          <div class="row">
            <div class="col-lg-6 grid-margin">
              <div class="card chart-bg">
                <div class="card-body">
                  <div class="wrapper d-flex justify-content-center mt-4 mb-4">
                    <div class="icon"><i class="mdi mdi-chart-pie icon-md text-success mr-2"></i></div>
                    <div class="details">
                       <h2 class="mb-1 mt-2">Total Sekolah Terdaftar</h2>
                      <h4><?=$jmlsekolah?></h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 grid-margin">
              <div class="card user-bg">
                <div class="card-body">
                  <div class="wrapper d-flex justify-content-center mt-4 mb-4">
                    <div class="icon"><i class="mdi mdi-account-multiple icon-md text-info mr-2"></i></div>
                    <div class="details">
                     <h2 class="mb-1 mt-2">Total Sekolah Request</h2>
                      <h4><?=$jmlreq?></h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
           
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>