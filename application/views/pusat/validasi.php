<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 
  ?>

  
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">
                <div class="card-body">
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data jadwal</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Madrasah </h4>
                  </div> 
                 
                <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No</th>
                         <th><center>Nama Sekolah</center></th>
                        <th><center>NSN</center></th>
                        <th><center>Provinsi</center></th>
                        <th><center>Kota/Kab</center></th>
                        <th><center>Kepala Sekolah</center></th>
                        <th><center>Aksi</center></th>
                      </tr>
                    </thead>
                    
                   <tbody>
                        <?php
                        $no = 1;
                        foreach($skl->result() as $b){
                        ?>
                        <tr>
                          <td><center> <?php echo $no++?></center></td>
                          <td><center> <?php echo $b->nama_sekolah?></center></td>
                          <td><center> <?php echo $b->NSN ?></center></td>
                          <td><center> <?php echo $b->nama_provinsi ?></center></td>
                          <td><center> <?php echo $b->nama_kota ?></center></td>
                          <td><center> <?php echo $b->kepala_RA ?></center></td>
                           <td><center>
            

                              <a href='#' class='edit-record btn btn-primary btn-sm' data-id='<?php echo $b->id_sekolah?>'> <i class='icon-eye'></i> lihat</a></A></td>

                          </center></td>
                       
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <form action="<?php echo site_url('Registrasi/approve/')?>" method="post" enctype="multipart/form-data"> <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Detail Madrasah</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">

                        </div>
                       <div class="modal-footer">
                          
                           
                          <button type="Submit" class="btn btn-success">Submit</button>
                          <a href="<?php echo site_url('Registrasi/reject/').$b->id_sekolah?>" class="btn btn-light"> Tolak</a>
                         
                        </div>
                      </div>
                    </div>
                  </form>

                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
 <script>
            $(function () {
                $(document).on('click', '.edit-record', function (e) {
                    e.preventDefault();
                    $("#exampleModal").modal('show');
                    $.post('<?php echo site_url('Registrasi/popup');?>',
                            {id: $(this).attr('data-id')},
                    function (html) {
                        $(".modal-body").html(html);
                    }
                    );
                });
            });
        </script>
</body>

</html>