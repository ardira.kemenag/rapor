<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
  
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">

                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-6 text-left">
                  <h4 class="card-title">Jadwal</h4></div>
                   <div class="col-lg-6 text-right">
                    <a  class="btn btn-light btn-sm"  href="<?php echo site_url('penjadwalan')?>">Tambah</a>
                  </div>
                </div>
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Home/dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data jadwal</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    
                  </div> 
                 
                <div class="col-lg-12">
                    <form method="post" action="<?php echo site_url('penjadwalan/jadwalfilter');?>">
                          <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-2 col-form-label">Tahun Ajaran</label>
                          <div class="col-sm-8">
                         <select name="thn" class=" form-control form-control-sm">
                           <option value="0"> Pilih tahun ajaran </option>
                             <?php
                            foreach($thn as $r){
                            echo "<option value='".$r->id_tahunajaran."'>".$r->nama_tahun."</option>";}
                        ?> 
                         </select>
                           </div>
                     
                      
                          <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Cari
                                </button>
                            </span> </div>
                        </form>
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No</th>
                         <th><center>Hari/tanggal</center></th>
                        <th><center>Minggu Ke</center></th>
                        <th><center>Kelompok</center></th>
                        <th><center>Guru</center></th>
                        <th><center>Program</center></th>
                        <th><center>Tema</center></th>
                        <th><center>Subtema</center></th>
                        <th><center>KD</center></th>
                        <th><center>Indikator</center></th>
                        
                      </tr>
                    </thead>
                    
                 <tbody>
                        <?php
                        $no = 1;
                        foreach($jdw->result() as $b){
                        ?>
                        <tr>
                           <td><center> <?php echo $no++?></center></td>
                           <td><center> 
                       <?php
                       $dayList = array('Sunday' => 'Minggu','Monday' => 'Senin','Tuesday' => 'Selasa',
                              'Wednesday' => 'Rabu','Thursday' => 'Kamis','Friday' => 'Jumat','Saturday' => 'Sabtu');
                       $format=date('d F Y', strtotime($b->tanggal_awal));
                       $day = date('l', strtotime($b->tanggal_awal));
                       echo $dayList[$day].',';
                       echo $format;?>
                    </center></td>
                     <td><center> <?php echo $b->mingguke ?></center></td>
                     <td><center> <?php echo $b->nama_kelas ?></center></td>
                     <td><center> <?php echo $b->nama_guru ?></center></td>
                     <td><center> <?php echo $b->nama_program?></center></td>
                     <td><center> <?php echo $b->nama_tema?></center></td>
                      <td><center> <?php echo $b->nama_subtema ?></center></td>
                      <td> <?php
                               $j=json_decode($b->kd);
                               foreach ($j as $key ) {
                                  echo '('.$this->model_m->cek_kd3($key).')';
                               }
                               ?></td>
                      <td> <?php
                               $j=json_decode($b->id_indikator);
                               foreach ($j as $key ) {
                                  echo $this->model_m->cek_ind($key).'<br>';
                               }
                               ?></td> 

                      
                  
                        <!-- <td><center>
                          <a class='btn btn-primary btn-sm' href="<?php echo site_url('admin/riwayat/').$b->id_jadwal?>">
                            <i class='icon-eye'></i> Lihat</a>
                          </center></td>
                             -->
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>