<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Rapor</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/flag-icon-css/css/flag-icon.min.css">

   <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/perfect-scrollbar/css/perfect-scrollbar.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo_kemenag.png">
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth" style="
    background-color: background: #1e5799; /* Old browsers */
    background: -moz-linear-gradient(left, #1e5799 0%, #ce27b5 0%, #ad56a0 11%, #ad56a0 28%, #f91d42 100%, #7db9e8 100%, #f91d42 102%); /* FF3.6-15 */
    background: -webkit-linear-gradient(left, #1e5799 0%,#ce27b5 0%,#ad56a0 11%,#ad56a0 28%,#f91d42 100%,#7db9e8 100%,#f91d42 102%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to right, #1e5799 0%,#ce27b5 0%,#ad56a0 11%,#ad56a0 28%,#f91d42 100%,#7db9e8 100%,#f91d42 102%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#f91d42',GradientType=1 ); /* IE6-9 */
">
<style type="text/css">
  .count-indicator .count {
    position: absolute;
    left: 46%;
    width: 17px;
    height: 17px;
    top: 10px;
    color: #ffffff;
    border-radius: 50px;
    text-align: center;
    line-height: 1;
    padding: 3px 7px 1px 6px;
    font-size: 0.75rem;
}
</style>
        <div class="row w-100">
          <div class="col-lg-9 mx-auto">
            <div class="auth-form-light text-left p-5" style=" background-color: white;">
              <h2>Register</h2>
                <form action="<?php echo site_url('registrasi/aksi_insert/');?>" method="post" enctype="multipart/form-data">
                  <div class="form-group row">
                       <label style="padding-top: 20px;" class="col-sm-3 col-form-label">Nama RA</label>
                          <div class="col-sm-9"><input type="text" name="namaS" class="form-control form-control-lg"></div>
                      </div>
                    <div class="form-group row">
                       <label style="padding-top: 20px;" class="col-sm-3 col-form-label">NSN</label>
                          <div class="col-sm-9"><input type="text" name="nsn" class="form-control form-control-lg" required></div>
                      </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Alamat</label>
                  </div>
                   <div class="form-group row" >
                       <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Jalan</label>
                          <div class="col-sm-9">
                          <input type="text" name="jln" class="form-control form-control-lg" >
                           </div>
                           </div>
                          <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kelurahan/Desa/Dusun</label>
                          <div class="col-sm-9">
                          <input type="text" name="kel" class="form-control form-control-lg" >
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kecamatan</label>
                          <div class="col-sm-9">
                          <input type="text" name="kec" class="form-control form-control-lg" >
                           </div>
                           </div>
                           <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kode POS</label>
                          <div class="col-sm-9">
                          <input type="text" name="pos" class="form-control form-control-lg" >
                           </div>
                           </div>
                           <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Provinsi</label>
                          <div class="col-sm-9">
                         <select required name="prov" class=" form-control form-control-sm" id="prov" onchange="get_kota()">
                           <option value=""> Pilih Provinsi</option>
                             <?php
                            foreach($prov as $r){
                            echo "<option value='".$r->id_provinsi."'>".$r->nama_provinsi.$r->id_provinsi."</option>";}
                        ?> 
                         </select>
                           </div></div>
                             <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kota/Kabupaten</label>
                          <div class="col-sm-9" id="div_kota">
                          <select required name="kab" class=" form-control form-control-sm">
                           <option> Pilih kabupaten</option>
                         </select>
                           </div>
                           </div>

                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> Web site</label>
                          <div class="col-sm-9">
                          <input type="text" name="web" class="form-control form-control-lg">
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> Telpon</label>
                          <div class="col-sm-9">
                          <input type="text" name="tlp" class="form-control form-control-lg" >
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> Email</label>
                          <div class="col-sm-9">
                          <input required type="text" name="email" class="form-control form-control-lg" >
                           </div>
                           </div>

                            <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama Kepala RA</label>
                          <div class="col-sm-9">
                           <input type="text" name="kepRA" class="form-control form-control-lg">
                           </div>
                      </div>
                        <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tahun Ajaran</label>
                          <div class="col-sm-9">
                           <input type="text" name="thn" class="form-control form-control-lg" placeholder="cth: 2018/2019">
                           </div>
                      </div>
                     
                 
                  <div class="mt-5">
                    <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium">Register</button>
                    
                  </div>
                             </form>                  
             
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
 <?php $this->load->view('footer.php'); ?>
 <script>
            function get_kota(){
                var id_provinsi = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('Registrasi/get_kota'); ?>", 
                    data:"id_provinsi="+id_provinsi, 
                    success: function(msg) {
                            $("#div_kota").html(msg);
                    }
                });
            }
        </script>
</body>

</html>
