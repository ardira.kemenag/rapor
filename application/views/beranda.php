<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" type="image/icon" href="<?php echo base_url('/assets/portal/attachment/images/favicon.png?date='.date('YmdHis')); ?>"/>
		<title>SRD Madrasahku</title>
		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/portal/css/bootstrap.min.css'); ?>"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/portal/css/font-awesome.css'); ?>"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/portal/css/bootstrap3-wysihtml5.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/portal/css/AdminLTE.min.css'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/portal/css/jquery.dataTables.min.css'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/portal/css/dataTables.bootstrap.min.css'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/portal/css/datepicker.css'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/portal/css/animate.css'); ?>" />
			
		<!-- JS -->
		<script type="text/javascript" src="<?php echo base_url('/assets/portal/js/jquery-3.1.1.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('/assets/portal/js/bootstrap.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('/assets/portal/js/app.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('/assets/portal/js/jquery.dataTables.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('/assets/portal/js/dataTables.responsive.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('/assets/portal/js/bootstrap3-wysihtml5.all.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('/assets/portal/js/bootstrap-datepicker.js'); ?>"></script>
			
		<style type="text/css">
			.blue{
				background-color:#047C9E;
				font-size:15px;
			}
			
			.navbar-brand{
				font-size:20px;
				color:#2e2e2e;
			}
			
			.navbar-brand:focus{
				font-size:20px;
				color:#f7425a;
				text-shadow: 0 0 3px #f3f3f3, 0 0 5px #f3f3f3, 0 0 7px #f3f3f3;
			}
			
			.navbar-brand:hover{
				font-size:20px;
				color:#f7425a;
				text-shadow: 0 0 3px #f3f3f3, 0 0 5px #f3f3f3, 0 0 7px #f3f3f3;
			}
			
			.kanan{
				margin-right:5%;
			}
			
			.kiri{
				margin-left:5%;
			}
			
			.nav li a{
				color:#5a5a5a;
				font-size:12pt;
				font-weight:bold;
			}
			
			.nav li a:focus{
				color:#F22E4F;
			}
			 
			.nav li a:hover{
				background-color:transparent;
				color:#F22E4F;
			}
			 
			.dropdown-menu{
				background:rgba(0,0,0,0.6);
			}
			 
			.bg1{
				background:linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('<?php echo base_url(); ?>/assets/portal/attachment/images/background/1.jpg') repeat;
				background-position:center;
				min-height:450px;
				width:100%;
			}

			.bg2{
				background:linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('<?php echo base_url(); ?>/assets/portal/attachment/images/background/2.jpg') repeat;
				background-position:center;
				min-height:450px;
				width:100%;
			}
			
			.bg3{
				background:linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('<?php echo base_url(); ?>/assets/portal/attachment/images/background/3.jpg') repeat;
				background-position:center;
				min-height:450px;
				width:100%;
			}
			
			.bg4{
				background:linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('<?php echo base_url(); ?>/assets/portal/attachment/images/background/4.jpg') repeat;
				background-position:center;
				min-height:450px;
				width:100%;
			}
			
			.bg-xs1{
				background:linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('<?php echo base_url(); ?>/assets/portal/attachment/images/background/1.jpg') repeat;
				background-position:center;
				min-height:250px;
			}
			
			.bg-xs2{
				background:linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('<?php echo base_url(); ?>/assets/portal/attachment/images/background/2.jpg') repeat;
				background-position:center;
				min-height:250px;
			}
			
			.bg-xs3{
				background:linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('<?php echo base_url(); ?>/assets/portal/attachment/images/background/3.jpg') repeat;
				background-position:center;
				min-height:250px;
			}
			
			.bg-xs4{
				background:linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('<?php echo base_url(); ?>/assets/portal/attachment/images/background/4.jpg') repeat;
				background-position:center;
				min-height:250px;
			}
			
			.nav .open>a, .nav .open>a:focus, .nav .open>a:hover
			{
				background:rgba(0,0,0,0.6);
				border:none;
			}
			
			.navbar {
				background-color: transparent;
			}
			
			.modal-content{
				background:rgba(232, 240, 252, 1);
				border-radius:0;
			}
			
			@media screen and (max-width: 765px) and (min-width: 200px) {
				.navbar-top{
					position:fixed:
					margin-top:0;
					top:0;
					z-index:999;
					left:0;
					right:0;
				}
				.navbar{
					background:rgba(0,0,0,0.5);
					position:fixed;
				}
				
				.nav li a{
					color:#fff;
					font-size:12pt;
					font-weight:bold;
				}
			}
			
			@media screen and (max-width: 1366px) and (min-width: 765px) {
				.navbar-top{
					margin-top:3%;
				}
			}
			
			.hover{
				background-color:#F22E4F;
				color:white;
				cursor: pointer;
			}
			
			.hover:hover{
				background-color:#BF2640;
				color:#f3f3f3;
			}
			
			footer {
				color:#5a5a5a;
				padding: 1em;
				clear: left;
				text-align: center;
			}
			
			.judul {
				margin-top:100px;
				margin-right:640px;
				margin-left:5%;
				padding:1% 0%;
			}
		  
			.logo {
				margin-top:-100px;
				margin-left:750px;
				padding:1% 0%;
			}
		  
			.sambutan{
				margin-top:50px;
				margin-right:640px;
				margin-left:5%;
				padding:0% 5%;
				min-width:300px;
			}
			
			.slide{
				margin-top:-200px;
				margin-left:240px;
				min-width:400px;
			}
			
			.carousel-logo .carousel-inner .active.left { left: -25%; }
			.carousel-logo .carousel-inner .next        { left:  25%; }
			.carousel-logo .carousel-inner .prev    	{ left: -25%; }
			.carousel-logo .carousel-control 			{ width:  4%; }
			.carousel-logo .carousel-control.left,.carousel-logo .carousel-control.right {margin-left:15px;background-image:none;}
		
		</style>
	</head>
	<body>
		<div class="row" style="margin:0;">
			<div class="col-md-12">
				<nav class="navbar navbar-top animated anim1 fadeIn" role="navigation">			
					<div class="navbar-header kiri">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							 <span class="sr-only">Toggle navigation</span><span style="color:#2e2e2e;" class="fa fa-reorder"></span><span class="icon-bar"></span>
						</button>
						<div class="hidden-xs hidden-sm hidden-md animated anim1 fadeIn">
							<img class="img" style="max-width:300px;" src="<?php echo base_url('/assets/portal/attachment/images/logocbts_login.png'); ?>">
						</div>
						<div class="visible-xs animated anim1 fadeIn" style="width:150px;">
							<img class="img" style="padding-top:10px; width:150px;" src="<?php echo base_url('/assets/portal/attachment/images/logocbts_login.png'); ?>">
						</div>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav kanan navbar-right">
							<li>
								<a <?php if($this->uri->segment(1) == 'beranda') { echo 'style="color:#F22E4F;"'; } ?> href="<?php echo site_url('beranda'); ?>"> <span class="fa fa-home"></span> Beranda</a>
							</li>
							<li>
								<a <?php if($this->uri->segment(1) == 'info') { echo 'style="color:#F22E4F;"'; } ?> href="<?php echo site_url('info'); ?>">Info</a>
							</li>
							<li>
								<a <?php if($this->uri->segment(1) == 'rekapitulasi') { echo 'style="color:#F22E4F;"'; } ?> href="<?php echo site_url('rekapitulasi'); ?>">Rekapitulasi</a>
							</li>
							<li>
								<a data-toggle="modal" data-target=".bs-example-modal-sm" href="#">Login</a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
			<div class="col-md-12 visible-xs visible-sm" style="background-color:#f7f7f7; padding:4% 10%; margin:50px 0px;">
				<div class="row">
					<div class="col-xs-12 col-sm-6 animated anim2 fadeIn" style="padding:10px 20px;">
						<div class="row bg-xs1">
							<a href="#">
								<div class="center-block hover animated anim6 fadeInUp" style="width:100%; height:75px; margin-top:50%;">
									<div class="col-xs-5">
										<i class="fa fa-home" style="padding-top:8px; font-size:40pt;"></i>
									</div>
									<div class="col-xs-7 text-right">
										<label style="font-size:12pt; cursor:pointer; padding-top:10px;">Raudatul<br/>Athfal</label>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 animated anim3 fadeIn" style="padding:10px 20px;">
						<div class="row bg-xs2">
							<a href="#">
								<div class="center-block hover animated anim6 fadeInUp" style="width:100%; height:75px; margin-top:50%;">
									<div class="col-xs-5">
										<i class="fa fa-home" style="padding-top:8px; font-size:40pt;"></i>
									</div>
									<div class="col-xs-7 text-right">
										<label style="font-size:12pt; cursor:pointer; padding-top:10px;">Madrasah<br/>Ibtidaiyah</label>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 animated anim4 fadeIn" style="padding:10px 20px;">
						<div class="row bg-xs3">
							<a href="#">
								<div class="center-block hover animated anim6 fadeInUp" style="width:100%; height:75px; margin-top:50%;">
									<div class="col-xs-5">
										<i class="fa fa-home" style="padding-top:8px; font-size:40pt;"></i>
									</div>
									<div class="col-xs-7 text-right">
										<label style="font-size:12pt; cursor:pointer; padding-top:10px;">Madrasah<br/>Tsanawiyah</label>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 animated anim5 fadeIn" style="padding:10px 20px;">
						<div class="row bg-xs4">
							<a href="#">
								<div class="center-block hover animated anim6 fadeInUp" style="width:100%; height:75px; margin-top:50%;">
									<div class="col-xs-5">
										<i class="fa fa-home" style="padding-top:8px; font-size:40pt;"></i>
									</div>
									<div class="col-xs-7 text-right">
										<label style="font-size:12pt; cursor:pointer; padding-top:10px;">Madrasah<br/>Aliyah</label>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 hidden-xs hidden-sm" style="background-color:#f7f7f7; padding:4% 20%;">
				<div class="row">
					<div class="col-md-3 col-sm-6 animated anim2 fadeIn" style="padding:0px 5px;">
						<div class="row bg1">
							<a href="#">
								<div class="center-block hover animated anim6 fadeInUp" style="width:100%; height:75px; margin-top:150%;">
									<div class="col-md-5">
										<i class="fa fa-home" style="padding-top:8px; font-size:40pt;"></i>
									</div>
									<div class="col-md-7 text-right">
										<label style="font-size:12pt; cursor:pointer; padding-top:10px;">Raudatul<br/>Athfal</label>
									</div>
								</div>
							</a>
						</div>
					</div>
					
					
					
				</div>
			</div>
			<footer>
				<label>Copyright &copy; SRD Madrasahku <?php echo date('Y'); ?>.</label>
			</footer>
		</div>
		
		<div class="modal fade bs-example-modal-sm"" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header" style="background-color:#6164C1;">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<span class="modal-title" id="exampleModalLabel">
							<img class="img" src="<?php echo base_url('/assets/portal/attachment/images/logocbts.png'); ?>"><label class="glow-white" style="font-size:20px; margin:5px 0px 0px 10px;">| Login</label>
						</span>
					</div>
					<form action="<?php echo site_url('home/verifikasi'); ?>" method="post">
						<div class="modal-body">
							<div class="form-group">
								<label for="recipient-name" class="control-label">User</label>
								<input type="text" class="form-control" name="username" placeholder="User">
							</div>
							<div class="form-group">
								<label for="message-text" class="control-label">Password</label>
								<input type="password" class="form-control" name="password" placeholder="Password">
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Masuk</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
	
	<script type="text/javascript">
		$(document).ready(function () {
			$('#myCarousel').carousel({
				interval: 3000
			});

			$('.carousel-logo .item').each(function(){
				var next = $(this).next();
				if (!next.length) {
					next = $(this).siblings(':first');
				}
				next.children(':first-child').clone().appendTo($(this));
	  
				for (var i=0;i<1;i++) {
					next=next.next();
					if (!next.length) {
						next = $(this).siblings(':first');
					}
		
				next.children(':first-child').clone().appendTo($(this));
				}
			});
			
			<?php if($this->session->flashdata('alert')){ ?>
				alert('<?php echo $this->session->flashdata('alert'); ?>');
			<?php } ?>
		});
	</script>
</html>