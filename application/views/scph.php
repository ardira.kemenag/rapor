<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
    <script>
            function get_scph(){
                var id_tahunajaran = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('report/get_scph'); ?>", 
                    data:"id_tahunajaran="+id_tahunajaran, 
                    success: function(msg) {
                            $("#div_ktd").html(msg);
                    }
                });
            }
        </script>

<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Skala Capaian Perkembangan Harian </h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Skala Capaian Perkembangan Harian </li>
                      </ol>
                    </nav>
                     <div class="col-lg-12">
                  
                    <br>
                   <form method="post" action="<?php echo site_url('report/cari_report');?>">
                     <div class="form-group row">
                          <label  class="col-sm-2 col-form-label" >Tahun Ajaran </label>
                         <div class="col-sm-2">
                           <select required name="thn" class=" form-control form-control-sm" id="prov"  onchange="get_scph()" style="
                             margin-left: -60px;">
                           <option value=""> Pilih tahun ajaran </option>
                             <?php
                            foreach($thn as $r){
                            echo "<option value='".$r->id_tahunajaran."'>".$r->nama_tahun."</option>";}
                        ?> 
                         </select>
                           </div> 
                            <label for="exampleInputPassword2" class="col-form-label" style="
                                margin-left: -30px;">Siswa </label>
                            <div class="form-group row col-lg-6" id="div_ktd">
                           <div class="col-sm-5">
                            <select name="siswa" class="form-control form-control-sm">
                         <?php
                             foreach ($siswa as $k) {
                            ?>
                         <option value="<?php echo $k->id_siswa; ?>" <?php if($sw->id_siswa==$k->id_siswa)echo "selected"; ?>>  <?php echo $k->nama_siswa; ?></option>
                         <?php
                              } ?> 
                          </select>
                        </div>
                         <label for="exampleInputPassword2" class="col-form-label" style=" margin-left: 30px;">Minggu</label>
                           <div class="col-sm-4">
                           <select required name="keyword" class="form-control" id="keyword">
                              <option value="">Pilih Minggu</option>
                          </select>
                          </select>
                        </div></div>
                         <span class="input-group-btn"  >
                                <button class="btn btn-primary btn-sm" type="submit">Cari</button>
                            </span>
                      </div>
                        </form>
                      
                    <?php 
                          foreach ( $jdw->result() as $m )
                          { ?>
                            <th width="15%">
                          <?php  $id[]=$m->id_perencanaan;?> </th> 
                          <?php }?> 
                    <div class="col-lg-12">
                      <?php
                                  $data = $dt->result();
                                  ?>
                                   <div class="form-group row">
                       <label class="col-sm-2 col-form-label">Nama siswa</label>
                          <div class="col-sm-4 col-form-label">: <?php echo $data[0]->nama_siswa?></div>
                          <label class="col-sm-2 col-form-label">Kelompok</label>
                          <div class="col-sm-4 col-form-label">: <?php echo $data[0]->nama_kelas?></div>
                      </div>
                      <?php
                                  $data1 = $dtjdw->result();
                                  ?>
                      <div class="form-group row">
                       <label class="col-sm-2 col-form-label">Minggu Ke</label>
                      <div class="col-sm-4 col-form-label">: <?php echo $data1[0]->mingguke?></div>
                      <label class="col-sm-2 col-form-label">Bulan</label>
                      <div class="col-sm-4 col-form-label">: <?php echo $format=date('F', strtotime($data1[0]->tanggal_awal))?></div>
                      </div>
                       
                  <div class="table-responsive">
                    <br>
                     <a href="<?php echo site_url('report/scph_print/'.$data[0]->id_siswa.'/'.$data1[0]->mingguke);?>" class="btn btn-danger btn-sm" ><i class="icon-printer"></i> Cetak</a>
                  <table id="" class="table table-striped table-advance table-hover nowrap">
                    <thead>
                      <tr> 
                         <th>No</th>
                         <th><center>Indikator Penilaian</center></th>
                             <th><center>Teknik Penilaian</center></th>
                         <?php 
                         $awal=$hari->tanggal_awal;
                         $akhir=$hari->tanggal_akhir;
                         $begin   = new DateTime($awal);
                         $end  = new DateTime($akhir);
                        $interval = DateInterval::createFromDateString('1 day');
                        $period = new DatePeriod($begin, $interval, $end);
                          foreach ( $period as $dt )
                          { ?><th width="15%">
                          <?php echo $dt->format("Y-m-d");?> </th> 
                          <?php }?>    
                       <th width="15%"><center><?php echo $akhir;?></center></th>
                        <th width="15%"><center>Capaian Akhir</center></th>
                      </tr>
                    </thead>
                 <tbody>
                        <?php
                        $no = 1;

                         foreach($jdw->result()as $b){?>
                        <tr>
                           <td><center> <?php echo $no++?></center></td>
                           <td><?php echo $b->nama_indikator?></td>
                           <td><?php echo $b->nama_teknik?></td>
                        <?php  foreach($period as $c){?>
                        <?php $tanggal_nilai= $c->format('Y-m-d');?>
                          <td><a href="<?php echo site_url('penilaian/ubahnilai/').$b->kode_indikator.'/'.$b->id_siswa.'/'.$tanggal_nilai ?>">
                           <input type="hidden" value="<?php echo $c->format('Y-m-d').'/'.$b->kode_indikator.'/'.$b->id_siswa; ?>">
                          <?php 
                           $nilai=$this->model_m->rekaphari($c->format('Y-m-d'),$b->id_siswa,$b->kode_indikator);
                           foreach ($nilai->result() as $key ) {
                         if($key->nilai_perkembangan=='1') {
                          echo "BB";
                         }
                         elseif ($key->nilai_perkembangan=='2') {
                           echo "MB";
                         }
                         elseif ($key->nilai_perkembangan=='3') {
                           echo "BSH";
                         }
                         else{
                          echo "BSB";
                         }?>
                          <a  onClick='return konfirmasi();' href="<?php echo site_url('penilaian/hapus_nilai/').$key->id_nilai?>">
                            <i class='icon-trash'></i></a>
                          <?php }
                           ?>
                            </a></td> 
                      <?php } ?>
                          <td><a href="<?php echo site_url('penilaian/ubahnilai/').$b->kode_indikator.'/'.$b->id_siswa.'/'.$akhir ?>">
                      
                      <?php 
                       $nilai=$this->model_m->rekaphari($akhir,$b->id_siswa,$b->kode_indikator);
                       foreach ($nilai->result() as $key ) {
                           if($key->nilai_perkembangan=='1') {
                          echo "BB";
                         }
                         elseif ($key->nilai_perkembangan=='2') {
                           echo "MB";
                         }
                         elseif ($key->nilai_perkembangan=='3') {
                           echo "BSH";
                         }
                         else{
                          echo "BSB";
                         }
                       }
                       ?>
                           </a> </td> 
                              <td>
                      
                      <?php 
                       $nilai=$this->model_m->harirkp($b->id_siswa,$b->kode_indikator,$b->id_jadwal);
                       foreach ($nilai->result() as $key ) {
                           if($key->na=='1') {
                          echo "BB";

                         }
                         elseif ($key->na=='2') {
                           echo "MB";
                         }
                         elseif ($key->na=='3') {
                           echo "<b>BSH</b>";
                         }
                         else{
                          echo "BSB";
                         }
                       }
                       ?>
                            </td> 


                            
                      </tr>
                        <?php  } ?>
                               
                  </tbody>
                  </table>
                </div>
                </div>
                      
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

<?php $this->load->view('footer.php'); ?>
</body>
 <script type="text/javascript" language="JavaScript">
 function konfirmasi()
 {
 tanya = confirm("Anda Yakin Menghapus Nilai ini ?");
 if (tanya == true) return true;
 else return false;
 }</script>


</html>