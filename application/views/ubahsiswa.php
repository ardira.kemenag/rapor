<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
  
<body>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row user-profile">
            <div class="col-lg-4 side-left d-flex align-items-stretch">
              <div class="row">
                <div class="col-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body avatar">
                      <h4 class="card-title">Info</h4>
                       <?php
                                  $datasiswa = $dtsiswa->result();
                                  ?>
                      <img  src="
                               <?php echo base_url('uploads/'.$datasiswa[0]->foto);?>
                            ">
                      <p class="name"><?php echo $datasiswa[0]->nama_siswa?></p>
                      <p class="designation"><?php echo $datasiswa[0]->jenis_kelamin?></p>
                      <a class="d-block text-center text-dark" href="#"><?php echo $datasiswa[0]->nama_kelas?></a>
                      <a class="d-block text-center text-dark" href="#"><?php echo $datasiswa[0]->no_induk?></a>
                    </div>
                  </div>
                </div>
                <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body overview">
                      <ul class="achivements">
                        <li><p>34</p><p>Projects</p></li>
                        <li><p>23</p><p>Task</p></li>
                        <li><p>29</p><p>Completed</p></li>
                      </ul>
                      <div class="wrapper about-user">
                        <h4 class="card-title mt-4 mb-3">About</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam consectetur ex quod.</p>
                      </div>
                      <div class="info-links">
                        <a class="website" href="http://www.bootstrapdash.com/">
                          <i class="mdi mdi-earth text-gray"></i>
                          <span>http://www.bootstrapdash.com/</span>
                        </a>
                        <a class="social-link" href="#">
                          <i class="mdi mdi-facebook text-gray"></i>
                          <span>https://www.facebook.com/johndoe</span>
                        </a>
                        <a class="social-link" href="#">
                          <i class="mdi mdi-linkedin text-gray"></i>
                          <span>https://www.linkedin.com/johndoe</span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-8 side-right stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="wrapper d-block d-sm-flex align-items-center justify-content-between">
                    <h4 class="card-title mb-0">Details</h4>
                    <ul class="nav nav-tabs tab-solid tab-solid-primary mb-0" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-expanded="true">Info</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="avatar-tab" data-toggle="tab" href="#avatar" role="tab" aria-controls="avatar">Foto Anak</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="security-tab" data-toggle="tab" href="#security" role="tab" aria-controls="security">Edit Data</a>
                      </li>
                    </ul>
                  </div>
                  <div class="wrapper">
                    <hr>
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info">
                        <form action="" method="post">
                          <div class="form-group2 row" >
                       <label  class="col-sm-3 col-form-label">Nama:</label><br>

                      </div>
                                              <div class="form-group row" >
                       <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Nama lengkap</label>
                          <div class="col-sm-7">
                          <input type="text" name="namaL" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->nama_siswa?>">
                           </div>
                           </div>
                                                <div class="form-group row" >
                       <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Nama Panggilan</label>
                          <div class="col-sm-7">
                          <input type="text" name="namaP" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->nama_panggilan?>">
                           </div>
                           </div>

                         <div class="form-group row">
                       <label class="col-sm-5 col-form-label">No Induk</label>
                          <div class="col-sm-7">
                         <input type="text" name="noinduk" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->no_induk?>"></div>
                      </div>
                       <div class="form-group row">
                       <label class="col-sm-5 col-form-label">Tempat, Tanggal Lahir</label>
                           <div class="col-sm-3">
                         <input type="text" name="tlahir" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->tempat_lahir?>"></div>
                            <div class="col-sm-4">
                         <input type="date" name="tgllahir" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->tanggal_lahir?>"></div>
                      </div>
                     <div class="form-group row">
                       <label class="col-sm-5 col-form-label">Agama</label>
                           <div class="col-sm-7">
                         <input type="text" name="agama" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->agama?>"></div>
                      </div>
                      <div class="form-group row">
                       <label class="col-sm-5 col-form-label">Jenis Kelamin</label>
                         <div class="col-sm-7">
                          <select name="jk" class="form-control form-control-sm">
                            <option value="Laki-Laki"> Laki-Laki</option>
                            <option value="Perempuan"> Perempuan</option>
                          </select></div>
                      </div>
                       <div class="form-group row">
                       <label class="col-sm-5 col-form-label">Status Anak</label>
                         <div class="col-sm-7">
                          <select name="status" class="form-control form-control-sm">
                            <option value="Kandung"> Anak Kandung</option>
                            <option value="Angkat"> Anak Angkat</option>
                          </select></div>
                      </div>
                      <div class="form-group row">
                       <label class="col-sm-5 col-form-label">Anak Ke-</label>
                           <div class="col-sm-7">
                         <input type="text" name="anakke" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->anak_ke?>"></div>
                      </div>
                         <div class="form-group2 row" >
                       <label  class="col-sm-3 col-form-label">Nama Orang Tua:</label><br>

                      </div>
                      <div class="form-group row" >
                       <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Ayah</label>
                          <div class="col-sm-7">
                          <input type="text" name="ayah" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->nama_ayah?>" >
                           </div>
                           </div>
                      <div class="form-group row" >
                       <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Ibu</label>
                          <div class="col-sm-7">
                          <input type="text" name="ibu" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->nama_ibu?>">
                           </div>
                           </div>
                               <div class="form-group2 row" >
                       <label  class="col-sm-4 col-form-label">Pekerjaan Orang Tua:</label><br>

                      </div>
                      <div class="form-group row" >
                       <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Ayah</label>
                          <div class="col-sm-7">
                          <input type="text" name="payah" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->pekerjaan_ayah?>">
                           </div>
                           </div>
                      <div class="form-group row" >
                       <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Ibu</label>
                          <div class="col-sm-7">
                          <input type="text" name="pibu" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->pekerjaan_ibu?>">
                           </div>
                           </div>
                     
                        <div class="form-group2 row" >
                       <label  class="col-sm-3 col-form-label">Alamat Orangtua</label>
                      </div>
                       <div class="form-group row" >
                       <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Jalan</label>
                          <div class="col-sm-7">
                          <input type="text" name="jln" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->jalan?>">
                           </div>
                           </div>
                          <div class="form-group row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Kelurahan/Desa/Dusun</label>
                          <div class="col-sm-7">
                          <input type="text" name="kel" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->kelurahan?>">
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Kecamatan</label>
                          <div class="col-sm-7">
                          <input type="text" name="kec" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->kecamatan?>">
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Provinsi</label>
                          <div class="col-sm-7">
                         <select name="prov" class=" form-control form-control-sm" id="prov" onchange="get_kota()">
                           <option value="0"> Pilih Provinsi</option>
                             <?php
                            foreach($prov as $r){
                            echo "<option value='".$r->id_provinsi."'>".$r->nama_provinsi."</option>";}
                        ?> 
                         </select>
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-5 col-form-label"> &emsp;&emsp;&emsp;Kota/Kabupaten</label>
                          <div class="col-sm-7" id="div_kota">
                          <select name="kab" class=" form-control form-control-sm">
                           <option> Pilih Kabupaten/Kota</option>
                         </select>
                           </div>
                           </div>
                       <div class="form-group row">
                       <label class="col-sm-5 col-form-label">Diterima Tanggal</label>
                         <div class="col-sm-7">
                         <input type="date" name="terima" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->diterima_tanggal?>">

                          </div>
                      </div>
                       <div class="form-group row">
                       <label class="col-sm-5 col-form-label">Tahun ajaran</label>
                         <div class="col-sm-7">
                          <select name="thn" class="form-control form-control-sm">
                            <option value="2017/2018"> 2017/2018</option>
                            <option value="2018/2019"> 2018/2019</option>
                          </select></div>
                      </div>
                      <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Wali Siswa</label>
                      </div>
                      
                         <div class="form-group row" >
                       <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Nama</label>
                          <div class="col-sm-9">
                          <input type="text" name="wnama" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->nama_wali?>"  >
                           </div>
                           </div>
                          <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Alamat</label>
                          <div class="col-sm-9">
                          <input type="text" name="walamat" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->alamat_wali?>" >
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Pekerjaan</label>
                          <div class="col-sm-9">
                          <input type="text" name="wkerja" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->pekerjaan_wali?>">
                           </div>
                           </div>

                          
                          <div class="form-group mt-5">
                            <button type="submit" class="btn btn-success mr-2">Update</button>
                            <button class="btn btn-outline-danger">Cancel</button>
                          </div>
                       
                        </form>
                      </div><!-- tab content ends -->
                      <div class="tab-pane fade" id="avatar" role="tabpanel" aria-labelledby="avatar-tab">
                        <div class="wrapper mb-5 mt-4">
                          <span class="badge badge-warning text-white">Note : </span>
                          <p class="d-inline ml-3 text-muted">Image size is limited to not greater than 1MB .</p>
                        </div>
                        <form action="#">
                          <input type="file" class="dropify" data-max-file-size="1mb" data-default-file="../../images/faces/face6.jpg">
                          <div class="form-group mt-5">
                            <button type="submit" class="btn btn-success mr-2">Update</button>
                            <button class="btn btn-outline-danger">Cancel</button>
                          </div>
                        </form>
                      </div>
                      <div class="tab-pane fade" id="security" role="tabpanel" aria-labelledby="security-tab">
                        <form class="forms-sample" action="<?php echo site_url('Siswa/aksi_insert/');?>" method="post" enctype="multipart/form-data">
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
       <script>
            function get_kota(){
                var id_provinsi = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('siswa/get_kota'); ?>", 
                    data:"id_provinsi="+id_provinsi, 
                    success: function(msg) {
                            $("#div_kota").html(msg);
                    }
                });
            }
        </script>

 <?php $this->load->view('footer.php'); ?>
</body>

</html>