<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
  <script>
            function get_rekap(){
                var id_siswa = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('Report/get_rekapbulan'); ?>", 
                    data:"id_siswa="+id_siswa, 
                    success: function(msg) {
                            $("#div_ktd").html(msg);
                    }
                });
            }
        </script>
<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
         <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Report Bulanan</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Report Bulanan Siswa</h4>
                  </div> 
                  <div class="col-lg-6 text-right">
                 
                  </div>
                <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                            <form method="post" action="<?php echo site_url('Penilaian/filternilai');?>">
                            <select name="keyword" class="form-control" id="keyword">
                              <option value=""></option>
                                <?php
                                    foreach($jdw as $k){
                                      echo "<option value='".$k->tanggal_awal."'>".$k->tanggal_awal."s/d".$k->tanggal_akhir."</option>";}
                                ?>
                          </select>
                          <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Cari
                                </button>
                            </span>
                        </form>
                     
                      <table id="mytable" class="table table-striped table-advance table-hover">
                        <thead>
                          <div id="div_ktd">
                            </div>
                          
                        </thead>
                      </table>
             
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>