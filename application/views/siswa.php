<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">
                  <?php echo $this->session->flashdata('msg2');?>
                <div class="card-body">
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home/dashboard')?>">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data Siswa</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Data Siswa</h4>
                  </div> 
                  <div class="col-lg-6 text-right">
                    <a  class="btn btn-light btn-sm"  href="<?php echo site_url('Siswa')?>">Tambah</a>
                  </div>
                  <div class="col-lg-6 text-left">
                    <?php
                        $no = 1;
                        foreach($kelas->result() as $b){
                        ?>
                          <a class='btn btn-primary btn-sm' href="<?php echo site_url('siswa/kelompok/').$b->id_kelas?>">
                             <?php echo $b->nama_kelas ?></a>
                                <?php
                                    }
                                ?>
                  </div>
                <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No</th>
                          <th>No Induk</th>
                         <th> Nama </th>
                        <th> Tempat lahir </th>
                        <th> Kelompok </th>
                        <th> Jenis kelamin </th>
                        <th> Aksi </th>
                      </tr>
                    </thead>
                    
                 <tbody>
                        <?php
                        $no = 1;
                        foreach($siswa->result() as $b){
                        ?>
                        <tr>
                        <td>  <?php echo $no++ ?>            </td>
                        <td>  <?php echo $b->no_induk?>      </td>
                        <td>  <?php echo $b->nama_siswa ?>   </td>
                        <td>  <?php echo $b->tempat_lahir ?> </td>
                        <td>  <?php echo $b->nama_kelas ?>   </td>
                        <td>  <?php echo $b->jenis_kelamin ?> </td>

                        <td> 
                        <a class='btn btn-primary btn-sm' href="<?php echo site_url('siswa/detail/').$b->id_siswa.'/'?>"> <i class='icon-eye'></i> Lihat</a>
                        <a class='btn btn-danger btn-sm' onClick='return konfirmasi();' href="<?php echo site_url('siswa/delete_siswa/').$b->id_siswa?>"> <i class='icon-trash'></i> Hapus</a>
                        </td>
                        
                            
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
 <script type="text/javascript" language="JavaScript">
 function konfirmasi()
 {
 tanya = confirm("Anda Yakin Menghapus Data Ini ?");
 if (tanya == true) return true;
 else return false;
 }</script>
</body>

</html>