<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
 <script>
            function get_bln(){
                var id_tahunajaran = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('report/get_bln'); ?>", 
                    data:"id_tahunajaran="+id_tahunajaran, 
                    success: function(msg) {
                            $("#div_ktd").html(msg);
                    }
                });
            }
        </script>

<body>
      <div class="main-panel">
        <div class="content-wrapper">
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Laporan Bulanan </h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home/dashboard')?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Laporan Bulanan</li>
                      </ol>
                    </nav>
                    <div class="col-lg-12">
                       <form method="post" action="<?php echo site_url('report/search_bulan');?>">
                     <div class="form-group row">
                          <label  class="col-sm-2 col-form-label" >Tahun Ajaran </label>
                         <div class="col-sm-2">
                           <select required name="thn" class=" form-control form-control-sm" id="prov"  onchange="get_bln()" style="
                             margin-left: -60px;">
                           <option value=""> Pilih tahun ajaran </option>
                             <?php
                            foreach($thn as $r){
                            echo "<option value='".$r->id_tahunajaran."'>".$r->nama_tahun."</option>";}
                        ?> 
                         </select>
                           </div> 
                            <label for="exampleInputPassword2" class="col-form-label" style="
                                margin-left: -30px;">Siswa </label>
                            <div class="form-group row col-lg-6" id="div_ktd">
                           <div class="col-sm-5">
                            <select required name="siswa" class="form-control form-control-sm">
                         <?php
                             foreach ($siswa as $k) {
                            ?>
                         <option value="<?php echo $k->id_siswa; ?>" <?php if($sw->id_siswa==$k->id_siswa)echo "selected"; ?>>  <?php echo $k->nama_siswa; ?></option>
                         <?php
                              } ?> 
                          </select>
                        </div>
                         <label for="exampleInputPassword2" class="col-form-label" style=" margin-left: 30px;">Bulan</label>
                           <div class="col-sm-4">
                           <select required name="bulan" class="form-control" id="keyword">
                              <option value="">Pilih Bulan</option>
                              
                          </select>
                       
                        </div></div>
                         <span class="input-group-btn"  >
                                <button class="btn btn-primary btn-sm" type="submit">Cari
                                </button>
                            </span>
                      </div>

                         
                        </form>
                   <?php
                                  $data = $dt->result();
                                  ?>
                                   <div class="form-group row">
                       <label class="col-sm-2 col-form-label">Nama siswa</label>
                          <div class="col-sm-4 col-form-label">: <?php echo $data[0]->nama_siswa?></div>
                          <label class="col-sm-2 col-form-label">Kelompok</label>
                          <div class="col-sm-4 col-form-label">: <?php echo $data[0]->nama_kelas?></div>
                      </div>
                      <div class="form-group row">
                          <?php
                                  $data1 = $dtjdw->result();
                                  ?>
                       <label class="col-sm-2 col-form-label">Bulan</label>
                      <div class="col-sm-4 col-form-label">: <?php echo $format=date('F', strtotime($data1[0]->tanggal_awal))?></div>
                      <label class="col-sm-2 col-form-label">Tahun</label>
                      <div class="col-sm-4 col-form-label">: <?php echo $format=date('Y', strtotime($data1[0]->tanggal_awal))?></div>
                      </div>
                    <!--   <form method="post" action="<?php echo site_url('Report/search_bulan');?>">
                        <div class="row">
                          <div class="col-lg-3 text-left">
                            <select name="keyword" class="form-control" id="keyword">
                              <option value="">pilih siswa</option>
                                <?php
                                    foreach($siswa as $k){
                                      echo "<option value='".$k->id_siswa."'>"."(".$k->no_induk.")".$k->nama_siswa."</option>";}
                                ?>
                          </select></div>
                          <div class="col-lg-3 text-right">
                            <select name="bulan" class="form-control" id="bulan" >
                              <option value="">pilih bulan</option>
                                <?php
                                    foreach($bln as $k){
                                      echo "<option value='".$k->bulan."'>".'Bulan Ke-'.$k->bulan."</option>";}
                                ?>
                          </select></div>
                          <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Cari
                                </button>
                            </span>
                        </div>
                        </form> -->

                    <div class="table-responsive">
                    <br>
                         <?php 
                          foreach ( $tgl->result() as $m )
                          { ?>
                            <th width="15%">
                          <?php  $id[]=$m->id_perencanaan;?> </th> 
                          <?php }?>    
                     <a href="<?php echo site_url('report/bulan_print/'.$data[0]->id_siswa.'/'.$data1[0]->bln);?>" class="btn btn-danger btn-sm" ><i class="icon-printer"></i> Cetak</a>
                  <table id="" class="table table-striped table-advance table-hover nowrap">
                    <thead>
                      <tr> 
                         <th>No</th>
                          <th><center>Program</center></th>
                         <th><center>KD dan Indikator Penilaian</center></th>
                          <?php 
                         $no = 1;
                          foreach ( $mgg->result() as $m )
                          { ?>
                            <th width="15%">
                          <?php $m->tanggal_awal;
                          echo 'MINGGU KE-'.$no++;
                          ;?> </th> 
                          <?php }?>
                          
                      </tr>
                    </thead>
                 <tbody>
                        <?php
                        $no = 1;
                         foreach($jdw->result() as $b){?>
                        <tr>
                           <td><center> <?php echo $no++?></center></td>
                           <td><?php echo $b->nama_program?></td>
                           
                           <td><?php   
                              $ind=$this->model_m->kdbln($id,$b->bulan,$b->id_siswa,$b->program);
                               foreach ($ind->result() as $d ) {
                                $j=json_decode($d->id_indikator);
                               foreach ($j as $key ) {
                                   echo '-'.$this->model_m->cek_ind($key)//.$this->model_m->cek_ind2($key)
                                   ;
                                   echo '<br>';
                               } }?>
                            </td>
                            <?php  foreach($mgg->result() as $c){?>
                          <td>
                               <input type="hidden" value="<?php echo $c->mingguke; ?>">
                               <input type="hidden" value="<?php echo $b->bulan; ?>"><?php 
                              $ind=$this->model_m->inbln($id,$b->bulan,$b->id_siswa,$b->program,$c->mingguke)->result();
                              // var_dump($ind);
                               foreach ($ind as $d ) {
                               //  $j=json_decode($d->id_indikator);
                               //  var_dump($j);
                               // foreach ($j as $key ) {
                                  // echo '-'.$this->model_m->cek_ind2($key);
                                   
                                   if($d->nilai=='1') {
                                    echo "BB<br><br><br>";
                                   }
                                   elseif ($d->nilai=='2') {
                                     echo "MB<br><br><br>";
                                   }
                                   elseif ($d->nilai=='3') {
                                     echo "BSH<br><br><br>";
                                   }
                                   elseif ($d->nilai=='4') {
                                     echo "BSB<br><br><br>";
                                   }
                                   else{
                                    echo "-<br><br><br>";
                                   }
                                   echo '<br>';
                               //} 
                             }?>
                            </td>
                            <?php } ?>
                      </tr>
                        <?php  } ?>
                               
                  </tbody>
                  </table>
                </div>
                </div>
                      
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

<?php $this->load->view('footer.php'); ?>
</body>

</html>