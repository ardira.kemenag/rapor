<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
   <script>
            function get_ubah(){
                var tanggal_nilai = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('penilaian/get_ubah'); ?>", 
                    data:"tanggal_nilai="+tanggal_nilai, 
                    success: function(msg) {
                            $("#div_ktd").html(msg);
                    }
                });
            }
        </script>

<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Ubah Nilai </h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Penilaian</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Ubah penilaian</li>
                      </ol>
                    </nav><?php  $ind = $nilai->result(); ?>
                    <?php
                    $dataS = $dtsiswa->result();
                    ?>
                      <form class="forms-sample" action="<?php echo site_url('Penilaian/aksi_ubah/'.$ind[0]->id_nilai);?>" method="post" enctype="multipart/form-data">
                        
                    <div class="form-group row">
                    
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama Siswa</label>
                          <div class="col-sm-9">
                            <input type="Text" name="nama"  class="form-control form-control-lg" value="<?php echo $dataS[0]->nama_siswa?>" disabled>
                            
                          </div>
                        </div>
                     <div class="form-group row">
                    <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Indikator</label>
                        
                    <div class="col-sm-9">
                    <input type="Text" name="nama"  class="form-control form-control-lg" value="<?php echo $ind[0]->kode_indikator?>" disabled>
                    </div>
                    </div>
                         <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tanggal Nilai </label>
                           <input type="hidden" name="fid"  class="form-control form-control-lg" value="<?php echo $ind[0]->id_nilai?>">
                          <div class="col-sm-9">
                            
                             <input type="Text" name="nama"  class="form-control form-control-lg" value="<?php echo $ind[0]->tanggal_nilai?>" disabled>

                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nilai Skala Perkembangan</label>
                          <div class="col-sm-9">
                        <div class="form-group row">
                          <div class="col-sm-3">
                            <div class="form-radio">
                              <label class="form-check-label"><input type="radio" class="form-check-input" name="f6"  value="1" <?php echo ($ind[0]->nilai_perkembangan=='1')?'checked':'' ?>>
                                BB </label>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-radio">
                              <label class="form-check-label"><input type="radio" class="form-check-input" name="f6"  value="2" <?php echo ($ind[0]->nilai_perkembangan=='2')?'checked':'' ?>>
                               MB</label>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-radio">
                              <label class="form-check-label"><input type="radio" class="form-check-input" name="f6"  value="3" <?php echo ($ind[0]->nilai_perkembangan=='3')?'checked':'' ?>>
                               BSH</label>
                            </div>
                          </div>
                            <div class="col-sm-3">
                            <div class="form-radio">
                              <label class="form-check-label"><input type="radio" class="form-check-input" name="f6"  value="4" <?php echo ($ind[0]->nilai_perkembangan=='4')?'checked':'' ?>>BSB
                              </label>
                            </div>
                          </div>
                        </div>
                          </div>
                        </div>
                     
                         <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Deskripsi</label>
                          <div class="col-sm-9">
                         
                            <textarea name="f7" class="form-control" id="exampleTextarea1" rows="2"><?php echo $ind[0]->deskripsi ?></textarea>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Teknik Penilaian</label>
                          <div class="col-sm-9">
                         <select name="tek" class="form-control" >
                       <?php
                            foreach ($tek as $k) {
                        ?>
                          <option value="<?php echo $k->id_teknik; ?>" <?php if($ind[0]->id_teknik==$k->id_teknik)echo "selected"; ?>><?php echo $k->nama_teknik; ?></option>
                              <?php
                              } ?>    
                    </select>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Foto</label>
                          <div class="col-sm-9">
                             
                          <input type="file" name="foto" class="dropify" data-max-file-size="1mb" data-default-file=" <?php echo base_url('uploads/'.$ind[0]->foto);?>">

                          
                       
                          </div>
                        </div>
                        
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <button class="btn btn-light">Cancel</button>
                      </form>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>