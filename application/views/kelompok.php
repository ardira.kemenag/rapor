<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">
                <div class="card-body">
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data Siswa</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Data Siswa</h4>
                  </div> 

                  <div class="col-lg-6 text-right">
                    <a  class="btn btn-light btn-sm"  href="<?php echo site_url('Siswa')?>">Tambah</a>
                  </div>
                  <div class="col-lg-6 text-left">
                    <?php
                        $no = 1;
                        foreach($kelas->result() as $b){
                        ?>
                  
                          <a class='btn btn-primary btn-sm' href="<?php echo site_url('siswa/kelompok/').$b->id_kelas?>">
                             <?php echo $b->nama_kelas ?></a>
                         
                            
                      
                                <?php
                                    }
                                ?>
                  </div>
                <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No</th>
                          <th>No Induk</th>
                         <th><center>Nama</center></th>
                        <th><center>Tempat lahir</center></th>
                        <th><center>Kelompok</center></th>
                        <th><center>Jenis kelamin</center></th>
                        <th><center>Aksi</center></th>
                      </tr>
                    </thead>
                    
                 <tbody>
                        <?php
                        $no = 1;
                        foreach($siswa->result() as $b){
                        ?>
                        <tr>
                          <td><center> <?php echo $no++ ?></center></td>
                           <td><center> <?php echo $b->no_induk?></center></td>
                    <td><center> <?php echo $b->nama_siswa ?></center></td>
                    <td><center> <?php echo $b->tempat_lahir ?></center></td>
                    <td><center> <?php echo $b->nama_kelas ?></center></td>
                    <td><center> <?php echo $b->jenis_kelamin ?></center></td>
                        <td><center>
                          <a class='btn btn-primary btn-sm' href="<?php echo site_url('siswa/detail/').$b->id_siswa?>">
                            <i class='icon-eye'></i> Lihat</a>
                          </center></td>
                            
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>