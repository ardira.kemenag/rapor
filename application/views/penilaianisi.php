<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
  <script>
            function get_jadwal(){
                var id_perencanaan = $("#jdw").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('penilaian/get_jadwal'); ?>", 
                    data:"id_perencanaan="+id_perencanaan, 
                    success: function(msg) {
                            $("#div_jadwal").html(msg);
                    }
                });
            }
        </script>
          <script>
            function get_tanggal(){
                var id_perencanaan = $("#tgl").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('penilaian/get_tanggal'); ?>", 
                    data:"id_perencanaan="+id_perencanaan, 
                    success: function(msg) {
                            $("#div_tgl").html(msg);
                    }
                });
            }
        </script>
<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah Penilaian </h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Penilaian</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah penilaian</li>
                      </ol>
                    </nav>
                    <?php
                    $dataS = $siswa->result();
                    ?>
                      <form class="forms-sample" action="<?php echo site_url('Penilaian/insert_nilai/');?>" method="post" enctype="multipart/form-data">
                        
                    <div class="form-group row">
                     <input type="hidden" name="fid"  class="form-control form-control-lg" value="<?php echo $dataS[0]->id_siswa?>">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama Siswa</label>
                          <div class="col-sm-9">
                            <input type="Text" name="nama"  class="form-control form-control-lg" value="<?php echo $dataS[0]->nama_siswa?>" disabled>
                            <!-- <input type="Text" name="nama"  class="form-control form-control-lg" value="<?php echo $dataS[0]->id_kelas?>" disabled> -->
                          </div>
                        </div>

                         <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Minggu Ke</label>
                          <div class="col-sm-9">
                            
                            <SELECT name="jdw" class="form-control form-control-sm" onchange="get_tanggal()" id="tgl" required>
                              <option> Pilih tanggal</option>
                              <?php
                                   foreach($jdw as $a){
                                    $dayList = array('Sunday' => 'Minggu','Monday' => 'Senin','Tuesday' => 'Selasa',
                              'Wednesday' => 'Rabu','Thursday' => 'Kamis','Friday' => 'Jumat','Saturday' => 'Sabtu');
                       $format=date('d F Y', strtotime($a->tanggal_awal));
                       $day = date('l', strtotime($a->tanggal_awal));
                      // echo $dayList[$day].',';
                      // echo $format;
                              echo "<option value='".$a->id_perencanaan."'>".$dayList[$day].','.$format.' s/d '.$a->tanggal_akhir.$a->nama_program."</option>";
                                  }
                                         ?>
                            </SELECT>
                          </div>
                        </div>
                        <div  id="div_jadwal">
                        </div>
                        <div  id="div_tgl">
                        </div>
                        <!--  <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">KD</label>
                        
                        </div> -->
                        
                       <!-- <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Sub Sub Tema</label>
                          <div class="col-sm-9">
                          <input type="text" name="f4" class="form-control form-control-sm">
                           </div>
                      </div> 
                      <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Sub Materi</label>
                          <div class="col-sm-9">
                          <input type="text" name="fmateri" class="form-control form-control-sm">
                           </div>
                      </div>-->
                        <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nilai Skala Perkembangan</label>
                          <div class="col-sm-9">
                        <div class="form-group row">
                          <div class="col-sm-3">
                            <div class="form-radio">
                              <label class="form-check-label"><input type="radio" class="form-check-input" name="f6" id="membershipRadios1" value="1" >
                                BB </label>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-radio">
                              <label class="form-check-label"><input type="radio" class="form-check-input" name="f6" id="membershipRadios2" value="2">
                               MB</label>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-radio">
                              <label class="form-check-label"><input type="radio" class="form-check-input" name="f6" id="membershipRadios2" value="3">
                               BSH</label>
                            </div>
                          </div>
                            <div class="col-sm-3">
                            <div class="form-radio">
                              <label class="form-check-label"><input type="radio" class="form-check-input" name="f6" id="membershipRadios2" value="4">BSB
                              </label>
                            </div>
                          </div>
                        </div>
                          </div>
                        </div>
                     
                         <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Deskripsi</label>
                          <div class="col-sm-9">
                            <textarea name="f7" class="form-control" id="exampleTextarea1" rows="2"></textarea>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Teknik Penilaian</label>
                          <div class="col-sm-9">
                           <select  class="form-control form-control-sm" name="tk" required>
                           <option value="0">
                            Pilih Teknik Penilaian</option>
                            <?php
                                   foreach($tk as $a){
                              echo "<option value='".$a->id_teknik."'>".$a->nama_teknik."</option>";
                                  }
                                         ?> </select>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Foto</label>
                          <div class="col-sm-9">
                            
                            <input type="file" name="foto"  class="form-control form-control-lg" id="exampleInputPassword2" placeholder="Password">
                          </div>
                        </div>
                        
                        <button type="submit"   class="btn btn-success mr-2">Submit</button>
                        <a href="<?php echo site_url('penilaian/nilai');?>" class="btn btn-danger" value="Batal"><i class="icon-remove"></i> Batal</a>
                      </form>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>