<html>
 <head>
  <title></title>
 </head>
  <?php
                                  // $siswaT = $datasiswa->result();
                                  ?>
<?php
function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('/', $tanggal);
  
  // variabel pecahkan 0 = tanggal
  // variabel pecahkan 1 = bulan
  // variabel pecahkan 2 = tahun
 
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}?>
 <body bgcolor="white">

  <table width="100%">
  
        <tr>
          <td><div align="center" ><b><img src="<?php echo base_url();?>assets/images/garuda.png" width="150px" /></b></div></td>
        </tr>
        <tr>
          <td width="100%">
            <div align="center" style="font-size: 12pt;"><b><br/>KEMENTERIAN AGAMA </b></div>
          </td>
          <td rowspan="5" width="25%">&nbsp;</td>
        </tr>
        <tr>
          <td><div align="center" style="font-size: 12pt;"><b>REPUBLIK INDONESIA</b></div></td>
        </tr>
        <tr>
          <td><div align="center" style="font-size: 12pt;"><b>LAPORAN SKALA CAPAIAN PERKEMBANGAN HARIAN </b></div></td>
           
        </tr>
         <tr>
          <td><div align="center" style="font-size: 12pt;"><BR><b>RAUDHATUL ATHFAL(RA)</b></div></td>
        </tr>
       
        </table>
        <table> 
        <?php 
                          foreach ( $jdw->result() as $m )
                          { ?>
                            
                          <?php  $id[]=$m->id_perencanaan;?> 
                          <?php }?> 
                             <?php 
          foreach ( $jdw->result() as $j )
                          { ?>
                        <table style="border: 1px solid black; padding: 5px;/*  margin-left: 220px;*/ margin-bottom: 10px;">
                            <div class="col-lg-12 text-center">
                        <tr>
                      <td> <label class="col-sm-2 col-form-label">Nama siswa</label></td>
                      <td>   : <?php echo $j->nama_siswa?></td>
                        <td>  <label class="col-sm-2 col-form-label">Kelompok</label></td>
                        <td>: <?php echo $j->kelompok?></td>
                      </div></tr>
                       <div class="form-group row">
                        <tr>  
                       <td><label class="col-sm-2 col-form-label">Semester</label></td>
                     <td>: <?php echo $j->semester?></td>
                     <td> <label class="col-sm-2 col-form-label">Tahun</label></td>
                     <td>: <?php echo $format=date('Y', strtotime($j->tahun))?></td>
                      </div>
                    </tr>
                  <?php }?>
                      </table>   
                  <div class="table-responsive">
                    <br>
                  </table>
                  <table id="" class="table table-striped table-advance table-hover nowrap" border="1">
                    <thead>
                      <tr> 
                         <th>No</th>
                         <th><center>Indikator Penilaian</center></th>
                             <th><center>Teknik Penilaian</center></th>
                         <?php 
                         $awal=$hari->tanggal_awal;
                         $akhir=$hari->tanggal_akhir;
                         $begin   = new DateTime($awal);
                         $end  = new DateTime($akhir);
                        $interval = DateInterval::createFromDateString('1 day');
                        $period = new DatePeriod($begin, $interval, $end);
                          foreach ( $period as $dt )
                          { ?><th width="15%">
                          <?php echo $dt->format("Y-m-d");?> </th> 
                          <?php }?>    
                       <th width="15%"><center><?php echo $akhir;?></center></th>
                        <th width="15%"><center>Capaian Akhir</center></th>
                      </tr>
                    </thead>
                 <tbody>
                        <?php
                        $no = 1;

                         foreach($jdw->result()as $b){?>
                        <tr>
                           <td><center> <?php echo $no++?></center></td>
                           <td><?php echo $b->nama_indikator?></td>
                           <td><?php echo $b->nama_teknik?></td>
                        <?php  foreach($period as $c){?>
                        <?php $tanggal_nilai= $c->format('Y-m-d');?>
                          <td><a href="<?php echo site_url('penilaian/ubahnilai/').$b->kode_indikator.'/'.$b->id_siswa.'/'.$tanggal_nilai ?>">
                           <input type="hidden" value="<?php echo $c->format('Y-m-d').'/'.$b->kode_indikator.'/'.$b->id_siswa; ?>">
                          <?php 
                           $nilai=$this->model_m->rekaphari($c->format('Y-m-d'),$b->id_siswa,$b->kode_indikator);
                           foreach ($nilai->result() as $key ) {
                         if($key->nilai_perkembangan=='1') {
                          echo "BB";
                         }
                         elseif ($key->nilai_perkembangan=='2') {
                           echo "MB";
                         }
                         elseif ($key->nilai_perkembangan=='3') {
                           echo "BSH";
                         }
                         else{
                          echo "BSB";
                         }
                           }
                           ?>
                            </a></td> 
                      <?php } ?>
                          <td><a href="<?php echo site_url('penilaian/ubahnilai/').$b->kode_indikator.'/'.$b->id_siswa.'/'.$akhir ?>">
                      
                      <?php 
                       $nilai=$this->model_m->rekaphari($akhir,$b->id_siswa,$b->kode_indikator);
                       foreach ($nilai->result() as $key ) {
                           if($key->nilai_perkembangan=='1') {
                          echo "BB";
                         }
                         elseif ($key->nilai_perkembangan=='2') {
                           echo "MB";
                         }
                         elseif ($key->nilai_perkembangan=='3') {
                           echo "BSH";
                         }
                         else{
                          echo "BSB";
                         }
                       }
                       ?>
                           </a> </td> 
                              <td>
                      
                      <?php 
                       $nilai=$this->model_m->harirkp($b->id_siswa,$b->kode_indikator,$b->id_jadwal);
                       foreach ($nilai->result() as $key ) {
                           if($key->na=='1') {
                          echo "BB".$key->na;

                         }
                         elseif ($key->na=='2') {
                           echo "MB".$key->na;
                         }
                         elseif ($key->na=='3') {
                           echo "<b>BSH</b>";
                         }
                         else{
                          echo "BSB";
                         }
                       }
                       ?>
                            </td> 


                            
                      </tr>
                        <?php  } ?>
                               
                  </tbody>
                  </table>
                </div>
                </div>

  
 </body>
 <script>

    window.print();
</script>
</html>