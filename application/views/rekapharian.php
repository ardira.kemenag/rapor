<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
  <script>
            function get_data(){
                var id_tahunajaran = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('report/get_data'); ?>", 
                    data:"id_tahunajaran="+id_tahunajaran, 
                    success: function(msg) {
                            $("#div_ktd").html(msg);
                    }
                });
            }
        </script>
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
         <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Laporan Harian</li>
                      </ol>
                </nav>
                  <div class="row">
                  <div class="col-lg-12">
                    <h4 class="card-title">Laporan Harian Siswa</h4>
                  </div> 
                  <div class="col-lg-12">
                
                    <br>
                        <form method="post" action="<?php echo site_url('report/cari_report');?>
                          ">
                          <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tahun Ajaran</label>
                          <div class="col-sm-9">
                         <select required name="thn" class=" form-control form-control-sm" id="prov" onchange="get_data()">
                           <option value=""> Pilih tahun</option>
                             <?php
                            foreach($thn as $r){
                            echo "<option value='".$r->id_tahunajaran."'>".$r->nama_tahun."</option>";}
                        ?> 
                         </select>
                           </div>
                      </div>
                      <div  id="div_ktd">
                      <div class="form-group row">
                      <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Pilih Siswa </label>
                          <div class="col-sm-9">
                         <select required name="siswa" class="form-control" id="keyword">
                              <option value="">Pilih Siswa</option>
                                <!-- <?php 
                                    foreach($siswa as $k){
                                      echo "<option value='".$k->id_siswa."'>"."(".$k->no_induk.")".$k->nama_siswa."</option>";}
                                ?>-->
                          </select> </div>
                      </div>
                       <div class="form-group row">
                      <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Pilih Minggu </label>
                          <div class="col-sm-9">
                            <select required name="keyword" class="form-control" >
                              <option value="">Pilih Minggu</option>
                                <!-- <?php 
                                    foreach($minggu as $k){
                                       $format=date('d F Y', strtotime($k->tanggal_awal));
                                      echo "<option value='".$k->tanggal_awal."'>".$format=date('F', strtotime($k->tanggal_awal)).',Minggu Ke-'.$k->mingguke."</option>";}
                                ?>-->
                          </select></div></div>
                        </div>
                          <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Cari
                                </button>
                            </span>
                        </form>
                     
             
               
               
                </div>
               
                </div>
                </div>
                </div>
              </div>
            </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>