<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">
                <div class="card-body">
                   <?php echo $this->session->flashdata('msg2');?>
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home/dashboard')?>">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data Indikator</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Data Indikator</h4>
                  </div> 
                   <div class="col-lg-6 text-right">
                    <a  class="btn btn-light btn-sm"  href="<?php echo site_url('Indikator')?>">Tambah</a>
                  </div>
                <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No</th>
                         
                        <th> Nama Program </th>
                        <th> Nama KD </th>
                        <th> Nama Indikator </th>
                        <th> Aksi</th>
                       
                      </tr>
                    </thead>
                    
                 <tbody>
                        <?php
                        $no = 1;
                        foreach($ind as $b){
                        ?>
                        <tr>
                           <td>  <?php echo $no++?> </td>
                           <td>  <?php echo $b->nama_program ?> </td>
                           <td>  <?php
                               $j=json_decode($b->kode_kd);
                               foreach ($j as $key ) {
                                  echo $b->kode_kd.$this->model_m->cek_kd2($key).'<br>';
                               }
                               ?></td> </td>
                           <td>  <?php echo $b->kode_indikator.$b->nama_indikator ?> </td>
                           <td> <a class='btn btn-primary btn-sm-xs' href="<?php echo site_url('Indikator/ubahindikator/').$b->id_indikator.'/'?>"> <i class='icon-pencil'></i> Ubah</a></td>
                   
                            
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>