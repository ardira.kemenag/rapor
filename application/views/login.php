<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Rapor</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/flag-icon-css/css/flag-icon.min.css">

   <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/perfect-scrollbar/css/perfect-scrollbar.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="..\..\images\favicon.png">
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth" style="
    background-color: background: #1e5799; /* Old browsers */
    background: -moz-linear-gradient(left, #1e5799 0%, #ce27b5 0%, #ad56a0 11%, #ad56a0 28%, #f91d42 100%, #7db9e8 100%, #f91d42 102%); /* FF3.6-15 */
    background: -webkit-linear-gradient(left, #1e5799 0%,#ce27b5 0%,#ad56a0 11%,#ad56a0 28%,#f91d42 100%,#7db9e8 100%,#f91d42 102%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to right, #1e5799 0%,#ce27b5 0%,#ad56a0 11%,#ad56a0 28%,#f91d42 100%,#7db9e8 100%,#f91d42 102%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#f91d42',GradientType=1 ); /* IE6-9 */
">
        <div class="row w-100">
          <div class="col-lg-8 mx-auto">
            <div class="row">
               <div class="col-lg-6 login-half-bg d-flex flex-row">
                <p class="text-white font-weight-medium text-center flex-grow align-self-end" style="margin-bottom: -30px;">Copyright &copy; 2018  All rights reserved.</p>
              </div>
              <div class="col-lg-6 bg-white" style="border-radius: 30px;">

                <div class="auth-form-light text-left p-5" style="border-radius: 30px;">
                <!--  <h2><center><img src="<?php echo base_url();?>assets/images/logo_kemenag.png" style="margin-bottom: 15px; margin-top: 25px;" height="86px"></center></h2>
                  <h4 class="font-weight-light"><center>Aplikasi Rapor Digital RA</center></h4> -->
                   <h2 class="font-weight-light"><center>LOGIN </center> </h2>
                  <form action="<?php echo site_url();?>/Login/verifikasi" method="post" class="pt-5">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Username</label>
                      <input class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="username" placeholder="Username">
                      <i class="mdi mdi-account"></i>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
                      <i class="mdi mdi-eye"></i>
                    </div>
                    <div class="mt-5">
                     
                      <input type="submit" value="login" class="btn btn-block btn-success btn-lg font-weight-medium">
                    </div>
          <!-- <center>
          <a class="btn btn-inverse-danger btn-block btn-fw" href="<?php echo site_url();?>/Registrasi/" > Registrasi</a></center> -->
                                    
                                    
                  </form>
                </div>
              </div>
             
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
 <?php $this->load->view('footer.php'); ?>
</body>

</html>
