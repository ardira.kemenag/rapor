<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">
                <div class="card-body">
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Penilaian</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url('report/harian/')?>">Laporan Harian</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data Report Siswa Per Kelompok</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-12">
                    <h4 class="card-title">Report Siswa Per Kelompok</h4>
                  </div> 

                 
                  <div class="col-lg-6 text-left">
                    <?php
                        $no = 1;
                        foreach($kelas->result() as $b){
                        ?>
                  
                          <a class='btn btn-inverse-primary btn-fw btn-xs' href="<?php echo site_url('report/kelompok/').$b->id_kelas?>">
                             <?php echo $b->nama_kelas ?></a>
                         
                            
                      
                                <?php
                                    }
                                ?>
                  </div>
                <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                     <thead>
                      <tr> 
                         <th>Nama</th>
                         <th><center>Tema</center></th>
                         <th><center>SubTema</center></th>
                         <th><center>Indikator</center></th>
                        <th><center>Tahun/Bulan</center></th>
                         <th><center>Nilai</center></th>
                          <th><center>Deskripsi</center></th>
                       
                      </tr>
                    </thead>

                 <tbody>
                    <?php  $no = 1;
                        foreach($hari as $b){
                        ?>
                        <tr>
                          <td><center> <?php echo $b->nama_siswa?></center></td>
                          <td> <?php echo $b->nama_tema?></td>
                          <td> <?php echo $b->nama_subtema?></td>
                           <td> <?php
                               $j=json_decode($b->id_indikator);
                               foreach ($j as $key ) {
                                  echo $this->model_m->cek_ind($key).'<br>';
                               }
                               ?></td> 
                          <td> <?php
                                $format=date(' d Y F ', strtotime($b->tanggal_awal));
                                echo $format;?></td>
                                <td><center> <?php if ($b->nilai_perkembangan=='1'){
                          echo 'BB';
                        }
                        elseif ($b->nilai_perkembangan=='2') {
                          echo 'MB';
                        }
                        elseif ($b->nilai_perkembangan=='3') {
                          echo 'BSH';
                        }
                        else{
                          echo 'BSB';
                        }?></center></td>
                          <td><center> <?php echo $b->deskripsi?></center></td>
                                         
                      </tr>
                                <?php
                                    }
                                ?>
                       
                  </tbody>
                  </table>
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>