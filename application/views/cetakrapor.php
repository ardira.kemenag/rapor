<html>
 <head>
  <title></title>
 </head>
  <?php
                                  $siswaT = $datasiswa->result();
                                  ?>
<?php
function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('/', $tanggal);
  
  // variabel pecahkan 0 = tanggal
  // variabel pecahkan 1 = bulan
  // variabel pecahkan 2 = tahun
 
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}?>
 <body bgcolor="white">

  <table width="100%">
  
        <tr>
          <td><div align="center" ><b><img src="<?php echo base_url();?>assets/images/garuda.png" width="150px" /></b></div></td>
        </tr>
        <tr>
          <td width="100%">
            <div align="center" style="font-size: 12pt;"><b><br/>KEMENTERIAN AGAMA </b></div>
          </td>
          <td rowspan="5" width="25%">&nbsp;</td>
        </tr>
        <tr>
          <td><div align="center" style="font-size: 12pt;"><b>REPUBLIK INDONESIA</b></div></td>
        </tr>
        <tr>
          <td><div align="center" style="font-size: 12pt;"><b>SURAT KETERANGAN TAMAT BELAJAR</b></div></td>
           
        </tr>
         <tr>
          <td><div align="center" style="font-size: 12pt;"><BR><b>RAUDHATUL ATHFAL(RA)</b></div></td>
        </tr>
        <tr>
          <td align="justify" style="font-size: 14pt;"><BR><BR>
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Yang bertanda tangan dibawah ini, Kepala Raudhatul Athfal ........  
          </td>
        </tr>
        <tr>
          <td align="justify" style="font-size: 14pt;">
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; menerangkan bahwa:
          </td>
        </tr>
        </table>
        <table>
         
         <tr>
          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->nama_siswa))?></td>
        </tr>
        <tr>

          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tempat, Tanggal Lahir </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->tempat_lahir)?>,<?php echo ucwords($siswaT[0]->tanggal_lahir)?></td>
        </tr>
        <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Orangtua </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->nama_ayah)?></td>
        </tr>
        <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tempat, Tgl Lahir </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords($siswaT[0]->tempat_lahir)?>,<?php echo ucwords($siswaT[0]->tanggal_lahir)?></td>
        </tr>
        <tr>
          <td style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Induk Sekolah </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->NSN))?></td>
        </tr>
        </table>

        <table>
            <table width="100%">
               <tr>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3" style="font-size: 14pt;" ><div align="justify" style=" padding-left: 55PX;
">Telah TAMAT  pendidikannya di Raudhatul Athfal  sesuai kriteria Standar Tingkat Pencapaian Perkembangan Anak ( STPPA ) dan siap melanjutkan pendidikan dijenjang yang lebih tinggi </div></td>
        </tr>
        <tr>
          <td rowspan="5" width="20%">
            <img class="img-responsive" src="<?php if($siswaT[0]->foto == NULL){
                                echo " ";
                            }else{
                                echo base_url('uploads/'.$siswaT[0]->pic);
                            }?>" style=" height: 220px ">

            <!--  <img  src="<?php echo base_url('uploads/'.$siswaT[0]->foto);?>
                       " width="150px" style="margin: 50PX 30px 30px 20px"> -->
            
          </td>
          <td width="60%">
            <br>
             <div align="right" style="font-size: 12pt;"><b><?php echo ucfirst($siswaT[0]->provinsi)?>,<?php echo tgl_indo(date('Y/m/d'));?></b></div>
            <div align="right" style="font-size: 12pt;"><b>Kepala Raudhatul Athfal</b></div>
          </td>
          <td rowspan="5" width="25%">&nbsp;</td>
        </tr>
        <tr>
          <td></td>
        </tr>
        <tr><td><div align="center" style="font-size: 12pt;"><br> </div></td></tr>
        <tr><td><div align="center" style="font-size: 10pt;"></div></td></tr>
        <tr><td><div align="center" style="font-size: 10pt;"></div></td></tr>
        <tr>
          <td>
            <div align="center" style="font-size: 10pt;"><td align="right">&nbsp;<?php echo ucwords(($siswaT[0]->kepala_RA))?></td>
            </div>
          </td>
        </tr>
        <!-- <tr>
          <td><div align="center" style="font-size: 10pt;"><td align="right">&nbsp;NIP:093743276476</td>
            </div></td>
        </tr> -->
        </table>
        <tr><td colspan="3">&nbsp;</td></tr>
        </table>
        <br><br><br><br><br><br><br><br><br>
          <table width="100%">
        <tr>
        <td><div align="center" style="font-size: 18pt;"><b>STANDAR TINGKAT PENCAPAIAN PERKEMBANGAN ANAK
        ( STPPA )
        </b></div></td>
        </tr>
        </table>

        <table>
         
        
         <tr>

          <td width="35%" style="font-size: 14pt;"><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama </td>
          <td width="2%"><br><br>:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><br><br><?php echo ucwords(($siswaT[0]->nama_siswa))?></td>
        </tr>

         <tr>

          <td width="35%" style="font-size: 14pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Induk </td>
          <td width="2%">:&nbsp;</td>
          <td width="75%" style="font-size: 14pt;"><?php echo ucwords(($siswaT[0]->no_induk))?></td>
        </tr>
        </table><br><br>
        <table border="1" style="
    margin-left: 56px;
">
    <tr>
        <td style="width: 50px;">No</td>
        <td style="width: 890px;"><center>Program Pengembangan</center></td>
        
    </tr>
    <?php
                        $no = 1;
                        foreach($program->result() as $b){
                        ?>
                        <tr>
                          <td style="width: 50px;"><center> <?php echo $no++ ?></center></td>
                           <td style="width: 890px;"><center> <?php echo $b->nama_program?></center></td>
                  
                            
                      </tr>
                                <?php
                                    }
                                ?>

    
</table>
<table>
 <tr>
          <td style="width: 50px;"></td>
          <td width="100%">
            <br>
             <div align="right" style="font-size: 12pt;"><b><?php echo ucfirst($siswaT[0]->provinsi)?>,<?php echo tgl_indo(date('Y/m/d'));?></b></div>
            <div align="right" style="font-size: 12pt;"><b>Kepala Raudhatul Athfal</b></div>
          </td>
          <td rowspan="5" width="55%">&nbsp;</td>
        </tr>
        <tr>
          <td></td>
        </tr>

        <tr>
          <td>
            <div align="center" style="font-size: 12pt;"><br> </div>

          </td>
        </tr>
        <tr>
          <td>
            <div align="center" style="font-size: 10pt;">
            
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <div align="center" style="font-size: 10pt;">
            
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <div align="center" style="font-size: 10pt;">
             <td align="right">&nbsp;(Drs. Sularno, MM)</td>

            </div>
          </td>
        </tr>
        <tr>
          <td> <div align="center" style="font-size: 10pt;">
             <td align="right">&nbsp;NIP:093743276476</td>

            </div></td>
        </tr>
        </table>

  
 </body>
 <script>

    window.print();
</script>
</html>