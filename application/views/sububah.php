<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>

<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Ubah Sub Tema</h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Home/dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Subtheme/sub');?>">SubTema</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Ubah Sub Tema Pembelajaran</li>
                      </ol>
                    </nav>
                    <?php
                    $datatm = $sub->result();
                    ?>
                      <form class="forms-sample" action="<?php echo site_url('Subtheme/aksi_ubah/');?>" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama Tema</label>
                          <div class="col-sm-9">
                          <select required name="f1" class="form-control form-control-sm">
                            <option value=""> Pilih Tema</option>
                             <?php
                            foreach ($tm->result() as $k) {
                        ?>
                          <option value="<?php echo $k->id_tema; ?>" <?php if($datatm[0]->id_tema==$k->id_tema)echo "selected"; ?>><?php echo $k->nama_tema; ?></option>
                              <?php
                              } ?>   
                            
                          </select>

                           </div>
                      </div>
                       <div class="form-group row">
                       
                        <input type="hidden" name="fid" class="form-control form-control-lg" value="<?php echo $datatm[0]->id_subtema?>">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama SubTema</label>
                          <div class="col-sm-9">
                          <input  type="text" class="form-control form-control-sm" name="f2" value="<?php echo $datatm[0]->nama_subtema?>">

                           </div>
                      </div>
                     

                        <button type="submit" class="btn btn-success mr-2">Ubah</button>
                        <a href="<?php echo site_url('Subtheme/subtheme');?>" class="btn btn-danger" value="Batal"><i class="icon-remove"></i> Batal</a>
                      </form>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>