<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>

<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah Penilaian </h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Penilaian</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah penilaian</li>
                      </ol>
                    </nav>
                    <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                  <table id="" class="table table-striped table-advance table-hover nowrap">
                    <thead>
                      <tr> 
                         <th>No</th>
                         <th><center>Indikator Penilaian</center></th>
                            <th>KD</th>
                         
                         <?php 
                         
                         $awal=$hari->tanggal_awal;
                         $akhir=$hari->tanggal_akhir;
                         $begin   = new DateTime($awal);
                         $end  = new DateTime($akhir);
                        $interval = DateInterval::createFromDateString('1 day');
                        $period = new DatePeriod($begin, $interval, $end);
                          foreach ( $period as $dt )
                          { ?><th>

                          <?php echo $dt->format("Y-m-d");?> </th> 
                          <?php }?>    
                       <th><center><?php echo $akhir;?></center></th>
                    
                      </tr>
                    </thead>
                    
                 <tbody>
                        <?php
                        $no = 1;
                         foreach($jdw->result() as $b){?>
                        
                        <tr>
                           <td><center> <?php echo $no++?></center></td>
                           <td width="30%"> <?php echo $b->nama_program?></td>
                           <td width="30%"> <?php echo $b->id_indikator?></td>

                        
                    
                     <?php  foreach($period as $c){?>
                    <td>
                      <input type="hidden" value="<?php echo $c->format('Y-m-d'); ?>">
                      <div class="form-radio">
                              <label class="form-check-label"><input type="radio" class="form-check-input" name="f6" id="membershipRadios1" value="1" >
                                BB </label>
                            </div>
                            <div class="form-radio">
                              <label class="form-check-label"><input type="radio" class="form-check-input" name="f6" id="membershipRadios1" value="1" >
                                BsB </label>
                            </div></td> 
                          <?php
                                    }
                                ?>
                                 <td>
                      <input type="hidden" value="<?php echo $akhir; ?>">
                      <div class="form-radio">
                              <label class="form-check-label"><input type="radio" class="form-check-input" name="f6" id="membershipRadios1" value="1" >
                                BB </label>
                            </div>
                            <div class="form-radio">
                              <label class="form-check-label"><input type="radio" class="form-check-input" name="f6" id="membershipRadios1" value="1" >
                                BsB </label>
                            </div></td> 
                      </tr>
                        <?php
                                    }
                                ?>
                                <tr>
                                  <td></td><td></td><td>DESKRIPSI</td>

                     <?php  foreach($period as $c){?>
                    <td>
                       <div class="form-group row">
                         
                          <div class="col-sm-9">
                            <textarea name="f5" class="form-control" id="exampleTextarea1" rows="2"></textarea>
                          </div>
                        </div></td> 

                          <?php
                                    }
                                ?>
                                 
                                </tr>
                            <tr>
                                  <td></td><td></td><td>DESKRIPSI</td>

                     <?php  foreach($period as $c){?>
                    <td>
                       <div class="form-group row">
                         
                          <div class="col-sm-9">
                            <input type="file" class="form-control">
                          </div>
                        </div></td> 
                        
                          <?php
                                    }
                                ?>
                                 
                                </tr>
                                <tr>
                                  <td></td><td></td><td>DESKRIPSI</td>

                     <?php  foreach($period as $c){?>
                    <td>
                       <div class="form-group row">
                         
                          <div class="col-sm-9">
                            <button class="btn btn-primary" type="submit">Submit
                                </button>
                          </div>
                        </div></td> 
                        
                          <?php
                                    }
                                ?>
                                 
                                </tr>

                               
                  </tbody>
                  </table>
                </div>
                </div>
                      <form class="forms-sample" action="<?php echo site_url('Penilaian/aksi_insert/');?>" method="post" enctype="multipart/form-data">
                      
                     
                         <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Deskripsi</label>
                          <div class="col-sm-9">
                            <textarea name="f5" class="form-control" id="exampleTextarea1" rows="2"></textarea>
                          </div>
                        </div>
                        
                         
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <button class="btn btn-light">Cancel</button>
                      </form>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Polres Tangsel </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>