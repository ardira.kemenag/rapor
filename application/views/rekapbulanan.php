<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
  <script>
            function get_bulan(){
                var id_tahunajaran = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('report/get_bulan'); ?>", 
                    data:"id_tahunajaran="+id_tahunajaran, 
                    success: function(msg) {
                            $("#div_ktd").html(msg);
                    }
                });
            }
        </script>
<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
         <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Laporan Bulanan</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Laporan Bulanan Siswa</h4>
                  </div> 
                  <div class="col-lg-6 text-right">
                 
                  </div>
                <div class="col-lg-12">
                
                    <br>
                        <form method="post" action="<?php echo site_url('report/search_bulan');?>">
                          <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tahun Ajaran</label>
                          <div class="col-sm-9">
                         <select required name="thn" class=" form-control form-control-sm" id="prov" onchange="get_bulan()">
                           <option value="0"> Pilih tahun ajaran </option>
                             <?php
                            foreach($thn as $r){
                            echo "<option value='".$r->id_tahunajaran."'>".$r->nama_tahun."</option>";}
                        ?> 
                         </select>
                           </div>
                      </div>
                      <div  id="div_ktd">
                      <div class="form-group row">
                      <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Pilih Siswa </label>
                          <div class="col-sm-9">
                        <select required name="siswa" class="form-control" id="keyword">
                        <option value="">Pilih siswa</option>
                        <!-- <?php 
                                    foreach($siswa as $k){
                                      echo "<option value='".$k->id_siswa."'>"."(".$k->no_induk.")".$k->nama_siswa."</option>";}
                                ?>-->
                          </select> </div>
                      </div>
                       <div class="form-group row">
                      <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Pilih Bulan </label>
                          <div class="col-sm-9">
                            <select required name="bulan" class="form-control" id="bulan" >
                              <option value="">Pilih bulan</option>
                                <!-- <?php 
                                    foreach($bulan as $k){
                                      echo "<option value='".$k->bulan."'>".'Bulan Ke-'.$k->bulan."</option>";}
                                ?>-->
                          </select></div></div>
                        </div>
                          <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Cari
                                </button>
                            </span>
                        </form>
                     
             
               
               
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>