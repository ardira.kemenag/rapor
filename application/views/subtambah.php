<!DOCTYPE html>
<html lang="en">
<<?php $this->load->view('header.php'); ?>

<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah Sub Tema</h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Home');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Subtheme/sub');?>">Tema</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Sub tema</li>
                      </ol>
                    </nav>
                      <form class="forms-sample" action="<?php echo site_url('Subtheme/aksi_insert/');?>" method="post" enctype="multipart/form-data">
                       <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tema</label>
                          <div class="col-sm-9">
                          <select name="f1" class="form-control form-control-sm" required>
                            <option value=""> Pilih Tema</option>
                             <?php
                            foreach($tm->result() as $r){
                            echo "<option value='".$r->id_tema."'>".$r->nama_tema."</option>";}
                        ?> 
                          </select>
                           </div>
                       
                      </div>
                        <div class="form-group row">
                        <input type="hidden" name="sekolah" class="form-control form-control-lg" value="<?=$this->session->userdata('id_sekolah');?>">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama SubTema</label>
                          <div class="col-sm-9">
                          <input  type="text" class="form-control form-control-sm" name="f2">
                           </div>
                      </div>
                     

                        <button type="submit"  onclick="alert('Data Anda Disimpan')" class="btn btn-success mr-2">Tambah</button>
                        <a href="<?php echo site_url('Subtheme/subtheme');?>" class="btn btn-danger" value="Batal"><i class="icon-remove"></i> Batal</a>
                      </form>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>