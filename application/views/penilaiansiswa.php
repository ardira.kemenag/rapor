<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
  <script>
            function get_sub(){
                var id_tema = $("#sub").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('penjadwalan/get_sub'); ?>", 
                    data:"id_tema="+id_tema, 
                    success: function(msg) {
                            $("#div_sub").html(msg);
                    }
                });
            }
        </script>
<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah Penilaian </h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Home/dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="<?php echo site_url('penilaian/nilai');?>">Penilaian</a></li>
                       <!--  <li class="breadcrumb-item active" aria-current="page">Tambah penilaian</li> -->
                      </ol>
                    </nav>
                     
                      <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                     <form method="post" action="<?php echo site_url('penilaian/nilai_siswa');?>">
                          <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-2 col-form-label">Tahun Ajaran</label>
                          <div class="col-sm-8">
                         <select name="thn" class=" form-control form-control-sm">
                           <option value=""> Pilih tahun ajaran </option>
                             <?php
                            foreach($tahun->result() as $r){
                            echo "<option value='".$r->id_tahunajaran."'>".$r->nama_tahun."</option>";}
                        ?> 
                         </select>
                           </div>
                     
                      
                          <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Cari
                                </button>
                            </span> </div>
                        </form>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No Induk </th>
                         <th><center>Nama</center></th>
                         <th><center>Nama Kelompok </center></th>
                         <th><center>Tahun Ajaran</center></th>
                        <th><center>Tempat lahir</center></th>
                        <th><center>Agama</center></th>
                        <th><center>Aksi</center></th>
                      </tr>
                    </thead>
                    
                 <tbody>
                        <?php
                        $no = 1;
                        foreach($siswa->result() as $b){
                        ?>
                        <tr>
                        <td><center> <?php echo $b->no_induk?></center></td>
                        <td><center> <?php echo $b->nama_siswa ?></center></td>
                        <td><center> <?php echo $b->nama_kelas ?></center></td>
                        <td><center> <?php echo $b->nama_tahun ?></center></td>
                        <td><center> <?php echo $b->tempat_lahir ?></center></td>
                        <td><center> <?php echo $b->agama ?></center></td>
                        <td><center>
                          <a class='btn btn-primary btn-sm' href="<?php echo site_url('penilaian/isi_nilai/').$b->id_siswa.'/'.$b->id_kelas?>">
                            <i class='icon-note'></i> Nilai</a>
                          </center></td>
                            
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                </div>
                </div>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>