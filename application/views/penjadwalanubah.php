<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
  <script>
            function get_sub(){
                var id_tema = $("#tema").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('penjadwalan/get_sub'); ?>", 
                    data:"id_tema="+id_tema, 
                    success: function(msg) {
                            $("#div_sub").html(msg);
                    }
                });
            }
        </script>
        <script>
            function get_subsub(){
                var id_subtema = $("#sub").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('Penjadwalan/get_subsub'); ?>", 
                    data:"id_subtema="+id_subtema, 
                    success: function(msg) {
                            $("#div_subsub").html(msg);
                    }
                });
            }
        </script>
<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        <?php
                                  $datajdw= $dtjdw->result();
                                  ?>
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Ubah Perencanaan</h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Home/dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Penjadwalan/jadwal');?>">Perencanaan</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Ubah perencanaan</li>
                      </ol>
                    </nav>
                      <form class="col-sm-12" action="<?php echo site_url('Penjadwalan/aksi_ubah/'.$this->uri->segment(3));?>" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tahun Ajaran</label>
                          <div class="col-sm-9">
                          <select required name="thn" class="form-control form-control-sm">
                               <?php
                        foreach ($thn as $k) {
                        ?>
                          <option value="<?php echo $k->id_tahunajaran; ?>" <?php if($datajdw[0]->id_tahunajaran==$k->id_tahunajaran)echo "selected"; ?>><?php echo $k->nama_tahun; ?></option>
                              <?php
                              } ?>
                          </select>

                          
                         
                           </div>
                      </div>
                      <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Semester</label>
                          <div class="col-sm-9">
                          <select required name="smt" class="form-control form-control-sm">

                               <?php
                        foreach ($smt as $k) {
                        ?>
                          <option value="<?php echo $k->id_semester; ?>" <?php if($datajdw[0]->id_semester==$k->id_semester)echo "selected"; ?>><?php echo $k->nama_semester; ?></option>
                              <?php }?>
                          </select>
                           </div>
                       
                      </div>
                       <div class="form-group row">
                    <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tanggal Awal</label>
                      <div class="col-sm-3">
                       <input  type="date" class="form-control form-control-sm" name="f1" value="<?php echo $datajdw[0]->tanggal_awal; ?>">
                       </div>
                       <label for="exampleInputPassword2" class="col-sm-2 col-form-label">Tanggal Akhir</label>
                          <div class="col-sm-3">
                       <input  type="date" class="form-control form-control-sm" name="tglakhir" value="<?php echo $datajdw[0]->tanggal_akhir; ?>">
                           </div>
                      </div>
                        <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Kelompok</label>
                          <div class="col-sm-9">
                           <select class="js-example-basic-multiple" multiple="multiple" style="width:100%" class="form-control form-control-sm" name="f2[]" id="f72">

                            <?php
                             $m= json_decode($datajdw[0]->id_kelas);  
                            foreach ($kelas as $k) {
                        ?>
                          <option value="<?php echo $k->id_kelas; ?>"  <?php if(in_array($k->id_kelas, $m)) echo "selected"; ?>><?php echo $k->nama_kelas; ?></option>
                              <?php
                              } ?>
                        </select>
                           </div>
                       
                      </div>
                      <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Aspek</label>
                          <div class="col-sm-9">
                             <select required name="f3" class="form-control"> 
                             
                               <?php
                        foreach ($prg as $k) {
                        ?>
                          <option value="<?php echo $k->id_program; ?>" <?php if($datajdw[0]->id_program==$k->id_program)echo "selected"; ?>><?php echo $k->nama_program; ?></option>
                              <?php }?>
                                       </select>
                           </div>
                      </div>
                          <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> Tema</label>
                          <div class="col-sm-9">
                         <select name="f4" class=" form-control form-control-sm" id="tema" onchange="get_sub()">
                           
                               <?php
                        foreach ($tema as $k) {
                        ?>
                          <option value="<?php echo $k->id_tema; ?>" <?php if($datajdw[0]->id_tema==$k->id_tema)echo "selected"; ?>><?php echo $k->nama_tema; ?></option>
                              <?php }?>
                            
                         </select>
                           </div>
                           </div>
                        <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label">Subtema</label>
                        <div class="col-sm-9" id="div_sub">
                          <select name="f5" class=" form-control form-control-sm" id="sub" onchange="get_subsub();">
                          
                               <?php
                        foreach ($sub as $k) {
                        ?>
                          <option value="<?php echo $k->id_subtema; ?>" <?php if($datajdw[0]->id_subtema==$k->id_subtema)echo "selected"; ?>><?php echo $k->nama_subtema; ?></option>
                              <?php }?>
                         </select>
                        </div>
                        </div>
                          <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label">Subsubtema</label>
                        <div class="col-sm-9" id="div_subsub">
                          <select name="fsubsub" class=" form-control form-control-sm">
                             <?php
                        foreach ($subsub as $k) {
                        ?>
                          <option value="<?php echo $k->id_subsubtema; ?>" <?php if($datajdw[0]->id_subsubtema==$k->id_subsubtema)echo "selected"; ?>><?php echo $k->nama; ?></option>
                              <?php }?>
                         </select>
                        </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Kompetensi Dasar</label>
                        <div class="col-sm-9">
                        <select class="js-example-basic-multiple" multiple="multiple" style="width:100%" class="form-control form-control-sm" name="f6[]" id="f6">
                          
                         <?php 
                           $c= json_decode($datajdw[0]->kd); 
                            
                            foreach ($kd as $b) {
                        ?>
                          <option value="<?php echo $b->id_kd; ?>"  <?php if(in_array($b->id_kd, $c)) echo "selected"; ?>><?php echo "(".$b->kode_kd.")".$b->nama_kd; ?></option>
                              <?php
                              } ?>
                        </select>
                        </div>
                      </div>
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Indikator</label>
                        <div class="col-sm-9">
                        <select class="js-example-basic-multiple" multiple="multiple" style="width:100%" class="form-control form-control-sm" name="f7[]" id="f7">
                          
                        
                         <?php 
                           $c= json_decode($datajdw[0]->id_indikator); 
                           
                            foreach ($ind as $b) {
                        ?>
                          <option value="<?php echo $b->id_indikator; ?>"  <?php if(in_array($b->id_indikator, $c)) echo "selected"; ?>><?php echo $b->nama_indikator; ?></option>
                              <?php
                              } ?>
                        </select>
                        </div>
                      </div>
                       <!-- <div class="form-group row"> 
                    <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Materi</label>
                          <div class="col-sm-9">
                          <input  type="Text" class="form-control form-control-sm" name="mtr">
                           </div>
                      </div>-->
                      
                        <button type="submit"   class="btn btn-success mr-2">Submit</button>
                        <a href="<?php echo site_url('Penjadwalan/jadwal');?>" class="btn btn-danger" value="Batal"><i class="icon-remove"></i> Batal</a>
                      </form>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>