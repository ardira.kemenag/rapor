<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">
                <div class="card-body">
                  <?php echo $this->session->flashdata('msg2');?>
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home/dashboard')?>">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data Tema </li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Data Tema </h4>
                  </div> 
                  <div class="col-lg-6 text-right">
                    <a  class="btn btn-light btn-sm"  href="<?php echo site_url('Tema')?>">Tambah</a>
                  </div>
                <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No</th>
                         <th> Nama Tema </th>
                        <th width="25%"> Aksi </th>
                      </tr>
                    </thead>
                    
                 <tbody>
                        <?php
                        $no = 1;
                        foreach($tm->result() as $b){
                        ?>
                        <tr>
                          <td> <?php echo $no++ ;?>
                          <td>  <?php echo $b->nama_tema?> </td>
                        <td> 
                          <a class='btn btn-primary btn-sm-xs' href="<?php echo site_url('tema/temaubah/').$b->id_tema.'/'?>"> <i class='icon-pencil'></i> Ubah</a>
                           <?php 
                             $where=  array('id_tema' => $b->id_tema );
                             $data = $this->model_m->selectX('subtema',$where);   
                              $row = $data->row();  
                           if(!isset($row)) { ?>
                                <a class='btn btn-primary btn-sm-xs' onClick='return konfirmasi();' href="<?php echo site_url('tema/hapus_tema/').$b->id_tema?>">
                            <i class='icon-trash'></i> hapus</a>
                          <?php }else{
                            echo "";
                                 } ?>
                           </td>
                            
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
  <script type="text/javascript" language="JavaScript">
 function konfirmasi()
 {
 tanya = confirm("Anda Yakin Menghapus Data Ini ?");
 if (tanya == true) return true;
 else return false;
 }</script>
</body>

</html>