<?php
    $objPHPExcel = new PHPExcel();
      // Set document properties
      $objPHPExcel->getProperties()->setCreator("indah")
                     ->setLastModifiedBy("indah")
                     ->setTitle("Office 2007 XLSX Test Document")
                     ->setSubject("Office 2007 XLSX Test Document")
                     ->setDescription("Riwayat")
                     ->setKeywords("office 2007 openxml php")
                     ->setCategory("Test result file");


      // Add some data
      $styleArray = array(
          'font'  => array(
          'bold'  => true,
          'size'  => 15,
      ));
      $objPHPExcel->setActiveSheetIndex(0);
      $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
      $objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('A')->setAutosize(true);
      $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setAutosize(true);
      $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('C')->setAutosize(true);
      $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('D')->setAutosize(true);
      $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('E')->setAutosize(true);
      $objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:F1')->applyFromArray($styleArray);
      $objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:F2')->getFont()->setBold(true);
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Riwayat Visitor');
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'program');

                         $no = 1;
                          foreach ( $mgg->result() as $m )
                          { 
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.($i+2), $m->tanggal_awal);
     }     
      for ($i=1;$i<=count($riwayat);$i++){
        $object = $riwayat[$i-1];
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.($i+2), $object->nama_siswa);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.($i+2), $object->nama_program);
       
      }
      // Rename worksheet
      $objPHPExcel->getActiveSheet()->setTitle('Riwayat');


      // Set active sheet index to the first sheet, so Excel opens this as the first sheet
      $objPHPExcel->setActiveSheetIndex(0);


      // Redirect output to a client’s web browser (Excel2007)
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment;filename="riwayatvisitor.xlsx"');
      header('Cache-Control: max-age=0');
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');

      // If you're serving to IE over SSL, then the following may be needed
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0

      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $objWriter->save('php://output');
      exit;

?>