<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('header.php'); ?>
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">
                <div class="card-body">
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home/dashboard')?>">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data Siswa</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Detail Siswa</h4>
                  </div> 
                  <?php
                                  $datasiswa = $dtsiswa->result();
                                  ?>
                            </div>
                              <form class="forms-sample" action="<?php echo site_url('Siswa/aksi_insert/');?>" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                     
                          <div class="col-sm-9">
                           <input type="hidden" name="sekolah" class="form-control form-control-lg" value="<?=$this->session->userdata('id_sekolah');?>">
                           </div>
                      </div>
                        
                        <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama Lengkap </label>
                         <div class="col-sm-5">
                           <input type="text" name="namaL" class="form-control form-control-lg" placeholder=" Nama lengkap" value="<?php echo $datasiswa[0]->nama_siswa?>">
                           </div>  <label for="exampleInputPassword2" class="col-form-label">Nama Panggilan </label>
                           <div class="col-sm-2">
                           <input type="text" name="namaP" class="form-control form-control-lg" placeholder=" nama panggilan" value="<?php echo $datasiswa[0]->nama_panggilan?>">
                        </div>
                      </div>

                         <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">No Induk</label>
                          <div class="col-sm-9">
                           <input type="text" name="noinduk" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->no_induk?>">
                           </div>
                      </div>
                      <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tempat/ Tanggal Lahir</label>
                          <div class="col-sm-5">
                           <input type="text" name="tlahir" class="form-control form-control-lg" value="<?php echo $datasiswa[0]->tempat_lahir?>"></div> 
                           <div class="col-sm-3">
                            <input type="date" name="tgllahir"  class="form-control form-control-lg" value="<?php echo $datasiswa[0]->tanggal_lahir?>" >
                           </div>
                      </div>
                      <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Agama</label>
                          <div class="col-sm-9">
                            <?php echo $datasiswa[0]->agama?>
                           </div>
                      </div>
                       <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Jenis kelamin</label>
                          <div class="col-sm-9">
                          <?php if ($datasiswa[0]->jenis_kelamin=='P'){
                             echo "Perempuan";
                          } 
                          else {
                            echo "Laki_laki";
                          }
                          ?>
                           </div>
                       
                      </div>
                      <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama Orang Tua</label>

                          <div class="col-sm-4">
                           <input type="text" name="ayah" class="form-control form-control-lg" placeholder="Ayah" value="<?php echo $datasiswa[0]->nama_ayah?>">
                           </div>
                           <div class="col-sm-4">
                           <input type="text" name="ibu" class="form-control form-control-lg" placeholder="Ibu" value="<?php echo $datasiswa[0]->nama_ibu?>">
                           </div>
                      </div>
                       <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Pekerjaan Orang Tua</label>
                     
                          <div class="col-sm-4">
                           <input type="text" name="payah" class="form-control form-control-lg" placeholder="Ayah" value="<?php echo $datasiswa[0]->pekerjaan_ayah?>">
                           </div>
                           <div class="col-sm-4">
                           <input type="text" name="pibu" class="form-control form-control-lg" placeholder="Ibu" value="<?php echo $datasiswa[0]->pekerjaan_ibu?>">
                           </div>
                      </div>
                       <div class="form-group row" >
                       <label  class="col-sm-3 col-form-label">Alamat Orangtua</label>
                      </div>
                       <div class="form-group row" >
                       <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Jalan</label>
                          <div class="col-sm-9">
                          <?php echo $datasiswa[0]->jalan?>
                           </div>
                           </div>
                          <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kelurahan/Desa/Dusun</label>
                          <div class="col-sm-9">
                           <?php echo $datasiswa[0]->kelurahan?>
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kecamatan</label>
                          <div class="col-sm-9">
                           <?php echo $datasiswa[0]->kecamatan?>
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Provinsi</label>
                          <div class="col-sm-9">
                        <?php echo $datasiswa[0]->nama_provinsi?>
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kota/Kabupaten</label>
                          <div class="col-sm-9" id="div_kota">
                        <?php echo $datasiswa[0]->nama_kota?>
                         
                           </div>
                           </div>
                      <div class="form-group row" >
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Wali Siswa</label>
                      </div>
                       <div class="form-group row" >
                       <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Nama</label>
                          <div class="col-sm-9">
                          <input type="text" name="wnama" class="form-control form-control-lg" >
                           </div>
                           </div>
                          <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Alamat</label>
                          <div class="col-sm-9">
                          <input type="text" name="walamat" class="form-control form-control-lg" >
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Pekerjaan</label>
                          <div class="col-sm-9">
                          <input type="text" name="wkerja" class="form-control form-control-lg" >
                           </div>
                           </div>

                        <!-- <button type="submit" class="btn btn-success mr-2">Submit</button> 
                        <a href="<?php echo site_url('Siswa/siswa');?>" class="btn btn-danger" value="Batal"><i class="icon-remove"></i> Batal</a>-->
                      </form>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>