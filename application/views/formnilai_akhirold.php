<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 

 


 ?>
<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
      
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title"> Penilaian Perkembangan </h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Penilaian</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Penilaian</li>
                      </ol>
                    </nav>
                    <?php
                    $dataS = $siswa->result();
                    ?>
                    <div class="form-group row">
                    <!--  <input type="hidden" name="fid"  class="form-control form-control-lg" value="<?php echo $dataS[0]->id_siswa?>"> -->
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama Siswa</label>
                          <div class="col-sm-9">
                            <input type="Text" name="nama"  class="form-control form-control-lg" value="<?php echo $dataS[0]->nama_siswa?>" disabled>
                            <input type="hidden" name="siswa"  class="form-control form-control-lg" value="<?php echo $dataS[0]->id_siswa?>" >
                          </div>
                        </div>
                        
                  
                     <form method="post" action="<?php echo site_url('Penilaian/report_perkembangan/'.$dataS[0]->id_siswa);?>" style="">
                        <div class="form-group row">
                         
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Semester</label>
                          <div class="col-sm-6">
                            <select required  name="smt" class="form-control" >
                              <?php if($this->input->post('smt'));{
                                $s=$this->input->post('smt');
                                 if ($s==1){
                                  echo $select='Semester 1';
                                }elseif($s==2){
                                  echo $select='Semester 2';
                                }else{
                                  echo $select='Pilih Semester';
                                }?>
                                 <option value="<?php echo $s?>" selected><?php echo $select;?></option>
                              <?php } ?>
                              <option value="1"> Semester 1</option>
                               <option value="2"> Semester 2</option>

                            </select>
                          </div>
                          <div class="col-sm-3"> <button class="btn btn-success btn-xs mr-2" >Cari</button></div>
                        </div>
                      </form>         
             <?php  if($this->input->post('smt')){
            $sem= $this->input->post('smt') ?>

                    <?php  
                        $no = '1';
                foreach($program->result() as $b){
                         $s= $this->input->post('smt');
                            $tampil=$this->model_m->perkembangan($dataS[0]->id_siswa,$b->id_program,$s);
                              $row = $tampil->row();
                      if(!isset($row)) {
                        $title='Submit';
                        ?> 
                        <form class="forms-sample" action="<?php echo site_url('Penilaian/perkembangan_aksi/'.$dataS[0]->id_siswa);?>" method="post" enctype="multipart/form-data">   
                          <div class="form-group row" >
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Deskripsi</label>
                             <div class="col-sm-9"> 
                               <input type="hidden" name="fid"  class="form-control form-control-lg" value="<?php echo $dataS[0]->id_siswa?>">
                              <input type="hidden" name="id[]" value="<?php echo $b->id_program?>" class="form-control form-control-lg" >
                              <input type="hidden" name="sem"  class="form-control form-control-lg" value="<?php echo $sem?>">
                             <input type="text" name="" value="<?php echo $b->nama_program?>" class="form-control form-control-lg" readonly> 
                           </div>
                         <label for="exampleInputPassword2" class="col-sm-3 col-form-label"></label>
                          <div class="col-sm-9"> 
                        <textarea class="form-control" name="deskripsi<?php echo $b->id_program?>"></textarea>
                      </div>
                    </div>

                    <?php 
                  }else{
                      foreach ($tampil->result() as $t) { 
                        $title='Ubah';?>
                        <form class="forms-sample" action="<?php echo site_url('Penilaian/perkembangan_aksi_ubah/'.$dataS[0]->id_siswa);?>" method="post" enctype="multipart/form-data"> 
                        <div class="form-group row" >

                        <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Deskripsi</label>
                        <div class="col-sm-9"> 
                          <input type="hidden" name="pid"  class="form-control form-control-lg" value="<?php echo $t->id_perkembangan?>">
                         
                        <input type="hidden" name="id[]" value="<?php echo $b->id_program?>" class="form-control form-control-lg" >
                        <input type="hidden" name="sem"  class="form-control form-control-lg" value="<?php echo $sem?>">
                         <input type="text" name="" value="<?php echo $b->nama_program?>" class="form-control form-control-lg" readonly> </div>
                         <label for="exampleInputPassword2" class="col-sm-3 col-form-label"></label>
                          <div class="col-sm-9"> 
                        <textarea class="form-control" name="deskripsi<?php echo $b->id_program?>"><?php echo $t->deskripsi?></textarea>
                      </div>
                    </div>
             <?php  }
                   } 
           }?><button type="submit"   class="btn btn-success mr-2"><?php echo $title;?></button>
                        <a href="<?php echo site_url('report/Perkembangan_akhir');?>" class="btn btn-danger" value="Batal"><i class="icon-remove"></i> Batal</a>
       <?php  }?>
                   
                 
                   

               
                      
                       
                      </form>
                    </div>
                  </div>
                </div>
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
 


 <?php $this->load->view('footer.php'); ?>
</body>

</html>