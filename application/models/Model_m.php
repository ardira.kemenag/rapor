<?php
	class Model_m extends CI_Model {

		  public function cekLogin($username,$password) {
      $sql = "SELECT * FROM user
        WHERE username = '$username' AND password = '$password'";
      return $this->db->query($sql);
    } 
		 function selectX($tbl,$where){
        return $this->db->get_where($tbl,$where);
      }
      function input_data($tbl,$data){
      $this->db->insert($tbl,$data);
    }
        
        public function get_by_cookie($cookie)
    {
        $this->db->where('cookie', $cookie);
        return $this->db->get($this->table);
    }
    
        function update_data($tbl,$data,$where){
            $this->db->where($where);
      $this->db->update($tbl,$data);
    }
      
       public function selectSemua($tbl){
      return $this->db->get($tbl);//nama tabel
      } 
      
      function delete_data($tbl,$where){
            $this->db->delete($tbl,$where);
      }
      function hapus_data($tbl){
            $this->db->truncate($tbl); 
      }
       function getkodeunik() { 
            $kodejadi = "Guru".time();
            return $kodejadi;
        }
       public function ra()
      {
        $query = "SELECT * from sekolah    ";

        return $this->db->query($query);

      }
      public function akunguru($id_sekolah)
      {
        $query = "SELECT * from user as u join guru as g on g.id_guru=u.id_guru where u.id_sekolah='$id_sekolah'    ";

        return $this->db->query($query);

      }

       public function dataInnerJoin()
      {
        $query = "SELECT *, week(tanggal_awal) as mingguke  FROM nilai as p JOIN siswa as s ON p.id_siswa = s.id_siswa join perencanaan as pr on pr.id_perencanaan=p.id_jadwal  ORDER BY `mingguke` ASC  ";

        return $this->db->query($query);

      }
       public function datasiswa($id_sekolah)
      {
        
        $query = "SELECT * from siswa as s join kelas as k on k.id_kelas=s.id_kelas join tahun_ajaran as t on t.id_tahunajaran=s.id_tahunajaran where s.id_sekolah='$id_sekolah'  ";

        return $this->db->query($query);

      }
       public function datasiswa2($id_sekolah,$id_kelas)
      {
        $query = "SELECT * from siswa as s join kelas as k on k.id_kelas=s.id_kelas where s.id_sekolah='$id_sekolah' and s.id_kelas='$id_kelas'  ";

        return $this->db->query($query);}

      
      public function datasiswafilter($id_sekolah,$id_tahunajaran)
      {
        $query = "SELECT * from siswa as s join kelas as k on k.id_kelas=s.id_kelas join tahun_ajaran as t on t.id_tahunajaran=s.id_tahunajaran where s.id_sekolah='$id_sekolah' and s.id_tahunajaran='$id_tahunajaran'  ";

        return $this->db->query($query);

      }
     
       public function dataguru($id_sekolah)
      {
        $query = "SELECT * from guru as s  where s.id_sekolah='$id_sekolah'  ";

        return $this->db->query($query);

      }
       public function datakelas($id_sekolah)
      {
        $query = "SELECT * from kelas as k  where k.id_sekolah='$id_sekolah'  ";

        return $this->db->query($query);

      }
      public function sklh($id_sekolah)
      {
        $query = "SELECT * from sekolah as s join user as u on s.id_sekolah=u.id_sekolah  where u.id_sekolah='$id_sekolah'  ";

        return $this->db->query($query);

      }
      
      public function jadwal($id_sekolah)
      {
        $query = "SELECT *,week(tanggal_awal) as mingguke, j.id_kd as kd from perencanaan as j join program as p on p.id_program=j.id_program join kelas as k on k.id_kelas=j.id_kelas join guru as g on g.id_guru=k.id_guru join tema as t on t.id_tema=j.id_tema join subtema as s on s.id_subtema=j.id_subtema where g.id_sekolah='$id_sekolah' order by j.tanggal_awal";

        return $this->db->query($query);

     } 
 public function jadwalfilter($id_sekolah,$id_tahunajaran)
      {
        $query = "SELECT *,week(tanggal_awal) as mingguke, j.id_kd as kd from perencanaan as j join program as p on p.id_program=j.id_program join kelas as k on k.id_kelas=j.id_kelas join guru as g on g.id_guru=k.id_guru join tema as t on t.id_tema=j.id_tema join subtema as s on s.id_subtema=j.id_subtema where g.id_sekolah='$id_sekolah' and j.id_tahunajaran='$id_tahunajaran' order by j.tanggal_awal";

        return $this->db->query($query);

     } 
       
      public function sekolah()
      {
        $query = "SELECT * from sekolah as s join provinsi as p on p.id_provinsi=s.id_provinsi join kota_kab as k on k.id_kotaKab=s.id_kotaKab   ";

        return $this->db->query($query);

      }
      public function kelas($id_sekolah)
      {
        $query = "SELECT * from kelas as k join guru as g on g.id_guru=k.id_guru  where k.id_sekolah='$id_sekolah'";

        return $this->db->query($query);

     } 
     public function guru($id_sekolah)
      {
        $query = "SELECT * from guru as g  where g.id_sekolah='$id_sekolah'  ";

        return $this->db->query($query);

      }
       public function kelasjoin($id_kelas)
      {
        $query = "SELECT * from kelas as k join guru as g on g.id_guru=k.id_guru  where k.id_kelas='$id_kelas'";

        return $this->db->query($query);

     }
      public function tema($id_sekolah)
      {
        $query = "SELECT * from tema as t  where t.id_sekolah='$id_sekolah'";

        return $this->db->query($query);

     }
      public function subtema($id_sekolah)
      {
        $query = "SELECT * from subtema as s join tema as t on s.id_tema=t.id_tema where t.id_sekolah='$id_sekolah'";

        return $this->db->query($query);

     }
      public function subsubtema($id_sekolah)
      {
        $query = "SELECT * from subsubtema as s join tema as t on s.id_tema=t.id_tema join subtema as st on st.id_subtema=s.id_subtema where t.id_sekolah='$id_sekolah'";

        return $this->db->query($query);

     }
     public function kd()
      {
        $query = "SELECT * from kd as k  ";

        return $this->db->query($query);

     }
      public function joinindikator()
      {
        $query = "SELECT * from indikator as i join program as p on p.id_program= i.id_program  ";

        return $this->db->query($query);

     }

      function nilaiubah($kode_indikator)
    {
         $this->db->select('*');
        $this->db->from('nilai');
       
        $this->db->where('kode_indikator','$kode_indikator');
      
        return $this->db->get();
    }
     
     public function jdw($id_siswa)
      {
        $query = "SELECT * from perencanaan as p  join nilai as pr on pr.id_jadwal=p.id_perencanaan where pr.id_siswa='$id_siswa'";

        return $this->db->query($query)->result();

     }
     public function jdwa($id_sekolah)
      {
        $query = "SELECT * from perencanaan as p  join program as pr on pr.id_program=p.id_program join tema as t on t.id_tema=p.id_tema where t.id_sekolah='$id_sekolah' ";

        return $this->db->query($query)->result();

     } 
     public function dataharian()
      {
        $query = "SELECT * from nilai as p join siswa as s on p.id_siswa= s.id_siswa join perencanaan as pr on pr.id_perencanaan=p.id_jadwal join tema as t on t.id_tema=pr.id_tema join subtema as st on st.id_subtema=pr.id_subtema join program as pg on pg.id_program=pr.id_program";

        return $this->db->query($query);

     }
      public function hariankelompok($id_kelas)
      {
        $query = "SELECT * from nilai as p join siswa as s on p.id_siswa= s.id_siswa join perencanaan as pr on pr.id_perencanaan=p.id_jadwal join tema as t on t.id_tema=pr.id_tema join subtema as st on st.id_subtema=pr.id_subtema where s.id_kelas='$id_kelas'";

        return $this->db->query($query);

     }
     public function dataharianfilter($id_siswa)
      {
        $query = "SELECT * from nilai as p join siswa as s on p.id_siswa= s.id_siswa join perencanaan as pr on pr.id_perencanaan=p.id_jadwal join tema as t on t.id_tema=pr.id_tema join subtema as st on st.id_subtema=pr.id_subtema join program as aspek on aspek.id_program=pr.id_program where p.id_siswa='$id_siswa' ";

        return $this->db->query($query);

     }
    
      public function nilaiscph($tanggal_awal,$id_siswa)
      {
        $query = "SELECT *, p.id_indikator as jadwal from nilai as n  join perencanaan as p on n.id_jadwal=p.id_perencanaan join siswa as s on s.id_siswa=n.id_siswa join program as aspek on aspek.id_program=p.id_program join indikator as i on i.kode_indikator=n.kode_indikator join teknik_penugasan as t on t.id_teknik=n.id_teknik where p.tanggal_awal='$tanggal_awal' and n.id_siswa='$id_siswa' group by n.kode_indikator ";

        return $this->db->query($query);

     }

       public function tgl($bulan)
      {
        $query = "SELECT *, p.id_indikator as jadwal ,month(tanggal_awal) as bulan from perencanaan as p WHERE month(tanggal_awal)='$bulan'";

        return $this->db->query($query);

     }
     public function tglsmt($id_semester,$id_tahunajaran)
      {
        $query = "SELECT *, p.id_indikator as jadwal from perencanaan as p WHERE id_semester='$id_semester' and id_tahunajaran='$id_tahunajaran'";

        return $this->db->query($query);

     }
       function rkpbulansend($bulan,$id_tahunajaran)
    {
         $this->db->select('*');
       $this->db->select('month(tanggal_awal) as bulan');
       $this->db->select('perencanaan.id_program as program ');
         $this->db->select('perencanaan.id_indikator as jadwal ');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
       $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
        $this->db->where('siswa.id_tahunajaran',$id_tahunajaran);
        $this->db->where('month(tanggal_awal)',$bulan);
         $this->db->group_by('nilai.id_siswa');
       
        return $this->db->get();
      }
       function rkpbulan($bulan,$id_siswa)
    {
         $this->db->select('*');
       $this->db->select('month(tanggal_awal) as bulan');
       $this->db->select('perencanaan.id_program as program ');
         $this->db->select('perencanaan.id_indikator as jadwal ');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
       $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
        $this->db->where('nilai.id_siswa',$id_siswa);
        $this->db->where('month(tanggal_awal)',$bulan);
         $this->db->group_by('perencanaan.id_program');
       
        return $this->db->get();
      }
       function rkpsmt($id_siswa,$id_semester)
    {
         $this->db->select('*');
       $this->db->select('month(tanggal_awal) as bulan');
       $this->db->select('perencanaan.id_program as program ');
         $this->db->select('perencanaan.id_indikator as jadwal ');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
       $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
        $this->db->where('nilai.id_siswa',$id_siswa);
        $this->db->where('perencanaan.id_semester',$id_semester);
         $this->db->group_by('perencanaan.id_program');
       
        return $this->db->get();
      }
    
    
       function searchscph($id_siswa,$tanggal_awal)
    {
         $this->db->select('*');
       $this->db->select('week(perencanaan.tanggal_awal) as mingguke');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
        $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
        $this->db->where('nilai.id_siswa',$id_siswa);
        $this->db->where('perencanaan.tanggal_awal',$tanggal_awal);
         $this->db->group_by('nilai.kode_indikator');
       
        return $this->db->get();
    }

       function rkpminggu($id_siswa,$tanggal_awal)
    {
         $this->db->select('*');
       $this->db->select('week(perencanaan.tanggal_awal) as mingguke');
         $this->db->select('perencanaan.id_indikator as jadwal ');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
        $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
        $this->db->where('nilai.id_siswa',$id_siswa);
        $this->db->where('perencanaan.tanggal_awal',$tanggal_awal);
         $this->db->group_by('perencanaan.id_kd');
       
        return $this->db->get();
    } 
    function searchall($bulan,$tahun,$akhir)
    {
        
        $this->db->from('riwayat_visitor');
        $this->db->join('data_visitor', 'data_visitor.id_visitor = riwayat_visitor.id_visitor');
        $this->db->join('jenis_visitor', 'data_visitor.id_jenisvisitor = jenis_visitor.id_jenisvisitor');
         $this->db->join('list_referensi', 'riwayat_visitor.id_referensi = list_referensi.id_referensi');
         if ($keyword!="999"){
           $this->db->where('jenis_visitor.id_jenisvisitor',$keyword);
         }

         $this->db->where('riwayat_visitor.jam_masuk >=',$awal);
           $this->db->where('riwayat_visitor.jam_masuk  <=',$akhir);
            $this->db->where('jam_keluar!=', '0000-00-00 00:00:00');
       
        return $this->db->get();
    }
     public function rekaphari($tanggal_nilai,$id_siswa,$kode_indikator)
      {
        $query = "SELECT * from nilai as n  join siswa as s on s.id_siswa=n.id_siswa join indikator as i on i.kode_indikator=n.kode_indikator where n.tanggal_nilai='$tanggal_nilai' and n.id_siswa='$id_siswa' and n.kode_indikator='$kode_indikator'";

        return $this->db->query($query);

     }
      public function nilaiharian($tanggal_awal,$id_siswa)
      {
        $query = "SELECT * from  perencanaan as p where p.tanggal_awal='$tanggal_awal'  ";

        return $this->db->query($query);

     }
     public function jwdmgg($id_kelas)
      {
        $query = "SELECT * from perencanaan as p  join kelas as k on k.id_kelap.id_kelas where p.id_kelas='$id_kelas' ";

        return $this->db->query($query);

     }
     
     public function mingguan()
      {
        $query = "SELECT *,week(tanggal_awal) as mingguke from perencanaan as p group by tanggal_awal  ";

        return $this->db->query($query);
     }
     public function bulan()
      {
        $query = "SELECT *,month(tanggal_awal) as bulan from perencanaan as p GROUP by month(tanggal_awal) ";

        return $this->db->query($query);

     }
     public function bulanrekap($bulan,$id_siswa)
      {
        $query = "SELECT *,month(tanggal_awal) as bulan FROM `perencanaan`  join nilai on nilai.id_jadwal=id_perencanaan join program as pg on pg.id_program=perencanaan.id_program where month(tanggal_awal)='$bulan' and nilai.id_siswa='$id_siswa' GROUP by perencanaan.id_kd ";
        return $this->db->query($query);

     }

   
     public function rkpsmt1($id_siswa)
      {
        $query = "SELECT *, p.id_indikator as jadwal ,month(tanggal_awal) as bulan from perencanaan as p join nilai as n on n.id_jadwal=p.id_perencanaan join program as pr on pr.id_program=p.id_program join siswa as s on s.id_siswa=n.id_siswa WHERE n.id_siswa='$id_siswa' GROUP by p.id_indikator ";

        return $this->db->query($query);

     }
     public function kdbulan($bulan,$id_siswa,$id_program)
      {
        $query = "SELECT *, p.id_indikator as jadwal ,month(p.tanggal_awal) as bulan from perencanaan as p join nilai as n on n.id_jadwal=p.id_perencanaan join program as pr on pr.id_program=p.id_program join siswa as s on s.id_siswa=n.id_siswa WHERE month(tanggal_awal)='$bulan' and n.id_siswa='$id_siswa' and p.id_program='$id_program'  ";

        return $this->db->query($query);

     }
     public function harirkp($id_siswa,$kode_indikator,$jd)
      {
        $query = "SELECT *, max(n.nilai_perkembangan) as na from nilai as n  join siswa as s on s.id_siswa=n.id_siswa join indikator as i on i.kode_indikator=n.kode_indikator where  n.id_siswa='$id_siswa' and n.kode_indikator='$kode_indikator' and n.id_jadwal='$jd'";

        return $this->db->query($query);

     }
    public function listminggu($bulan){
       $query = "SELECT *,week(tanggal_awal) as mingguke , month(tanggal_awal) as bln FROM `perencanaan` where month(tanggal_awal)='$bulan' group by tanggal_awal";
       return $this->db->query($query);
    }
     public function listbulan($id_semester,$id_tahunajaran){
       $query = "SELECT *,month(tanggal_awal) as bulan FROM `perencanaan` where id_semester='$id_semester' and id_tahunajaran='$id_tahunajaran' group by month(tanggal_awal)";
       return $this->db->query($query);
    }
    public function dtsiswa($id_siswa)
      {
        $query = "SELECT * from siswa as s join kelas as k on k.id_kelas=s.id_kelas where s.id_siswa='$id_siswa'";

        return $this->db->query($query);

     }

           function dtjdw($tanggal_awal)
    {
       $this->db->select('*');
        $this->db->select('week(tanggal_awal) as mingguke');
        $this->db->from('perencanaan');
        $this->db->where('tanggal_awal',$tanggal_awal);
       
        return $this->db->get();
    }
        function dtsmt($id_semester)
    {
       $this->db->select('*');
        $this->db->select('week(tanggal_awal) as mingguke');
        $this->db->from('perencanaan');
        $this->db->where('id_semester',$id_semester);
       
        return $this->db->get();
    }

           function getminggu($id,$id_teknik,$id_siswa,$kode_indikator)
    {
        $this->db->select('MAX(nilai_perkembangan) as nilai');
        $this->db->from('nilai');
        $this->db->where('id_siswa',$id_siswa);
        $this->db->where('id_teknik',$id_teknik);
        $this->db->where('kode_indikator',$kode_indikator);
        $this->db->where_in('id_jadwal',$id);

        return $this->db->get();
    }
    function kdbln($id,$bulan,$id_siswa,$id_program)
    {
         $this->db->select('*');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
         $this->db->where('month(perencanaan.tanggal_awal)',$bulan);
        $this->db->where('id_siswa',$id_siswa);
        $this->db->where('id_program',$id_program);
       $this->db->where_in('id_perencanaan',$id);
        $this->db->group_by('id_kd');
        return $this->db->get();
    }
      function insmt($id,$id_semester,$id_siswa,$id_program)
    {
         $this->db->select('*');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
         $this->db->where('id_semester',$id_semester);
        $this->db->where('id_siswa',$id_siswa);
        $this->db->where('id_program',$id_program);
       $this->db->where_in('id_perencanaan',$id);
        $this->db->group_by('id_kd');
        return $this->db->get();
    }
    function inbln($id,$bulan,$id_siswa,$id_program,$mgg)
    {
         $this->db->select('*');
         $this->db->select('MAX(nilai_perkembangan) as nilai');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
         $this->db->where('month(perencanaan.tanggal_awal)',$bulan);
        $this->db->where('id_siswa',$id_siswa);
        $this->db->where('id_program',$id_program);
        $this->db->where('week(perencanaan.tanggal_awal)',$mgg);
       $this->db->where_in('id_perencanaan',$id);
        $this->db->group_by('id_program');
        return $this->db->get();
    }
    // public function blnrkp($bln,$id_siswa,$mgg)
    //   {
    //     $query = "SELECT * ,max(nilai_perkembangan)as na FROM `nilai` join perencanaan on perencanaan.id_perencanaan=nilai.id_jadwal where month(perencanaan.tanggal_awal)='0$bln' and id_siswa='$id_siswa' and week(perencanaan.tanggal_awal)='$mgg' GROUP by id_program' ";

    //     return $this->db->query($query);

    //  }
      function smt($id,$bulan,$id_siswa,$id_program)
    {
         $this->db->select('*');
         $this->db->select('MAX(nilai_perkembangan) as nilai');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
         $this->db->where('month(perencanaan.tanggal_awal)',$bulan);
        $this->db->where('id_siswa',$id_siswa);
        $this->db->where('id_program',$id_program);
       // $this->db->where('week(perencanaan.tanggal_awal)',$mgg);
       $this->db->where_in('id_perencanaan',$id);
        $this->db->group_by('month(tanggal_awal)');
        return $this->db->get();
    }
      function getminggurekap($id,$id_siswa,$kode_indikator)
    {
       $this->db->select('MAX(nilai_perkembangan) as nilai');
        $this->db->from('nilai');
        $this->db->where('id_siswa',$id_siswa);
        $this->db->where('kode_indikator',$kode_indikator);
        $this->db->where_in('id_jadwal',$id);
        return $this->db->get();
    }
     function getbulanrekap($id,$id_siswa,$ind,$mgg)
    {
       $this->db->select('MAX(nilai_perkembangan) as nilai');
        $this->db->from('nilai');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');

        $this->db->where('id_siswa',$id_siswa);
        $this->db->where('id_indikator',$ind);
       
         $this->db->where('week(tanggal_nilai)',$mgg);
        $this->db->where_in('id_jadwal',$id);
        return $this->db->get();
    }
    function getbulan($id,$id_siswa,$id_kd)
    {
        $this->db->select('MAX(nilai_perkembangan) as nilai');
        $this->db->from('nilai');
         $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->where('id_siswa',$id_siswa);
        $this->db->where('perencanaan.id_kd',$id_kd);
        $this->db->where_in('id_jadwal',$id);
        return $this->db->get();
    }
     
    
     public function getnilai($id_perencanaan)
      {
        $query = "SELECT * FROM `perencanaan` as p1 join program as p2 on p1.id_program=p2.id_program join tema as t on t.id_tema=p1.id_tema join subtema as s on s.id_subtema=p1.id_subtema WHERE id_perencanaan ='$id_perencanaan' ";

        return $this->db->query($query);

     }
        function getDataKD($kode_sk){
            $query="SELECT * FROM `kd` WHERE kode_sk ='$kode_sk'";
            $q=$this->db->query($query);    
            if ($q->num_rows() > 0){
                foreach($q->result() as $row){
                    $data[]=$row;
                }
                return $data;
            }
        }
         public function getrencana($id_perencanaan)
      {
        $query = "SELECT *, p1.id_kd as kd FROM `perencanaan` as p1 join program as p2 on p1.id_program=p2.id_program join tema as t on t.id_tema=p1.id_tema join subtema as s on s.id_subtema=p1.id_subtema WHERE id_perencanaan ='$id_perencanaan' ";

        return $this->db->query($query)->result();

     }
         function getJadwal($id_perencanaan){
            $query="SELECT *, p1.id_kd as kd FROM `perencanaan` as p1 join program as p2 on p1.id_program=p2.id_program join tema as t on t.id_tema=p1.id_tema join subtema as s on s.id_subtema=p1.id_subtema WHERE id_perencanaan ='$id_perencanaan'";
            $q=$this->db->query($query);    
            if ($q->num_rows() > 0){
                foreach($q->result() as $row){
                    $data[]=$row;
                }
                return $data;
            }
        }
        
         function getDatabln($id_siswa){
            // $query="SELECT * FROM `nilai` join siswa on siswa.id_siswa=nilai.id_siswa WHERE nilai.id_siswa ='$id_siswa' GROUP BY YEARWEEK(nilai.tanggal)";
          $query="SELECT *, s.nama_siswa,  pr.id_kd ,pr.tanggal_awal,COUNT(*) AS jumlah_mingguan,month(tanggal_awal) as bulan,year(tanggal_awal) as thn,week(tanggal_awal) as mingguke, max(if(month(tanggal_awal), nilai_perkembangan, 0)) AS rekapbulan from nilai as p JOIN siswa as s ON p.id_siswa = s.id_siswa join perencanaan as pr on pr.id_perencanaan=p.id_jadwal
            where p.id_siswa='$id_siswa'
            GROUP by month(tanggal_awal)";
             return $this->db->query($query)->result();
        }

          function getDataKota($nama_provinsi){
            $query="SELECT * FROM `kota_kab` join provinsi on provinsi.id_provinsi=kota_kab.id_provinsi WHERE provinsi.nama_provinsi ='$nama_provinsi'";
            $q=$this->db->query($query);    
            if ($q->num_rows() > 0){
                foreach($q->result() as $row){
                    $data[]=$row;
                }
                return $data;
            }
        }
         function getDatasubtema($id_tema){
            $query="SELECT * FROM `subtema` join tema on tema.id_tema=subtema.id_tema WHERE tema.id_tema ='$id_tema'";
            $q=$this->db->query($query);    
            if ($q->num_rows() > 0){
                foreach($q->result() as $row){
                    $data[]=$row;
                }
                return $data;
            }
        }
        function getDatasubsubtema($id_subtema){
            $query="SELECT * FROM `subsubtema` join subtema on subtema.id_subtema=subsubtema.id_subtema WHERE subtema.id_subtema ='$id_subtema'";
            $q=$this->db->query($query);    
            if ($q->num_rows() > 0){
                foreach($q->result() as $row){
                    $data[]=$row;
                }
                return $data;
            }
        }
         function getDataKota1($id_provinsi){
            $query="SELECT * FROM `kota_kab` join provinsi on provinsi.id_provinsi=kota_kab.id_provinsi WHERE provinsi.id_provinsi ='$id_provinsi'";
            $q=$this->db->query($query);    
            if ($q->num_rows() > 0){
                foreach($q->result() as $row){
                    $data[]=$row;
                }
                return $data;
            }
        }
         function getDatamgg1($id_siswa){
            // $query="SELECT s.nama_siswa, p.id_kti, p.id_ktd ,p.tanggal,p.nilai_perkembangan,week(tanggal) as mingguke, max(if(week(tanggal), nilai_perkembangan, 0)) AS rekapminggu where p.id_siswa='$id_siswa' GROUP by week(tanggal)  ORDER BY `mingguke` ASC)";
          
         // SELECT *,s.nama_siswa, p.id_ki, p.id_kd ,p.tanggal,COUNT(*) AS jumlah_mingguan,p.nilai_perkembangan,week(tanggal) as mingguke, max(if(week(tanggal), nilai_perkembangan, 0)) AS rekapminggu from nilai as p JOIN siswa as s ON p.id_siswa = s.id_siswa join kti as t on t.id_kti=p.id_kti join ktd as d on d.id_ktd=p.id_ktd  
             $query="SELECT *,  p.id_kd as kd ,p.tanggal_awal,COUNT(*) AS jumlah_mingguan,week(tanggal_awal) as mingguke, max(if(week(tanggal_awal), nilai_perkembangan, 0)) AS rekapminggu from perencanaan as p  join tema as t on t.id_tema=p.id_tema join subtema as sb on sb.id_subtema=p.id_subtema join nilai  as n on n.id_jadwal=p.id_jadwal join siswa as s on s.id_siswa=n.id_siswa
             join program as pg on pg.id_program=p.id_program

              where p2.id_siswa='$id_siswa' 

              GROUP by week(tanggal_awal) 
              ORDER BY `mingguke` ASC";
            return $this->db->query($query)->result();
        }
        public function cek_kd($id)
  {
    $query = $this->db->get_where('kd',array('id_kd'=>$id))->row();
    
    return $query->nama_kd;
  }
    public function cek_kd2($id)
  {
    $query = $this->db->get_where('kd',array('kode_kd'=>$id))->row();
    
    return $query->nama_kd;
  }
  public function cek_kd3($id)
  {
    $query = $this->db->get_where('kd',array('id_kd'=>$id))->row();
    
    return $query->kode_kd;
  }
    public function cek_ind($id)
  {
    $query = $this->db->get_where('indikator',array('id_indikator'=>$id))->row();
    
    return $query->nama_indikator;
  }
   public function cek_ind2($id)
  {
    $query = $this->db->get_where('indikator',array('id_indikator'=>$id))->row();
    
    return $query->kode_indikator;
  }
   public function cek_ind3($id)
  {
    $query = $this->db->get_where('indikator',array('kode_indikator'=>$id))->row();
    
    return $query->nama_indikator;
  }
       public function details($id_siswa)
      {
        $query = "SELECT * from siswa as s join kota_kab as k on k.nama_kotaKab=s.id_kotaKab 
        join kelas as kl on kl.id_kelas=s.id_kelas join sekolah as sk on sk.id_sekolah =s.id_sekolah join provinsi as p on sk.id_provinsi=p.id_provinsi  where s.id_siswa='$id_siswa'  ";

        return $this->db->query($query);

      }
       public function detailsw($id_siswa)
      {
        $query = "SELECT *, s.jalan as jln, s.kelurahan as kel, s.kecamatan as kec, s.foto as pic, s.provinsi as prov  from siswa as s 
        join kelas as kl on kl.id_kelas=s.id_kelas join sekolah as sk on sk.id_sekolah =s.id_sekolah join provinsi as p on p.id_provinsi=sk.id_provinsi   where s.id_siswa='$id_siswa'  ";

        return $this->db->query($query);

      }
             public function ubahind($kode_indikator,$tanggal_nilai)
      {
        $query = "SELECT * from nilai  as n join indikator as i on i.kode_indikator=n.kode_indikator where n.kode_indikator='$kode_indikator' and tanggal_nilai='$tanggal_nilai' ";

        return $this->db->query($query);

      }

      function getDataSiswa($id_tahunajaran,$id_sekolah){
            $query="SELECT * FROM `siswa` WHERE id_tahunajaran ='$id_tahunajaran' and id_sekolah='$id_sekolah'";
            $q=$this->db->query($query);    
            if ($q->num_rows() > 0){
                foreach($q->result() as $row){
                    $data[]=$row;
                }
                return $data;
            }
        }
        function getDataMgg($id_tahunajaran){
            $query="SELECT *,week(tanggal_awal) as mingguke FROM `perencanaan` WHERE id_tahunajaran ='$id_tahunajaran' group by week(tanggal_awal) ";
            $q=$this->db->query($query);    
            if ($q->num_rows() > 0){
                foreach($q->result() as $row){
                    $data[]=$row;
                }
                return $data;
            }
        }
        function getBln($id_tahunajaran){
            $query="SELECT *,month(tanggal_awal) as bulan FROM `perencanaan` WHERE id_tahunajaran ='$id_tahunajaran' group by month(tanggal_awal) ";
            $q=$this->db->query($query);    
            if ($q->num_rows() > 0){
                foreach($q->result() as $row){
                    $data[]=$row;
                }
                return $data;
            }
        }
          function getSmt($id_tahunajaran){
            $query="SELECT *,month(tanggal_awal) as bulan FROM `perencanaan` WHERE id_tahunajaran ='$id_tahunajaran' group by id_semester ";
            $q=$this->db->query($query);    
            if ($q->num_rows() > 0){
                foreach($q->result() as $row){
                    $data[]=$row;
                }
                return $data;
            }
        }
        
      function getDataSub($id_tema){
            $query="SELECT * FROM `subtema` WHERE id_tema ='$id_tema'";
            $q=$this->db->query($query);    
            if ($q->num_rows() > 0){
                foreach($q->result() as $row){
                    $data[]=$row;
                }
                return $data;
            }
        }

     
    public function rencana($id_sekolah)
      {
        $query = "SELECT *,week(tanggal_awal) as mingguke, j.id_kd as kd from perencanaan as j join program as p on p.id_program=j.id_program  join tema as t on t.id_tema=j.id_tema join subtema as s on s.id_subtema=j.id_subtema where t.id_sekolah='$id_sekolah'  order by j.tanggal_awal";

        return $this->db->query($query);

     }
   public function rencana2($id_perencanaan)
      {
        $query = "SELECT *,week(tanggal_awal) as mingguke, j.id_kd as kd from perencanaan as j join program as p on p.id_program=j.id_program  join tema as t on t.id_tema=j.id_tema join subtema as s on s.id_subtema=j.id_subtema  where j.id_perencanaan='$id_perencanaan' order by j.tanggal_awal";

        return $this->db->query($query);

     }
    public function cek_kls($id)
  {
    $query = $this->db->get_where('kelas',array('id_kelas'=>$id))->row();
    
    return $query->nama_kelas;
  }
      public function cek_kls2($id)
  {
     $this->db->join('guru', 'guru.id_guru=kelas.id_guru');
    $query = $this->db->get_where('kelas',array('id_kelas'=>$id))->row();
    
    return $query->nama_guru;
  }
    
         public function detailRA($id_sekolah)
      {
        $query = "SELECT * from sekolah as s join provinsi as p on p.id_provinsi=s.id_provinsi join kota_kab as k on k.id_kotaKab=s.id_kotaKab where s.id_sekolah='$id_sekolah'  ";

        return $this->db->query($query);

      }
        public function rarequest()
      {
        $query = "SELECT * from sekolah as s join provinsi as p on p.id_provinsi=s.id_provinsi join kota_kab as k on k.id_kotaKab=s.id_kotaKab   where s.status='0'";

        return $this->db->query($query);

      }
      function getpassword() { 
            $kodejadi = "RA".time();
            return $kodejadi;
        }
           function rkpsmtsend($smt,$id_tahunajaran)
    {
         $this->db->select('*');
         $this->db->select('month(tanggal_awal) as bulan');
         $this->db->select('perencanaan.id_program as program ');
         $this->db->select('perencanaan.id_indikator as jadwal ');
         $this->db->from('nilai');
         $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
         $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
         $this->db->join('program', 'program.id_program = perencanaan.id_program');
         $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
         $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
         $this->db->where('siswa.id_tahunajaran',$id_tahunajaran);
         $this->db->where('perencanaan.id_semester',$smt);
         $this->db->group_by('nilai.id_siswa');
        return $this->db->get();
      }
function smtsend($id_tahunajaran,$id_semester)
    {
         $this->db->select('*');
       $this->db->select('month(tanggal_awal) as bulan');
       $this->db->select('perencanaan.id_program as program ');
         $this->db->select('perencanaan.id_indikator as jadwal ');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
       $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
        $this->db->where('siswa.id_tahunajaran',$id_tahunajaran);
        $this->db->where('perencanaan.id_semester',$id_semester);
         $this->db->group_by('perencanaan.id_program');
       
        return $this->db->get();
      }
      function rkpsmt2($id_siswa,$id_semester)
    {
         $this->db->select('*');
       $this->db->select('month(tanggal_awal) as bulan');
       $this->db->select('perencanaan.id_program as program ');
         $this->db->select('perencanaan.id_indikator as jadwal ');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
       $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
         $this->db->join('semester', 'perencanaan.id_semester = semester.id_semester');
        $this->db->where('nilai.id_siswa',$id_siswa);
        $this->db->where('perencanaan.id_semester',$id_semester);
         $this->db->group_by('nilai.kode_indikator');
       
        return $this->db->get();
      }
       function semester($id,$bulan,$id_siswa,$kode_indikator)
    {
         $this->db->select('*');
         $this->db->select('MAX(nilai_perkembangan) as nilai');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
         $this->db->where('month(perencanaan.tanggal_awal)',$bulan);
        $this->db->where('id_siswa',$id_siswa);
        $this->db->where('kode_indikator',$kode_indikator);
       // $this->db->where('week(perencanaan.tanggal_awal)',$mgg);
       $this->db->where_in('id_perencanaan',$id);
        $this->db->group_by('kode_indikator');
        return $this->db->get();
    }
    function insmtr($id,$id_semester,$id_siswa,$id_program)
    {
         $this->db->select('*');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
         $this->db->where('id_semester',$id_semester);
        $this->db->where('id_siswa',$id_siswa);
        $this->db->where('id_program',$id_program);
       $this->db->where_in('id_perencanaan',$id);
        $this->db->group_by('kode_indikator');
        return $this->db->get();
    }
      public function rakanwil($id_provinsi)
      {
        $query = "SELECT * from sekolah as s join provinsi as p on p.id_provinsi=s.id_provinsi join kota_kab as k on k.id_kotaKab=s.id_kotaKab   where s.status='0' and s.id_provinsi='$id_provinsi'";

        return $this->db->query($query);

      }
           function hslminggu($id,$id_siswa,$kode_indikator)
    {
        $this->db->select('MAX(nilai_perkembangan) as nilai');
        $this->db->from('nilai');
        $this->db->where('id_siswa',$id_siswa);
        $this->db->where('id_teknik!=','4');
           $this->db->where('id_teknik!=','5');
        $this->db->where('kode_indikator',$kode_indikator);
        $this->db->where_in('id_jadwal',$id);
        return $this->db->get();
    }
       function dtjdw_print($tanggal_awal)
    {
       $this->db->select('*');
        $this->db->select('week(tanggal_awal) as mingguke');
        $this->db->from('perencanaan');
        $this->db->where('week(tanggal_awal)',$tanggal_awal);
       
        return $this->db->get();
    }
        function searchscph_print($id_siswa,$tanggal_awal)
    {
         $this->db->select('*');
       $this->db->select('week(perencanaan.tanggal_awal) as mingguke');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
        $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
        $this->db->where('nilai.id_siswa',$id_siswa);
        $this->db->where('week(perencanaan.tanggal_awal)',$tanggal_awal);
         $this->db->group_by('nilai.kode_indikator');
       
        return $this->db->get();
    }

    function rkpminggu_print($id_siswa,$tanggal_awal)
    {
         $this->db->select('*');
       $this->db->select('week(perencanaan.tanggal_awal) as mingguke');
         $this->db->select('perencanaan.id_indikator as jadwal ');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
        $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
        $this->db->where('nilai.id_siswa',$id_siswa);
        $this->db->where('week(perencanaan.tanggal_awal)',$tanggal_awal);
         $this->db->group_by('perencanaan.id_program');
       
        return $this->db->get();
    } 
     function excel_bln($id,$bulan,$id_siswa,$id_program,$mgg)
    {
         $this->db->select('*');
         $this->db->select('MAX(nilai_perkembangan) as nilai');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
         $this->db->where('month(perencanaan.tanggal_awal)',$bulan);
        $this->db->where('id_siswa',$id_siswa);
        $this->db->where('id_program',$id_program);
        $this->db->where('week(perencanaan.tanggal_awal)',$mgg);
       $this->db->where_in('id_perencanaan',$id);
        $this->db->group_by('perencanaan.id_program');
        return $this->db->get();
    }
public function minggurkp($id_siswa,$id_program,$jd)
      {
        $query = "SELECT *, max(n.nilai_perkembangan) as na from nilai as n  join siswa as s on s.id_siswa=n.id_siswa join perencanaan as p on p.id_perencanaan=n.id_jadwal where  n.id_siswa='$id_siswa' and p.id_program='$id_program' and n.id_jadwal='$jd'";

        return $this->db->query($query);

     }
      public function ubahrencana($id_perencanaan)
      {
        $query = "SELECT *,week(tanggal_awal) as mingguke, j.id_kd as kd from perencanaan as j join program as p on p.id_program=j.id_program  join tema as t on t.id_tema=j.id_tema join subtema as s on s.id_subtema=j.id_subtema join tahun_ajaran as ta on ta.id_tahunajaran=j.id_tahunajaran  where j.id_perencanaan='$id_perencanaan' order by j.tanggal_awal";

        return $this->db->query($query);

     }
      function hasilminggu($id_siswa,$tanggal_awal,$id_program)
    {
         $this->db->select('*');
       $this->db->select('week(perencanaan.tanggal_awal) as mingguke');
         $this->db->select('perencanaan.id_indikator as jadwal ');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
        $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
        $this->db->where('nilai.id_siswa',$id_siswa);
         $this->db->where('perencanaan.id_program',$id_program);
        $this->db->where('perencanaan.tanggal_awal',$tanggal_awal);
         $this->db->group_by('perencanaan.id_kd');
       
        return $this->db->get();
    } 
     function ind_week($id_siswa,$tanggal_awal,$id_program)
    {
         $this->db->select('*');
         $this->db->select('MAX(nilai_perkembangan) as nilai');
         $this->db->select('week(perencanaan.tanggal_awal) as mingguke');
         $this->db->from('nilai');
         $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
         $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
         $this->db->join('program', 'program.id_program = perencanaan.id_program');
         $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
         $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
         $this->db->where('nilai.id_siswa',$id_siswa);
         $this->db->where('perencanaan.tanggal_awal',$tanggal_awal);
         $this->db->where('perencanaan.id_program',$id_program);
         $this->db->group_by('nilai.kode_indikator');
       
        return $this->db->get();
    }
    
    
      function rkp_week($id_siswa,$tanggal_awal)
    {
         $this->db->select('*');
         $this->db->select('MAX(nilai_perkembangan) as nilai');
       $this->db->select('week(perencanaan.tanggal_awal) as mingguke');
         $this->db->select('perencanaan.id_indikator as jadwal ');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
        $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
        $this->db->where('nilai.id_siswa',$id_siswa);
        $this->db->where('perencanaan.tanggal_awal',$tanggal_awal);
         $this->db->group_by('perencanaan.id_kd');
       
        return $this->db->get();
    } 
     function week_tek($id_siswa,$tanggal_awal,$id_program,$id_teknik)
    {
         $this->db->select('*');
         $this->db->select('MAX(nilai_perkembangan) as nilai');
       $this->db->select('week(perencanaan.tanggal_awal) as mingguke');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
        $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
        $this->db->where('nilai.id_siswa',$id_siswa);
        $this->db->where('nilai.id_teknik',$id_teknik);
        $this->db->where('perencanaan.tanggal_awal',$tanggal_awal);
         $this->db->where('perencanaan.id_program',$id_program);
         // $this->db->group_by('perencanaan.kode_indikator');
       
        return $this->db->get();
    }
    function hsl_week($id_siswa,$tanggal_awal,$id_program)
    {
         $this->db->select('*');
         $this->db->select('MAX(nilai_perkembangan) as nilai');
       $this->db->select('week(perencanaan.tanggal_awal) as mingguke');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
        $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
        $this->db->where('nilai.id_siswa',$id_siswa);
        $this->db->where('perencanaan.tanggal_awal',$tanggal_awal);
         $this->db->where('perencanaan.id_program',$id_program);
           $this->db->where('perencanaan.id_program',$id_program);
         $this->db->group_by('perencanaan.id_program');
       
        return $this->db->get();
    } 
        function searchscph_all($id_tahunajaran,$tanggal_awal)
    {
         $this->db->select('*');
       $this->db->select('week(perencanaan.tanggal_awal) as mingguke');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
        $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
        $this->db->where('siswa.id_tahunajaran',$id_tahunajaran);
        $this->db->where('week(perencanaan.tanggal_awal)',$tanggal_awal);
         $this->db->group_by('nilai.kode_indikator');
       
        return $this->db->get();
    }

public function rkp_smt($bln,$id_siswa,$id_program)
      {
        $query = "SELECT *,max(nilai_perkembangan) as nilai from nilai join perencanaan as p on p.id_perencanaan=nilai.id_jadwal WHERE month(p.tanggal_awal)='$bln' and id_siswa='$id_siswa' and id_program='$id_program' group by id_program";

        return $this->db->query($query);

     }
     function getsmtr($id_tahunajaran){
            $query="SELECT *,month(tanggal_awal) as bulan FROM `perencanaan` WHERE id_tahunajaran ='$id_tahunajaran' group by id_tahunajaran";
            $q=$this->db->query($query);    
            if ($q->num_rows() > 0){
                foreach($q->result() as $row){
                    $data[]=$row;
                }
                return $data;
            }
        }
          function rkpsmt_excel($semester,$id_tahunajaran)
    {
         $this->db->select('*');
       $this->db->select('month(tanggal_awal) as bulan');
       $this->db->select('perencanaan.id_program as program ');
         $this->db->select('perencanaan.id_indikator as jadwal ');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
       $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
        $this->db->where('siswa.id_tahunajaran',$id_tahunajaran);
        $this->db->where('perencanaan.id_semester',$semester);
         $this->db->group_by('nilai.id_siswa');
       
        return $this->db->get();
      }
      function rkpsmtex2($id_siswa,$id_semester)
    {
         $this->db->select('*');
       $this->db->select('month(tanggal_awal) as bulan');
       $this->db->select('perencanaan.id_program as program ');
         $this->db->select('perencanaan.id_indikator as jadwal ');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
       $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
         $this->db->join('semester', 'perencanaan.id_semester = semester.id_semester');
        $this->db->where('nilai.id_siswa',$id_siswa);
        $this->db->where('perencanaan.id_semester',$id_semester);
         $this->db->group_by('perencanaan.id_program');
       
        return $this->db->get();
      }
 public function siswa($id_tahunajaran)
      {
        $query = "SELECT * from siswa as s join kelas as k on k.id_kelas=s.id_kelas where s.id_tahunajaran='$id_tahunajaran'";

        return $this->db->query($query);

     }
      function smtexcel($id_siswa,$id_semester)
    {
       
       //  return $this->db->get();

 $query = "SELECT *, month(tanggal_awal) as bulan, perencanaan.id_indikator as jadwal from nilai
          join perencanaan on perencanaan.id_perencanaan=nilai.id_jadwal 
          join siswa on siswa.id_siswa=nilai.id_siswa
          join program on program.id_program=perencanaan.id_program
          join indikator on indikator.kode_indikator=nilai.kode_indikator
          join kelas on kelas.id_kelas=siswa.id_kelas
          join semester on semester.id_semester=semester.id_semester
          where nilai.id_siswa='$id_siswa' and perencanaan.id_semester='$id_semester'
          group by perencanaan.id_program having count('nilai.id_siswa')";

        return $this->db->query($query);

      }
          function perkembangan($id_siswa,$id_program,$id_semester)
    {
       $this->db->select('*');
        $this->db->from('perkembangan_akhir');
        $this->db->where('id_siswa',$id_siswa);
        $this->db->where('id_program',$id_program);
        $this->db->where('id_semester',$id_semester);
        //$this->db->group_by('id_program');       
        return $this->db->get();
    }
      function rkpbulan_all($bulan)
    {
         $this->db->select('*');
       $this->db->select('month(tanggal_awal) as bulan');
       $this->db->select('perencanaan.id_program as program ');
         $this->db->select('perencanaan.id_indikator as jadwal ');
        $this->db->from('nilai');
        $this->db->join('perencanaan', 'perencanaan.id_perencanaan = nilai.id_jadwal');
        $this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa');
       $this->db->join('program', 'program.id_program = perencanaan.id_program');
        $this->db->join('teknik_penugasan', 'teknik_penugasan.id_teknik = nilai.id_teknik');
        $this->db->join('indikator', 'indikator.kode_indikator = nilai.kode_indikator');
         $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
      
        $this->db->where('month(tanggal_awal)',$bulan);
         $this->db->group_by('perencanaan.id_program');
       
        return $this->db->get();
      }
 public function cek_kota($id)
  {
    $query = $this->db->get_where('kota_kab',array('id_kotaKab'=>$id))->row();
    
    return $query->nama_kota;
  }
  }

	


?>