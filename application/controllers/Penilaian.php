<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penilaian extends CI_Controller {

	public function __construct(){
			parent::__construct();

			$this->load->model('model_m');
			if(! $this->session->userdata('username')){
				 redirect('login');
			}
           
          }
	public function index()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['tema']=$this->model_m->tema($id_sekolah)->result();
		$data['siswa']=$this->model_m->datasiswa($id_sekolah);
		$data['tahun']=$this->model_m->selectsemua('tahun_ajaran');
		$this->load->view('penilaian',$data);
		
	}
	public function nilai()
	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['tema']=$this->model_m->tema($id_sekolah)->result();
		$data['siswa']=$this->model_m->datasiswa($id_sekolah);
		$data['tahun']=$this->model_m->selectsemua('tahun_ajaran');
		$this->load->view('penilaian',$data);
	}
	
	public function nilai_siswa()
	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['tema']=$this->model_m->tema($id_sekolah)->result();
		$id_tahunajaran = $this->input->post('thn');
		$data['tema']=$this->model_m->tema($id_sekolah)->result();
		$data['tahun']=$this->model_m->selectsemua('tahun_ajaran');
		$data['siswa']=$this->model_m->datasiswafilter($id_sekolah,$id_tahunajaran);
		$this->load->view('penilaian',$data);
	}

	public function isi_nilai($id_siswa)
	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$where = array('id_siswa' => $id_siswa );
		
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['tema']=$this->model_m->tema($id_sekolah)->result();
  		$data['prg']=$this->model_m->selectsemua('program')->result();
  		$data['ind']=$this->model_m->selectsemua('indikator')->result();
  		$data['tk']=$this->model_m->selectsemua('teknik_penugasan')->result();

  		$data['jdw']=$this->model_m->jdwa($id_sekolah);
   		$data['kd']=$this->model_m->kd()->result();
   		//$data['hari'] = $this->model_m->nilaifilter($tanggal_awal)->row();
		$data['siswa']=$this->model_m->selectX('siswa',$where);
		$this->load->view('penilaianisi',$data);
	}

	 function get_jadwal(){
        $this->load->model('model_m');
        $id_perencanaan = $this->input->post('id_perencanaan');
        $dataSub = $this->model_m->getjadwal($id_perencanaan);
        
        foreach($dataSub as $r){
            echo ' 
            <div class="form-group row">
				<label for="exampleInputPassword2" class="col-sm-3 col-form-label">Program</label>
                  <div class="col-sm-9">'.$r->nama_program. ' / '.$r->nama_tema.' / '.$r->nama_subtema.' / '.$r->nama_subtema. ' / '.$r->materi. ' </div></div>
              <div class="form-group row">
				<label for="exampleInputPassword2" class="col-sm-3 col-form-label">KD dan Indikator</label>
                  <div class="col-sm-9">'
                  ;$j=json_decode($r->kd);
                               foreach ($j as $key ) {
                                  echo '('.$this->model_m->cek_kd3($key).')';
                               }'<br>'
                   ;$j=json_decode($r->id_indikator);
                               foreach ($j as $key ) {
                                  echo $this->model_m->cek_ind($key).'<br>';
                               }
                    ' </div></div>      '
                      ;
        }
       ;
    }
    function get_tanggal(){
        $this->load->model('model_m');
        $id_perencanaan = $this->input->post('id_perencanaan');
        $datajadwal = $this->model_m->getnilai($id_perencanaan)->row();
         $data1 = $this->model_m->getnilai($id_perencanaan);
        $awal=$datajadwal->tanggal_awal;
        $akhir=$datajadwal->tanggal_akhir;
      
        $begin 	= new DateTime($awal);
			$end 	= new DateTime($akhir);
			 
			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);


			 echo '
			  <div class="form-group row">
			   <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tanggal</label>
               <div class="col-sm-9"><select name="tgl" class="form-control form-control-sm" id="kota" required>
        		<option value="">Show Tanggal</option>';

        foreach( $period as $a ){
            echo '
            <option value="'.$a->format("Y-m-d").'">'.$a->format("Y-m-d").'</option>'
            ;
        } 
        echo '<option value="'.$akhir.'">'.$akhir.'</option>';
        echo '</select></div></div>
        <div class="form-group row">
			   <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Indikator</label>
               <div class="col-sm-9"><select name="indikator" class="form-control form-control-sm" id="kota" required>
        		<option value="">Show Indikator</option>';

        foreach( $data1->result() as $b){
            echo  $j=json_decode($b->id_indikator);
            foreach ($j as $key ) {
              echo '        
            <option value="'.$this->model_m->cek_ind2($key).'">'.$this->model_m->cek_ind($key).'<br>';
                               }
            '</option>';
        }
        echo '</select></div></div>
         <div class="form-group row">
				<label for="exampleInputPassword2" class="col-sm-3 col-form-label">Program</label>
                  <div class="col-sm-9">'.$b->nama_program. ' / '.$b->nama_tema.' / '.$b->nama_subtema. ' </div></div>';

      
    } 
    public function periode()
		{

			$begin 	= new DateTime('2018-05-22');
			$end 	= new DateTime('2018-05-27');
			echo $begin;
			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);
			foreach ( $period as $dt )
			{		
			echo $dt->format("Y-m-d");
				
				
			}
		}

	public function insert_nilai()

	{
		$config['upload_path'] = './uploads';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto')){
			$data=$this->upload->data();			
			//$config['file_name'];
			$foto	= $data["file_name"];				
		}else{ $foto	=''; }
		$data1 = array(
							'id_jadwal'=>$this->input->post('jdw'),
							'id_siswa'=>$this->input->post('fid'),
							'kode_indikator'=>$this->input->post('indikator'),
							'tanggal_nilai'=>$this->input->post('tgl'),
							'nilai_perkembangan'=>$this->input->post('f6'),
							
							'deskripsi'=>$this->input->post('f7'),
							'id_teknik'=>$this->input->post('tk'),
							'id_penilai'=>$this->session->userdata('id_user'),
							'foto'=>$foto,
						);
					$this->model_m->input_data('nilai',$data1);
					 echo "<script>
							alert('Data Harian Telah ditambahkan');
							window.location.href='".site_url("penilaian/nilai")."';
						  </script>";
	}
	 function filternilai()

    {
        $id_sekolah=$this->session->userdata('id_sekolah');
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
     $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
          $tanggal_awal = $this->input->post('keyword');
        $data['hari'] = $this->model_m->nilaifilter($tanggal_awal)->row();
          $data['jdw'] = $this->model_m->nilaifilter($tanggal_awal);

       // echo $data;
       // echo $id_siswa;
      $this->load->view('scph',$data);
    }
    public function ubahnilai($kode_indikator,$id_siswa,$tanggal_nilai)
	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['thn']=$this->model_m->selectsemua('tahun_ajaran');
		$kode = array('kode_indikator'=>$kode_indikator);
		$tanggal_nilai = ($tanggal_nilai);
		$data['jdw']=$this->model_m->jdw($id_siswa);
        $data['nilai'] = $this->model_m->ubahind($kode_indikator,$tanggal_nilai);
		$data['dtsiswa'] = $this->model_m->detailsw($id_siswa);
        $data['siswa'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
        $where = array('kode_indikator'=>$kode_indikator, 'id_siswa'=>$id_siswa);
        $data['tn'] = $this->model_m->selectX('nilai',$where);
        $data['tek']=$this->model_m->selectsemua('teknik_penugasan')->result();
          
		
		$this->load->view('penilaianubah',$data);
	}
	
	public function aksi_ubah($id_nilai)
	{
	  $data['nilai'] = $this->model_m->selectX('nilai','id_nilai='.$id_nilai)->row();
      $foto=$this->updateGambarG($id_nilai);
      if($foto==NULL){
      $foto=$data['nilai']->foto;
      }
      $datanilai=array(
              
							'nilai_perkembangan'=>$this->input->post('f6'),
							
							'deskripsi'=>$this->input->post('f7'),
							'id_teknik'=>$this->input->post('tek'),
							
							'foto'=>$foto,
      );
      $where = array('id_nilai'=>$this->input->post('fid'));
					
      $resultInsert = $this->model_m->update_data("nilai", $datanilai, $where);
      
        redirect('report/harian/');
      
	}
	private function updateGambarG($id_nilai){
				$tipeFile=explode('.',$_FILES["foto"]["name"]);
				$tipeFile=$tipeFile[count($tipeFile)-1];
				$url=$id_nilai.'.'.$tipeFile;
				if(in_array($tipeFile,array("png","jpg","PNG","jpeg"))){
					if(is_uploaded_file($_FILES["foto"]["tmp_name"])){
							if(move_uploaded_file($_FILES["foto"]["tmp_name"], "./uploads/".$url)){
								return $url;
							}
					}
				}else{
					
				}
		}
public function perkembangan_search()
	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['tema']=$this->model_m->tema($id_sekolah)->result();
		$id_tahunajaran = $this->input->post('thn');
		$data['tema']=$this->model_m->tema($id_sekolah)->result();
		$data['tahun']=$this->model_m->selectsemua('tahun_ajaran');
		$data['siswa']=$this->model_m->datasiswafilter($id_sekolah,$id_tahunajaran);
		$this->load->view('penilaian',$data);
	}
	public function perkembangan_aksi($id_siswa){
	
		$perkembangan_akhir = $this->db->query("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'perkembangan_akhir' AND TABLE_SCHEMA = 'madrasah_rapor' ")->num_rows();
		if($perkembangan_akhir==0){	
			$this->db->query("CREATE TABLE `madrasah_rapor`.`perkembangan_akhir` ( `id_perkembangan` INT NOT NULL AUTO_INCREMENT , `id_siswa` INT NOT NULL , `deskripsi` TEXT NOT NULL , `id_program` INT NOT NULL ,`id_semester` INT NOT NULL , PRIMARY KEY (`id_perkembangan`)) ENGINE = InnoDB;");
			$a=$this->input->post('id');
			for($i=1; $i<=(count($a)); $i++) {
			$data=array(
						'id_siswa'=>$id_siswa,
						'id_semester'=>$this->input->post('sem'),
						'deskripsi'=>$this->input->post('deskripsi'.$i),
						'id_program'=>$a[$i-1],
						'catatan'=>$this->input->post('cat'),
			);
			$this->model_m->input_data('perkembangan_akhir',$data);
			}
		}
		else{
			$a=$this->input->post('id');
			for($i=1; $i<=(count($a)); $i++) {
			$data=array(
						'id_siswa'=>$id_siswa,
						'deskripsi'=>$this->input->post('deskripsi'.$i),
						'id_program'=>$a[$i-1],
						'id_semester'=>$this->input->post('sem'),
						'catatan'=>$this->input->post('cat'),
			);
			$this->model_m->input_data('perkembangan_akhir',$data);
			}

		}
		redirect('Penilaian/report_perkembangan/'.$id_siswa.'/');
	}
	  public function report_perkembangan($id_siswa)
  {
  	$perkembangan_akhir = $this->db->query("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'perkembangan_akhir' AND TABLE_SCHEMA = 'madrasah_rapor' ")->num_rows();
		if($perkembangan_akhir==0){	
			$this->db->query("CREATE TABLE `madrasah_rapor`.`perkembangan_akhir` ( `id_perkembangan` INT NOT NULL AUTO_INCREMENT , `id_siswa` INT NOT NULL , `deskripsi` TEXT NOT NULL , `id_program` INT NOT NULL ,`id_semester` INT NOT NULL ,`catatan` TEXT  NULL , PRIMARY KEY (`id_perkembangan`)) ENGINE = InnoDB;");
		}else{
			$cat=$this->db->query(" ALTER TABLE perkembangan_akhir  ADD COLUMN IF NOT EXISTS catatan TEXT  NULL");
		}
    $id_sekolah=$this->session->userdata('id_sekolah');
		$where = array('id_siswa' => $id_siswa );
		$w=$this->input->post('smt');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
  		$data['program']=$this->model_m->selectsemua('program');
		$data['siswa']=$this->model_m->selectX('siswa',$where);
    $this->load->view('formnilai_akhir',$data);
  }
 
  public function perkembangan_aksi_ubah($id_siswa){
  	$a=$this->input->post('id');
  	$b=$this->input->post('pid');
			for($i=1; $i<=(count($a)); $i++) {
			$data=array(
						'id_siswa'=>$id_siswa,
						'deskripsi'=>$this->input->post('deskripsi'.$i),
						'id_program'=>$a[$i-1],
						'id_semester'=>$this->input->post('sem'),
						'catatan'=>$this->input->post('cat'),
						//'id_perkembangan'=>$this->input->post('pid'),
			);
		//var_dump($b);
			$where=array('id_perkembangan'=>$b[$i-1]);
			$this->model_m->update_data('perkembangan_akhir',$data,$where);
			}
		redirect('Penilaian/report_perkembangan/'.$id_siswa);
		
  }
  public function reset($id_siswa){
            $where = array("id_siswa"=>$id_siswa);
            $this->model_m->delete_data('perkembangan_akhir',$where);
			//$this->menu_m->delete_data('menu',$where);
			echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-danger' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil direset
                  </div>");
          redirect('Penilaian/report_perkembangan/'.$id_siswa);
		
        }
        public function hapus_nilai($id_nilai){
            $where = array("id_nilai"=>$id_nilai);
            $this->model_m->delete_data('nilai',$where);
			//$this->menu_m->delete_data('menu',$where);
			echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-danger' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil dihapus
                  </div>");
          redirect('Penilaian/nilai/');
		
        }
  // public function aksiinsert()

	// {
	// 	$config['upload_path'] = './uploads';
	// 	$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
	// 	$this->upload->initialize($config);
	// 	if($this->upload->do_upload('foto')){
	// 		$data=$this->upload->data();			
	// 		//$config['file_name'];
	// 		$foto	= $data["file_name"];				
	// 	}else{ $foto	=''; }
		 
	// 	$data1 = array(
	// 						'id_siswa'=>$this->input->post('idsis'),
	// 						'id_perencanaan'=>$this->input->post('jdw'),
	// 						'nilai_perkembangan'=>$this->input->post('f6'),
	// 						'deskripsi'=>$this->input->post('f7'),
	// 						'id_teknik'=>$this->input->post('tk'),
							
	// 						'foto'=>$foto,
	// 					);
	// 				$this->model_m->input_data('penilaian',$data1);
	// 				 echo "<script>
	// 						alert('Data Harian Telah ditambahkan');
	// 						window.location.href='".site_url("penilaian/nilai")."';
	// 					  </script>";
	// }
		 //    	public function aksi_insert()

	// {
	// 	$config['upload_path'] = './uploads';
	// 	$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
	// 	$this->upload->initialize($config);
	// 	if($this->upload->do_upload('foto')){
	// 		$data=$this->upload->data();			
	// 		//$config['file_name'];
	// 		$foto	= $data["file_name"];				
	// 	}else{ $foto	=''; }
	// 	$data1 = array(
	// 						'id_kti'=>$this->input->post('f1'),
	// 						'id_ktd'=>$this->input->post('f2'),
	// 						'id_siswa'=>$this->input->post('f3'),
	// 						'nilai_perkembangan'=>$this->input->post('f4'),
							
	// 						'deskripsi'=>$this->input->post('f5'),
	// 						'tanggal'=>$this->input->post('tgl'),
	// 						'foto'=>$foto,
	// 					);
	// 				$this->model_m->input_data('penilaian',$data1);
	// 				 echo "<script>
	// 						alert('Data Harian Telah ditambahkan');
	// 						window.location.href='".site_url("penilaian")."';
	// 					  </script>";
	// }

	
	
}
