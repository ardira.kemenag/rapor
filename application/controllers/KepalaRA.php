<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KepalaRA extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('model_m');
		 if(!$this->session->userdata('username')){
				 redirect('login');
			}
	 
	}
	
	public function index()
	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
			$data['jmlguru']=$this->model_m->guru($id_sekolah)->num_rows();
    $data['jmlsiswa']=$this->model_m->datasiswa($id_sekolah)->num_rows();
		$this->load->view('sekolah/home',$data);
	}
	public function detailRA()
	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['prov']=$this->model_m->selectsemua('provinsi')->result();
		$data['kota']=$this->model_m->selectsemua('kota_kab')->result();
		$data['kelas']=$this->model_m->datakelas($id_sekolah)->result();
		$data['thn']=$this->model_m->selectsemua('tahun_ajaran');
		 
            $data['dtRA'] = $this->model_m->detailRA($id_sekolah);
            $data['RA'] = $this->model_m->selectX('sekolah','id_sekolah='.$id_sekolah)->row();
		$this->load->view('sekolah/madrasahdetail',$data);
	}
	public function aksi_ubah($id_sekolah)

	{
	  $data['RA'] = $this->model_m->selectX('sekolah','id_sekolah='.$id_sekolah)->row();
      $datasiswa=array(
            	'nama_sekolah'=>$this->input->post('f1'),
							'NSN'=>$this->input->post('f2'),
							'jalan'=>$this->input->post('jln'),
							'kelurahan'=>$this->input->post('kel'),
							'kecamatan'=>$this->input->post('kec'),
							'kode_pos'=>$this->input->post('pos'),
							'id_provinsi'=>$this->input->post('prov'),
							'id_kotaKab'=>$this->input->post('kab'),
							'website'=>$this->input->post('web'),
							'telepon'=>$this->input->post('tlp'),
							'email'=>$this->input->post('email'),
							'kepala_RA'=>$this->input->post('kepRA'),
							'nip'=>$this->input->post('nip'),
							'tahun_ajaran'=>$this->input->post('thn'),
      );
     
     // $where = array('id_siswa'=>$this->input->post('fid'));
	 	$this->model_m->update_data('sekolah',$datasiswa,'id_sekolah='.$id_sekolah);
	 	redirect('kepalaRA/detailRA/');
	}
	// public function aksi_ubahfoto($id_sekolah)
	// {
	//   $data['RA'] = $this->model_m->selectX('sekolah','id_sekolah='.$id_sekolah)->row();
 //      $foto=$this->updateGbr($id_sekolah);
 //      if($foto==NULL){
 //      $foto=$data['RA']->foto;
 //      }
 //      $datagaleri=array(
              
	// 		  'foto'=>$foto,
 //      );
 //       $this->model_m->update_data("sekolah", $datagaleri, 'id_sekolah='.$id_sekolah);
     
 //        redirect('kepalaRa/detailRA/');
      
	// }
	// private function updateGbr($id_sekolah){
	// 			$tipeFile=explode('.',$_FILES["foto"]["name"]);
	// 			$tipeFile=$tipeFile[count($tipeFile)-1];
	// 			$url=$id_sekolah.'.'.$tipeFile;
	// 			if(in_array($tipeFile,array("png","jpg","PNG","jpeg"))){
	// 				if(is_uploaded_file($_FILES["foto"]["tmp_name"])){
	// 						if(move_uploaded_file($_FILES["foto"]["tmp_name"], "./uploads/".$url)){
	// 							return $url;
	// 						}
	// 				}
	// 			}else{
					
	// 			}
	// 	}
		public function aksi_ubahfoto($id_sekolah)
	{
	 $ft=$this->input->post('foto');
	 $config['upload_path'] = './uploads';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto')){
			$data=$this->upload->data();
			$foto	= $data["file_name"];	
			if(file_exists('uploads/'.$foto)){
				$config['source_image'] = 'uploads/'.$foto;
				$config['new_image'] = 'uploads'.$foto;
				$config['maintain_ratio'] = TRUE;
				$config['overwrite'] = TRUE;
				$this->load->library('image_lib', $config);
				// $this->image_lib->resize();
				unlink('uploads/'.$ft);
			}			
		unlink('uploads/'.$ft);
		}else{ $foto	=$ft; }
		$fotosiswa=array(
              
              'foto'=> $foto
      );
       $this->model_m->update_data("sekolah", $fotosiswa, 'id_sekolah='.$id_sekolah);
     
        redirect('KepalaRA/detailRA/');
      
	}
	 public function aksi_edit()

	{
		$data1 = array(
							'username'=>$this->input->post('username'),
							'password'=>$this->input->post('password'),
						);
					$where = array('id_guru'=>$this->input->post('user'));
					$this->model_m->update_data('user',$data1,$where);
					echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-warning' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil diubah
                  </div>");
					  redirect('Guru/account');
	}
	
      


}
