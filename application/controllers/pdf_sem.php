<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); ?>
<script>
            function get_semester(){
                var id_tahunajaran = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('report/get_semester'); ?>", 
                    data:"id_tahunajaran="+id_tahunajaran, 
                    success: function(msg) {
                            $("#div_ktd").html(msg);
                    }
                });
            }
        </script>

<body>
      <div class="main-panel">
        <div class="content-wrapper">
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Laporan Semester </h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home/dashboard')?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Penilaian</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Laporan Semester</li>
                      </ol>
                    </nav>
                    <div class="col-lg-12">
                      <form method="post" action="<?php echo site_url('Report/nilaisemester');?>">
                     <div class="form-group row">
                          <label  class="col-sm-2 col-form-label" >Tahun Ajaran </label>
                         <div class="col-sm-2">
                             <select required name="thn" class=" form-control form-control-sm" id="prov" onchange="get_semester()" style="
                             margin-left: -60px;">
                           <option value=""> Pilih tahun ajaran </option>
                             <?php
                            foreach($thn as $r){
                            echo "<option value='".$r->id_tahunajaran."'>".$r->nama_tahun."</option>";}
                        ?> 
                         </select>
                           </div> 
                            <label for="exampleInputPassword2" class="col-form-label" style="
                                margin-left: -30px;">Siswa </label>
                            <div class="form-group row col-lg-6" id="div_ktd">
                           <div class="col-sm-5">
                           <select required name="siswa" class="form-control form-control-sm">
                         <?php
                             foreach ($siswa as $k) {
                            ?>
                         <option value="<?php echo $k->id_siswa; ?>" <?php if($sw->id_siswa==$k->id_siswa)echo "selected"; ?>>  <?php echo $k->nama_siswa; ?></option>
                         <?php
                              } ?> 
                          </select>
                        </div>
                         <label for="exampleInputPassword2" class="col-form-label" style=" margin-left: 30px;">Semester</label>
                           <div class="col-sm-4">
                          <select required name="smt" class="form-control" >
                        <option value="">Pilih semester</option>
                       
                          </select>
                       
                          </select>
                        </div></div>
                         <span class="input-group-btn"  >
                                <button class="btn btn-primary btn-sm" type="submit">Cari
                                </button>
                            </span>
                      </div>

                         
                        </form>
                  <?php foreach($dt->result() as $sw){ ?>
                         
                    
                                   <div class="form-group row">
                       <label class="col-sm-2 col-form-label">Nama siswa</label>
                          <div class="col-sm-4 col-form-label">: <?php echo $sw->nama_siswa;?></div>
                          <label class="col-sm-2 col-form-label">Kelompok</label>
                          <div class="col-sm-4 col-form-label">: <?php echo $sw->nama_kelas;?></div>
                      </div>
                  
                     <!--  <div class="form-group row">
                        <?php
                                  $data1 = $dtjdw->result();
                                  ?>
                       <label class="col-sm-2 col-form-label">Bulan</label>
                      <div class="col-sm-4 col-form-label">: <?php echo $format=date('F', strtotime($data1[0]->tanggal_awal))?></div>
                      <label class="col-sm-2 col-form-label">Tahun</label>
                      <div class="col-sm-4 col-form-label">: <?php echo $format=date('Y', strtotime($data1[0]->tanggal_awal))?></div>
                      </div> -->
                     
                    <div class="table-responsive">
                    <br>
                         <?php 
                          foreach ( $tgl->result() as $m )
                          { ?>
                            <th width="15%">
                          <?php  $id[]=$m->id_perencanaan;?> </th> 
                          <?php }?>    
                  <table id="" class="table table-striped table-advance table-hover nowrap">
                    <thead>
                      <tr> 
                         <th>No</th>
                          <th><center>Program</center></th>
                         <th><center>KD dan Indikator Penilaian</center></th>
                          <?php 
                         $no = 1;
                          foreach ( $mgg->result() as $m )
                          { ?>
                            <th width="15%">
                          <?php echo $format=date('F', strtotime($m->tanggal_awal));
                           
                          ;?> </th> 
                          <?php }?>
                          
                      </tr>
                    </thead>
                 <tbody>
                        <?php
                        $no = 1;
                       
                         foreach($smt->result() as $b){?>
                        <tr>
                           <td><center> <?php echo $no++?></center></td>
                           <td><?php echo $b->nama_program.$b->id_semester?></td>
                           <td><?php   
                              $ind=$this->model_m->insmt($id,$b->id_semester,$b->id_siswa,$b->program);
                               foreach ($ind->result() as $d ) {
                                $j=json_decode($d->id_indikator);
                               foreach ($j as $key ) {
                                   echo '-'.$this->model_m->cek_ind($key)//.$this->model_m->cek_ind2($key)
                                   ;
                                   echo '<br>';
                               } }?>
                            </td>
                            <?php  foreach($mgg->result() as $c){?>
                          <td>
                               <input type="hidden" value="<?php echo $c->bulan; ?>"><?php   
                              $ind=$this->model_m->smt($id,$c->bulan,$b->id_siswa,$b->id_program);
                               foreach ($ind->result() as $d ) {
                               //  $j=json_decode($d->id_indikator);
                               // foreach ($j as $key ) {
                               //    echo '-'.$this->model_m->cek_ind2($key);
                                   
                                   if($d->nilai=='1') {
                                    echo "BB<br>";
                                   }
                                   elseif ($d->nilai=='2') {
                                     echo "MB<br>";
                                   }
                                   elseif ($d->nilai=='3') {
                                     echo "BSH<br>";
                                   }
                                   elseif ($d->nilai=='4') {
                                     echo "BSB<br>";
                                   }
                                   else{
                                    echo "-<br><br><br>";
                                   }
                                   echo '<br>';
                             //  } 
                             }?>
                            </td>
                            <?php } ?>
                      </tr>
                        <?php  } ?>
                               
                  </tbody>
                  </table>
                </div><br><br>
                 <?php   }?>
                </div>
                      
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

<?php $this->load->view('footer.php'); ?>
</body>

</html>