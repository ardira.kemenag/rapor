<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subtheme extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			if(!$this->session->userdata('username')){
				 redirect('login');
			}
           
          }
	public function index()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['tm']=$this->model_m->tema($id_sekolah);
		$this->load->view('subtambah',$data);
	}
 
 	public function subtheme()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['sub']=$this->model_m->subtema($id_sekolah);
		$this->load->view('subtema',$data);
	}
	public function supersub()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['sub']=$this->model_m->subsubtema($id_sekolah);
		$this->load->view('subsubtema',$data);
	}
		public function aksi_insert()

	{
		$data1 = array(
							'nama_subtema'=>$this->input->post('f2'),
							'id_tema'=>$this->input->post('f1')
						);
					$this->model_m->input_data('subtema',$data1);
					echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-success' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil ditambah
                  </div>");
					  redirect('Subtheme/subtheme');
	}
	public function ubah($id_sub)

	{
		$where = array('id_subtema' => $id_sub );
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['tm']=$this->model_m->tema($id_sekolah);
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['sub']=$this->model_m->selectX('subtema',$where);
		//$data['siswa'] = $this->model_m->selectX('tema','id_tema='.$id_tema)->row();
		$this->load->view('sububah',$data);
	}
		public function aksi_ubah()

	{
		$data1 = array(
							'nama_subtema'=>$this->input->post('f2'),
							'id_tema'=>$this->input->post('f1')
						);
					$where = array('id_subtema'=>$this->input->post('fid'));
					$this->model_m->update_data('subtema',$data1,$where);
					echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-warning' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil diubah
                  </div>");
					  redirect('Subtheme/subtheme');
	}
	public function hapus_subtema($id_subtema){
            $where = array("id_subtema"=>$id_subtema);
            $this->model_m->delete_data('subtema',$where);
			//$this->menu_m->delete_data('menu',$where);
			echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-danger' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil dihapus
                  </div>");
            redirect('subtheme/subtheme');
        }

	
	
	
}
