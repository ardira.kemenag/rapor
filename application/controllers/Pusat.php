<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pusat extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('model_m');

		if(!$this->session->userdata('username')){
				 redirect('login');
			}
	 
	}
	
	public function index()
	{
		 $data['jmlsekolah']=$this->model_m->selectx('sekolah','status='.'1')->num_rows();
     $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		$this->load->view('pusat/home',$data);
	}
}
