<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sekolah extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			if(!$this->session->userdata('username')){
				 redirect('login');
			}
           
          }
	public function index(){

	
		$data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		$data['prov']=$this->model_m->selectsemua('provinsi')->result();
		$this->load->view('pusat/tambahsekolah',$data);
	}
 
 	public function school()

	{
		$data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		$data['skl']=$this->model_m->sekolah();
		$this->load->view('pusat/sekolah',$data);
	}
	public function kanwilschool()

	{
		 $id_provinsi=$this->session->userdata('id_provinsi');
		$where = array('id_provinsi' => $id_provinsi );
		$data['skl']=$this->model_m->selectX('sekolah',$where);
		$this->load->view('pusat/sekolah',$data);
	}
		
function get_kota(){
      
        $id_provinsi = $this->input->post('id_provinsi');
        $dataKota = $this->model_m->getDataKota1($id_provinsi);
        
        echo '<select name="kab" class="form-control form-control-sm" id="kota">
        <option value="">Show All Kota/Kabupaten</option>';

        foreach($dataKota as $a){
            echo '
            <option value="'.$a->id_kotaKab.'">'.$a->nama_kota.'</option>';
        }
        echo '</select>';
    }
    public function aksi_insert()

	{
		$config['upload_path'] = './uploads';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto')){
			$data=$this->upload->data();			
			//$config['file_name'];
			$foto	= $data["file_name"];				
		}else{ $foto	=''; }
		$data1 = array(
							'nama_sekolah'=>$this->input->post('namaS'),
							'NSN'=>$this->input->post('nsn'),
							'jalan'=>$this->input->post('jln'),
							'kelurahan'=>$this->input->post('kel'),
							'kecamatan'=>$this->input->post('kec'),
							'kode_pos'=>$this->input->post('pos'),
							'id_provinsi'=>$this->input->post('prov'),
							'id_kotaKab'=>$this->input->post('kab'),
							'website'=>$this->input->post('web'),
							'telepon'=>$this->input->post('tlp'),
							'email'=>$this->input->post('email'),
							'kepala_RA'=>$this->input->post('kepRA'),
							'tahun_ajaran'=>$this->input->post('thn'),
							//'foto'=>$foto,
						);
					$this->model_m->input_data('sekolah',$data1);
					  redirect('sekolah/school');
	}
	

	
	
}
