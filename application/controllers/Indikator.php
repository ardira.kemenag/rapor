<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indikator extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			if(!$this->session->userdata('username')){
				 redirect('login');
			}
           
          }
	public function index()

	{
       $id_sekolah=$this->session->userdata('id_sekolah');
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $data['prg']=$this->model_m->selectsemua('program')->result();
   $data['kd']=$this->model_m->kd()->result();
    $data['ind']=$this->model_m->joinindikator()->result();
    $this->load->view('tambahindikator',$data);
	}

  public function indikator()

  {
     $id_sekolah=$this->session->userdata('id_sekolah');
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $data['ind']=$this->model_m->joinindikator()->result();
    $this->load->view('dataIndikator',$data);
  }
 
   public function insertindikator(){
   
        $kd= json_encode($_POST['f5']);
    $data=array(
      'id_program'=>$this->input->post('f1'),
      'kode_kd'=>$kd,
      'kode_indikator'=>$this->input->post('kode'),
      'nama_indikator'=>$this->input->post('ind'),
       'id_sekolah'=>$this->input->post('sekolah'),
    );
    //print_r($data);
   
    $this->model_m->input_data('indikator',$data);
    
    redirect('indikator/indikator');
    
   }
   public function ubahindikator($id_indikator){
    $id_sekolah=$this->session->userdata('id_sekolah');
     $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
     $data['prg']=$this->model_m->selectsemua('program')->result();
     $data['kd']=$this->model_m->kd()->result();
    $data['ind']=$this->model_m->joinindikator()->result();
      $data['data']=$this->model_m->selectX('indikator','id_indikator='.$id_indikator)->row();
      $this->load->view('tambahindikator',$data);

    }
    public function aksi_ubahindikator($id_indikator){
      $kd= json_encode($_POST['f5']);
    $data=array(
      'id_program'=>$this->input->post('f1'),
      'kode_kd'=>$kd,
      'kode_indikator'=>$this->input->post('kode'),
      'nama_indikator'=>$this->input->post('ind'),
       'id_sekolah'=>$this->input->post('sekolah'),
    );
    $where=array('id_indikator'=>$id_indikator);
    $this->model_m->update_data('indikator',$data,$where);
     echo $this->session->set_flashdata('msg2', "
                  <div class='alert alert-fill-warning' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil diubah
                  </div>");
     redirect('indikator/indikator');

}

	
}
