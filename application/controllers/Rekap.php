<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			if(!$this->session->userdata('username')){
          redirect('login');
       }
           
          
           
          }
	public function index()

	{    
		$data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		$data['data']= $this->model_m->terkirim();
		$this->load->view('pusat/rekap',$data);
	}
  public function popupSK(){
		$data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
        	$where = array('id_pengawas'=>$this->input->post('fid'));
            $data['dpX'] = $this->model_m->selectX('datapengawas',$where);
        	$data['id']=$_POST['id'];
			$this->load->view('pusat/showSK',$data);
        }
 	public function rkp($id_sekolah,$id_tahunajaran)

	{
  $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
    $where = array('id_sekolah' => $id_sekolah,'id_tahunajaran' => $id_tahunajaran);
    $data['rkp']= $this->model_m->selectX('coba_rekap',$where);
    $data['nama']= $this->model_m->nama($id_tahunajaran);
	 $this->load->view('pusat/rekapbulanan',$data);
	}
	public function popup(){

        	$id=$_POST['id'];
        	$a=$id[2];
			$b=$id[6];

       		$data['hasil']=$this->model_m->hasil2($a,$b)->result();
       		
			$this->load->view('pusat/showrekap',$data);
           
            
        }


	
	
}

?>
