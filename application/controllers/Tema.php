<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tema extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			if(!$this->session->userdata('username')){
				 redirect('login');
			}
           
          }
	public function index()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$this->load->view('tematambah',$data);
	}
 
 	public function tema()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');

		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['tm']=$this->model_m->tema($id_sekolah);
		$this->load->view('tema',$data);
	}
		public function aksi_insert()

	{
		$data1 = array(
							'nama_tema'=>$this->input->post('f1'),
							'id_sekolah'=>$this->input->post('sekolah')
						);
					$this->model_m->input_data('tema',$data1);
					echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-success' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil ditambahkan
                  </div>");
					  redirect('Tema/tema');
	}
	public function temaubah($id_tema)

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$where = array('id_tema' => $id_tema );
		$data['tm']=$this->model_m->selectX('tema',$where);
		$this->load->view('temaubah',$data);
	}
	public function aksi_ubah()

	{
		$data1 = array(
							'nama_tema'=>$this->input->post('f1'),
							'id_sekolah'=>$this->input->post('sekolah')
						);
					$where = array('id_tema'=>$this->input->post('fid'));
					$this->model_m->update_data('tema',$data1,$where);
					echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-warning' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil diubah
                  </div>");
					  redirect('Tema/tema');
	}
	public function hapus_tema($id_tema){
            $where = array("id_tema"=>$id_tema);
            $this->model_m->delete_data('tema',$where);
			//$this->menu_m->delete_data('menu',$where);
			echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-danger' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil dihapus
                  </div>");
            redirect('tema/tema');
        }

	
	
	
}
