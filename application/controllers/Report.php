<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
       // $this->load->library('tcpdf6213/tcpdf');
     $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
     if(! $this->session->userdata('username')){
         redirect('login');
      }
           
          }

	public function index()

	{
    $id_sekolah=$this->session->userdata('id_sekolah');
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
	
		$this->load->view('formpenilaian',$data);
	}

  public function harian()

  {
    $id_sekolah=$this->session->userdata('id_sekolah');
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
    $data['minggu']=$this->model_m->mingguan()->result();
     $data['hari']=$this->model_m->dataharian()->result();
     $data['kelas']=$this->model_m->datakelas($id_sekolah);
     $data['prov']=$this->model_m->selectsemua('provinsi')->result();
     $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
     
    $this->load->view('rekapharian',$data);
  }
  public function perkembangan_akhir()
  {
    $id_sekolah=$this->session->userdata('id_sekolah');
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
     $data['tema']=$this->model_m->tema($id_sekolah)->result();
     $data['siswa']=$this->model_m->datasiswa($id_sekolah);
     $data['tahun']=$this->model_m->selectsemua('tahun_ajaran');
     $data['program']=$this->model_m->selectsemua('program');
    $this->load->view('penilaian',$data);
  }

  function get_data(){
        $this->load->model('model_m');
        $id_sekolah=$this->session->userdata('id_sekolah');
        $id_tahunajaran = $this->input->post('id_tahunajaran');
        $datasiswa = $this->model_m->getDataSiswa($id_tahunajaran,$id_sekolah);
        $datamgg = $this->model_m->getDataMgg($id_tahunajaran);
        echo ' <div class="form-group row">
         <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Pilih Siswa </label>
         <div class="col-sm-9">
        <select required name="siswa" class="form-control form-control-sm" id="kota" >
        
        ';

        foreach($datasiswa as $a){
            echo '
            <option value="'.$a->id_siswa.'">'.$a->nama_siswa.'</option>';
        }
        echo '</select></div></div>
          <div class="form-group row">
          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Pilih Minggu </label>
          <div class="col-sm-9">
          <select required name="keyword" class="form-control form-control-sm" >
          <option value="">Pilih Minggu</option>';
         foreach($datamgg as $k){
                                       $format=date('d F Y', strtotime($k->tanggal_awal));
                                      echo "<option value='".$k->tanggal_awal."'>".$format=date('F', strtotime($k->tanggal_awal)).',Minggu Ke-'.$k->mingguke."</option>";}
        echo '</select></div></div>';
        
    }
      function get_scph(){
        $this->load->model('model_m');
        $id_sekolah=$this->session->userdata('id_sekolah');
        $id_tahunajaran = $this->input->post('id_tahunajaran');
        $datasiswa = $this->model_m->getDataSiswa($id_tahunajaran,$id_sekolah);
        $datamgg = $this->model_m->getDataMgg($id_tahunajaran);
        echo ' 
              <div class="col-sm-5">
            <select required name="siswa" class="form-control form-control-sm" >
        ';

        foreach($datasiswa as $a){
            echo '
            <option value="'.$a->id_siswa.'">'.$a->nama_siswa.'</option>';
        }
        echo '</select></div>
         <label for="exampleInputPassword2" class="col-form-label" style=" margin-left: 30px;">Minggu</label>
                           <div class="col-sm-4">
      <select required name="keyword" class="form-control" id="keyword" >
                              <option value="">Pilih Minggu</option>';
         foreach($datamgg as $k){
                                       $format=date('d F Y', strtotime($k->tanggal_awal));
                                      echo "<option value='".$k->tanggal_awal."'>".$format=date('F', strtotime($k->tanggal_awal)).',Minggu Ke-'.$k->mingguke."</option>";}
        echo '</select></div></div>';
        
    }
     function get_bulan(){
        $this->load->model('model_m');
        $id_sekolah=$this->session->userdata('id_sekolah');
        $id_tahunajaran = $this->input->post('id_tahunajaran');
        $datasiswa = $this->model_m->getDataSiswa($id_tahunajaran,$id_sekolah);
        $databln = $this->model_m->getBln($id_tahunajaran);
        echo ' <div class="form-group row">
          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Pilih Siswa </label>
          <div class="col-sm-9">
        <select required name="siswa" class="form-control form-control-sm" >
        <option value="0">Show All Siswa</option>';

        foreach($datasiswa as $a){
            echo '
            <option value="'.$a->id_siswa.'">'.$a->nama_siswa.'</option>';
        }
        echo '</select></div></div>
        <div class="form-group row">
          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Pilih Bulan </label>
          <div class="col-sm-9">
          <select required name="bulan" class="form-control">
          <option value="">Pilih Bulan</option>';
         foreach($databln as $k){
                                      echo "<option value='".$k->bulan."'>".'Bulan Ke-'.$k->bulan."</option>";}
        echo '</select></div></div>';
        
    }
    function get_send(){
        $this->load->model('model_m');
        $id_sekolah=$this->session->userdata('id_sekolah');
        $id_tahunajaran = $this->input->post('id_tahunajaran');
        $datasiswa = $this->model_m->getDataSiswa($id_tahunajaran,$id_sekolah);
        $databln = $this->model_m->getBln($id_tahunajaran);
        
        echo '</select></div></div>
        <div class="form-group row">
          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Pilih Bulan </label>
          <div class="col-sm-9">
          <select required name="bulan" class="form-control"  >
          <option value="">Pilih Bulan</option>';
         foreach($databln as $k){
                                      echo "<option value='".$k->bulan."'>".'Bulan Ke-'.$k->bulan."</option>";}
        echo '</select></div></div>';
        
    }
     function get_smt(){
        $this->load->model('model_m');
        $id_sekolah=$this->session->userdata('id_sekolah');
        $id_tahunajaran = $this->input->post('id_tahunajaran');
        $datasiswa = $this->model_m->getDataSiswa($id_tahunajaran,$id_sekolah);
        $datasm = $this->model_m->getSmt($id_tahunajaran);
        
        echo '</select></div></div>
        <div class="form-group row">
          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Pilih Semester </label>
          <div class="col-sm-9">
          <select required name="smt" class="form-control"  >
          <option value="">Pilih Semester</option>';
         foreach($datasm as $k){
                                      echo "<option value='".$k->id_semester."'>".'Semester Ke-'.$k->id_semester."</option>";}
        echo '</select></div></div>';
        
    }
    function get_bln(){
        $this->load->model('model_m');
        $id_tahunajaran = $this->input->post('id_tahunajaran');
        $id_sekolah=$this->session->userdata('id_sekolah');
        $datasiswa = $this->model_m->getDataSiswa($id_tahunajaran,$id_sekolah);
        $databln = $this->model_m->getBln($id_tahunajaran);
        echo ' <div class="col-sm-5">
            <select required name="siswa" class="form-control form-control-sm">
        ';

        foreach($datasiswa as $a){
            echo '
            <option value="'.$a->id_siswa.'">'.$a->nama_siswa.'</option>';
        }
        echo '</select></div>
          <label for="exampleInputPassword2" class="col-form-label" style=" margin-left: 30px;">Bulan</label>
          <div class="col-sm-4">
              <select required name="bulan" class="form-control" >
          <option value="">Pilih Bulan</option>';
         foreach($databln as $k){
                                      echo "<option value='".$k->bulan."'>".'Bulan Ke-'.$k->bulan."</option>";}
        echo '</select></div>';
        
    }
   
   
   function get_semester(){
        $this->load->model('model_m');
        $id_tahunajaran = $this->input->post('id_tahunajaran');
        $id_sekolah=$this->session->userdata('id_sekolah');
        $datasiswa = $this->model_m->getDataSiswa($id_tahunajaran,$id_sekolah);
        $databln = $this->model_m->getSmt($id_tahunajaran);
        echo ' <div class="col-sm-5">
            <select required name="siswa" class="form-control form-control-sm" >
        <option value="0">Semua Siswa</option>';
          foreach($datasiswa as $a){
            echo '
            <option value="'.$a->id_siswa.'">'.$a->nama_siswa.'</option>';
        }
           
       
        echo '</select></div>
      
         <label for="exampleInputPassword2" class="col-form-label" style=" margin-left: 30px;">Semester</label>
          <div class="col-sm-4">
          <select required name="smt" class="form-control" >
          <option value="">Pilih Semester</option>';
        
          foreach($databln as $k){
        echo "<option value='".$k->id_semester."'>".'Semester'.$k->id_semester."</option>";}
        echo '</select></div>';
        
    }
	
	public function bulanan()

	{
    $id_sekolah=$this->session->userdata('id_sekolah');
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
		 $data['minggu']=$this->model_m->mingguan()->result();
		$data['bln']=$this->model_m->selectsemua('nilai')->result();
    $data['bulan']=$this->model_m->bulan()->result();
    $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
		$this->load->view('rekapbulanan',$data);
	}
  public function review()

  {
        $id_sekolah=$this->session->userdata('id_sekolah');
        $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
        $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
        $id_tahunajaran = $this->input->post('thn');
        $bulan = $this->input->post('bulan');
        $data['tgl']    =   $this->model_m->tgl($bulan);
        $data['jdw']    =   $this->model_m->rkpbulansend($bulan,$id_tahunajaran);
        $data['bulan']    =   $this->model_m->rkpbulansend($bulan,$id_tahunajaran);
              // $data['nilai']    =   $this->model_m->rkpbulan($bulan,$id_tahunajaran,$id_siswa);
        //$data['sw'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
        $data['bln']=$this->model_m->bulan()->result();
        $data['mgg']    =   $this->model_m->listminggu($bulan);
        $data['dtjdw'] = $this->model_m->listminggu($bulan);
        // $data['dt'] = $this->model_m->dtsiswa($id_siswa);
        $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
    $this->load->view('reviewbulan',$data);
  }
	public function mingguan()

	{
    $id_sekolah=$this->session->userdata('id_sekolah');
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
    $data['kti']=$this->model_m->selectsemua('ki')->result();
    $data['bln']=$this->model_m->selectsemua('nilai')->result();
     $data['mgg']=$this->model_m->dataInnerjoin();
     $data['minggu']=$this->model_m->mingguan()->result();
      $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
		$this->load->view('rekapmingguan',$data);
	}
  public function rapot()

  {
    $id_sekolah=$this->session->userdata('id_sekolah');
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $data['siswa']=$this->model_m->datasiswa($id_sekolah);
     $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
  
    $this->load->view('rapor',$data);
  }
   public function cetak($id_siswa){
    $where = array('id_siswa'=>$id_siswa);
    $data['datasiswa'] = $this->model_m->detailsw($id_siswa);
    $data['program'] = $this->model_m->selectsemua('program');
      $this->load->view('cetakrapor',$data);
            
        }
         public function cetakrapor($id_siswa){
    $where = array('id_siswa'=>$id_siswa);
    $data['datasiswa'] = $this->model_m->detailsw($id_siswa);
    $data['program'] = $this->model_m->selectsemua('program');
      $this->load->view('cetak',$data); 
        }
   
    function cari_report()

    {
           $id_sekolah=$this->session->userdata('id_sekolah');
          $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
          $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
          $tanggal_awal = $this->input->post('keyword');
          $id_siswa = $this->input->post('siswa');

           $data['minggu']=$this->model_m->mingguan()->result();
           $data['hari'] = $this->model_m->dtjdw($tanggal_awal)->row();
          $data['jdw'] = $this->model_m->searchscph($id_siswa,$tanggal_awal);
          $data['dt'] = $this->model_m->dtsiswa($id_siswa);
          $data['dtjdw'] = $this->model_m->dtjdw($tanggal_awal);
             $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();

           $data['sw'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
           //$where = array('id_perencanaan'=>$id_perencanaan);
         // $data['p'] = $this->model_m->selectX('perencanaan','tanggal_awal='.$tanggal_awal)->row();
        $this->load->view('scph',$data);
    }

   function search_keyword()

    {
        $id_sekolah=$this->session->userdata('id_sekolah');
        $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
        $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
        $id_siswa = $this->input->post('siswa');
        $tanggal_awal = $this->input->post('keyword');
        $data['mgg']    =   $this->model_m->rkpminggu($id_siswa,$tanggal_awal);
        $data['minggu']=$this->model_m->mingguan()->result();
        $data['sw'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
        $data['dtjdw'] = $this->model_m->dtjdw($tanggal_awal);
        $data['dt'] = $this->model_m->dtsiswa($id_siswa);
        $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
        $data['prg']=$this->model_m->selectsemua('program')->result();
        $this->load->view('hasilmingguan',$data);
    }
    function search_bulan()

    {
      $id_siswa = $this->input->post('siswa');
        $bulan = $this->input->post('bulan');
        $id_tahunajaran = $this->input->post('thn');
      if($id_siswa!='0'){
        $id_sekolah=$this->session->userdata('id_sekolah');
        $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
        $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
        
        $data['tgl']    =   $this->model_m->tgl($bulan);
        $data['jdw']    =   $this->model_m->rkpbulan($bulan,$id_siswa);
        $data['bulan']    =   $this->model_m->rkpbulan($bulan,$id_siswa);
        $data['sw'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
        $data['bln']=$this->model_m->bulan()->result();
        $data['mgg']    =   $this->model_m->listminggu($bulan);
        $data['dtjdw'] = $this->model_m->listminggu($bulan);
        $data['dt'] = $this->model_m->dtsiswa($id_siswa);
        $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
       // print_r($data);

        $this->load->view('hasilbulanan',$data);
      }else{
        $id_sekolah=$this->session->userdata('id_sekolah');
        $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
        $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
        // $data['bln']=$this->input->post('bulan');
        $data['tgl']    =   $this->model_m->tgl($bulan);
        $data['jdw']    =   $this->model_m->rkpbulan_all($bulan);
        $data['bulan']    =   $this->model_m->rkpbulan($bulan,$id_siswa);
        $data['sw'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
        $data['bln']=$this->model_m->bulan()->result();
        $data['mgg']    =   $this->model_m->listminggu($bulan);
        $data['dtjdw'] = $this->model_m->listminggu($bulan);
        $data['dt'] = $this->model_m->siswa($id_tahunajaran);
        $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
       // print_r($data);

        $this->load->view('bulanan_all',$data);

      }
    }   
    function nilaisemester()

    {
       $id_siswa = $this->input->post('siswa');
      $id_semester = $this->input->post('smt');
      $id_tahunajaran = $this->input->post('thn');
      if($id_siswa!='0'){
      $id_sekolah=$this->session->userdata('id_sekolah');
      $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
      $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
     
      $data['tgl']    =   $this->model_m->tglsmt($id_semester,$id_tahunajaran);
        $data['smt']    =   $this->model_m->rkpsmt($id_siswa,$id_semester);
        $data['bulan']    =   $this->model_m->rkpsmt($id_siswa,$id_semester);
        $data['bln']=$this->model_m->bulan()->result();
        $data['mgg']    =   $this->model_m->listbulan($id_semester,$id_tahunajaran);
        $data['sw'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
        $data['dtjdw'] = $this->model_m->dtsmt($id_semester);
        $data['dt'] = $this->model_m->dtsiswa($id_siswa);
        $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
       // print_r($data);
        $this->load->view('hasilsemester',$data);
        }else{
        $id_sekolah=$this->session->userdata('id_sekolah');
        $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
        $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
        $id_tahunajaran=$this->input->post('thn');
        $smt = $this->input->post('smt');
        $data['tgl']= $this->model_m->tglsmt($smt,$id_tahunajaran);
        $data['jdw']=$this->model_m->rkpsmtsend($smt,$id_tahunajaran);
        $data['bulan']=$this->model_m->rkpsmtsend($smt,$id_tahunajaran);
        $data['bln']=$this->model_m->bulan()->result();
        $data['mgg']=$this->model_m->listbulan($smt,$id_tahunajaran);
        $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
    
             
   $this->load->view('pdf_semester', $data);
 }
    }
     function search_hari()

    {
        $id_sekolah=$this->session->userdata('id_sekolah');
        $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
        $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
        $id_siswa = $this->input->post('keyword');
        $data['hari'] = $this->model_m->dataharianfilter($id_siswa)->result();
        $this->load->view('reportharian',$data);
    }
 
    public function kelompok($id_kelas)
  {
    $id_sekolah=$this->session->userdata('id_sekolah');
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $data['kelas']=$this->model_m->datakelas($id_sekolah);
    $where = array('id_kelas'=>$id_kelas);
    $data['hari']=$this->model_m->hariankelompok($id_kelas)->result();
    
    $this->load->view('reportkelompok',$data);
  }
  function rekap_bulan()

    {
     $id_sekolah=$this->session->userdata('id_sekolah');
     $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
     $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
     $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
     $this->load->view('rekap_send',$data);
    } 
	 public function tampil_pdf(){
        
            $id_sekolah=$this->session->userdata('id_sekolah');
            $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
            $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
            $id_tahunajaran=$this->input->post('thn');
           // $bulan = $this->input->post('bulan');
            $smt = $this->input->post('smt');

            $data['tgl']= $this->model_m->tglsmt($smt,$id_tahunajaran);
            $data['jdw']=$this->model_m->rkpsmtsend($smt,$id_tahunajaran);
            $data['bulan']=$this->model_m->rkpsmtsend($smt,$id_tahunajaran);
            // $data['smt']    =   $this->model_m->rkpsmt($id_siswa,$id_semester);
                // $data['nilai']    =   $this->model_m->rkpbulan($bulan,$id_tahunajaran,$id_siswa);
              //$data['sw'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
            $data['bln']=$this->model_m->bulan()->result();
            $data['mgg']=$this->model_m->listbulan($smt,$id_tahunajaran);
              // $data['dtjdw']=$this->model_m->listminggu($bulan);
              // $data['dt'] = $this->model_m->dtsiswa($id_siswa);
            $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
            $this->load->view('importexcel', $data);
    }
     public function cetak_excel(){

       $id_tahunajaran = $this->input->post('thn');
       $bulan = $this->input->post('bulan');
        $id_sekolah=$this->session->userdata('id_sekolah');
        $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
        $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
        $data['tgl']    =   $this->model_m->tgl($bulan);
        $data['jdw']    =   $this->model_m->rkpbulansend($bulan,$id_tahunajaran);
        $data['bulan']    =   $this->model_m->rkpbulansend($bulan,$id_tahunajaran);
        // $data['nilai']    =   $this->model_m->rkpbulan($bulan,$id_tahunajaran,$id_siswa);
        //$data['sw'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
        $data['bln']=$this->model_m->bulan()->result();
        $data['mgg']    =   $this->model_m->listminggu($bulan);
        $data['dtjdw'] = $this->model_m->listminggu($bulan);
        // $data['dt'] = $this->model_m->dtsiswa($id_siswa);
        $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
        $data['riwayat'] = $this->model_m->rkpbulansend($bulan,$id_tahunajaran)->result();
         // print_r($data);
       // echo "a";
    $this->load->view('excelsend', $data);
    }
     function scph_print()

    {
          $id_sekolah=$this->session->userdata('id_sekolah');
          $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
          $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
          $tanggal_awal = $this->uri->segment(4);
          $id_siswa = $this->uri->segment(3);

          $data['minggu']=$this->model_m->mingguan()->result();
          $data['hari'] = $this->model_m->dtjdw_print($tanggal_awal)->row();
          $data['jdw'] = $this->model_m->searchscph_print($id_siswa,$tanggal_awal);
          $data['dt'] = $this->model_m->dtsiswa($id_siswa);
          $data['dtjdw'] = $this->model_m->dtjdw_print($tanggal_awal);
          $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
          $data['sw'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
        $this->load->view('scph_print',$data);
    }
    function mingguan_print()

    {
          $id_sekolah=$this->session->userdata('id_sekolah');
          $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
          $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
          $tanggal_awal = $this->uri->segment(4);
          $id_siswa = $this->uri->segment(3);
          $data['mgg']    =   $this->model_m->rkpminggu_print($id_siswa,$tanggal_awal);
          $data['minggu']=$this->model_m->mingguan()->result();
          $data['sw'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
          $data['dtjdw'] = $this->model_m->dtjdw_print($tanggal_awal);
          $data['dt'] = $this->model_m->dtsiswa($id_siswa);
          $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
          $data['prg']=$this->model_m->selectsemua('program')->result();
        $this->load->view('mingguan_print',$data);
    
    }
    function bulan_print()

    {
     //$bulan = $this->uri->segment(4);
      if($this->uri->segment(4)){
        $id_sekolah=$this->session->userdata('id_sekolah');
        $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
        $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
        $id_siswa = $this->uri->segment(3);
        $bulan = $this->uri->segment(4);
        $data['tgl']    =   $this->model_m->tgl($bulan);
        $data['jdw']    =   $this->model_m->rkpbulan($bulan,$id_siswa);
        $data['bulan']    =   $this->model_m->rkpbulan($bulan,$id_siswa);
        $data['sw'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
        $data['bln']=$this->model_m->bulan()->result();
        $data['mgg']    =   $this->model_m->listminggu($bulan);
        $data['dtjdw'] = $this->model_m->listminggu($bulan);
        $data['dt'] = $this->model_m->dtsiswa($id_siswa);
        $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
       // print_r($data);     
       // print_r($data);

        $this->load->view('bulanan_print',$data);

        }else{
           $bulan = $this->uri->segment(3);
        $id_tahunajaran = $this->input->post('thn');
           $id_sekolah=$this->session->userdata('id_sekolah');
        $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
        $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
        // $data['bln']=$this->input->post('bulan');
        $data['tgl']    =   $this->model_m->tgl($bulan);
         $data['jdw']    =   $this->model_m->rkpbulan_all($bulan);
        // $data['bulan']    =   $this->model_m->rkpbulan($bulan,$id_siswa);
        // $data['sw'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
        $data['bln']=$this->model_m->bulan()->result();
        $data['mgg']    =   $this->model_m->listminggu($bulan);
        $data['dtjdw'] = $this->model_m->listminggu($bulan);
        $data['dt'] = $this->model_m->siswa($id_tahunajaran);
        $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
        $this->load->view('bulanan_print_all',$data);
        }
      }
      public function bulan_print_all()

    {
      // $id_siswa = $this->input->post('siswa');
        $bulan = $this->uri->segment(3);

        $id_tahunajaran = $this->uri->segment(4);
      
        $id_sekolah=$this->session->userdata('id_sekolah');
        $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
        $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
        // $data['bln']=$this->input->post('bulan');
        $data['tgl']    =   $this->model_m->tgl($bulan);
        $data['jdw']    =   $this->model_m->rkpbulan_all($bulan);
        // $data['bulan']    =   $this->model_m->rkpbulan($bulan,$id_siswa);
        // $data['sw'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
        $data['bln']=$this->model_m->bulan()->result();
        $data['mgg']    =   $this->model_m->listminggu($bulan);
        $data['dtjdw'] = $this->model_m->listminggu($bulan);
        $data['dt'] = $this->model_m->siswa($id_tahunajaran);
        $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
       // print_r($data);

        $this->load->view('bulanan_print_all',$data);
      
      }
      public function download_excel(){
        
         $id_sekolah=$this->session->userdata('id_sekolah');
         $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
         $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
         $id_tahunajaran = $this->input->post('thn');
         $bulan = $this->input->post('bulan');
         $data['tgl']    =   $this->model_m->tgl($bulan);
         $data['jdw']    =   $this->model_m->rkpbulansend($bulan,$id_tahunajaran);
         $data['bulan']    =   $this->model_m->rkpbulansend($bulan,$id_tahunajaran);
          // $data['nilai']    =   $this->model_m->rkpbulan($bulan,$id_tahunajaran,$id_siswa);
        //$data['sw'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
        $data['bln']=$this->model_m->bulan()->result();
        $data['mgg']    =   $this->model_m->listminggu($bulan);
        $data['dtjdw'] = $this->model_m->listminggu($bulan);
        // $data['dt'] = $this->model_m->dtsiswa($id_siswa);
        $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
    $this->load->view('excel_download', $data);
    }
    public function pdf_smt(){

          $id_sekolah=$this->session->userdata('id_sekolah');
            $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
            $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
            $id_tahunajaran=$this->input->post('thn');
            $smt = $this->input->post('smt');

             $data['tgl']= $this->model_m->tglsmt($smt,$id_tahunajaran);
            $data['jdw']=$this->model_m->rkpsmtsend($smt,$id_tahunajaran);
            $data['bulan']=$this->model_m->rkpsmtsend($smt,$id_tahunajaran);
              $data['bln']=$this->model_m->bulan()->result();
              $data['mgg']=$this->model_m->listbulan($smt,$id_tahunajaran);
              $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
    
             
   $this->load->view('pdf_semester', $data);
 }
   
     public function excel_semester(){

          $id_sekolah=$this->session->userdata('id_sekolah');
            $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
            $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
            $id_tahunajaran=$this->input->post('thn');
            $smt = $this->input->post('smt');

             $data['tgl']= $this->model_m->tglsmt($smt,$id_tahunajaran);
            $data['jdw']=$this->model_m->rkpsmtsend($smt,$id_tahunajaran);
            $data['bulan']=$this->model_m->rkpsmtsend($smt,$id_tahunajaran);
              $data['bln']=$this->model_m->bulan()->result();
              $data['mgg']=$this->model_m->listbulan($smt,$id_tahunajaran);
              $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
     
             
   $this->load->view('excel_semester', $data);
   redirect('pdf_smt');
    }
    // public function download_semester(){
        
    //         $id_sekolah=$this->session->userdata('id_sekolah');
    //         $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    //         $data['siswa']=$this->model_m->datasiswa($id_sekolah)->result();
    //         $id_tahunajaran=$this->input->post('thn');
    //        // $bulan = $this->input->post('bulan');
    //         $smt = $this->input->post('smt');

    //          $data['tgl']= $this->model_m->tglsmt($smt,$id_tahunajaran);
    //         $data['jdw']=$this->model_m->rkpsmtsend($smt,$id_tahunajaran);
    //         $data['bulan']=$this->model_m->rkpsmtsend($smt,$id_tahunajaran);
    //           $data['mgg']=$this->model_m->listbulan($smt,$id_tahunajaran);
    //           $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
    //           $this->load->view('importexcel', $data);
    // }
    
  }
