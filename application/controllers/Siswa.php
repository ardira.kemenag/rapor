<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
			//$this->load->helper(array('url','download'));	
			$this->load->helper("file");
			if(!$this->session->userdata('username')){
				 redirect('login');
			}
           
          }
	public function index()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['kls']=$this->model_m->kelas($id_sekolah);
		$data['prov']=$this->model_m->selectsemua('provinsi')->result();
		$data['thn']=$this->model_m->selectsemua('tahun_ajaran');
		$this->load->view('siswatambah',$data);
	}
 
 	public function dt()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['siswa']=$this->model_m->datasiswa($id_sekolah);
		$data['kelas']=$this->model_m->datakelas($id_sekolah);
		$this->load->view('siswa',$data);
	}
	public function siswatmbh()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['kls']=$this->model_m->kelas($id_sekolah);
		$data['prov']=$this->model_m->selectsemua('provinsi')->result();
		$data['thn']=$this->model_m->selectsemua('tahun_ajaran');
		$this->load->view('siswatambah',$data);
	}
	public function uploadsw()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['siswa']=$this->model_m->datasiswa($id_sekolah);
		$data['kelas']=$this->model_m->datakelas($id_sekolah);
		$data['thn']=$this->model_m->selectsemua('tahun_ajaran');
		$this->load->view('siswaupload',$data);
	}
	public function downloadformat()

	{
	force_download('./uploads/siswa.xls', NULL);
	}
		public function absensi()

	{
		$this->load->view('absensi');
	}

	function get_kota(){
        $this->load->model('model_m');
        $nama_provinsi = $this->input->post('nama_provinsi');
        $dataKota = $this->model_m->getDataKota($nama_provinsi);
        
        echo '<select name="kab" class="form-control form-control-sm" id="kota">
        <option value="0">Show All Kota/Kabupaten</option>';

        foreach($dataKota as $a){
            echo '
            <option value="'.$a->nama_kota.'">'.$a->nama_kota.'</option>';
        }
        echo '</select>';
    }
    public function aksi_insert()

	{
		$config['upload_path'] = './uploads';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto')){
			$data=$this->upload->data();			
			//$config['file_name'];
			$foto	= $data["file_name"];				
		}else{ $foto	=''; }
		$data1 = array(
							'nama_siswa'=>$this->input->post('namaL'),
							'nama_panggilan'=>$this->input->post('namaP'),
							'no_induk'=>$this->input->post('noinduk'),
							'tempat_lahir'=>$this->input->post('tlahir'),
							'tanggal_lahir'=>$this->input->post('tgllahir'),
							'jenis_kelamin'=>$this->input->post('jk'),
							'agama'=>$this->input->post('agama'),
							'nama_ayah'=>$this->input->post('ayah'),
							'nama_ibu'=>$this->input->post('ibu'),
							'pekerjaan_ayah'=>$this->input->post('payah'),
							'pekerjaan_ibu'=>$this->input->post('pibu'),
							'jalan'=>$this->input->post('jln'),
							'kelurahan'=>$this->input->post('kel'),
							'kecamatan'=>$this->input->post('kec'),
							'provinsi'=>$this->input->post('prov'),
							'kotaKab'=>$this->input->post('kab'),
							'id_sekolah'=>$this->input->post('sekolah'),
							'id_kelas'=>$this->input->post('kls'),
							'status_anak'=>$this->input->post('status'),
							'anak_ke'=>$this->input->post('anakke'),
							'diterima_tanggal'=>$this->input->post('terima'),
							'id_tahunajaran'=>$this->input->post('thn'),
							'foto'=>$foto
							//'foto'=>$foto,
						);
					$this->model_m->input_data('siswa',$data1);
					echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-success' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil ditambahkan
                  </div>");
					  redirect('Siswa/dt');
	}
		public function detail($id_siswa)
	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['prov']=$this->model_m->selectsemua('provinsi')->result();
		$data['kota']=$this->model_m->selectsemua('kota_kab')->result();
		$data['kelas']=$this->model_m->datakelas($id_sekolah)->result();
		$data['thn']=$this->model_m->selectsemua('tahun_ajaran');
		 $where = array('id_siswa'=>$id_siswa);
            $data['dtsiswa'] = $this->model_m->detailsw($id_siswa);
            $data['siswa'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
		
		$this->load->view('siswadata',$data);
	}
		public function aksi_ubahfoto($id_siswa)
	{
	 $ft=$this->input->post('foto');
	 $config['upload_path'] = './uploads/siswa';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto')){
			$data=$this->upload->data();
			$foto	= $data["file_name"];	
			if(file_exists('uploads/siswa/'.$foto)){
				$config['source_image'] = 'uploads/siswa/'.$foto;
				$config['new_image'] = 'uploads/siswa/'.$bursa;
				$config['maintain_ratio'] = TRUE;
				$config['overwrite'] = TRUE;
				$this->load->library('image_lib', $config);
				// $this->image_lib->resize();
				unlink('uploads/siswa/'.$ft);
			}			
		unlink('uploads/siswa/'.$ft);
		}else{ $foto	=$ft; }
		$fotosiswa=array(
              
              'foto'=> $foto
      );
       $this->model_m->update_data("siswa", $fotosiswa, 'id_siswa='.$id_siswa);
     
        redirect('Siswa/detail/'.$id_siswa.'/');
      
	}


		public function kelompok($id_kelas)
	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['kelas']=$this->model_m->datakelas($id_sekolah);
		$where = array('id_kelas'=>$id_kelas);
         $data['siswa']=$this->model_m->datasiswa2($id_sekolah,$id_kelas);
		
		$this->load->view('kelompok',$data);
	}
	 public function cetak($id_siswa){
	 	$where = array('id_siswa'=>$id_siswa);
            $data['datasiswa'] = $this->model_m->detailsw($id_siswa);
			$this->load->view('portofolio',$data);
            
        }
	public function aksi_ubah($id_siswa)

	{
	  $data['siswa'] = $this->model_m->selectX('siswa','id_siswa='.$id_siswa)->row();
      $datasiswa=array(
             'nama_siswa'=>$this->input->post('namaL'),
							'nama_panggilan'=>$this->input->post('namaP'),
							'no_induk'=>$this->input->post('noinduk'),
							'tempat_lahir'=>$this->input->post('tlahir'),
							'tanggal_lahir'=>$this->input->post('tgllahir'),
							'jenis_kelamin'=>$this->input->post('jk'),
							'agama'=>$this->input->post('agama'),
							'nama_ayah'=>$this->input->post('ayah'),
							'nama_ibu'=>$this->input->post('ibu'),
							'pekerjaan_ayah'=>$this->input->post('payah'),
							'pekerjaan_ibu'=>$this->input->post('pibu'),
							'jalan'=>$this->input->post('jln'),
							'kelurahan'=>$this->input->post('kel'),
							'kecamatan'=>$this->input->post('kec'),
							'provinsi'=>$this->input->post('prov'),
							'kotaKab'=>$this->input->post('kab'),
							'id_sekolah'=>$this->input->post('sekolah'),
							'id_kelas'=>$this->input->post('kls'),
							'status_anak'=>$this->input->post('status'),
							'anak_ke'=>$this->input->post('anakke'),
							'diterima_tanggal'=>$this->input->post('terima'),
							'id_tahunajaran'=>$this->input->post('thn')
              
      );
     
     // $where = array('id_siswa'=>$this->input->post('fid'));
	 	$this->model_m->update_data('siswa',$datasiswa,'id_siswa='.$id_siswa);
	 	echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-warning' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil diubah
                  </div>");
	 	redirect('Siswa/detail/'.$id_siswa.'/');
	}
private function updateGbr($id_berita){
				$tipeFile=explode('.',$_FILES["foto"]["name"]);
				$tipeFile=$tipeFile[count($tipeFile)-1];
				$url=$id_berita.'.'.$tipeFile;
				if(in_array($tipeFile,array("png","jpg","PNG","jpeg"))){
					if(is_uploaded_file($_FILES["foto"]["tmp_name"])){
							if(move_uploaded_file($_FILES["foto"]["tmp_name"], "./uploads/".$url)){
								return $url;
							}
					}
				}else{
					
				}
		}
public function importsiswa(){
  $fileName = $this->input->post('file', TRUE);

  $config['upload_path'] = './uploads/'; 
  $config['file_name'] = $fileName;
  $config['allowed_types'] = 'xls|xlsx|csv|ods|ots';
  $config['max_size'] = 10000;

  $this->load->library('upload', $config);
  $this->upload->initialize($config); 
  
  if (!$this->upload->do_upload('file')) {
   $error = array('error' => $this->upload->display_errors());
   $this->session->set_flashdata('msg','Ada kesalah dalam upload'); 
   // redirect('Welcome'); 
  } else {
   $media = $this->upload->data();
   $inputFileName = 'uploads/'.$media['file_name'];
   
   try {
    $inputFileType = IOFactory::identify($inputFileName);
    $objReader = IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
   } catch(Exception $e) {
    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
   }

   $sheet = $objPHPExcel->getSheet(0);
   $highestRow = $sheet->getHighestRow();
   $highestColumn = $sheet->getHighestColumn();

   for ($row = 2; $row <= $highestRow; $row++){  
     $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
       NULL,
       TRUE,
       FALSE);
     $data = array(
    //"id_siswa"=> $rowData[0][0],
                   					 "nama_siswa"=> $rowData[0][0],
				                    "nama_panggilan"=> $rowData[0][1],
				                    "no_induk"=> $rowData[0][2],
				                    "diterima_tanggal"=> $rowData[0][3],
				                    "tempat_lahir"=> $rowData[0][4],
				                     "tanggal_lahir"=> $rowData[0][5],
				                    "jenis_kelamin"=> $rowData[0][6],
				                    "agama"=> $rowData[0][7],
				                    "status_anak"=> $rowData[0][8],
				                    "anak_ke"=> $rowData[0][9],
				                    "nama_ayah"=> $rowData[0][10],
				                    "nama_ibu"=> $rowData[0][11],
				                    "pekerjaan_ayah"=> $rowData[0][12],
				                    "pekerjaan_ibu"=> $rowData[0][13],
				                    "jalan"=> $rowData[0][14],
				                    "kelurahan"=> $rowData[0][15],
				                    "kecamatan"=> $rowData[0][16],
				                     "provinsi"=> $rowData[0][17],
				                     "kotakab"=> $rowData[0][18],
				                    "nama_wali"=> $rowData[0][19],
				                    "pekerjaan_wali"=> $rowData[0][20],
				                    "alamat_wali"=> $rowData[0][21],
				                    'id_sekolah'=>$this->input->post('sekolah'),
							'id_kelas'=>$this->input->post('kls'),
							'id_tahunajaran'=>$this->input->post('thn'),
    );
    $this->db->insert("siswa",$data);
   } 
  unlink($inputFileName);
						 redirect('siswa/dt');
  }  
 } 
 public function delete_siswa($id_siswa){
            $where = array("id_siswa"=>$id_siswa);
            $this->model_m->delete_data('siswa',$where);
            $this->model_m->delete_data('nilai',$where);
            $perkembangan_akhir = $this->db->query("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'perkembangan_akhir' AND TABLE_SCHEMA = 'madrasah_rapor' ")->num_rows();
		if($perkembangan_akhir!=0){	
            $this->model_m->delete_data('perkembangan_akhir',$where);
        }
			//$this->menu_m->delete_data('menu',$where);
			echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-danger' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil dihapus
                  </div>");
            redirect('siswa/dt');
        }
        public function download_format(){
              
              
			echo '<script>
			 window.location.href = "'.base_url('uploads/siswa.xlsx').'"
			</script>'; 
			
		}
// 		public function upload(){
//         $fileName = $_FILES['file']['name'];
         
//         $config['upload_path'] = './assets/'; //buat folder dengan nama assets di root folder
//         $config['file_name'] = $fileName;
//         $config['allowed_types'] = 'xls|xlsx|csv';
//         $config['max_size'] = 10000;
         
//         $this->load->library('upload');
//         $this->upload->initialize($config);
         
//         if(! $this->upload->do_upload('file') )
//         $this->upload->display_errors();
             
//         $media = $this->upload->data('file');
//         $inputFileName = './assets/'.$media['file_name'];
         
//         try {
//                 $inputFileType = IOFactory::identify($inputFileName);
//                 $objReader = IOFactory::createReader($inputFileType);
//                 $objPHPExcel = $objReader->load($inputFileName);
//             } catch(Exception $e) {
//                 die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
//             }
 
//             $sheet = $objPHPExcel->getSheet(0);
//             $highestRow = $sheet->getHighestRow();
//             $highestColumn = $sheet->getHighestColumn();
             
//             for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
//                 $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
//                                                 NULL,
//                                                 TRUE,
//                                                 FALSE);
                                                 
//                 //Sesuaikan sama nama kolom tabel di database                                
//                  $data = array(
//                     "id_siswa"=> $rowData[0][0],
//                     "nama"=> $rowData[0][1],
//                     "alamat"=> $rowData[0][2],
//                     "kontak"=> $rowData[0][3]
//                 );
                 
//                 //sesuaikan nama dengan nama tabel
//                 $insert = $this->db->insert("coba_siswa",$data);
//                // delete_files($media['file_path']);

                     
//             }
//         redirect('siswa/');
    
// }
// public function coba()
// 	{
// 		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));	
// 		 $fileName = $_FILES['file']['name'];
         
//         $config['upload_path'] = './assets/'; //buat folder dengan nama assets di root folder
//         $config['file_name'] = $fileName;
//         $config['allowed_types'] = 'xls|xlsx|csv';
//         $config['max_size'] = 10000;
         
//         $this->load->library('upload');
//         $this->upload->initialize($config);
         
//         if(! $this->upload->do_upload('file') )
//         $this->upload->display_errors();
             
//         $media = $this->upload->data('file');
//      // $inputFileName = './assets/'.$media['file_name'];
		
// 		$inputFileName = './assets/siswa.xlsx';
// 		try {
// 			$inputFileType = IOFactory::identify($inputFileName);
// 			$objReader = IOFactory::createReader($inputFileType);
// 			$objPHPExcel = $objReader->load($inputFileName);
// 		} catch(Exception $e) {
// 			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
// 		}

// 		$sheet = $objPHPExcel->getSheet(0);
// 		$highestRow = $sheet->getHighestRow();
// 		$highestColumn = $sheet->getHighestColumn();
// 			for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
// 					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
// 													NULL,
// 													TRUE,
// 													FALSE);
// 					// $jenis_id = $this->jenis_id($rowData[0][10]);
							
// 								$data = array(					  
// 									//"id_siswa"=> $rowData[0][0],
//                    					 "nama_siswa"=> $rowData[0][0],
// 				                    "nama_panggilan"=> $rowData[0][1],
// 				                    "no_induk"=> $rowData[0][2],
// 				                    "diterima_tanggal"=> $rowData[0][3],
// 				                    "tempat_lahir"=> $rowData[0][4],
// 				                     "tanggal_lahir"=> $rowData[0][5],
// 				                    "jenis_kelamin"=> $rowData[0][6],
// 				                    "agama"=> $rowData[0][7],
// 				                    "status_anak"=> $rowData[0][8],
// 				                    "anak_ke"=> $rowData[0][9],
// 				                    "nama_ayah"=> $rowData[0][10],
// 				                    "nama_ibu"=> $rowData[0][11],
// 				                    "pekerjaan_ayah"=> $rowData[0][12],
// 				                    "pekerjaan_ibu"=> $rowData[0][13],
// 				                    "jalan"=> $rowData[0][14],
// 				                    "kelurahan"=> $rowData[0][15],
// 				                    "kecamatan"=> $rowData[0][16],
// 				                     "provinsi"=> $rowData[0][17],
// 				                     "kotakab"=> $rowData[0][18],
// 				                    "nama_wali"=> $rowData[0][19],
// 				                    "pekerjaan_wali"=> $rowData[0][20],
// 				                    "alamat_wali"=> $rowData[0][21],
// 				                    'id_sekolah'=>$this->input->post('sekolah'),
// 							'id_kelas'=>$this->input->post('kls'),
// 							'id_tahunajaran'=>$this->input->post('thn'),
// 							//'kotakab'=>$this->input->post('kota'),
// 								);
								
// 							$insert = $this->db->insert("siswa",$data);
						
// 						}unlink($inputFileName);
// 						 redirect('siswa/');
					
// 					//sesuaikan nama dengan nama tabel
					
// 		}



	
	
}
