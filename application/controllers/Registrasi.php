<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			 $this->load->library('email');
           
          }
	public function index()

	{
		$data['prov']=$this->model_m->selectsemua('provinsi')->result();
		$this->load->view('registrasi',$data);
	}
 	public function validasi()

	{
		 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		$data['skl']=$this->model_m->rarequest();
		$this->load->view('pusat/validasi',$data);
	}
	public function validasikanwil()

	{
		$id_provinsi=$this->session->userdata('id_provinsi');
		 $data['jmlsekolah']=$this->model_m->selectx('sekolah','status='.'0' and 'id_provinsi='.$id_provinsi)->num_rows();
		$data['skl']=$this->model_m->rakanwil($id_provinsi);
		$this->load->view('provinsi/validasi',$data);
	}
 
 	// function get_kota(){
  //       $this->load->model('model_m');
  //       $id_provinsi = $this->input->post('id_provinsi');
  //       $dataKota = $this->model_m->getDataKota1($id_provinsi);
        
  //       echo '<select name="kab" class="form-control form-control-sm" id="kota">
  //       <option value="">Show All KOta/Kabupaten</option>';

  //       foreach($dataKota as $a){
  //           echo '
  //           <option value="'.$a->id_kotaKab.'">'.$a->nama_kota.'</option>';
  //       }
  //       echo '</select>';
  //   }
    public function aksi_insert()

	{
		$config['upload_path'] = './uploads';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto')){
			$data=$this->upload->data();			
			//$config['file_name'];
			$foto	= $data["file_name"];				
		}else{ $foto	=''; }
		$data1 = array(
							'nama_sekolah'=>$this->input->post('namaS'),
							'NSN'=>$this->input->post('nsn'),
							'jalan'=>$this->input->post('jln'),
							'kelurahan'=>$this->input->post('kel'),
							'kecamatan'=>$this->input->post('kec'),
							'kode_pos'=>$this->input->post('pos'),
							'id_provinsi'=>$this->input->post('prov'),
							'id_kotaKab'=>$this->input->post('kab'),
							'website'=>$this->input->post('web'),
							'telepon'=>$this->input->post('tlp'),
							'email'=>$this->input->post('email'),
							'kepala_RA'=>$this->input->post('kepRA'),
							'tahun_ajaran'=>$this->input->post('thn'),
							//'foto'=>$foto,
						);
					$this->model_m->input_data('sekolah',$data1);
					 echo '
      <script>
      window.alert("Data Akan Di proses");
  window.location.href = "'.site_url('Login').'"
     </script>';
	}
		public function popup(){

        	$id=$_POST['id'];
        	
       		$data['hasil']=$this->model_m->detailRA($id)->result();

       		$data['kode'] = $this->model_m->getpassword();
			$this->load->view('pusat/showRA',$data);
           
            
        }
          public function approve($id_sekolah){
		$where = array('id_sekolah'=>$this->input->post('fid'));
		$status = array('status'=>'1');
		$data1 = array(

							'username'=>$this->input->post('username'),
							'password'=>$this->input->post('password'),
							'id_sekolah'=>$this->input->post('fid'),
							'id_role'=>$this->input->post('role'),
							'status'=>$this->input->post('status'),
							
							//'foto'=>$foto,
						);
					$this->model_m->input_data('user',$data1);

		$update = $this->model_m->update_data('sekolah',$status,$where);
		// $to_email = 'indah.rsydh17@gmail.com';
		$username = $this->input->post('username');
		$to_mail = $this->input->post('email');
    	$password = $this->input->post('password');
		 $this->load->library('email');
    $from_email = "no-reply";

    $config['protocol']    = 'smtp';
    $config['smtp_host']    = 'ssl://smtp.gmail.com';
    $config['smtp_port']    = '465';
    $config['smtp_timeout'] = '7';
    $config['smtp_user']    = 'indahrsydh10@gmail.com';
    $config['smtp_pass']    = '1717ocidi';
    $config['charset']    = 'utf-8';
    $config['newline']    = "\r\n";
    $config['mailtype'] = 'html'; // or html
    $config['validation'] = TRUE; // bool whether to validate email or not

    $this->email->initialize($config);

    $this->email->from($from_email, 'Kementrian Agama');
    $this->email->to($to_mail);
    $this->email->subject('Konfirmasi');

    // $base = base_url();
    $this->email->message('
          <h2>EMAIL KONFIRMASI Registrasi RA</h2>
          <h4> Selamat Ra Anda telah terdaftar pada sistem untuk penggunaan aplikasi rapor digital</h4>
          <p> Silakan login dengan menggunakan :
          <br>  <p>Username : <strong>'.$username.'</strong>
          <br>  <p>Password : <strong>'.$password.'</strong></p>

          <p>
          Terima Kasih<br>
		  Kementrian Agama RI</p>
          
          
    ');

    //Send mail
    $this->email->send();
		
		redirect('Registrasi/validasi',$data);
	}
	public function reject($id_sekolah){
            $where = array("id_sekolah"=>$id_sekolah);
            $this->model_m->delete_data('sekolah',$where);
			//$this->menu_m->delete_data('menu',$where);
            redirect('Registrasi/validasi');
        }
	function get_kota(){
      
        $id_provinsi = $this->input->post('id_provinsi');
        $dataKota = $this->model_m->getDataKota1($id_provinsi);
        
        echo '<select required name="kab" class="form-control form-control-sm" id="kota">
        <option value="">Show All Kota/Kabupaten</option>';

        foreach($dataKota as $a){
            echo '
            <option value="'.$a->id_kotaKab.'">'.$a->nama_kota.'</option>';
        }
        echo '</select>';
    }
    public function edit_akun(){

        	 $id=$_POST['id'];
        	$where= array('id_guru'=>$id);
       		$data['hasil']=$this->model_m->selectX('user',$where)->result();

       		// $data['kode'] = $this->model_m->getpassword();
			$this->load->view('sekolah/showedit',$data);
           
            
        }

	
	
	
}
