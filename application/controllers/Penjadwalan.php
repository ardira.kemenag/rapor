<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjadwalan extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			if(!$this->session->userdata('username')){
				 redirect('login');
			}
           
          }
	public function index()

	{
    $id_sekolah=$this->session->userdata('id_sekolah');
    $data['kls']=$this->model_m->datakelas($id_sekolah)->result();
    $data['tema']=$this->model_m->tema($id_sekolah)->result();
    $data['prg']=$this->model_m->selectsemua('program')->result();
    $data['kd']=$this->model_m->kd()->result();
    $data['ind']=$this->model_m->selectsemua('indikator')->result();
    $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
    $data['smt']=$this->model_m->selectsemua('semester')->result();
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $this->load->view('penjadwalantambah',$data);
	}


	 public function jadwal(){
    $id_sekolah=$this->session->userdata('id_sekolah');
    $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $id_sekolah=$this->session->userdata('id_sekolah');
	  $data['jdw']=$this->model_m->rencana($id_sekolah);
    $this->load->view('penjadwalan',$data);
    }
     public function jadwalfilter(){
      $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
    $id_sekolah=$this->session->userdata('id_sekolah');
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $id_tahunajaran= $this->input->post('thn');
    $data['jdw']=$this->model_m->jadwalfilter($id_sekolah,$id_tahunajaran);
    $this->load->view('jadwalfilter',$data);
    }

  function get_sub(){
        $this->load->model('model_m');
        $id_tema = $this->input->post('id_tema');
        $dataSub = $this->model_m->getDataSub($id_tema);
        echo '<select name="f5" class="form-control" id="sub" onchange="get_subsub();" >';
        echo '<option value="">---Pilih sub tema---</option>';
        foreach($dataSub as $r){
            echo '<option value="'.$r->id_subtema.'">'.$r->nama_subtema. ' </option>';
        }
        echo '</select>';
    }
    function get_subsub(){
        $this->load->model('model_m');
        $id_subtema = $this->input->post('id_subtema');
        $subsub = $this->model_m->getDatasubsubtema($id_subtema);
        
        echo '<select name="fsubsub" class="form-control form-control-sm" >
        <option value="">Pilih Subsubtema</option>';

        foreach($subsub as $a){
            echo '
            <option value="'.$a->id_subsubtema.'">'.$a->nama.'</option>';
        }
        echo '</select>';
    }
    
    function get_kd(){
        $this->load->model('model_m');
        $kode_sk = $this->input->post('kode_sk');
        $datakd = $this->model_m->getDataKD($kode_sk);
        echo '<select name="fkd" id="kd" class="form-control">';
        echo '<option value="">---Pilih kompetensi dasar---</option>';
                                
        foreach($datakd as $r){
            echo '<option value="'.$r->id_kd.'">'.$r->nama_kd.' </option>';
        }
        echo '</select>';
        
    }
     public function aksiinsert(){
      
        $kd= json_encode($_POST['f6']);
        $ind= json_encode($_POST['f7']);
        $kls= json_encode($_POST['f2']);
    $data=array(
              'tanggal_awal'=>$this->input->post('f1'),
              'tanggal_akhir'=>$this->input->post('tglakhir'),
              'id_kelas'=>$kls,
              'id_program'=>$this->input->post('f3'),
              'id_tema'=>$this->input->post('f4'),
              'id_subtema'=>$this->input->post('f5'),
              'id_tahunajaran'=>$this->input->post('thn'),
              'id_semester'=>$this->input->post('smt'),
              'id_subsubtema'=>$this->input->post('fsubsub'),
             'id_kd'=>$kd,
             'id_indikator'=>$ind
    );
    //print_r($data);
   
    $this->model_m->input_data('perencanaan',$data);
   echo '
      <script>
      window.alert("Jadwal Ditambahkan");
  window.location.href = "'.site_url('Penjadwalan/jadwal').'"
     </script>';

   
    
   }
    public function ubahjadwal($id_perencanaan)
  {
    $id_sekolah=$this->session->userdata('id_sekolah');
    $data['kls']=$this->model_m->datakelas($id_sekolah)->result();
    $data['subsub']=$this->model_m->selectsemua('subsubtema')->result();
     $data['sub']=$this->model_m->selectsemua('subtema')->result();

     $data['kelas']=$this->model_m->selectsemua('kelas')->result();
    $data['tema']=$this->model_m->tema($id_sekolah)->result();
    $data['prg']=$this->model_m->selectsemua('program')->result();
    $data['kd']=$this->model_m->kd()->result();
    $data['ind']=$this->model_m->selectsemua('indikator')->result();
    $data['thn']=$this->model_m->selectsemua('tahun_ajaran')->result();
    $data['smt']=$this->model_m->selectsemua('semester')->result();
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $where=array($id_perencanaan=>$id_perencanaan);
    $data['dtjdw']=$this->model_m->ubahrencana($id_perencanaan);
          
    
    $this->load->view('penjadwalanubah',$data);
  }
 public function aksi_ubah(){
      
        $kd= json_encode($_POST['f6']);
        $ind= json_encode($_POST['f7']);
        $kls= json_encode($_POST['f2']);
    $data=array(
              'tanggal_awal'=>$this->input->post('f1'),
              'tanggal_akhir'=>$this->input->post('tglakhir'),
              'id_kelas'=>$kls,
              'id_program'=>$this->input->post('f3'),
              'id_tema'=>$this->input->post('f4'),
              'id_subtema'=>$this->input->post('f5'),
              'id_tahunajaran'=>$this->input->post('thn'),
              'id_semester'=>$this->input->post('smt'),
              'id_subsubtema'=>$this->input->post('fsubsub'),
             'id_kd'=>$kd,
             'id_indikator'=>$ind
    );
    //print_r($data);
   $where=array('id_perencanaan'=>$this->uri->segment(3));
    $this->model_m->update_data('perencanaan',$data,$where);
  echo $this->session->set_flashdata('msg2', "
                  <div class='alert alert-fill-warning' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil diubah
                  </div>");
  redirect('penjadwalan/jadwal');

   
    
   }
   public function hapus_jadwal($id_perencanaan){
            $where = array("id_perencanaan"=>$id_perencanaan);
            $this->model_m->delete_data('perencanaan',$where);
      //$this->menu_m->delete_data('menu',$where);
      echo $this->session->set_flashdata('msg2', "
                  <div class='alert alert-fill-danger' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil dihapus
                  </div>");
            redirect('penjadwalan/jadwal');
        }

        
	
}
