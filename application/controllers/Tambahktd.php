<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tambahktd extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function coba()
	{
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));	
		
		$inputFileName = './uploads/indikator.xlsx';
		try {
			$inputFileType = IOFactory::identify($inputFileName);
			$objReader = IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();
			for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
													NULL,
													TRUE,
													FALSE);
					// $jenis_i
								$data = array(					  
									
									"id_program"=>$rowData[0][0],					
									"kode_kd"=> $rowData[0][1],
									"kode_indikator"=> $rowData[0][2],
									"nama_indikator"=> $rowData[0][3],
									// "kecamatan"=> $rowData[0][4],
									// "kode_pos"=> $rowData[0][5],
									// "id_kotaKab"=> $rowData[0][6],
									// "id_provinsi"=> $rowData[0][7],
									// "website"=> $rowData[0][8],
									// "telepon"=> $rowData[0][9],
									// "email"=> $rowData[0][10],
									// "kepala_RA"=> $rowData[0][11],
									// "tahun_ajaran"=> $rowData[0][12],
									// "foto"=> $rowData[0][13],
									// "status"=> $rowData[0][14],
									// "tgl_daftar"=> $rowData[0][15],
									
						
								);
								
							$insert = $this->db->insert("indikator_2",$data);
						}
					
					 
					 
					//sesuaikan nama dengan nama tabel
					
		}
	
	
	public function data_kurang()
	{
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));	
		$this->load->model('M_data');
		$inputFileName = './upload/mts.xlsx';
		try {
			$inputFileType = IOFactory::identify($inputFileName);
			$objReader = IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();
			for ($row = 3; $row <= $highestRow; $row++)
			{                  //  Read a row of data into an array                 
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
												NULL,
												TRUE,
												FALSE);
				// $jenis_id = $this->jenis_id($rowData[0][10]);
				$postdata1['npsn'] = $rowData[0][3];
				$query1 = $this->M_data->tampil_kurang($postdata1);
				if($query1->num_rows() <1){
					$postdata['npsn'] = $rowData[0][3];
					$query = $this->M_data->tampil($postdata);
					if($query->num_rows() <1){
						$data = array(					
								"npsn"=> $rowData[0][3],							
								"name"=> $rowData[0][4],									
								"provinsi_id"=> $this->M_data->tampil_provinsi($rowData[0][0]),
								"kabupaten_id"=> $this->M_data->tampil_kabupaten($rowData[0][1]),								
								"school_type_id"=> 105,
								"provinsi_name"=> $rowData[0][0],
								"kabupaten_name"=> $rowData[0][1],									
								"school_type_name"=> 'MTS',									
								"peserta"=> $this->M_data->clean_number($rowData[0][5]),					
								"server"=> $this->M_data->clean_number($rowData[0][6]),
								"client"=> $this->M_data->clean_number($rowData[0][7]),
								"status"=> $this->M_data->clean_number($rowData[0][8]),
								"tempat_pelaksana"=> $this->M_data->clean($rowData[0][9]),
								"pj_name"=> $this->M_data->clean($rowData[0][10]),
								"pj_hp"=> $this->M_data->clean($rowData[0][11]),
								"proktor_name"=> $this->M_data->clean($rowData[0][12]),
								"proktor_hp"=> $this->M_data->clean($rowData[0][13]),
								"proktor_email"=> $this->M_data->clean($rowData[0][14]),
								"teknisi_name"=> $this->M_data->clean($rowData[0][15]),
								"teknisi_hp"=> $this->M_data->clean($rowData[0][16]),
								"teknisi_email"=> $this->M_data->clean($rowData[0][17]),						
							);			
						
						$insert = $this->db->insert("school_belum",$data);
					}
				}	
			}
	}
}
