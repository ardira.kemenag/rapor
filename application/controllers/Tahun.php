<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahun extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			if(!$this->session->userdata('username')){
				 redirect('login');
			}
           
          }
	public function index()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		
		$data['guru']=$this->model_m->guru($id_sekolah);
		$this->load->view('sekolah/inserttahun',$data);
	}
 
 	public function thn()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['kls']=$this->model_m->selectsemua('tahun_ajaran');
		$this->load->view('sekolah/tahun',$data);
	}
		

	
    public function aksi_insert()

	{
		$config['upload_path'] = './uploads';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto')){
			$data=$this->upload->data();			
			//$config['file_name'];
			$foto	= $data["file_name"];				
		}else{ $foto	=''; }
		$data1 = array(
							'nama_tahun'=>$this->input->post('f1'),
							
							
							//'foto'=>$foto,
						);
					$this->model_m->input_data('tahun_ajaran',$data1);
					  redirect('Tahun/data');
	}
	public function ubah_tahun($id_tahun)

	{
		$where= array('id_tahunajaran' => $id_tahun );
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		//$data['guru']=$this->model_m->guru($id_sekolah);
		$data['kls']=$this->model_m->selectX('tahun_ajaran',$where);
		$this->load->view('sekolah/ubahtahun',$data);
	}
	public function aksi_ubah()

	{
		$data1 = array(
							'nama_kelas'=>$this->input->post('f1'),
							'id_guru'=>$this->input->post('f2'),
							'id_sekolah'=>$this->input->post('sekolah'),
						);
					$where = array('id_tahunajaran'=>$this->input->post('fid'));
					$this->model_m->update_data('tahun',$data1,$where);
					  redirect('Tahun/data');
	}

	
	
}
