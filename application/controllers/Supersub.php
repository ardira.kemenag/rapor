<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supersub extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			if(!$this->session->userdata('username')){
				 redirect('login');
			}
           
          }
	public function index()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['tm']=$this->model_m->tema($id_sekolah);
		$this->load->view('subsubtambah',$data);
	}
 function get_tema(){
        $this->load->model('model_m');
        $id_tema = $this->input->post('id_tema');
        $dataKota = $this->model_m->getDatasubtema($id_tema);
        
        echo '<select name="fsub" class="form-control form-control-sm" id="subsub" >
        <option value="">Pilih Subtema</option>';

        foreach($dataKota as $a){
            echo '
            <option value="'.$a->id_subtema.'">'.$a->nama_subtema.'</option>';
        }
        echo '</select>';
    }
   
 
	public function listdata()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['sub']=$this->model_m->subsubtema($id_sekolah);
		$this->load->view('subsubtema',$data);
	}
		public function aksi_insert()

	{
		$data1 = array(
							'id_subtema'=>$this->input->post('fsub'),
							'id_tema'=>$this->input->post('f1'),
							'nama'=>$this->input->post('f2')
						);
					$this->model_m->input_data('subsubtema',$data1);
					echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-success' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil ditambahkan 
                  </div>");
					  redirect('Supersub/listdata');
	}
	public function ubah($id_sub)

	{
		$where = array('id_subsubtema' => $id_sub );
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['st']=$this->model_m->selectsemua('subtema');
		$data['tm']=$this->model_m->tema($id_sekolah);
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['sub']=$this->model_m->selectX('subsubtema',$where);
		//$data['siswa'] = $this->model_m->selectX('tema','id_tema='.$id_tema)->row();
		$this->load->view('subsububah',$data);
	}
		public function aksi_ubah()

	{
		$data1 = array(
							'id_tema'=>$this->input->post('f1'),
							'id_subtema'=>$this->input->post('fsub'),
							
							'nama'=>$this->input->post('f2')
						);
					$where = array('id_subsubtema'=>$this->input->post('fid'));
					$this->model_m->update_data('subsubtema',$data1,$where);
					echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-warning' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil diubah
                  </div>");
					  redirect('Supersub/listdata');
	}
	public function hapus_subsubtema($id_subsubtema){
            $where = array("id_subsubtema"=>$id_subsubtema);
            $this->model_m->delete_data('subsubtema',$where);
			//$this->menu_m->delete_data('menu',$where);
			echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-danger' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil dihapus
                  </div>");
            redirect('Supersub/listdata');
        }

	
	
	
}
