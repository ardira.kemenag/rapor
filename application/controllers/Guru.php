<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			 if(!$this->session->userdata('username')){
				 redirect('login');
			}
           
          }
	public function index()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['kode'] = $this->model_m->getkodeunik();
		$this->load->view('sekolah/tambahguru',$data);
	}
 
 	public function guru()

	{

		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['guru']=$this->model_m->dataguru($id_sekolah);
		$this->load->view('sekolah/guru',$data);
	}
	

	
    public function aksi_insert()

	{
		$data1 = array(
							'nama_guru'=>$this->input->post('f1'),
							'id_sekolah'=>$this->input->post('sekolah')
							//'foto'=>$foto,
						);
		$guru=$this->model_m->input_data('guru',$data1);
		$data2 = array(
							'username'=>$this->input->post('username'),
							'password'=>$this->input->post('password'),
							'id_role'=>$this->input->post('role'),
							'status'=>$this->input->post('status'),
							'id_sekolah'=>$this->input->post('sekolah'),
								'id_guru'=>$this->db->insert_id($guru),
							//'foto'=>$foto,
						);
					
						$this->model_m->input_data('user',$data2);
						echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-success' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil ditambahkan
                  </div>");
					  redirect('Guru/guru');
	}
	public function account()

	{

		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['guru']=$this->model_m->akunguru($id_sekolah);
		$this->load->view('sekolah/akun',$data);
	}
	public function ubah_guru($id_guru)

	{
		$where= array('id_guru' => $id_guru );
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		
		$data['guru']=$this->model_m->selectX('guru',$where);
		
		$this->load->view('sekolah/ubahguru',$data);
	}
	public function aksi_ubah()

	{
		$data1 = array(
							'nama_guru'=>$this->input->post('f1'),
							
						);
					$where = array('id_guru'=>$this->input->post('fid'));
					$this->model_m->update_data('guru',$data1,$where);
					echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-warning' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil diubah
                  </div>");
					  redirect('Guru/guru');
	}
	public function hapus_guru($id_guru){
            $where = array("id_guru"=>$id_guru);
            $this->model_m->delete_data('guru',$where);
             $this->model_m->delete_data('user',$where);
			//$this->menu_m->delete_data('menu',$where);
			echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-danger' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil dihapus
                  </div>");
            redirect('Guru/guru');
        }
	
	
	
}
