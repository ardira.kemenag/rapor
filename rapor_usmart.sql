-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2020 at 04:41 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rapor_usmart`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id_absen` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coba_rekap`
--

CREATE TABLE `coba_rekap` (
  `id_rekap` int(11) NOT NULL,
  `id_sekolah` text NOT NULL,
  `id_tahunajaran` int(11) NOT NULL,
  `nama_siswa` text NOT NULL,
  `bulan` text NOT NULL,
  `semester` text,
  `kelompok` text NOT NULL,
  `tahun` text NOT NULL,
  `program` text NOT NULL,
  `indikator` text,
  `nilai1` text,
  `nilai2` text,
  `nilai3` text,
  `nilai4` text,
  `nilai5` text,
  `nilai6` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(11) NOT NULL,
  `nama_guru` text NOT NULL,
  `id_sekolah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `indikator`
--

CREATE TABLE `indikator` (
  `id_indikator` int(11) NOT NULL,
  `id_program` int(11) NOT NULL,
  `kode_kd` varchar(23) NOT NULL,
  `kode_indikator` varchar(10) NOT NULL,
  `nama_indikator` text NOT NULL,
  `id_sekolah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `indikator`
--

INSERT INTO `indikator` (`id_indikator`, `id_program`, `kode_kd`, `kode_indikator`, `nama_indikator`, `id_sekolah`) VALUES
(1, 1, '["1.1"]', '1.1.1', 'Mempercayai adanya Tuhan melalui ciptaan Nya', 1),
(2, 1, '["1.1"]', '1.1.2', 'Sikap mengagungkan Allah  melalui Do’a-do’a secara tertib (Adab doa)', 0),
(3, 1, '["1.1"]', '1.1.3', 'Membiasakan dan mengungkapkan  Kalimat toyyibah', 0),
(4, 1, '["1.1"]', '1.1.4', 'Menyebut beberapa asmaul husna', 0),
(5, 1, '["1.1"]', '1.1.5', 'Menyebutkan beberapa nama malaikat', 0),
(6, 1, '["1.1"]', '1.1.6', 'Menyebutkan tugas-tugas nabi', 0),
(7, 1, '["1.1"]', '1.1.7', 'Menyebutkan beberapa nama Nabi dan Rosul ALLAH', 0),
(8, 1, '["1.1"]', '1.1.8', 'Menyebutkan keluarga Nabi', 0),
(9, 1, '["1.1"]', '1.1.9', 'Menyebutkan sahabat Nabi', 0),
(10, 1, '["1.1"]', '1.1.10', 'Menyebutkan beberapa hadits Nabi', 0),
(11, 1, '["1.1"]', '1.1.11', 'Menyebutkan nama kitab suci umat Islam', 0),
(12, 1, '["1.1"]', '1.1.12', 'Menyebutkan hurup hijaiyah', 0),
(13, 1, '["1.1"]', '1.1.13', 'Menghafalkan beberapa surat pendek dalam Al-qur’an', 0),
(14, 1, '["1.1"]', '1.1.14', 'Mengucapkan syahadat Tauhid', 0),
(15, 1, '["1.1"]', '1.1.15', 'Sikap Khusyu saat beribadah', 0),
(16, 1, '["1.1"]', '1.1.16', 'Terbiasa menutup aurat', 0),
(17, 1, '["1.1"]', '1.1.17', 'Membaca rangkaian huruf hijaiyah', 0),
(18, 1, '["1.1"]', '1.1.18', 'Mengucapkan syahadat tauhid', 0),
(19, 1, '["1.1"]', '1.1.19', 'Menyebutkan arti syahadat tauhid', 0),
(20, 1, '["1.1"]', '1.1.20', 'Menyebutkan nama-nama shalat lima waktu', 0),
(21, 1, '["1.1"]', '1.1.21', 'Menyebutkan jumlah rakaat shalat lima waktu', 0),
(22, 1, '["1.1"]', '1.1.22', 'Menirukan gerakan shalat', 0),
(23, 1, '["1.1"]', '1.1.23', 'Melafalkan bacaan shalat', 0),
(24, 1, '["1.1"]', '1.1.24', 'Menirukan gerakan wudhu', 0),
(25, 1, '["1.1"]', '1.1.25', 'Menyebutkan arti zakat', 0),
(26, 1, '["1.1"]', '1.1.26', 'Menyebutkan arti shodaqoh', 0),
(27, 1, '["1.1"]', '1.1.27', 'Mempraktekan zakat', 0),
(28, 1, '["1.1"]', '1.1.28', 'Mempraktekan shodaqoh', 0),
(29, 1, '["1.1"]', '1.1.29', 'Menyebutkan arti shaum', 0),
(30, 1, '["1.1"]', '1.1.30', 'Menyebutkan arti sholat ied', 0),
(31, 1, '["1.1"]', '1.1.31', 'Menyebutkan tatacara haji secara sederhana', 0),
(32, 1, '["1.1"]', '1.1.32', 'Menyebut tokoh ibadah qurban', 0),
(33, 1, '["1.1"]', '1.1.33', 'Menghapal beberapa doa harian', 0),
(34, 1, '["1.1"]', '1.1.34', 'Menyebutkan beberapa kalimah toyyibah', 0),
(35, 1, '["1.2"]', '1.2.1', 'Sikap sopan santun saat berbicara ', 0),
(36, 1, '["1.2"]', '1.2.2', 'Sikap meminta maaf dan memaafkan orang lain', 0),
(37, 1, '["1.2"]', '1.2.3', 'Berpakaian yang baik dan menutup aurat', 0),
(38, 1, '["2.13"]', '1.3.1', 'Terbiasa tidak sombong', 0),
(39, 1, '["2.13"]', '1.3.2', 'Terbiasa menghargai kepemilikan orang lain', 0),
(40, 1, '["2.13"]', '1.3.3', 'Terbiasa mengembalikan benda yang bukan hak nya', 0),
(41, 1, '["3.1","4.1"]', '1.5.1', 'Mengucapkan doa-doa pendek melakukan ibadah sehari-hari,berdoa sebelum dan sesudah kegiatan', 0),
(42, 1, '["3.1","4.1"]', '1.5.2', 'Berprilaku sesuai dengan ajaran agama,tidak sombong', 0),
(43, 1, '["3.1","4.1"]', '1.5.3', 'Menyebutkan hari-hari besar agama', 0),
(44, 1, '["3.1","4.1"]', '1.5.4', 'Menyebutkan tempat ibadah', 0),
(45, 1, '["3.1","4.1"]', '1.5.5', 'Menceritakan kembali nabi nabi utusan Allah', 0),
(46, 1, '["3.2","4.2"]', '1.7.1', 'Berprilaku sopan dan peduli ', 0),
(47, 1, '["3.2","4.2"]', '1.7.2', 'Mau menolong orang tua,guru,dan teman', 0),
(48, 1, '["3.2","4.2"]', '1.7.3', 'Mau menerima tugas dengan ikhlas', 0),
(49, 1, '["3.2","4.2"]', '1.7.4', 'Terbiasa melakukan kegiatan sendiri', 0),
(50, 1, '["3.2","4.2"]', '1.7.5', 'Terbiasa mengikuti tata tertib dan aturan sekolah', 0),
(51, 1, '["3.2","4.2"]', '1.7.6', 'Tepat waktu saat berangkat dan pulang sekolah', 0),
(52, 1, '["3.2","4.2"]', '1.7.7', 'Terbiasa berhenti bermain pada waktunya', 0),
(53, 1, '["3.2","4.2"]', '1.7.8', 'Rapi dalam bertindak dan bekerja', 0),
(54, 1, '["3.2","4.2"]', '1.7.9', 'Tanggung jawab atas tugas yang diberikan', 0),
(55, 1, '["3.2","4.2"]', '1.7.10', 'Terbiasa mengembalikan mainan ke tempatnya', 0),
(56, 1, '["3.2","4.2"]', '1.7.11', 'Dapat membedakan milik sendiri dan sekolah', 0),
(57, 1, '["3.2","4.2"]', '1.7.12', 'Terbiasa mengambil makanan secukupnya dan makan sendiri', 0),
(58, 1, '["3.2","4.2"]', '1.7.13', 'Berani karena benar dan mempunyai rasa ingin tahu yang besar', 0),
(59, 1, '["3.2","4.2"]', '1.7.14', 'Terbiasa mengerjakan keperluannya sendiri', 0),
(60, 1, '["3.2","4.2"]', '1.7.15', 'Sabar menunggu giliran', 0),
(61, 1, '["3.2","4.2"]', '1.7.16', 'Dapat dibujuk', 0),
(62, 1, '["3.2","4.2"]', '1.7.17', 'Tidak cengeng', 0),
(63, 1, '["3.2","4.2"]', '1.7.18', 'Mengendalikan emosi dengan cara yang wajar', 0),
(64, 1, '["3.2","4.2"]', '1.7.19', 'Berani tampil didepan umum', 0),
(65, 1, '["3.2","4.2"]', '1.7.20', 'Berani mempertahankan pendapatnya', 0),
(66, 2, '["2.1"]', '2.1.1', 'Terbiasa melakukan kegiatan-kegiatan sendiri', 0),
(67, 2, '["2.1"]', '2.1.2', 'Terbiasa makan makanan bergizi dan seimbang', 0),
(68, 2, '["2.1"]', '2.1.3', 'Terbiasa memelihara kebersihan lingkungan', 0),
(69, 2, '["3.3","4.3"]', '2.3.1', 'Melakukan berbagai gerakan terkordinasi secara terkontrol,seimbang dan lincah.', 0),
(70, 2, '["3.3","4.3"]', '2.3.2', 'Melakukan gerakan mata, tangan,kaki, kepala secara terkordinasi dalam menirukan berbagai gerakan yang teratur ( misal: senam dan tarian).', 0),
(71, 2, '["3.3","4.3"]', '2.3.3', 'Melakukan permainan fisik dengan aturan', 0),
(72, 2, '["3.3","4.3"]', '2.3.4', 'Terampil menggunakan tangan kanan dan kiri dalam berbagai aktivitas (misal: mengancingkannya baju, menali sepatu, menggambar, menempel, menggunting pola, meniru bentuk, menggunakan alat makan )', 0),
(73, 2, '["3.4","4.4"]', '2.5.1', 'Melakukan kebiasaan hidup bersih dan sehat (misal: mandi 2x sehari, memakai baju bersih, membuang sampah pada tempatnya, menutup hidung dan mulut ketika batuk dan bersin, membersihkan dan membereskan dan membersihkan tempat bermain)', 0),
(74, 2, '["3.4","4.4"]', '2.5.2', 'Mampu melindungi diri dari percobaan kekerasan, termasuk kekerasan seksual dan bullying (misal : dengan berteriak dan atau berlari)', 0),
(75, 2, '["3.4","4.4"]', '2.5.3', 'Mampu menjaga keamanan diri dari benda-benda berbahaya (misal: Listrik, pisau, pembasmi serangga, kendaraan di jalan raya)', 0),
(76, 2, '["3.4","4.4"]', '2.5.4', 'Menggunakan toilet dengan benar tanpa bantuan', 0),
(77, 2, '["3.4","4.4"]', '2.5.5', 'Mengenal kebiasaan buruk bagi kesehatan (makan permen, jajan sembarang tempat)', 0),
(78, 3, '["2.2"]', '3.1.1', 'Terbiasa menunjukkan aktivitas yang bersifat eksploratif dan menyelidik (seperti: apa yang terjadi ketika air ditumpahkan)', 0),
(79, 3, '["2.2"]', '3.1.2', 'Terbiasa aktif bertanya', 0),
(80, 3, '["2.2"]', '3.1.3', 'Terbiasa mencoba atau melakukan melakukan sesuatu  untuk mendapatkan jawaban ', 0),
(81, 3, '["2.3"]', '3.2.1', 'Kreatif dalam menyelesaikan masalah ( ide, gagasan, diluar kebiasaan atau cara yang tidak biasa )', 0),
(82, 3, '["2.3"]', '3.2.2', 'Menunjukan inisiatif dalam memilih permainan ( seperti:’ayo kita bermain pura-pura seperti burung”)', 0),
(83, 3, '["2.3"]', '3.2.3', 'Senang menerapkan pengetahuan atau pengalam dalam situasi atau sesuatu yang baru.', 0),
(84, 3, '["3.5","4.5"]', '3.4.1', 'Mampu memecahkan sendiri masalah sederhana yang dihadapi', 0),
(85, 3, '["3.5","4.5"]', '3.4.2', 'Menyelesaikan tugas meskipun menghadapi kesulitan', 0),
(86, 3, '["3.5","4.5"]', '3.4.3', 'Menyusun perencanaan kegiatan yang akan dilakukan', 0),
(87, 3, '["3.5","4.5"]', '3.4.4', 'Menyesuaikan diri dengan cuaca dan kondisi alam', 0),
(88, 3, '["3.5","4.5"]', '3.4.5', 'Menyelesaikan tugas meskipun menghadapi kesulitan', 0),
(89, 3, '["3.6","4.6"]', '3.6.1', 'Mengenal benda dengan mengelompokkan berbagai benda dilingkungannya berdasarkan ukuran,sifat, suara, tekstur, fungsi dan ciri-ciri lainnya', 0),
(90, 3, '["3.6","4.6"]', '3.6.2', 'Mengenal benda dengan menghubungkan satu benda dengan benda yang lain', 0),
(91, 3, '["3.6","4.6"]', '3.6.3', 'Menghubungkan atau menjodohkan nama benda dengan tulisan sederhana melalui berbagai aktifitas', 0),
(92, 3, '["3.6","4.6"]', '3.6.4', 'Mengenal konsep besar kecil, banyak sedikit, panjang pendek, berat ringan, tinggi rendah, dengan mengukur menggunakan alat ukur tidak baku', 0),
(93, 3, '["3.6","4.6"]', '3.6.5', 'Membuat pola ABCD -ABCD', 0),
(94, 3, '["3.6","4.6"]', '3.6.6', 'Mampu mengurutkan lima seriasi atau lebih berdasarkan warna, bentuk, ukuran atau jumlah,', 0),
(95, 3, '["3.6","4.6"]', '3.6.7', 'Mengenal perbedaan berdasarkan ukuran : “ lebih dari”.,”kurang dari’., dan “paling/ter” ', 0),
(96, 3, '["3.6","4.6"]', '3.6.8', 'Mengklasifikasikan benda berdasarkan tiga pariabel warna, bentuk dan ukuran', 0),
(97, 3, '["3.6","4.6"]', '3.6.9', 'Menyebutkan lambang bilangan 1-10', 0),
(98, 3, '["3.6","4.6"]', '3.6.10', 'Menggunakan lambang bilangan untuk menghitung', 0),
(99, 3, '["3.6","4.6"]', '3.6.11', 'Mencocokkan bilangan dengan lambang bilangan', 0),
(100, 3, '["3.7","4.7"]', '3.8.1', 'Menyebutkan nama anggota keluarga danteman serta ciri-ciri husus mereka secara lebih rinci ( warna kulit,warna rambut, jenis rambut dll)', 0),
(101, 3, '["3.7","4.7"]', '3.8.2', 'Menjelaskan lingkungan sekitarnya secara sederhana', 0),
(102, 3, '["3.7","4.7"]', '3.8.3', 'Menyebutkan arah ketempat yang sering dikunjungi dan alat transportasi yang digunakan', 0),
(103, 3, '["3.7","4.7"]', '3.8.4', 'Menyebutkan peran-peran dan pekerjaan termasuk didalamnya perlengkapan/ atribut dan tugas-tugas yang dilakukan dalam pekerjaan tersebut', 0),
(104, 3, '["3.7","4.7"]', '3.8.5', 'Membuat dan mengikuti aturan ', 0),
(105, 3, '["3.8","4.8"]', '3.10.1', 'Menceritakan peristiwa-peristiwa alam dengan melakukan percobaan sederhana ', 0),
(106, 3, '["3.8","4.8"]', '3.10.2', 'Mengungkapkan hasil karya yang dibuatnya secara lengkap/utuh yang berhubungan dengan benda-benda yang ada dilingkungan alam', 0),
(107, 3, '["3.8","4.8"]', '3.10.3', 'Menceritakan perkembang biakan makhluk hidup', 0),
(108, 3, '["3.8","4.8"]', '3.10.4', 'Mengenal sebab akibat tentang lingkungannya ( angin bertiup menyebabkan daun bergerak, air dapat menyebabkan sesuatu basah)', 0),
(109, 3, '["3.9","4.9"]', '3.12.1', 'Melakukan kegiatan dengan menggunakan alat tekhnologi sederhana sesuai pungsinya secara aman dan bertanggung jawab', 0),
(110, 3, '["3.9","4.9"]', '3.12.2', 'Membuat alat-alat tekhnologi sederhana (misalnya: baling-baling)', 0),
(111, 3, '["3.9","4.9"]', '3.12.3', 'Melakukan proses kerja sesuai dengan prosedurnya (misal: membuat teh dimulai dari menyediakan air panas)', 0),
(112, 4, '["2.14"]', '4.1.1', 'Terbiasa ramah menyapa siapapun', 0),
(113, 4, '["2.14"]', '4.1.2', 'Terbiasa berkata lembut dan santun', 0),
(114, 4, '["3.10","4.10"]', '4.3.1', 'Menceritakan kembali apa yang didengar dengan kosa kata yang lebih banyak', 0),
(115, 4, '["3.10","4.10"]', '4.3.2', 'Melaksanakan perintah yang lebik komplek sesuai dengan aturan yang sesuai dengan aturan yang disampaikan', 0),
(116, 4, '["3.10","4.10"]', '4.3.3', 'Mengulang kalimat yang lebih komplek', 0),
(117, 4, '["3.10","4.10"]', '4.3.4', 'Memahami aturan dalam suatu permainan', 0),
(118, 4, '["3.11","4.11"]', '4.5.1', 'Mengungkapkan keinginan, perasaan, dan pendapat dengan kalimat sederhana dalam berkomunikasi dengan anak atau orang dewasa', 0),
(119, 4, '["3.11","4.11"]', '4.5.2', 'Menunjukkan perilaku senang membaca buku terhadap buku-buku yang dikenali', 0),
(120, 4, '["3.11","4.11"]', '4.5.3', 'Mengungkapkan perasaan, ide dengan pilihan kata yang sesuai ketika berkomunikasi', 0),
(121, 4, '["3.11","4.11"]', '4.5.4', 'Menceritakan kembali isi cerita cesara sederhana', 0),
(122, 4, '["3.11","4.11"]', '4.5.5', 'Menjawab pertanyaan yang lebik kompleks', 0),
(123, 4, '["3.11","4.11"]', '4.5.6', 'Menyebutkan kelompok gambar yang memiliki bunyi yang sama', 0),
(124, 4, '["3.11","4.11"]', '4.5.7', 'Menyusun kalimat sederhana dalam struktur lengkap', 0),
(125, 4, '["3.12","4.12"]', '4.7.1', 'Menunjukkan bentuk-bentuk simbol ( pola Menulis )', 0),
(126, 4, '["3.12","4.12"]', '4.7.2', 'Menyebutkan lambang-lambang huruf yang dikenal', 0),
(127, 4, '["3.12","4.12"]', '4.7.3', 'Menulis hurup- hurup dari namanya sendiri', 0),
(128, 4, '["3.12","4.12"]', '4.7.4', 'Mengenal suara hurup awal dari nama-nama benda disekitarnya', 0),
(129, 4, '["3.12","4.12"]', '4.7.5', 'Menyebutkan kelompok gambar yang memiliki bunyi/hurup awal yang sama', 0),
(130, 4, '["3.12","4.12"]', '4.7.6', 'Mengenal berbagai macam lambang  huruf fokal dan konsonan', 0),
(131, 4, '["3.12","4.12"]', '4.7.7', 'Memahami hubungan antara bunyi dan bentuk huruf', 0),
(132, 4, '["3.12","4.12"]', '4.7.8', 'Membaca nama sendiri ', 0),
(133, 4, '["3.12","4.12"]', '4.7.9', 'Membuat gambar dengan beberapa coretan /tulisan yang berbentuk hurup/ kata', 0),
(134, 4, '["3.12","4.12"]', '4.7.10', 'Menyebutkan angka bila diperlihatkan lambang bilangannya ( mengucapkan bunyi lambang bilangan)', 0),
(135, 4, '["3.12","4.12"]', '4.7.11', 'Senang dan menghargai bacaan', 0),
(136, 4, '["3.12","4.12"]', '4.7.12', 'Memahami arti kata dalam cerita', 0),
(137, 4, '["3.12","4.12"]', '4.7.13', 'Memiliki perbendahaan kata ', 0),
(138, 4, '["3.12","4.12"]', '4.7.14', 'Mengenal simbol-simbol untuk persiapan membaca,menulis dan berhitung', 0),
(139, 5, '["2.5"]', '5.1.1', 'Terbiasa menyapa guru saat penyambutan', 0),
(140, 5, '["2.5"]', '5.1.2', 'Berani tambil didepan teman, guru, orang tua dan lingkungan.dan sosial lainnya', 0),
(141, 5, '["2.5"]', '5.1.3', 'Berani mengemukakan pendapat', 0),
(142, 5, '["2.5"]', '5.1.4', 'Berani menyampaikan keinginan', 0),
(143, 5, '["2.5"]', '5.1.5', 'Berkomunikasi dengan orang yang belum dikenal sebelumnya dengan pengawasan guru', 0),
(144, 5, '["2.5"]', '5.1.6', 'Bangga menunjukkan hasil karya', 0),
(145, 5, '["2.5"]', '5.1.7', 'Senang ikut serta dalam kegiatan bersama', 0),
(146, 5, '["2.5"]', '5.1.8', 'Tidak berpengaruh penilaan oranng tentang dirinya', 0),
(147, 5, '["2.6"]', '5.2.1', 'Tau akan haknya', 0),
(148, 5, '["2.6"]', '5.2.2', 'Mentaati aturan kelas ( kegiatan, aturan)', 0),
(149, 5, '["2.6"]', '5.2.3', 'Mengatur diri sendiri', 0),
(150, 5, '["2.7"]', '5.3.1', 'Kesediaan diri untuk menahan diri', 0),
(151, 5, '["2.7"]', '5.3.2', 'Bersikap tenang tidak lekas marah dan dapat menunda keinginan', 0),
(152, 5, '["2.7"]', '5.3.3', 'Sikap mau menunggu giliran , mau mendengarkan ketika orang lain bicara', 0),
(153, 5, '["2.7"]', '5.3.4', 'Tidak menangis saat berpisah dengan orang tuanya', 0),
(154, 5, '["2.7"]', '5.3.5', 'Tidak mudah mengeluh ', 0),
(155, 5, '["2.7"]', '5.3.6', 'Tidak tergesa-gesa', 0),
(156, 5, '["2.7"]', '5.3.7', 'Selalu menyesaikan gagasan-gagasannya hingga tuntas', 0),
(157, 5, '["2.7"]', '5.3.8', 'Berusaha tidak  menyakiti atau membalas kekerasan', 0),
(158, 5, '["2.8"]', '5.4.1', 'Terbiasa tidak bergantung pada orang lain', 0),
(159, 5, '["2.8"]', '5.4.2', 'Terbiasa mengambil keputusan secara mandiri', 0),
(160, 5, '["2.8"]', '5.4.3', 'Merencanakan, memilih, memiliki inisiatif untuk belajar atau melakukan sesuatu tanpa harus dibantu atau dibantu seperlunya', 0),
(161, 5, '["2.9"]', '5.5.1', 'Mengetahui perasaan temannya dan meresponn secara wajar', 0),
(162, 5, '["2.9"]', '5.5.2', 'Berbagi dengan orang lain', 0),
(163, 5, '["2.9"]', '5.5.3', 'Menghargai/hak/pendapat/karya orang lain', 0),
(164, 5, '["2.9"]', '5.5.4', 'Terbiasa mengindahkan dan memperhatikan kondisi teman', 0),
(165, 5, '["2.9"]', '5.5.5', 'Mau menemani teman melakukan kegiatan bersama', 0),
(166, 5, '["2.9"]', '5.5.6', 'Senang menawarkan bantuan pada teman atau gurupeka untuk membantu orang lain yang membutuhkan', 0),
(167, 5, '["2.9"]', '5.5.7', 'Mampu menenangkan diri dan temannya dalam berbagai situasai,', 0),
(168, 5, '["2.9"]', '5.5.8', 'Senang mengajak temannya untuk berkomunikasi,beraksi positif kepada semua temannya', 0),
(169, 5, '["2.10"]', '5.6.1', 'Bermain dengan teman sebaya', 0),
(170, 5, '["2.10"]', '5.6.2', 'Menerima perbedaan teman dengan dirinya', 0),
(171, 5, '["2.10"]', '5.6.3', 'Menghargai karya teman', 0),
(172, 5, '["2.10"]', '5.6.4', 'Tidak ingin menang sendiri', 0),
(173, 5, '["2.10"]', '5.6.5', 'Menghargai pendapat pendapat teman dan mendengarkan dengan sabar pendapat teman', 0),
(174, 5, '["2.10"]', '5.6.6', 'Senang berteman dengan semuanya', 0),
(175, 5, '["2.11"]', '5.7.1', 'Memperlihatkan diri untuk menyesuaikan dengan situasi', 0),
(176, 5, '["2.11"]', '5.7.2', 'Memperlihatkan kehati-hatian kepada orang yang belum dikenal ( menumbuhkan kepercayaan kepada pada orang dewasa yang tepat)', 0),
(177, 5, '["2.11"]', '5.7.3', 'Bersikap kooperatif dengan teman', 0),
(178, 5, '["2.11"]', '5.7.4', 'Menggunakan cara yang diterima secara sosial dalam menyelesaikan masalah ( menggunakan pikiran untuk menyelesaikan masalah)', 0),
(179, 5, '["2.11"]', '5.7.5', 'Tetap tenang saat ditempat yang baru dengan situasai baru misal:saat bertemu, berada dipusat perbelanjaan, atau saat bertemu dengan guru baru,', 0),
(180, 5, '["2.12"]', '5.8.1', 'Bertanggung jawab atas perilaku untuk kebaikan diri sendiri', 0),
(181, 5, '["2.12"]', '5.8.2', 'Bersedia untuk menerima konsekwensi atau menanggung akibat atas tindakan yang diperbuat baik secara sengaja maupuntidak sengaja', 0),
(182, 5, '["2.12"]', '5.8.3', 'Mau mengakui kesalahan dengan meminta maaf', 0),
(183, 5, '["2.12"]', '5.8.4', 'Merapihkan/ membereskan mainan pada tempat semula', 0),
(184, 5, '["2.12"]', '5.8.5', 'Mengerjakan sesuatu hingga tuntas', 0),
(185, 5, '["2.12"]', '5.8.6', 'Senang menjalankan kegiatan yang jadi tugasnya (misalnya piket sebagai pemimpin harus membantu menyiapkan alat makan )', 0),
(186, 5, '["3.14","4.14"]', '5.10.1', 'mengenal perasaan sendiri dan orang lain', 0),
(187, 5, '["3.14","4.14"]', '5.10.2', 'mengelolanya secara wajar ( mengendalikan diri secara wajar)', 0),
(188, 5, '["3.14","4.14"]', '5.10.3', 'Berprilaku yang membuat orang lain nyaman', 0),
(189, 5, '["3.14","4.14"]', '5.10.4', 'Mengekspresikan emosi yang sesuai dengan kondisi yang ada ( senang-sedih-antusias dsb )', 0),
(190, 5, '["3.14","4.14"]', '5.12.1', 'Memilih kegiatan/ benda yang paling sesuai dengan yang dibutuhkan dari beberapa pilihan yang ada', 0),
(191, 5, '["3.14","4.14"]', '5.12.2', 'Mengungkapkan yang dirasakan ( lapar ingin makan, kedinginan, memerlikan baju hangat, perlu payung agar tidak keehujanan, kepanasan, sakit perut perlu berobat)', 0),
(192, 5, '["3.14","4.14"]', '5.12.3', 'Menggunakan sesuatu sesuai kebutuhan', 0),
(193, 6, '["2.4"]', '6.1.1', 'Menghargai keindahan diri sendiri, karya sendiri atau orang lain, alam dan lingkungan sekitar', 0),
(194, 6, '["2.4"]', '6.1.2', 'Menjaga kerapihan diri', 0),
(195, 6, '["2.4"]', '6.1.3', 'Bertindak /berbuat yang mencerminkan sikap estetis', 0),
(196, 6, '["2.4"]', '6.1.4', 'Merawat kerapihan, kebersihan, dan keutuhan benda mainan atau milik pribadinya', 0),
(197, 6, '["3.15","4.15"]', '6.3.1', 'Membuat karya seni sesuai kreativitasnya misal seni musik, visual, gerak, tari yang dihasilkannya dengan menggunakan alat yang sesuai', 0),
(198, 6, '["3.15","4.15"]', '6.3.2', 'Menampilkan hasil karya seni baik dalam bentuk gambar', 0),
(199, 6, '["3.15","4.15"]', '6.3.3', 'Menghargai hasil karya baik dalam bentuk gambar', 0),
(200, 1, '["1.1"]', '1.1.15 ', 'Memperdalam keberadaan-NYA', 1),
(201, 1, '["1.2"]', '1.2.17', 'Bertukar pendapat dengan teman', 1),
(202, 1, '["1.1"]', '1.1.50', 'coba', 108);

-- --------------------------------------------------------

--
-- Table structure for table `indikator_2`
--

CREATE TABLE `indikator_2` (
  `id_indikator` int(11) NOT NULL,
  `id_program` int(11) NOT NULL,
  `kode_kd` varchar(23) NOT NULL,
  `kode_indikator` varchar(10) NOT NULL,
  `nama_indikator` text NOT NULL,
  `id_sekolah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `indikator_2`
--

INSERT INTO `indikator_2` (`id_indikator`, `id_program`, `kode_kd`, `kode_indikator`, `nama_indikator`, `id_sekolah`) VALUES
(1, 1, '[1.1]', '1.1.2', 'Sikap mengagungkan Allah  melalui Do’a-do’a secara tertib (Adab doa)', 0),
(2, 1, '[1.1]', '1.1.3', 'Membiasakan dan mengungkapkan  Kalimat toyyibah', 0),
(3, 1, '[1.1]', '1.1.4', 'Menyebut beberapa asmaul husna', 0),
(4, 1, '[1.1]', '1.1.5', 'Menyebutkan beberapa nama malaikat', 0),
(5, 1, '[1.1]', '1.1.6', 'Menyebutkan tugas-tugas nabi', 0),
(6, 1, '[1.1]', '1.1.7', 'Menyebutkan beberapa nama Nabi dan Rosul ALLAH', 0),
(7, 1, '[1.1]', '1.1.8', 'Menyebutkan keluarga Nabi', 0),
(8, 1, '[1.1]', '1.1.9', 'Menyebutkan sahabat Nabi', 0),
(9, 1, '[1.1]', '1.1.10', 'Menyebutkan beberapa hadits Nabi', 0),
(10, 1, '[1.1]', '1.1.11', 'Menyebutkan nama kitab suci umat Islam', 0),
(11, 1, '[1.1]', '1.1.12', 'Menyebutkan hurup hijaiyah', 0),
(12, 1, '[1.1]', '1.1.13', 'Menghafalkan beberapa surat pendek dalam Al-qur’an', 0),
(13, 1, '[1.1]', '1.1.14', 'Mengucapkan syahadat Tauhid', 0),
(14, 1, '[1.1]', '1.1.15', 'Sikap Khusyu saat beribadah', 0),
(15, 1, '[1.1]', '1.1.16', 'Terbiasa menutup aurat', 0),
(16, 1, '[1.1]', '1.1.17', 'Membaca rangkaian huruf hijaiyah', 0),
(17, 1, '[1.1]', '1.1.18', 'Mengucapkan syahadat tauhid', 0),
(18, 1, '[1.1]', '1.1.19', 'Menyebutkan arti syahadat tauhid', 0),
(19, 1, '[1.1]', '1.1.20', 'Menyebutkan nama-nama shalat lima waktu', 0),
(20, 1, '[1.1]', '1.1.21', 'Menyebutkan jumlah rakaat shalat lima waktu', 0),
(21, 1, '[1.1]', '1.1.22', 'Menirukan gerakan shalat', 0),
(22, 1, '[1.1]', '1.1.23', 'Melafalkan bacaan shalat', 0),
(23, 1, '[1.1]', '1.1.24', 'Menirukan gerakan wudhu', 0),
(24, 1, '[1.1]', '1.1.25', 'Menyebutkan arti zakat', 0),
(25, 1, '[1.1]', '1.1.26', 'Menyebutkan arti shodaqoh', 0),
(26, 1, '[1.1]', '1.1.27', 'Mempraktekan zakat', 0),
(27, 1, '[1.1]', '1.1.28', 'Mempraktekan shodaqoh', 0),
(28, 1, '[1.1]', '1.1.29', 'Menyebutkan arti shaum', 0),
(29, 1, '[1.1]', '1.1.30', 'Menyebutkan arti sholat ied', 0),
(30, 1, '[1.1]', '1.1.31', 'Menyebutkan tatacara haji secara sederhana', 0),
(31, 1, '[1.1]', '1.1.32', 'Menyebut tokoh ibadah qurban', 0),
(32, 1, '[1.1]', '1.1.33', 'Menghapal beberapa doa harian', 0),
(33, 1, '[1.1]', '1.1.34', 'Menyebutkan beberapa kalimah toyyibah', 0),
(34, 1, '[1.2]', '1.2.1', 'Sikap sopan santun saat berbicara ', 0),
(35, 1, '[1.2]', '1.2.2', 'Sikap meminta maaf dan memaafkan orang lain', 0),
(36, 1, '[1.2]', '1.2.3', 'Berpakaian yang baik dan menutup aurat', 0),
(37, 1, '[2.13]', '1.3.1', 'Terbiasa tidak sombong', 0),
(38, 1, '[2.13]', '1.3.2', 'Terbiasa menghargai kepemilikan orang lain', 0),
(39, 1, '[2.13]', '1.3.3', 'Terbiasa mengembalikan benda yang bukan hak nya', 0),
(40, 1, '[3.1,4.1]', '1.5.1', 'Mengucapkan doa-doa pendek melakukan ibadah sehari-hari,berdoa sebelum dan sesudah kegiatan', 0),
(41, 1, '[3.1,4.1]', '1.5.2', 'Berprilaku sesuai dengan ajaran agama,tidak sombong', 0),
(42, 1, '[3.1,4.1]', '1.5.3', 'Menyebutkan hari-hari besar agama', 0),
(43, 1, '[3.1,4.1]', '1.5.4', 'Menyebutkan tempat ibadah', 0),
(44, 1, '[3.1,4.1]', '1.5.5', 'Menceritakan kembali nabi nabi utusan Allah', 0),
(45, 1, '[3.2,4.2]', '1.7.1', 'Berprilaku sopan dan peduli ', 0),
(46, 1, '[3.2,4.2]', '1.7.2', 'Mau menolong orang tua,guru,dan teman', 0),
(47, 1, '[3.2,4.2]', '1.7.3', 'Mau menerima tugas dengan ikhlas', 0),
(48, 1, '[3.2,4.2]', '1.7.4', 'Terbiasa melakukan kegiatan sendiri', 0),
(49, 1, '[3.2,4.2]', '1.7.5', 'Terbiasa mengikuti tata tertib dan aturan sekolah', 0),
(50, 1, '[3.2,4.2]', '1.7.6', 'Tepat waktu saat berangkat dan pulang sekolah', 0),
(51, 1, '[3.2,4.2]', '1.7.7', 'Terbiasa berhenti bermain pada waktunya', 0),
(52, 1, '[3.2,4.2]', '1.7.8', 'Rapi dalam bertindak dan bekerja', 0),
(53, 1, '[3.2,4.2]', '1.7.9', 'Tanggung jawab atas tugas yang diberikan', 0),
(54, 1, '[3.2,4.2]', '1.7.10', 'Terbiasa mengembalikan mainan ke tempatnya', 0),
(55, 1, '[3.2,4.2]', '1.7.11', 'Dapat membedakan milik sendiri dan sekolah', 0),
(56, 1, '[3.2,4.2]', '1.7.12', 'Terbiasa mengambil makanan secukupnya dan makan sendiri', 0),
(57, 1, '[3.2,4.2]', '1.7.13', 'Berani karena benar dan mempunyai rasa ingin tahu yang besar', 0),
(58, 1, '[3.2,4.2]', '1.7.14', 'Terbiasa mengerjakan keperluannya sendiri', 0),
(59, 1, '[3.2,4.2]', '1.7.15', 'Sabar menunggu giliran', 0),
(60, 1, '[3.2,4.2]', '1.7.16', 'Dapat dibujuk', 0),
(61, 1, '[3.2,4.2]', '1.7.17', 'Tidak cengeng', 0),
(62, 1, '[3.2,4.2]', '1.7.18', 'Mengendalikan emosi dengan cara yang wajar', 0),
(63, 1, '[3.2,4.2]', '1.7.19', 'Berani tampil didepan umum', 0),
(64, 1, '[3.2,4.2]', '1.7.20', 'Berani mempertahankan pendapatnya', 0),
(65, 2, '[2.1]', '2.1.1', 'Terbiasa melakukan kegiatan-kegiatan sendiri', 0),
(66, 2, '[2.1]', '2.1.2', 'Terbiasa makan makanan bergizi dan seimbang', 0),
(67, 2, '[2.1]', '2.1.3', 'Terbiasa memelihara kebersihan lingkungan', 0),
(68, 2, '[3.3,4.3]', '2.3.1', 'Melakukan berbagai gerakan terkordinasi secara terkontrol,seimbang dan lincah.', 0),
(69, 2, '[3.3,4.3]', '2.3.2', 'Melakukan gerakan mata, tangan,kaki, kepala secara terkordinasi dalam menirukan berbagai gerakan yang teratur ( misal: senam dan tarian).', 0),
(70, 2, '[3.3,4.3]', '2.3.3', 'Melakukan permainan fisik dengan aturan', 0),
(71, 2, '[3.3,4.3]', '2.3.4', 'Terampil menggunakan tangan kanan dan kiri dalam berbagai aktivitas (misal: mengancingkannya baju, menali sepatu, menggambar, menempel, menggunting pola, meniru bentuk, menggunakan alat makan )', 0),
(72, 2, '[3.4,4.4]', '2.5.1', 'Melakukan kebiasaan hidup bersih dan sehat (misal: mandi 2x sehari, memakai baju bersih, membuang sampah pada tempatnya, menutup hidung dan mulut ketika batuk dan bersin, membersihkan dan membereskan dan membersihkan tempat bermain)', 0),
(73, 2, '[3.4,4.4]', '2.5.2', 'Mampu melindungi diri dari percobaan kekerasan, termasuk kekerasan seksual dan bullying (misal : dengan berteriak dan atau berlari)', 0),
(74, 2, '[3.4,4.4]', '2.5.3', 'Mampu menjaga keamanan diri dari benda-benda berbahaya (misal: Listrik, pisau, pembasmi serangga, kendaraan di jalan raya)', 0),
(75, 2, '[3.4,4.4]', '2.5.4', 'Menggunakan toilet dengan benar tanpa bantuan', 0),
(76, 2, '[3.4,4.4]', '2.5.5', 'Mengenal kebiasaan buruk bagi kesehatan (makan permen, jajan sembarang tempat)', 0),
(77, 3, '[2.2]', '3.1.1', 'Terbiasa menunjukkan aktivitas yang bersifat eksploratif dan menyelidik (seperti: apa yang terjadi ketika air ditumpahkan)', 0),
(78, 3, '[2.2]', '3.1.2', 'Terbiasa aktif bertanya', 0),
(79, 3, '[2.2]', '3.1.3', 'Terbiasa mencoba atau melakukan melakukan sesuatu  untuk mendapatkan jawaban ', 0),
(80, 3, '[2.3]', '3.2.1', 'Kreatif dalam menyelesaikan masalah ( ide, gagasan, diluar kebiasaan atau cara yang tidak biasa )', 0),
(81, 3, '[2.3]', '3.2.2', 'Menunjukan inisiatif dalam memilih permainan ( seperti:’ayo kita bermain pura-pura seperti burung”)', 0),
(82, 3, '[2.3]', '3.2.3', 'Senang menerapkan pengetahuan atau pengalam dalam situasi atau sesuatu yang baru.', 0),
(83, 3, '[3.5,4.5]', '3.4.1', 'Mampu memecahkan sendiri masalah sederhana yang dihadapi', 0),
(84, 3, '[3.5,4.5]', '3.4.2', 'Menyelesaikan tugas meskipun menghadapi kesulitan', 0),
(85, 3, '[3.5,4.5]', '3.4.3', 'Menyusun perencanaan kegiatan yang akan dilakukan', 0),
(86, 3, '[3.5,4.5]', '3.4.4', 'Menyesuaikan diri dengan cuaca dan kondisi alam', 0),
(87, 3, '[3.5,4.5]', '3.4.5', 'Menyelesaikan tugas meskipun menghadapi kesulitan', 0),
(88, 3, '[3.6,4.6]', '3.6.1', 'Mengenal benda dengan mengelompokkan berbagai benda dilingkungannya berdasarkan ukuran,sifat, suara, tekstur, fungsi dan ciri-ciri lainnya', 0),
(89, 3, '[3.6,4.6]', '3.6.2', 'Mengenal benda dengan menghubungkan satu benda dengan benda yang lain', 0),
(90, 3, '[3.6,4.6]', '3.6.3', 'Menghubungkan atau menjodohkan nama benda dengan tulisan sederhana melalui berbagai aktifitas', 0),
(91, 3, '[3.6,4.6]', '3.6.4', 'Mengenal konsep besar kecil, banyak sedikit, panjang pendek, berat ringan, tinggi rendah, dengan mengukur menggunakan alat ukur tidak baku', 0),
(92, 3, '[3.6,4.6]', '3.6.5', 'Membuat pola ABCD -ABCD', 0),
(93, 3, '[3.6,4.6]', '3.6.6', 'Mampu mengurutkan lima seriasi atau lebih berdasarkan warna, bentuk, ukuran atau jumlah,', 0),
(94, 3, '[3.6,4.6]', '3.6.7', 'Mengenal perbedaan berdasarkan ukuran : “ lebih dari”.,”kurang dari’., dan “paling/ter” ', 0),
(95, 3, '[3.6,4.6]', '3.6.8', 'Mengklasifikasikan benda berdasarkan tiga pariabel warna, bentuk dan ukuran', 0),
(96, 3, '[3.6,4.6]', '3.6.9', 'Menyebutkan lambang bilangan 1-10', 0),
(97, 3, '[3.6,4.6]', '3.6.10', 'Menggunakan lambang bilangan untuk menghitung', 0),
(98, 3, '[3.6,4.6]', '3.6.11', 'Mencocokkan bilangan dengan lambang bilangan', 0),
(99, 3, '[3.7,4.7]', '3.8.1', 'Menyebutkan nama anggota keluarga danteman serta ciri-ciri husus mereka secara lebih rinci ( warna kulit,warna rambut, jenis rambut dll)', 0),
(100, 3, '[3.7,4.7]', '3.8.2', 'Menjelaskan lingkungan sekitarnya secara sederhana', 0),
(101, 3, '[3.7,4.7]', '3.8.3', 'Menyebutkan arah ketempat yang sering dikunjungi dan alat transportasi yang digunakan', 0),
(102, 3, '[3.7,4.7]', '3.8.4', 'Menyebutkan peran-peran dan pekerjaan termasuk didalamnya perlengkapan/ atribut dan tugas-tugas yang dilakukan dalam pekerjaan tersebut', 0),
(103, 3, '[3.7,4.7]', '3.8.5', 'Membuat dan mengikuti aturan ', 0),
(104, 3, '[3.8,4.8]', '3.10.1', 'Menceritakan peristiwa-peristiwa alam dengan melakukan percobaan sederhana ', 0),
(105, 3, '[3.8,4.8]', '3.10.2', 'Mengungkapkan hasil karya yang dibuatnya secara lengkap/utuh yang berhubungan dengan benda-benda yang ada dilingkungan alam', 0),
(106, 3, '[3.8,4.8]', '3.10.3', 'Menceritakan perkembang biakan makhluk hidup', 0),
(107, 3, '[3.8,4.8]', '3.10.4', 'Mengenal sebab akibat tentang lingkungannya ( angin bertiup menyebabkan daun bergerak, air dapat menyebabkan sesuatu basah)', 0),
(108, 3, '[3.9,4.9]', '3.12.1', 'Melakukan kegiatan dengan menggunakan alat tekhnologi sederhana sesuai pungsinya secara aman dan bertanggung jawab', 0),
(109, 3, '[3.9,4.9]', '3.12.2', 'Membuat alat-alat tekhnologi sederhana (misalnya: baling-baling)', 0),
(110, 3, '[3.9,4.9]', '3.12.3', 'Melakukan proses kerja sesuai dengan prosedurnya (misal: membuat teh dimulai dari menyediakan air panas)', 0),
(111, 4, '2.14', '4.1.1', 'Terbiasa ramah menyapa siapapun', 0),
(112, 4, '2.14', '4.1.2', 'Terbiasa berkata lembut dan santun', 0),
(113, 4, '[3.10,4.10]', '4.3.1', 'Menceritakan kembali apa yang didengar dengan kosa kata yang lebih banyak', 0),
(114, 4, '[3.10,4.10]', '4.3.2', 'Melaksanakan perintah yang lebik komplek sesuai dengan aturan yang sesuai dengan aturan yang disampaikan', 0),
(115, 4, '[3.10,4.10]', '4.3.3', 'Mengulang kalimat yang lebih komplek', 0),
(116, 4, '[3.10,4.10]', '4.3.4', 'Memahami aturan dalam suatu permainan', 0),
(117, 4, '[3.12,4.12]', '4.5.1', 'Mengungkapkan keinginan, perasaan, dan pendapat dengan kalimat sederhana dalam berkomunikasi dengan anak atau orang dewasa', 0),
(118, 4, '[3.12,4.12]', '4.5.2', 'Menunjukkan perilaku senang membaca buku terhadap buku-buku yang dikenali', 0),
(119, 4, '[3.12,4.12]', '4.5.3', 'Mengungkapkan perasaan, ide dengan pilihan kata yang sesuai ketika berkomunikasi', 0),
(120, 4, '[3.12,4.12]', '4.5.4', 'Menceritakan kembali isi cerita cesara sederhana', 0),
(121, 4, '[3.12,4.12]', '4.5.5', 'Menjawab pertanyaan yang lebik kompleks', 0),
(122, 4, '[3.12,4.12]', '4.5.6', 'Menyebutkan kelompok gambar yang memiliki bunyi yang sama', 0),
(123, 4, '[3.12,4.12]', '4.5.7', 'Menyusun kalimat sederhana dalam struktur lengkap', 0),
(124, 4, '[3.12,4.12]', '4.7.1', 'Menunjukkan bentuk-bentuk simbol ( pola Menulis )', 0),
(125, 4, '[3.12,4.12]', '4.7.2', 'Menyebutkan lambang-lambang huruf yang dikenal', 0),
(126, 4, '[3.12,4.12]', '4.7.3', 'Menulis hurup- hurup dari namanya sendiri', 0),
(127, 4, '[3.12,4.12]', '4.7.4', 'Mengenal suara hurup awal dari nama-nama benda disekitarnya', 0),
(128, 4, '[3.12,4.12]', '4.7.5', 'Menyebutkan kelompok gambar yang memiliki bunyi/hurup awal yang sama', 0),
(129, 4, '[3.12,4.12]', '4.7.6', 'Mengenal berbagai macam lambang  huruf fokal dan konsonan', 0),
(130, 4, '[3.12,4.12]', '4.7.7', 'Memahami hubungan antara bunyi dan bentuk huruf', 0),
(131, 4, '[3.12,4.12]', '4.7.8', 'Membaca nama sendiri ', 0),
(132, 4, '[3.12,4.12]', '4.7.9', 'Membuat gambar dengan beberapa coretan /tulisan yang berbentuk hurup/ kata', 0),
(133, 4, '[3.12,4.12]', '4.7.10', 'Menyebutkan angka bila diperlihatkan lambang bilangannya ( mengucapkan bunyi lambang bilangan)', 0),
(134, 4, '[3.12,4.12]', '4.7.11', 'Senang dan menghargai bacaan', 0),
(135, 4, '[3.12,4.12]', '4.7.12', 'Memahami arti kata dalam cerita', 0),
(136, 4, '[3.12,4.12]', '4.7.13', 'Memiliki perbendahaan kata ', 0),
(137, 4, '[3.12,4.12]', '4.7.14', 'Mengenal simbol-simbol untuk persiapan membaca,menulis dan berhitung', 0),
(138, 5, '[2.5]', '5.1.1', 'Terbiasa menyapa guru saat penyambutan', 0),
(139, 5, '[2.5]', '5.1.2', 'Berani tambil didepan teman, guru, orang tua dan lingkungan.dan sosial lainnya', 0),
(140, 5, '[2.5]', '5.1.3', 'Berani mengemukakan pendapat', 0),
(141, 5, '[2.5]', '5.1.4', 'Berani menyampaikan keinginan', 0),
(142, 5, '[2.5]', '5.1.5', 'Berkomunikasi dengan orang yang belum dikenal sebelumnya dengan pengawasan guru', 0),
(143, 5, '[2.5]', '5.1.6', 'Bangga menunjukkan hasil karya', 0),
(144, 5, '[2.5]', '5.1.7', 'Senang ikut serta dalam kegiatan bersama', 0),
(145, 5, '[2.5]', '5.1.8', 'Tidak berpengaruh penilaan oranng tentang dirinya', 0),
(146, 5, '[2.6]', '5.2.1', 'Tau akan haknya', 0),
(147, 5, '[2.6]', '5.2.2', 'Mentaati aturan kelas ( kegiatan, aturan)', 0),
(148, 5, '[2.6]', '5.2.3', 'Mengatur diri sendiri', 0),
(149, 5, '[2.7]', '5.3.1', 'Kesediaan diri untuk menahan diri', 0),
(150, 5, '[2.7]', '5.3.2', 'Bersikap tenang tidak lekas marah dan dapat menunda keinginan', 0),
(151, 5, '[2.7]', '5.3.3', 'Sikap mau menunggu giliran , mau mendengarkan ketika orang lain bicara', 0),
(152, 5, '[2.7]', '5.3.4', 'Tidak menangis saat berpisah dengan orang tuanya', 0),
(153, 5, '[2.7]', '5.3.5', 'Tidak mudah mengeluh ', 0),
(154, 5, '[2.7]', '5.3.6', 'Tidak tergesa-gesa', 0),
(155, 5, '[2.7]', '5.3.7', 'Selalu menyesaikan gagasan-gagasannya hingga tuntas', 0),
(156, 5, '[2.7]', '5.3.8', 'Berusaha tidak  menyakiti atau membalas kekerasan', 0),
(157, 5, '[2.8]', '5.4.1', 'Terbiasa tidak bergantung pada orang lain', 0),
(158, 5, '[2.8]', '5.4.2', 'Terbiasa mengambil keputusan secara mandiri', 0),
(159, 5, '[2.8]', '5.4.3', 'Merencanakan, memilih, memiliki inisiatif untuk belajar atau melakukan sesuatu tanpa harus dibantu atau dibantu seperlunya', 0),
(160, 5, '[2.9]', '5.5.1', 'Mengetahui perasaan temannya dan meresponn secara wajar', 0),
(161, 5, '[2.9]', '5.5.2', 'Berbagi dengan orang lain', 0),
(162, 5, '[2.9]', '5.5.3', 'Menghargai/hak/pendapat/karya orang lain', 0),
(163, 5, '[2.9]', '5.5.4', 'Terbiasa mengindahkan dan memperhatikan kondisi teman', 0),
(164, 5, '[2.9]', '5.5.5', 'Mau menemani teman melakukan kegiatan bersama', 0),
(165, 5, '[2.9]', '5.5.6', 'Senang menawarkan bantuan pada teman atau gurupeka untuk membantu orang lain yang membutuhkan', 0),
(166, 5, '[2.9]', '5.5.7', 'Mampu menenangkan diri dan temannya dalam berbagai situasai,', 0),
(167, 5, '[2.9]', '5.5.8', 'Senang mengajak temannya untuk berkomunikasi,beraksi positif kepada semua temannya', 0),
(168, 5, '[2.10]', '5.6.1', 'Bermain dengan teman sebaya', 0),
(169, 5, '[2.10]', '5.6.2', 'Menerima perbedaan teman dengan dirinya', 0),
(170, 5, '[2.10]', '5.6.3', 'Menghargai karya teman', 0),
(171, 5, '[2.10]', '5.6.4', 'Tidak ingin menang sendiri', 0),
(172, 5, '[2.10]', '5.6.5', 'Menghargai pendapat pendapat teman dan mendengarkan dengan sabar pendapat teman', 0),
(173, 5, '[2.10]', '5.6.6', 'Senang berteman dengan semuanya', 0),
(174, 5, '[2.11]', '5.7.1', 'Memperlihatkan diri untuk menyesuaikan dengan situasi', 0),
(175, 5, '[2.11]', '5.7.2', 'Memperlihatkan kehati-hatian kepada orang yang belum dikenal ( menumbuhkan kepercayaan kepada pada orang dewasa yang tepat)', 0),
(176, 5, '[2.11]', '5.7.3', 'Bersikap kooperatif dengan teman', 0),
(177, 5, '[2.11]', '5.7.4', 'Menggunakan cara yang diterima secara sosial dalam menyelesaikan masalah ( menggunakan pikiran untuk menyelesaikan masalah)', 0),
(178, 5, '[2.11]', '5.7.5', 'Tetap tenang saat ditempat yang baru dengan situasai baru misal:saat bertemu, berada dipusat perbelanjaan, atau saat bertemu dengan guru baru,', 0),
(179, 5, '[2.12]', '5.8.1', 'Bertanggung jawab atas perilaku untuk kebaikan diri sendiri', 0),
(180, 5, '[2.12]', '5.8.2', 'Bersedia untuk menerima konsekwensi atau menanggung akibat atas tindakan yang diperbuat baik secara sengaja maupuntidak sengaja', 0),
(181, 5, '[2.12]', '5.8.3', 'Mau mengakui kesalahan dengan meminta maaf', 0),
(182, 5, '[2.12]', '5.8.4', 'Merapihkan/ membereskan mainan pada tempat semula', 0),
(183, 5, '[2.12]', '5.8.5', 'Mengerjakan sesuatu hingga tuntas', 0),
(184, 5, '[2.12]', '5.8.6', 'Senang menjalankan kegiatan yang jadi tugasnya (misalnya piket sebagai pemimpin harus membantu menyiapkan alat makan )', 0),
(185, 5, '[3.14,4.14]', '5.10.1', 'mengenal perasaan sendiri dan orang lain', 0),
(186, 5, '[3.14,4.14]', '5.10.2', 'mengelolanya secara wajar ( mengendalikan diri secara wajar)', 0),
(187, 5, '[3.14,4.14]', '5.10.3', 'Berprilaku yang membuat orang lain nyaman', 0),
(188, 5, '[3.14,4.14]', '5.10.4', 'Mengekspresikan emosi yang sesuai dengan kondisi yang ada ( senang-sedih-antusias dsb )', 0),
(189, 5, '[3.14,4.14]', '5.12.1', 'Memilih kegiatan/ benda yang paling sesuai dengan yang dibutuhkan dari beberapa pilihan yang ada', 0),
(190, 5, '[3.14,4.14]', '5.12.2', 'Mengungkapkan yang dirasakan ( lapar ingin makan, kedinginan, memerlikan baju hangat, perlu payung agar tidak keehujanan, kepanasan, sakit perut perlu berobat)', 0),
(191, 5, '[3.14,4.14]', '5.12.3', 'Menggunakan sesuatu sesuai kebutuhan', 0),
(192, 6, '[2.4]', '6.1.1', 'Menghargai keindahan diri sendiri, karya sendiri atau orang lain, alam dan lingkungan sekitar', 0),
(193, 6, '[2.4]', '6.1.2', 'Menjaga kerapihan diri', 0),
(194, 6, '[2.4]', '6.1.3', 'Bertindak /berbuat yang mencerminkan sikap estetis', 0),
(195, 6, '[2.4]', '6.1.4', 'Merawat kerapihan, kebersihan, dan keutuhan benda mainan atau milik pribadinya', 0),
(196, 6, '[3.15,4.15]', '6.3.1', 'Membuat karya seni sesuai kreativitasnya misal seni musik, visual, gerak, tari yang dihasilkannya dengan menggunakan alat yang sesuai', 0),
(197, 6, '[3.15,4.15]', '6.3.2', 'Menampilkan hasil karya seni baik dalam bentuk gambar', 0),
(198, 6, '[3.15,4.15]', '6.3.3', 'Menghargai hasil karya baik dalam bentuk gambar', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kd`
--

CREATE TABLE `kd` (
  `id_kd` int(11) NOT NULL,
  `id_KI` int(11) NOT NULL,
  `kode_kd` varchar(23) NOT NULL,
  `nama_kd` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kd`
--

INSERT INTO `kd` (`id_kd`, `id_KI`, `kode_kd`, `nama_kd`) VALUES
(1, 1, '1.1', 'Mengenal Tuhan Melalui Ciptaannya'),
(2, 1, '1.2', 'Menghargai diri sendiri, orang lain, dan lingkungan sekitar sebagai rasa syukur kepada Tuhan'),
(3, 2, '2.1', ' Memiliki perilaku yang mencerminkan hidup sehat'),
(4, 2, '2.2', 'Memiliki perilaku yang mencerminkan sikap ingin tahu'),
(5, 2, '2.3', 'Memiliki perilaku yang mencerminkan sikap kreatif'),
(6, 2, '2.4', 'Memiliki perilaku yang mencerminkan sikap Estetis'),
(7, 2, '2.5', ' Memiliki perilaku yang mencerminkan sikap percaya diri'),
(8, 2, '2.6', 'Memiliki perilaku yang mencerminkan sikap taat terhadap aturan sehari-hari untuk melatih kedisiplinan'),
(9, 2, '2.7', 'Memiliki perilaku yang mencerminkan sikap sabar (mau menunggu giliran, mau mendengar ketika orang lain berbicara) untuk melatih kedisiplinan'),
(10, 2, '2.8', 'Memiliki perilaku yang mencerminkan kemandirian'),
(11, 2, '2.9', 'Memiliki perilaku yang mencerminkan sikap peduli dan mau membantu jika diminta bantuannya'),
(12, 2, '2.10', ' Memiliki perilaku yang mencerminkan sikap menghargai dan toleran kepada orang lain'),
(13, 2, '2.11', ' Memiliki perilaku yang dapat menyesuaikan diri'),
(14, 2, '2.12', 'Memiliki perilaku yang mencerminkan sikap tanggungjawab'),
(15, 2, '2.13', 'Memiliki perilaku yang mencerminkan sikap jujur'),
(16, 2, '2.14', 'Memiliki perilaku yang mencerminkan sikap rendah hati dan santun kepada orang tua, pendidik, dan teman'),
(17, 3, '3.1', ' Mengenal kegiatan beribadah sehari-hari'),
(18, 3, '3.2', 'Mengetahui cara hidup sehat'),
(19, 3, '3.3', 'Mengenal perilaku baik sebagai cerminan akhlak mulia'),
(20, 3, '3.4', '   Mengenal anggota tubuh, fungsi, dan gerakannya untuk pengembangan motorik kasar dan motorik halus'),
(21, 3, '3.5', '   Mengetahui cara memecahkan masalah sehari-hari dan berperilaku kreatif'),
(22, 3, '3.6', '  Mengenal benda-benda disekitarnya (nama, warna, bentuk, ukuran, pola, sifat, suara, tekstur, fungsi, dan ciri-ciri lainnya)'),
(23, 3, '3.7', ' Mengenal lingkungan sosial (keluarga, teman, tempat tinggal, tempat ibadah, budaya, transportasi)'),
(24, 3, '3.8', '  Mengenal lingkungan alam (hewan, tanaman, cuaca, tanah, air, batu-batuan, dll)'),
(25, 3, '3.9', ' Mengenal teknologi sederhana (peralatan rumah tangga, peralatan bermain, peralatan pertukangan, dll)'),
(26, 3, '3.10', '  Memahami bahasa reseptif (menyimak dan membaca)'),
(27, 3, '3.11', ' Memahami bahasa ekspresif (mengungkapkan bahasa secara verbaldan non verbal)'),
(28, 3, '3.12', '  Mengenal keaksaraan awal melalui bermain'),
(29, 3, '3.13', '  Mengenal emosi diri dan orang lain'),
(30, 3, '3.14', '  Mengenali kebutuhan, keinginan, dan minat diri'),
(31, 3, '3.15', ' Mengenal berbagai karya dan aktivitas seni'),
(32, 4, '4.1', ' Melakukan kegiatan beribadah sehari-hari dengan tuntunan orang dewasa'),
(33, 4, '4.2', ' Menunjukkan perilaku santun sebagai cerminan akhlak mulia'),
(34, 4, '4.3', '   Menggunakan anggota tubuh untuk pengembangan motorik kasar dan halus'),
(35, 4, '4.4', '   Mampu menolong diri sendiri untuk hidup sehat'),
(36, 4, '4.5', '  Menyelesaikan masalah sehari-hari secara kreatif'),
(37, 4, '4.6', '   Menyampaikan tentang apa dan bagaimana benda-benda di sekitar yang dikenalnya (nama, warna, bentuk, ukuran, pola, sifat, suara, tekstur, fungsi, dan ciriciri lainnya) melalui berbagai hasil karya'),
(38, 4, '4.7', '   Menyajikan berbagai karya yang berhubungan dengan lingkungan sosial (keluarga, teman, tempat tinggal, tempat ibadah, budaya, transportasi) dalam bentuk gambar, bercerita, bernyanyi, dan gerak tubuh'),
(39, 4, '4.8', 'Menyajikan berbagai karya yang berhubungan dengan lingkungan alam (hewan, tanaman, cuaca, tanah, air, batubatuan, dll) dalam bentuk gambar, bercerita, bernyanyi, dan gerak tubuh'),
(40, 4, '4.9', '  Menggunakan teknologi sederhana untuk menyelesaikan tugas dan kegiatannya (peralatan rumah tangga, peralatan bermain, peralatan pertukangan, dll)'),
(41, 4, '4.10', ' Menunjukkan kemampuan berbahasa reseptif (menyimak dan membaca)'),
(42, 4, '4.11', '  Menunjukkan kemampuan berbahasa ekspresif (mengungkapkan bahasa secara verbal dan non verbal)'),
(43, 4, '4.12', ' Menunjukkan kemampuan keaksaraan awal dalam berbagai bentuk karya'),
(44, 4, '4.13', ' Menunjukkan reaksi emosi diri secara wajar'),
(45, 4, '4.14', ' Mengungkapkan kebutuhan, keinginan dan minat diri dengan cara yang tepat'),
(46, 4, '4.15', ' Menunjukkan karya dan aktivitas seni dengan menggunakan berbagai media');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `nama_kelas` text NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ki`
--

CREATE TABLE `ki` (
  `id_KI` int(11) NOT NULL,
  `nama_KI` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ki`
--

INSERT INTO `ki` (`id_KI`, `nama_KI`) VALUES
(1, ' Menerima ajaran agama yang dianutnya'),
(2, 'Memiliki perilaku hidup sehat, rasa ingin tahu, kreatif dan estetis, percaya diri, disiplin, mandiri, peduli,</n>\r\nmampu menghargai dan toleran kepada orang lain, mampu menyesuaikan diri, jujur, rendah hati dan santun dalam berinteraksi dengan keluarga, pendidik, dan teman'),
(3, 'KI-3. Mengenali diri, keluarga, teman,pendidik, lingkungan sekitar, agama, teknologi, seni, dan budaya di rumah,tempat bermain dan RA dengan cara: mengamati dengan indera(melihat, mendengar, menghidu, merasa, meraba);menanya;mengumpulkan informasi;menalar;\r\ndan mengomunikasikan melalui kegiatanbermain'),
(4, 'Menunjukkan yang diketahui,dirasakan, dibutuhkan, dan dipikirkan melalui bahasa, musik, gerakan, dan karya secara produktif dan kreatif, serta mencerminkan perilaku anak berakhlak mulia');

-- --------------------------------------------------------

--
-- Table structure for table `kota_kab`
--

CREATE TABLE `kota_kab` (
  `id_kotaKab` int(11) NOT NULL,
  `id_provinsi` int(11) NOT NULL,
  `nama_kota` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota_kab`
--

INSERT INTO `kota_kab` (`id_kotaKab`, `id_provinsi`, `nama_kota`) VALUES
(1101, 11, 'Kab. Simeulue'),
(1102, 11, 'Kab. Aceh Singkil'),
(1103, 11, 'Kab. Aceh Selatan'),
(1104, 11, 'Kab. Aceh Tenggara'),
(1105, 11, 'Kab. Aceh Timur'),
(1106, 11, 'Kab. Aceh Tengah'),
(1107, 11, 'Kab. Aceh Barat'),
(1108, 11, 'Kab. Aceh Besar'),
(1109, 11, 'Kab. Pidie'),
(1110, 11, 'Kab. Bireuen'),
(1111, 11, 'Kab. Aceh Utara'),
(1112, 11, 'Kab. Aceh Barat Daya'),
(1113, 11, 'Kab. Gayo Lues'),
(1114, 11, 'Kab. Aceh Tamiang'),
(1115, 11, 'Kab. Nagan Raya'),
(1116, 11, 'Kab. Aceh Jaya'),
(1117, 11, 'Kab. Bener Meriah'),
(1118, 11, 'Kab. Pidie Jaya'),
(1171, 11, 'Kota Banda Aceh'),
(1172, 11, 'Kota Sabang'),
(1173, 11, 'Kota Langsa'),
(1174, 11, 'Kota Lhokseumawe'),
(1175, 11, 'Kota Subulussalam'),
(1201, 12, 'Kab. Nias'),
(1202, 12, 'Kab. Mandailing Natal'),
(1203, 12, 'Kab. Tapanuli Selatan'),
(1204, 12, 'Kab. Tapanuli Tengah'),
(1205, 12, 'Kab. Tapanuli Utara'),
(1206, 12, 'Kab. Toba Samosir'),
(1207, 12, 'Kab. Labuhan Batu'),
(1208, 12, 'Kab. Asahan'),
(1209, 12, 'Kab. Simalungun'),
(1210, 12, 'Kab. Dairi'),
(1211, 12, 'Kab. Karo'),
(1212, 12, 'Kab. Deli Serdang'),
(1213, 12, 'Kab. Langkat'),
(1214, 12, 'Kab. Nias Selatan'),
(1215, 12, 'Kab. Humbang Hasundutan'),
(1216, 12, 'Kab. Pakpak Bharat'),
(1217, 12, 'Kab. Samosir'),
(1218, 12, 'Kab. Serdang Bedagai'),
(1219, 12, 'Kab. Batu Bara'),
(1220, 12, 'Kab. Padang Lawas Utara'),
(1221, 12, 'Kab. Padang Lawas'),
(1222, 12, 'Kab. Labuhan Batu Selatan'),
(1223, 12, 'Kab. Labuhan Batu Utara'),
(1224, 12, 'Kab. Nias Utara'),
(1225, 12, 'Kab. Nias Barat'),
(1271, 12, 'Kota Sibolga'),
(1272, 12, 'Kota Tanjung Balai'),
(1273, 12, 'Kota Pematang Siantar'),
(1274, 12, 'Kota Tebing Tinggi'),
(1275, 12, 'Kota Medan'),
(1276, 12, 'Kota Binjai'),
(1277, 12, 'Kota Padangsidimpuan'),
(1278, 12, 'Kota Gunungsitoli'),
(1301, 13, 'Kab. Kepulauan Mentawai'),
(1302, 13, 'Kab. Pesisir Selatan'),
(1303, 13, 'Kab. Solok'),
(1304, 13, 'Kab. Sijunjung'),
(1305, 13, 'Kab. Tanah Datar'),
(1306, 13, 'Kab. Padang Pariaman'),
(1307, 13, 'Kab. Agam'),
(1308, 13, 'Kab. Lima Puluh Kota'),
(1309, 13, 'Kab. Pasaman'),
(1310, 13, 'Kab. Solok Selatan'),
(1311, 13, 'Kab. Dharmasraya'),
(1312, 13, 'Kab. Pasaman Barat'),
(1371, 13, 'Kota Padang'),
(1372, 13, 'Kota Solok'),
(1373, 13, 'Kota Sawah Lunto'),
(1374, 13, 'Kota Padang Panjang'),
(1375, 13, 'Kota Bukittinggi'),
(1376, 13, 'Kota Payakumbuh'),
(1377, 13, 'Kota Pariaman'),
(1401, 14, 'Kab. Kuantan Singingi'),
(1402, 14, 'Kab. Indragiri Hulu'),
(1403, 14, 'Kab. Indragiri Hilir'),
(1404, 14, 'Kab. Pelalawan'),
(1405, 14, 'Kab. S I A K'),
(1406, 14, 'Kab. Kampar'),
(1407, 14, 'Kab. Rokan Hulu'),
(1408, 14, 'Kab. Bengkalis'),
(1409, 14, 'Kab. Rokan Hilir'),
(1410, 14, 'Kab. Kepulauan Meranti'),
(1471, 14, 'Kota Pekanbaru'),
(1473, 14, 'Kota D U M A I'),
(1501, 15, 'Kab. Kerinci'),
(1502, 15, 'Kab. Merangin'),
(1503, 15, 'Kab. Sarolangun'),
(1504, 15, 'Kab. Batang Hari'),
(1505, 15, 'Kab. Muaro Jambi'),
(1506, 15, 'Kab. Tanjung Jabung Timur'),
(1507, 15, 'Kab. Tanjung Jabung Barat'),
(1508, 15, 'Kab. Tebo'),
(1509, 15, 'Kab. Bungo'),
(1571, 15, 'Kota Jambi'),
(1572, 15, 'Kota Sungai Penuh'),
(1601, 16, 'Kab. Ogan Komering Ulu'),
(1602, 16, 'Kab. Ogan Komering Ilir'),
(1603, 16, 'Kab. Muara Enim'),
(1604, 16, 'Kab. Lahat'),
(1605, 16, 'Kab. Musi Rawas'),
(1606, 16, 'Kab. Musi Banyuasin'),
(1607, 16, 'Kab. Banyu Asin'),
(1608, 16, 'Kab. Ogan Komering Ulu Selatan'),
(1609, 16, 'Kab. Ogan Komering Ulu Timur'),
(1610, 16, 'Kab. Ogan Ilir'),
(1611, 16, 'Kab. Empat Lawang'),
(1671, 16, 'Kota Palembang'),
(1672, 16, 'Kota Prabumulih'),
(1673, 16, 'Kota Pagar Alam'),
(1674, 16, 'Kota Lubuklinggau'),
(1701, 17, 'Kab. Bengkulu Selatan'),
(1702, 17, 'Kab. Rejang Lebong'),
(1703, 17, 'Kab. Bengkulu Utara'),
(1704, 17, 'Kab. Kaur'),
(1705, 17, 'Kab. Seluma'),
(1706, 17, 'Kab. Mukomuko'),
(1707, 17, 'Kab. Lebong'),
(1708, 17, 'Kab. Kepahiang'),
(1709, 17, 'Kab. Bengkulu Tengah'),
(1771, 17, 'Kota Bengkulu'),
(1801, 18, 'Kab. Lampung Barat'),
(1802, 18, 'Kab. Tanggamus'),
(1803, 18, 'Kab. Lampung Selatan'),
(1804, 18, 'Kab. Lampung Timur'),
(1805, 18, 'Kab. Lampung Tengah'),
(1806, 18, 'Kab. Lampung Utara'),
(1807, 18, 'Kab. Way Kanan'),
(1808, 18, 'Kab. Tulangbawang'),
(1809, 18, 'Kab. Pesawaran'),
(1810, 18, 'Kab. Pringsewu'),
(1811, 18, 'Kab. Mesuji'),
(1812, 18, 'Kab. Tulang Bawang Barat'),
(1813, 18, 'Kab. Pesisir Barat'),
(1871, 18, 'Kota Bandar Lampung'),
(1872, 18, 'Kota Metro'),
(1901, 19, 'Kab. Bangka'),
(1902, 19, 'Kab. Belitung'),
(1903, 19, 'Kab. Bangka Barat'),
(1904, 19, 'Kab. Bangka Tengah'),
(1905, 19, 'Kab. Bangka Selatan'),
(1906, 19, 'Kab. Belitung Timur'),
(1971, 19, 'Kota Pangkal Pinang'),
(2101, 21, 'Kab. Karimun'),
(2102, 21, 'Kab. Bintan'),
(2103, 21, 'Kab. Natuna'),
(2104, 21, 'Kab. Lingga'),
(2105, 21, 'Kab. Kepulauan Anambas'),
(2171, 21, 'Kota B A T A M'),
(2172, 21, 'Kota Tanjung Pinang'),
(3101, 31, 'Kab. Kepulauan Seribu'),
(3171, 31, 'Kota Jakarta Selatan'),
(3172, 31, 'Kota Jakarta Timur'),
(3173, 31, 'Kota Jakarta Pusat'),
(3174, 31, 'Kota Jakarta Barat'),
(3175, 31, 'Kota Jakarta Utara'),
(3201, 32, 'Kab. Bogor'),
(3202, 32, 'Kab. Sukabumi'),
(3203, 32, 'Kab. Cianjur'),
(3204, 32, 'Kab. Bandung'),
(3205, 32, 'Kab. Garut'),
(3206, 32, 'Kab. Tasikmalaya'),
(3207, 32, 'Kab. Ciamis'),
(3208, 32, 'Kab. Kuningan'),
(3209, 32, 'Kab. Cirebon'),
(3210, 32, 'Kab. Majalengka'),
(3211, 32, 'Kab. Sumedang'),
(3212, 32, 'Kab. Indramayu'),
(3213, 32, 'Kab. Subang'),
(3214, 32, 'Kab. Purwakarta'),
(3215, 32, 'Kab. Karawang'),
(3216, 32, 'Kab. Bekasi'),
(3217, 32, 'Kab. Bandung Barat'),
(3218, 32, 'Kab. Pangandaran'),
(3271, 32, 'Kota Bogor'),
(3272, 32, 'Kota Sukabumi'),
(3273, 32, 'Kota Bandung'),
(3274, 32, 'Kota Cirebon'),
(3275, 32, 'Kota Bekasi'),
(3276, 32, 'Kota Depok'),
(3277, 32, 'Kota Cimahi'),
(3278, 32, 'Kota Tasikmalaya'),
(3279, 32, 'Kota Banjar'),
(3301, 33, 'Kab. Cilacap'),
(3302, 33, 'Kab. Banyumas'),
(3303, 33, 'Kab. Purbalingga'),
(3304, 33, 'Kab. Banjarnegara'),
(3305, 33, 'Kab. Kebumen'),
(3306, 33, 'Kab. Purworejo'),
(3307, 33, 'Kab. Wonosobo'),
(3308, 33, 'Kab. Magelang'),
(3309, 33, 'Kab. Boyolali'),
(3310, 33, 'Kab. Klaten'),
(3311, 33, 'Kab. Sukoharjo'),
(3312, 33, 'Kab. Wonogiri'),
(3313, 33, 'Kab. Karanganyar'),
(3314, 33, 'Kab. Sragen'),
(3315, 33, 'Kab. Grobogan'),
(3316, 33, 'Kab. Blora'),
(3317, 33, 'Kab. Rembang'),
(3318, 33, 'Kab. Pati'),
(3319, 33, 'Kab. Kudus'),
(3320, 33, 'Kab. Jepara'),
(3321, 33, 'Kab. Demak'),
(3322, 33, 'Kab. Semarang'),
(3323, 33, 'Kab. Temanggung'),
(3324, 33, 'Kab. Kendal'),
(3325, 33, 'Kab. Batang'),
(3326, 33, 'Kab. Pekalongan'),
(3327, 33, 'Kab. Pemalang'),
(3328, 33, 'Kab. Tegal'),
(3329, 33, 'Kab. Brebes'),
(3371, 33, 'Kota Magelang'),
(3372, 33, 'Kota Surakarta'),
(3373, 33, 'Kota Salatiga'),
(3374, 33, 'Kota Semarang'),
(3375, 33, 'Kota Pekalongan'),
(3376, 33, 'Kota Tegal'),
(3401, 34, 'Kab. Kulon Progo'),
(3402, 34, 'Kab. Bantul'),
(3403, 34, 'Kab. Gunung Kidul'),
(3404, 34, 'Kab. Sleman'),
(3471, 34, 'Kota Yogyakarta'),
(3501, 35, 'Kab. Pacitan'),
(3502, 35, 'Kab. Ponorogo'),
(3503, 35, 'Kab. Trenggalek'),
(3504, 35, 'Kab. Tulungagung'),
(3505, 35, 'Kab. Blitar'),
(3506, 35, 'Kab. Kediri'),
(3507, 35, 'Kab. Malang'),
(3508, 35, 'Kab. Lumajang'),
(3509, 35, 'Kab. Jember'),
(3510, 35, 'Kab. Banyuwangi'),
(3511, 35, 'Kab. Bondowoso'),
(3512, 35, 'Kab. Situbondo'),
(3513, 35, 'Kab. Probolinggo'),
(3514, 35, 'Kab. Pasuruan'),
(3515, 35, 'Kab. Sidoarjo'),
(3516, 35, 'Kab. Mojokerto'),
(3517, 35, 'Kab. Jombang'),
(3518, 35, 'Kab. Nganjuk'),
(3519, 35, 'Kab. Madiun'),
(3520, 35, 'Kab. Magetan'),
(3521, 35, 'Kab. Ngawi'),
(3522, 35, 'Kab. Bojonegoro'),
(3523, 35, 'Kab. Tuban'),
(3524, 35, 'Kab. Lamongan'),
(3525, 35, 'Kab. Gresik'),
(3526, 35, 'Kab. Bangkalan'),
(3527, 35, 'Kab. Sampang'),
(3528, 35, 'Kab. Pamekasan'),
(3529, 35, 'Kab. Sumenep'),
(3571, 35, 'Kota Kediri'),
(3572, 35, 'Kota Blitar'),
(3573, 35, 'Kota Malang'),
(3574, 35, 'Kota Probolinggo'),
(3575, 35, 'Kota Pasuruan'),
(3576, 35, 'Kota Mojokerto'),
(3577, 35, 'Kota Madiun'),
(3578, 35, 'Kota Surabaya'),
(3579, 35, 'Kota Batu'),
(3601, 36, 'Kab. Pandeglang'),
(3602, 36, 'Kab. Lebak'),
(3603, 36, 'Kab. Tangerang'),
(3604, 36, 'Kab. Serang'),
(3671, 36, 'Kota Tangerang'),
(3672, 36, 'Kota Cilegon'),
(3673, 36, 'Kota Serang'),
(3674, 36, 'Kota Tangerang Selatan'),
(5101, 51, 'Kab. Jembrana'),
(5102, 51, 'Kab. Tabanan'),
(5103, 51, 'Kab. Badung'),
(5104, 51, 'Kab. Gianyar'),
(5105, 51, 'Kab. Klungkung'),
(5106, 51, 'Kab. Bangli'),
(5107, 51, 'Kab. Karang Asem'),
(5108, 51, 'Kab. Buleleng'),
(5171, 51, 'Kota Denpasar'),
(5201, 52, 'Kab. Lombok Barat'),
(5202, 52, 'Kab. Lombok Tengah'),
(5203, 52, 'Kab. Lombok Timur'),
(5204, 52, 'Kab. Sumbawa'),
(5205, 52, 'Kab. Dompu'),
(5206, 52, 'Kab. Bima'),
(5207, 52, 'Kab. Sumbawa Barat'),
(5208, 52, 'Kab. Lombok Utara'),
(5271, 52, 'Kota Mataram'),
(5272, 52, 'Kota Bima'),
(5301, 53, 'Kab. Sumba Barat'),
(5302, 53, 'Kab. Sumba Timur'),
(5303, 53, 'Kab. Kupang'),
(5304, 53, 'Kab. Timor Tengah Selatan'),
(5305, 53, 'Kab. Timor Tengah Utara'),
(5306, 53, 'Kab. Belu'),
(5307, 53, 'Kab. Alor'),
(5308, 53, 'Kab. Lembata'),
(5309, 53, 'Kab. Flores Timur'),
(5310, 53, 'Kab. Sikka'),
(5311, 53, 'Kab. Ende'),
(5312, 53, 'Kab. Ngada'),
(5313, 53, 'Kab. Manggarai'),
(5314, 53, 'Kab. Rote Ndao'),
(5315, 53, 'Kab. Manggarai Barat'),
(5316, 53, 'Kab. Sumba Tengah'),
(5317, 53, 'Kab. Sumba Barat Daya'),
(5318, 53, 'Kab. Nagekeo'),
(5319, 53, 'Kab. Manggarai Timur'),
(5320, 53, 'Kab. Sabu Raijua'),
(5371, 53, 'Kota Kupang'),
(6101, 61, 'Kab. Sambas'),
(6102, 61, 'Kab. Bengkayang'),
(6103, 61, 'Kab. Landak'),
(6104, 61, 'Kab. Pontianak'),
(6105, 61, 'Kab. Sanggau'),
(6106, 61, 'Kab. Ketapang'),
(6107, 61, 'Kab. Sintang'),
(6108, 61, 'Kab. Kapuas Hulu'),
(6109, 61, 'Kab. Sekadau'),
(6110, 61, 'Kab. Melawi'),
(6111, 61, 'Kab. Kayong Utara'),
(6112, 61, 'Kab. Kubu Raya'),
(6171, 61, 'Kota Pontianak'),
(6172, 61, 'Kota Singkawang'),
(6201, 62, 'Kab. Kotawaringin Barat'),
(6202, 62, 'Kab. Kotawaringin Timur'),
(6203, 62, 'Kab. Kapuas'),
(6204, 62, 'Kab. Barito Selatan'),
(6205, 62, 'Kab. Barito Utara'),
(6206, 62, 'Kab. Sukamara'),
(6207, 62, 'Kab. Lamandau'),
(6208, 62, 'Kab. Seruyan'),
(6209, 62, 'Kab. Katingan'),
(6210, 62, 'Kab. Pulang Pisau'),
(6211, 62, 'Kab. Gunung Mas'),
(6212, 62, 'Kab. Barito Timur'),
(6213, 62, 'Kab. Murung Raya'),
(6271, 62, 'Kota Palangka Raya'),
(6301, 63, 'Kab. Tanah Laut'),
(6302, 63, 'Kab. Kota Baru'),
(6303, 63, 'Kab. Banjar'),
(6304, 63, 'Kab. Barito Kuala'),
(6305, 63, 'Kab. Tapin'),
(6306, 63, 'Kab. Hulu Sungai Selatan'),
(6307, 63, 'Kab. Hulu Sungai Tengah'),
(6308, 63, 'Kab. Hulu Sungai Utara'),
(6309, 63, 'Kab. Tabalong'),
(6310, 63, 'Kab. Tanah Bumbu'),
(6311, 63, 'Kab. Balangan'),
(6371, 63, 'Kota Banjarmasin'),
(6372, 63, 'Kota Banjar Baru'),
(6401, 64, 'Kab. Paser'),
(6402, 64, 'Kab. Kutai Barat'),
(6403, 64, 'Kab. Kutai Kartanegara'),
(6404, 64, 'Kab. Kutai Timur'),
(6405, 64, 'Kab. Berau'),
(6409, 64, 'Kab. Penajam Paser Utara'),
(6471, 64, 'Kota Balikpapan'),
(6472, 64, 'Kota Samarinda'),
(6474, 64, 'Kota Bontang'),
(6501, 65, 'Kab. Malinau'),
(6502, 65, 'Kab. Bulungan'),
(6503, 65, 'Kab. Tana Tidung'),
(6504, 65, 'Kab. Nunukan'),
(6571, 65, 'Kota Tarakan'),
(7101, 71, 'Kab. Bolaang Mongondow'),
(7102, 71, 'Kab. Minahasa'),
(7103, 71, 'Kab. Kepulauan Sangihe'),
(7104, 71, 'Kab. Kepulauan Talaud'),
(7105, 71, 'Kab. Minahasa Selatan'),
(7106, 71, 'Kab. Minahasa Utara'),
(7107, 71, 'Kab. Bolaang Mongondow Utara'),
(7108, 71, 'Kab. Siau Tagulandang Biaro'),
(7109, 71, 'Kab. Minahasa Tenggara'),
(7110, 71, 'Kab. Bolaang Mongondow Selatan'),
(7111, 71, 'Kab. Bolaang Mongondow Timur'),
(7171, 71, 'Kota Manado'),
(7172, 71, 'Kota Bitung'),
(7173, 71, 'Kota Tomohon'),
(7174, 71, 'Kota Kotamobagu'),
(7201, 72, 'Kab. Banggai Kepulauan'),
(7202, 72, 'Kab. Banggai'),
(7203, 72, 'Kab. Morowali'),
(7204, 72, 'Kab. Poso'),
(7205, 72, 'Kab. Donggala'),
(7206, 72, 'Kab. Toli-toli'),
(7207, 72, 'Kab. Buol'),
(7208, 72, 'Kab. Parigi Moutong'),
(7209, 72, 'Kab. Tojo Una-una'),
(7210, 72, 'Kab. Sigi'),
(7271, 72, 'Kota Palu'),
(7301, 73, 'Kab. Kepulauan Selayar'),
(7302, 73, 'Kab. Bulukumba'),
(7303, 73, 'Kab. Bantaeng'),
(7304, 73, 'Kab. Jeneponto'),
(7305, 73, 'Kab. Takalar'),
(7306, 73, 'Kab. Gowa'),
(7307, 73, 'Kab. Sinjai'),
(7308, 73, 'Kab. Maros'),
(7309, 73, 'Kab. Pangkajene Dan Kepulauan'),
(7310, 73, 'Kab. Barru'),
(7311, 73, 'Kab. Bone'),
(7312, 73, 'Kab. Soppeng'),
(7313, 73, 'Kab. Wajo'),
(7314, 73, 'Kab. Sidenreng Rappang'),
(7315, 73, 'Kab. Pinrang'),
(7316, 73, 'Kab. Enrekang'),
(7317, 73, 'Kab. Luwu'),
(7318, 73, 'Kab. Tana Toraja'),
(7322, 73, 'Kab. Luwu Utara'),
(7325, 73, 'Kab. Luwu Timur'),
(7326, 73, 'Kab. Toraja Utara'),
(7371, 73, 'Kota Makassar'),
(7372, 73, 'Kota Parepare'),
(7373, 73, 'Kota Palopo'),
(7401, 74, 'Kab. Buton'),
(7402, 74, 'Kab. Muna'),
(7403, 74, 'Kab. Konawe'),
(7404, 74, 'Kab. Kolaka'),
(7405, 74, 'Kab. Konawe Selatan'),
(7406, 74, 'Kab. Bombana'),
(7407, 74, 'Kab. Wakatobi'),
(7408, 74, 'Kab. Kolaka Utara'),
(7409, 74, 'Kab. Buton Utara'),
(7410, 74, 'Kab. Konawe Utara'),
(7471, 74, 'Kota Kendari'),
(7472, 74, 'Kota Baubau'),
(7501, 75, 'Kab. Boalemo'),
(7502, 75, 'Kab. Gorontalo'),
(7503, 75, 'Kab. Pohuwato'),
(7504, 75, 'Kab. Bone Bolango'),
(7505, 75, 'Kab. Gorontalo Utara'),
(7571, 75, 'Kota Gorontalo'),
(7601, 76, 'Kab. Majene'),
(7602, 76, 'Kab. Polewali Mandar'),
(7603, 76, 'Kab. Mamasa'),
(7604, 76, 'Kab. Mamuju'),
(7605, 76, 'Kab. Mamuju Utara'),
(8101, 81, 'Kab. Maluku Tenggara Barat'),
(8102, 81, 'Kab. Maluku Tenggara'),
(8103, 81, 'Kab. Maluku Tengah'),
(8104, 81, 'Kab. Buru'),
(8105, 81, 'Kab. Kepulauan Aru'),
(8106, 81, 'Kab. Seram Bagian Barat'),
(8107, 81, 'Kab. Seram Bagian Timur'),
(8108, 81, 'Kab. Maluku Barat Daya'),
(8109, 81, 'Kab. Buru Selatan'),
(8171, 81, 'Kota Ambon'),
(8172, 81, 'Kota Tual'),
(8201, 82, 'Kab. Halmahera Barat'),
(8202, 82, 'Kab. Halmahera Tengah'),
(8203, 82, 'Kab. Kepulauan Sula'),
(8204, 82, 'Kab. Halmahera Selatan'),
(8205, 82, 'Kab. Halmahera Utara'),
(8206, 82, 'Kab. Halmahera Timur'),
(8207, 82, 'Kab. Pulau Morotai'),
(8271, 82, 'Kota Ternate'),
(8272, 82, 'Kota Tidore Kepulauan'),
(9101, 91, 'Kab. Fakfak'),
(9102, 91, 'Kab. Kaimana'),
(9103, 91, 'Kab. Teluk Wondama'),
(9104, 91, 'Kab. Teluk Bintuni'),
(9105, 91, 'Kab. Manokwari'),
(9106, 91, 'Kab. Sorong Selatan'),
(9107, 91, 'Kab. Sorong'),
(9108, 91, 'Kab. Raja Ampat'),
(9109, 91, 'Kab. Tambrauw'),
(9110, 91, 'Kab. Maybrat'),
(9171, 91, 'Kota Sorong'),
(9401, 94, 'Kab. Merauke'),
(9402, 94, 'Kab. Jayawijaya'),
(9403, 94, 'Kab. Jayapura'),
(9404, 94, 'Kab. Nabire'),
(9408, 94, 'Kab. Kepulauan Yapen'),
(9409, 94, 'Kab. Biak Numfor'),
(9410, 94, 'Kab. Paniai'),
(9411, 94, 'Kab. Puncak Jaya'),
(9412, 94, 'Kab. Mimika'),
(9413, 94, 'Kab. Boven Digoel'),
(9414, 94, 'Kab. Mappi'),
(9415, 94, 'Kab. Asmat'),
(9416, 94, 'Kab. Yahukimo'),
(9417, 94, 'Kab. Pegunungan Bintang'),
(9418, 94, 'Kab. Tolikara'),
(9419, 94, 'Kab. Sarmi'),
(9420, 94, 'Kab. Keerom'),
(9426, 94, 'Kab. Waropen'),
(9427, 94, 'Kab. Supiori'),
(9428, 94, 'Kab. Mamberamo Raya'),
(9429, 94, 'Kab. Nduga'),
(9430, 94, 'Kab. Lanny Jaya'),
(9431, 94, 'Kab. Mamberamo Tengah'),
(9432, 94, 'Kab. Yalimo'),
(9433, 94, 'Kab. Puncak'),
(9434, 94, 'Kab. Dogiyai'),
(9435, 94, 'Kab. Intan Jaya'),
(9436, 94, 'Kab. Deiyai'),
(9471, 94, 'Kota Jayapura');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `id_nilai` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `kode_indikator` varchar(20) NOT NULL,
  `tanggal_nilai` date NOT NULL,
  `nilai_perkembangan` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `id_teknik` int(11) NOT NULL,
  `foto` text NOT NULL,
  `id_penilai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `perencanaan`
--

CREATE TABLE `perencanaan` (
  `id_perencanaan` int(11) NOT NULL,
  `tanggal_awal` date NOT NULL,
  `tanggal_akhir` date NOT NULL,
  `id_tahunajaran` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_kelas` varchar(60) NOT NULL,
  `id_program` text NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_subtema` int(11) NOT NULL,
  `id_subsubtema` int(11) NOT NULL,
  `id_kd` varchar(50) NOT NULL,
  `id_indikator` varchar(50) NOT NULL,
  `materi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `perkembangan_akhir`
--

CREATE TABLE `perkembangan_akhir` (
  `id_perkembangan` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `id_program` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `catatan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id_program` int(11) NOT NULL,
  `nama_program` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id_program`, `nama_program`) VALUES
(1, '(NAM) Nilai Agama dan Moral '),
(2, '(FM) Fisik Motorik'),
(3, '(KOG) Kognitif'),
(4, '(Bahasa) BAHASA'),
(5, '(SE) Sosial Emosional'),
(6, 'Seni');

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE `provinsi` (
  `id_provinsi` int(11) NOT NULL,
  `nama_provinsi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`id_provinsi`, `nama_provinsi`) VALUES
(11, 'Aceh'),
(12, 'Sumatera Utara'),
(13, 'Sumatera Barat'),
(14, 'Riau'),
(15, 'Jambi'),
(16, 'Sumatera Selatan'),
(17, 'Bengkulu'),
(18, 'Lampung'),
(19, 'Kepulauan Bangka Belitung'),
(21, 'Kepulauan Riau'),
(31, 'Dki Jakarta'),
(32, 'Jawa Barat'),
(33, 'Jawa Tengah'),
(34, 'Di Yogyakarta'),
(35, 'Jawa Timur'),
(36, 'Banten'),
(51, 'Bali'),
(52, 'Nusa Tenggara Barat'),
(53, 'Nusa Tenggara Timur'),
(61, 'Kalimantan Barat'),
(62, 'Kalimantan Tengah'),
(63, 'Kalimantan Selatan'),
(64, 'Kalimantan Timur'),
(65, 'Kalimantan Utara'),
(71, 'Sulawesi Utara'),
(72, 'Sulawesi Tengah'),
(73, 'Sulawesi Selatan'),
(74, 'Sulawesi Tenggara'),
(75, 'Gorontalo'),
(76, 'Sulawesi Barat'),
(81, 'Maluku'),
(82, 'Maluku Utara'),
(91, 'Papua Barat'),
(94, 'Papua');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `nama_role` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id_role`, `nama_role`) VALUES
(1, 'guru'),
(2, 'pusat'),
(3, 'sekolah'),
(4, 'kanwil');

-- --------------------------------------------------------

--
-- Table structure for table `sekolah`
--

CREATE TABLE `sekolah` (
  `id_sekolah` int(11) NOT NULL,
  `nama_sekolah` text,
  `NSN` varchar(20) DEFAULT NULL,
  `jalan` text,
  `kelurahan` text,
  `kecamatan` text,
  `kode_pos` text,
  `id_kotaKab` int(11) DEFAULT NULL,
  `id_provinsi` text,
  `website` text,
  `telepon` text,
  `email` text,
  `kepala_RA` text,
  `tahun_ajaran` text,
  `foto` text,
  `status` int(11) DEFAULT NULL,
  `tgl_daftar` date DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `id_semester` int(11) NOT NULL,
  `nama_semester` text NOT NULL,
  `tanggal_rapor` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id_semester`, `nama_semester`, `tanggal_rapor`) VALUES
(1, 'Semester 1', '0000-00-00 00:00:00'),
(2, 'Semester 2', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `id_sekolah` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `nama_siswa` text,
  `nama_panggilan` text,
  `no_induk` varchar(100) DEFAULT NULL,
  `tempat_lahir` text,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` text,
  `agama` text,
  `nama_ayah` text,
  `nama_ibu` text,
  `pekerjaan_ayah` text,
  `pekerjaan_ibu` text,
  `status_anak` text,
  `anak_ke` int(11) DEFAULT NULL,
  `jalan` text,
  `kelurahan` text,
  `kecamatan` text,
  `provinsi` text,
  `kotakab` text,
  `foto` text,
  `diterima_tanggal` date DEFAULT NULL,
  `id_tahunajaran` int(11) DEFAULT NULL,
  `nama_wali` text,
  `pekerjaan_wali` text,
  `alamat_wali` text,
  `telepon_wali` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subsubtema`
--

CREATE TABLE `subsubtema` (
  `id_subsubtema` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_subtema` int(11) NOT NULL,
  `nama` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subtema`
--

CREATE TABLE `subtema` (
  `id_subtema` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `nama_subtema` text NOT NULL,
  `id_kd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tahun_ajaran`
--

CREATE TABLE `tahun_ajaran` (
  `id_tahunajaran` int(11) NOT NULL,
  `nama_tahun` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahun_ajaran`
--

INSERT INTO `tahun_ajaran` (`id_tahunajaran`, `nama_tahun`) VALUES
(1, '2016/2017'),
(2, '2017/2018'),
(3, '2018/2019'),
(4, '2019/2020');

-- --------------------------------------------------------

--
-- Table structure for table `teknik_penugasan`
--

CREATE TABLE `teknik_penugasan` (
  `id_teknik` int(11) NOT NULL,
  `nama_teknik` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teknik_penugasan`
--

INSERT INTO `teknik_penugasan` (`id_teknik`, `nama_teknik`) VALUES
(1, 'Observasi'),
(2, 'Penugasan'),
(3, 'Unjuk kerja'),
(4, 'Anekdot'),
(5, 'Hasil Karya');

-- --------------------------------------------------------

--
-- Table structure for table `tema`
--

CREATE TABLE `tema` (
  `id_tema` int(11) NOT NULL,
  `nama_tema` text NOT NULL,
  `id_sekolah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tingkat`
--

CREATE TABLE `tingkat` (
  `id_tingkat` int(11) NOT NULL,
  `nama_tingkat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tingkat`
--

INSERT INTO `tingkat` (`id_tingkat`, `nama_tingkat`) VALUES
(1, 'Kelompok A'),
(2, 'Kelompok B');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_provinsi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user__logs`
--

CREATE TABLE `user__logs` (
  `id_log` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `browser_log` text NOT NULL,
  `platform_log` text NOT NULL,
  `ip_log` text NOT NULL,
  `date_login` datetime NOT NULL,
  `browser_version` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id_absen`);

--
-- Indexes for table `coba_rekap`
--
ALTER TABLE `coba_rekap`
  ADD PRIMARY KEY (`id_rekap`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `indikator`
--
ALTER TABLE `indikator`
  ADD PRIMARY KEY (`id_indikator`),
  ADD KEY `kode_kd` (`kode_kd`);

--
-- Indexes for table `indikator_2`
--
ALTER TABLE `indikator_2`
  ADD PRIMARY KEY (`id_indikator`),
  ADD KEY `kode_kd` (`kode_kd`);

--
-- Indexes for table `kd`
--
ALTER TABLE `kd`
  ADD PRIMARY KEY (`id_kd`),
  ADD KEY `id_KI` (`id_KI`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `ki`
--
ALTER TABLE `ki`
  ADD PRIMARY KEY (`id_KI`);

--
-- Indexes for table `kota_kab`
--
ALTER TABLE `kota_kab`
  ADD PRIMARY KEY (`id_kotaKab`),
  ADD KEY `id_provinsi` (`id_provinsi`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `perencanaan`
--
ALTER TABLE `perencanaan`
  ADD PRIMARY KEY (`id_perencanaan`);

--
-- Indexes for table `perkembangan_akhir`
--
ALTER TABLE `perkembangan_akhir`
  ADD PRIMARY KEY (`id_perkembangan`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id_program`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id_provinsi`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `sekolah`
--
ALTER TABLE `sekolah`
  ADD PRIMARY KEY (`id_sekolah`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id_semester`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `subsubtema`
--
ALTER TABLE `subsubtema`
  ADD PRIMARY KEY (`id_subsubtema`);

--
-- Indexes for table `subtema`
--
ALTER TABLE `subtema`
  ADD PRIMARY KEY (`id_subtema`),
  ADD KEY `id_tema` (`id_tema`);

--
-- Indexes for table `tahun_ajaran`
--
ALTER TABLE `tahun_ajaran`
  ADD PRIMARY KEY (`id_tahunajaran`);

--
-- Indexes for table `teknik_penugasan`
--
ALTER TABLE `teknik_penugasan`
  ADD PRIMARY KEY (`id_teknik`);

--
-- Indexes for table `tema`
--
ALTER TABLE `tema`
  ADD PRIMARY KEY (`id_tema`);

--
-- Indexes for table `tingkat`
--
ALTER TABLE `tingkat`
  ADD PRIMARY KEY (`id_tingkat`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `user__logs`
--
ALTER TABLE `user__logs`
  ADD PRIMARY KEY (`id_log`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id_absen` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coba_rekap`
--
ALTER TABLE `coba_rekap`
  MODIFY `id_rekap` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `indikator`
--
ALTER TABLE `indikator`
  MODIFY `id_indikator` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;
--
-- AUTO_INCREMENT for table `indikator_2`
--
ALTER TABLE `indikator_2`
  MODIFY `id_indikator` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;
--
-- AUTO_INCREMENT for table `kd`
--
ALTER TABLE `kd`
  MODIFY `id_kd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ki`
--
ALTER TABLE `ki`
  MODIFY `id_KI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `perencanaan`
--
ALTER TABLE `perencanaan`
  MODIFY `id_perencanaan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `perkembangan_akhir`
--
ALTER TABLE `perkembangan_akhir`
  MODIFY `id_perkembangan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id_program` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sekolah`
--
ALTER TABLE `sekolah`
  MODIFY `id_sekolah` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `id_semester` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subsubtema`
--
ALTER TABLE `subsubtema`
  MODIFY `id_subsubtema` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subtema`
--
ALTER TABLE `subtema`
  MODIFY `id_subtema` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tahun_ajaran`
--
ALTER TABLE `tahun_ajaran`
  MODIFY `id_tahunajaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `teknik_penugasan`
--
ALTER TABLE `teknik_penugasan`
  MODIFY `id_teknik` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tema`
--
ALTER TABLE `tema`
  MODIFY `id_tema` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tingkat`
--
ALTER TABLE `tingkat`
  MODIFY `id_tingkat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user__logs`
--
ALTER TABLE `user__logs`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
